/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Software aligner to create CIGAR strings (says how a read and
 * reference were aligned in detail).
 * Based on Corey Olson's code.
 */

#include "common.h"
#include "cigar.h"
#include "readFastq.h"
#include <iostream>

using namespace std;

cigarGen::cigarGen(int read_length, int ref_length) {
  printf("read length = %d, ref length = %d", read_length, ref_length);

  readlen = read_length;
  reflen = ref_length;

  if (readlen > MAX_READ_LENGTH) {
    cerr << "** Read Length " << readlen
	 << " is longer than MAX_READ_LENGTH " << MAX_READ_LENGTH << " **\n";
    exit(EXIT_FAILURE);
  }
  if (reflen > MAX_REF_LENGTH) {
    cerr << "** Reference Length " << reflen
	 << " is longer than MAX_REF_LENGTH " << MAX_REF_LENGTH << " **\n";
    exit(EXIT_FAILURE);
  }

  int maxCigarLen = (read_length+ref_length)*2+5; //Overkill,but safe
  cigarString = new char[maxCigarLen];
  cigarStringEnd = &(cigarString[maxCigarLen-1]);
  *cigarStringEnd='\0';

  mdString = new char[maxCigarLen];
  transcript = new char[maxCigarLen];

  perfectMDstring = new char[10];
  sprintf(perfectMDstring, "%d", read_length);

  initMatrices();
}

cigarGen::~cigarGen() {
  delete[] cigarString;
  delete[] mdString;
  delete[] transcript;
}

// Initializes all scoring matrices to a good known value.  Called by the
// constructor, and likely never needs to be called again.
void cigarGen::initMatrices(){
  
  // if you skip a portion of the read, pay the gap cost
  for(int i=1; i<=readlen; i++) {
    similarity[i][0] = (i-1)*C_GAP_EXTEND + C_GAP_OPEN;
    refGap    [i][0] = (i-1)*C_GAP_EXTEND + C_GAP_OPEN;
    readGap   [i][0] = (i-1)*C_GAP_EXTEND + C_GAP_OPEN;
    direction [i][0] = 'I';
  }

  // you can skip portions of the reference for free
  for (int j=0; j<=reflen; j++) {
    similarity[0][j] = 0;
    refGap    [0][j] = C_GAP_OPEN-C_GAP_EXTEND;
    readGap   [0][j] = C_GAP_OPEN-C_GAP_EXTEND;
    direction [0][j] = '*';
  }
}

// Helper function for alignRead.  Computes the values for position [i][j]
// of the matrices in this structure.  Assumes [i-1][j], [i][j-1], and 
// [i-1][j-1] are already set to the right value.
inline void cigarGen::computeSquare(int i, int j, char *read, char *reference)
{
  int refGapOpen = similarity[i-1][j] + C_GAP_OPEN;
  int refGapExt = refGap[i-1][j] + C_GAP_EXTEND;
  int refg = MAX(refGapOpen,refGapExt);
  refGap[i][j] = refg;
  
  int readGapOpen = similarity[i][j-1] + C_GAP_OPEN;
  int readGapExt = readGap[i][j-1] + C_GAP_EXTEND;
  int readg = MAX(readGapOpen, readGapExt);
  readGap[i][j] = readg;
  
  int diag = similarity[i-1][j-1]
    + ((read[i-1] == reference[j-1]) ? C_MATCH: C_MISMATCH);
  
  if (diag >= refg && diag >= readg) {
    similarity[i][j] = diag;
    direction[i][j] = (read[i-1] == reference[j-1]) ? '=' : 'X';
  } else if (refg > readg) {
    similarity[i][j] = refg;
    direction[i][j] = 'I';
  } else {
    similarity[i][j] = readg;
    direction[i][j] = 'D';
  }
}

// aligns the current read against the current reference.
// Assumes the entries [0][*] and [*][0] are already set via initMatrices(),
// but multiple alignReads calls can use the same initial initMatrices().
// 
// NOTE: read and ref must use the same character set, and only exact
// matches will be counted.  So, if there is a mix of upper and lower
// case values ("agCCTA") it will not match a=A.  Also, N characters if
// they are present will be matched to other N's, and nothing else.
void cigarGen::alignRead(char *read, char *reference)
{
  //   reference
  // r  --- ---
  // e | 0 | 1 |
  // a  --- ---
  // d | 2 | D |
  //    --- ---

  // Compute along the diagonal, to increase parallelism
  for(int diag = 1; diag < readlen+reflen; diag++) {
    // i indexes the read, j the reference
    int topRight_i, topRight_j, botLeft_i, botLeft_j;
    topRight_j = diag; topRight_i = 1;
    if (topRight_j > reflen) {
      topRight_j = reflen;
      topRight_i = diag - reflen + 1;
    }
    botLeft_i = diag; botLeft_j = 1;
    if (botLeft_i > readlen) {
      botLeft_i = readlen;
      botLeft_j = diag - readlen + 1;
    }

    int i, j;
    for (i=topRight_i, j=topRight_j;
	 i<=botLeft_i; // also j>=botLeft_j
	 i++, j--) {
      computeSquare(i, j, read, reference);
    }
  }
}

// Same as alignRead, but faster because it is banded - by knowing the
// best answer position and score, we compute what portion of the alignment
// array is actually needed to find the alignment.
//
//  [1,1]
//    +-------------
//    |             \
//    |              \
//    \               \
//     \              |
//      \             | ins_margin (# squares above position to eval)
//       \            |
//        ------------+ <- position (where best score should be found)
//          del_margin (# squares left of position to eval)
// 
// This banding will avoid going earlier than column 1.
//
// aligns the current read against the current reference.
// Assumes the entries [0][*] and [*][0] are already set via initMatrices(),
// but multiple alignReads calls can use the same initial initMatrices().
// 
// NOTE: read and ref must use the same character set, and only exact
// matches will be counted.  So, if there is a mix of upper and lower
// case values ("agCCTA") it will not match a=A.  Also, N characters if
// they are present will be matched to other N's, and nothing else.
void cigarGen::alignReadBanded(char *read, char *reference,
			       int position, int score)
{
  //   reference
  // r  --- ---
  // e | 0 | 1 |
  // a  --- ---
  // d | 2 | D |
  //    --- ---

  position++; // The 0th location in reference is in SmWa column 1, etc.

  // Figure out the banding margin, based on score.  We assume we have
  // an insert or delete gap right at the final position, which will give the
  // largest band needed to evaluate.
  int ins_margin, del_margin;
  int delta = (C_MATCH * readlen) - score;

  // ins_margin.  For each insertion we lose the gap penalty plus the loss of
  // locations to get a C_MATCH
  if (delta < C_MATCH - C_GAP_OPEN)
    ins_margin = 0;
  else {
    ins_margin = 1 + (delta - C_MATCH + C_GAP_OPEN)/(-C_GAP_EXTEND);
  }
  // del_margin.  Only loses the gap penalty, since all of read remains for
  // getting the C_MATCH bonus
  if (delta < -C_GAP_OPEN)
    del_margin = 0;
  else {
    del_margin = 1 + (delta + C_GAP_OPEN)/(-C_GAP_EXTEND);
  }

  // Go row by row
  for (int i = 1, raw_j = position - (readlen-1);
       i <= readlen; // Also raw_j <= position
       i++, raw_j++) {
    int j_min = MAX(raw_j - del_margin, 1);
    int j_max = MIN(raw_j + ins_margin, position);
    if (j_min > 1) {
      readGap[i][j_min] = refGap[i][j_min] = similarity[i][j_min]
	= readlen * C_MISMATCH;
      direction[i][j_min-1] = '.';
    }
    if (j_max < position) {
      readGap[i][j_max] = refGap[i][j_max] = similarity[i][j_max]
	= readlen * C_MISMATCH;
      direction[i][j_max+1] = '.';
    }
    for (int j=j_min; j<=j_max; j++) {
      computeSquare(i, j, read, reference);
    }
  }
}

// prints ones of the matrices to the screen
void cigarGen::printMatrices(char *read, char *ref) {
  
  // print the reference and the read to refer to them
  cout << "Reference = " << ref << "\n";
  cout << "Read      = " << read << "\n";
  
  printf("Format:\n\tSimilarity\n\tRefGap\n\tReadGap\n\tDirection\n\n");
  
  // print the reference along the top
  printf("     N ");
  for(int i=0; i<reflen; i++){
    printf("     %c ", ref[i]);
  }
  printf("\n");
  
  // print the array with read bases in the first column
  for(int i=0;i<=readlen;i++){
    for(int k=0;k<=3;k++){
      for(int j=0;j<=reflen;j++){
	// read printed down along the side
	if(i==0 && j==0 && k==1){
	  printf("N ");
	}else if(j==0 && k==1){
	  printf("%c ", read[i-1]);
	}else{
	  printf("  ");
	}
	
	// matrix data
	if(k==0){
	  printf("%4i ", similarity[i][j]);
	}else if(k==1){
	  printf("%4i ", refGap[i][j]);
	}else if(k==2){
	  printf("%4i ", readGap[i][j]);
	}else if(k==3){
	  if (direction[i][j]==0)
	    printf("   _ ");
	  else
	    printf("%4c ", direction[i][j]);
	}
      }
      printf("\n");
    }
    printf("\n");
  }
}

// Helper for computeCIGAR.  Adds a CIGAR entry of given type "d" and count.
// Prepends to string pointed to by cigarPtr, assuming positions before the
// pointer are preallocated and allowed to be overwritten.
char *storeCigar(char *cigarPtr, char d, int count)
{
  cigarPtr--;
  *cigarPtr = d;
  while (count > 0) {
    cigarPtr--;
    *cigarPtr = (count%10)+'0';
    count /= 10;
  }
  return cigarPtr;
}

// Helper for computeCIGAR.  Outputs the MD string for a matching with
// indels.  Uses the transcript to guide it, so the transcript should be
// filled in first (see header for details).
//
// transIndex - input, points to the position 1 past where the transcript
// data ends.
const char *cigarGen::computeMDindels(char *transIndex)
{
  char *result = mdString;

  *transIndex='\0';// Makes transcript easily printable

  // Convert the transcript into a MD string
  char *mdStringIndex = mdString;
  transIndex--; // Go back 1 to first occupied location
  while (transIndex >= transcript) {
    int i=0;
    while (transIndex >= transcript && *transIndex == 'M') { // matches
      transIndex--;
      i++;
    }
    mdStringIndex += sprintf(mdStringIndex, "%d", i); // Can be 0
    if (transIndex < transcript)
      break; // Done

    if (*transIndex == '^') { // deletes
      *(mdStringIndex++) = '^';
      while (*transIndex == '^') { // Combine multiple deletes together
	transIndex--; // skip the '^'
	*(mdStringIndex++) = *(transIndex--); // Copy the base
      }
    } else { // mismatches
      *(mdStringIndex++) = *(transIndex--); // Copy the base
      // return to loop without doing multiple mismatches - forces a 0
      // between consecutive mismatched bases
    }
  }

#if defined MRFAST
  if (result[0] == '0')
    result++; // Skip leading 0 if MRFAST, since they do it wrong
#elif defined BFAST
  if (*transcript != 'M') // MD string always ends with a number, except MRFAST does it wrong
    *(mdStringIndex++) = '0';
#else
#error Invalid version
#endif

  *mdStringIndex = '\0'; // Null-terminate the string

  /*
  cout << "Read:." << read
       << "\nRef:." << reference
       << "\nCIGAR:." << cigarPtr
       << "\ntrans:." << transcript 
       << "\nMD:." << mdString
       << endl;
  */

  return result;
}

// Compute and return the CIGAR string.  Score must
// be the score of aligning the result with the read - code can use
// the score to band the portion of the alignment it must do.
//
// read and reference are character arrays containing strings to compare.
// Generally they will be sequences of {'A','G','C','T'}, though this code
// simply says that if ref[i]==read[i] it is a match, and a mismatch
// otherwise.
//
// endBase is an input that says which base of the reference is the last one
// that aligns with the last base of the read.  If 0, we compute it from
// scratch, but if 1 or more we can do a faster calculation using this info.
// The position is relative to start of the reference (i.e. 1 is the second
// base of the reference).
//
// score is the score found by the hardware for this alignment.  It is used
// to calculate the band in banded alignment, and for error checking. If
// the software finds a higher score, it updates the score parameter to 
// this new value.
//
// startBase returned as the position (o..strlen(reference)-1) in the
// reference parameter that is the start of the CIGAR alignment.  Thus,
// if this code says N, then first base of the read aligns to reference[N].
//
// editDist is an output of the edit distance along the CIGAR alignment.  Each
// mismatch, insertion, or deletion counts as one. 
//
// MDstring: returns the optional MD string for SAM output
//
// NOTE: the char string returned, and the MDstring, will be reused for
// further calls to computeCIGAR on this object, so copy or otherwise finish
// using the string before calling this again.
//
// NOTE2: Can increase the score if the SW finds a better score than the HW
// (can happen if SW sees a larger portion of the reference than the HW)
// 
// NOTE3: read and ref must use the same character set, and only exact
// matches will be counted.  So, if there is a mix of upper and lower
// case values ("agCCTA") it will not match a=A.  Also, N characters if
// they are present will be matched to other N's, and nothing else.
const char *cigarGen::computeCIGAR(char *read, char *reference, int64_t endBase,
				   int32_t &score, uint64_t &startBase,
				   int &editDist, const char *&MDstring)
{
  static double allCalls = 0.0, badCalls = 0.0;
  allCalls++;

  int pos_i, pos_j, t;
  short best_found_score;

  char *transIndex = transcript;

  editDist = 0;

  pos_i = readlen;
  if (endBase == 0) {
    alignRead(read, reference);
    // Find best score and starting point where entire read is matched
    pos_j=0;
    best_found_score=similarity[pos_i][pos_j];
    for (t=1; t<=reflen; t++) {
      if (similarity[pos_i][t] > best_found_score) {
	best_found_score = similarity[pos_i][t];
	pos_j=t;
      }
    }
  } else {
    alignReadBanded(read, reference, endBase, score);
    pos_j = endBase+1; // SmWa puts ref[0] into table[1], etc.
    best_found_score=similarity[pos_i][pos_j];
  }
  //printMatrices(read, reference);
  
  char *cigarPtr = cigarStringEnd;
  // Start backtrack from [pos_i][pos_j].
  while(1) { // Go until you hit a '*' on the top row
    if (pos_i == 0) // Done with path - most likely ends in "D"
      break; 
    if (pos_j == 0) { // Exhausted reference - use I's to get the rest
      cigarPtr = storeCigar(cigarPtr, 'I', pos_i);
      editDist += pos_i;
      break;
    }
    char d = direction[pos_i][pos_j];
    //cout << "s["<<pos_i<<","<<pos_j<<"]"<<d<<"<"<<read[pos_i-1]<<","<<reference[pos_j-1]<<">: "<<similarity[pos_i][pos_j]<<endl;
    int count=1;
    if (d=='*') {
      break;
    } else if (d=='=' || d=='X') { // Follow M's up the diagonal
      if (d=='X') {
	editDist++;
	*(transIndex++) = reference[pos_j-1];
      } else {
	*(transIndex++) = 'M';
      }
      for(pos_i--, pos_j--;
	  direction[pos_i][pos_j] == '=' || direction[pos_i][pos_j] == 'X';
	  pos_i--, pos_j--) {
	//cout << "["<<pos_i<<","<<pos_j<<"]"<<d<<"<"<<read[pos_i-1]<<","<<reference[pos_j-1]<<">: "<<similarity[pos_i][pos_j]<<endl;
	count++;
	if (direction[pos_i][pos_j] == 'X') {
	  editDist++;
	  *(transIndex++) = reference[pos_j-1];
	} else {
	  *(transIndex++) = 'M';
	}
      }
      cigarPtr = storeCigar(cigarPtr, 'M', count);
    } else if (d=='I') { // Follow until we find the gap open penalty or edge
      for(pos_i--;
	  pos_i>0 && (similarity[pos_i][pos_j] + C_GAP_EXTEND
		      == similarity[pos_i+1][pos_j]);
	  pos_i--) {
	//cout << "["<<pos_i<<","<<pos_j<<"]"<<d<<"<"<<read[pos_i-1]<<","<<reference[pos_j-1]<<">: "<<similarity[pos_i][pos_j]<<endl;
	count++;
      }
      cigarPtr = storeCigar(cigarPtr, d, count);
      editDist += count;
    } else if (d=='D') { // Follow until we find the gap open penalty or edge
      *(transIndex++) = reference[pos_j-1];
      *(transIndex++) = '^';
      for(pos_j--;
	  pos_j>0 && (similarity[pos_i][pos_j] + C_GAP_EXTEND
		      == similarity[pos_i][pos_j+1]);
	  pos_j--) {
	//cout << "["<<pos_i<<","<<pos_j<<"]"<<d<<"<"<<read[pos_i-1]<<","<<reference[pos_j-1]<<">: "<<similarity[pos_i][pos_j]<<endl;
	count++;
	*(transIndex++) = reference[pos_j-1];
	*(transIndex++) = '^';
      }
      cigarPtr = storeCigar(cigarPtr, d, count);
      editDist += count;
    } else {
      cerr << "** Unknown char \"" << d << "\" in direction table **\n";
      printf("pos_i = %d, pos_j = %d score = %d, best score = %d, start = %lu\n"
             , pos_i, pos_j, score, best_found_score, startBase);
      printMatrices(read, reference);
      exit(EXIT_FAILURE);
    }
  }
  
  if (best_found_score > score) {
    score = best_found_score;
  } else if (best_found_score != score) {
    badCalls++;
    cerr << "--> BOGUS SCORE: software: " << best_found_score 
	 << " hardware: " << score
	 << " seeing " << 100*(badCalls/allCalls) << "% failures <--\n";
    cerr <<   "         Read: " << read
	 << "\nRef: " << reference
	 << "\nCIGAR: " << cigarPtr << "\n";
    //printMatrices(read, reference);
    //exit(EXIT_FAILURE);
  }

  // Output the startBase.  Note: the SmWa table has a 0 row and col that aren't
  // part of the ref and read, so must adjust
  if (pos_j == 0) startBase = 0;
  else startBase = pos_j-1;

  MDstring = computeMDindels(transIndex);
  return cigarPtr;
}

const char *cigarGen::computeMDnoIndel(char *read, char *reference)
{
  char *result = mdString;
  char *MDstringIndex = mdString;
  int i=0;

  while (i<readlen) {
    int count=0;
    while (i<readlen && read[i] == reference[i]) {
      count++; i++;
    }
    MDstringIndex += sprintf(MDstringIndex, "%d", count); // Can be 0
    if (i<readlen) { // means also that read[i] != reference[i])
      *(MDstringIndex++) = reference[i++];
    }
  }
#if defined MRFAST
  if (result[0] == '0')
    result++; // Skip leading 0 if MRFAST, since they do it wrong
#elif defined BFAST
  if (read[readlen-1] != reference[readlen-1]) {
    *(MDstringIndex++) = '0'; // MD strings always end with a number, except MRFAST does it wrong
  }
#else
#error Invalid option
#endif

  *MDstringIndex='\0';

  return result;
}
