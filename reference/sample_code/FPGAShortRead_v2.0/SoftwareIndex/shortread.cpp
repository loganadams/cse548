/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <stdint.h>
#include <numa.h>
#include <climits>
#include <vector>
#include <set>

extern "C" {
  #include <hugetlbfs.h>
  #include "queue/inline.h"
  #include "queue/sw_queue_astream.h"
}

#include "SAMoutput.h"
#include "ptrTable.h"
#include "CALtable.h"
#include "readFastq.h"
#include "hashfunc.h"
#include "CALfilter.h"
#include "cigar.h"
#include "verify.h"
#include "scoredSet.h"
#include "allqueues.h"
#include "shortread.h"
#include "conversions.h"

//#define NDEBUG
#include <cassert>
#include "fpga/fasta2ddr.h"

#include "common.h"
#include "perf_timer.h"

using namespace std;

// Set up the overall flow of the algorithm, instantiating the threads and
// queues needed to execute the overall computation.

// The system has an inThread, that brings in the short reads from disk, and
// divvys up the work for the tableThreads.
// 
// There are 2N tableThreads, in pairs.  Each pair gets a read to process.  One
// pair member is on socket 0, and does all lookups for reads in the memory of
// socket one.  The other pair member does the same on socket 1.  The resulting
// data is sent on to the FPGAcontrolThread to do the merge.
// tableThreads are designated by their id=pair_id*2 + pair_member, where
// pair_id is a number 0..(N-1) unique to each pair, and pair_member is 0 or 1.
// 0 is the pair member on the evenMask socket, 1 is on oddMask.
//
// The FPGAcontrolThread gets data from the tableThreads, merges the results
// from the two members of a pair, and sends it for processing on the FPGA.
//
// The FPGAs are controlled by SmithWaterman objects, which have a sender
// and a reciever thread for each FPGA board, so that when the queues
// to/from hardware block the software system can still make progress.
//
// The getScores thread gets data back from the FPGAs, throws away the
// data that doesn't meet the quality goals, and outputs the final results.
//
// *** Definitions of the inter-thread queues ***
// See allqueues.h
//

SmithWaterman **sws;

// We keep a copy of the reference here
uint64_t *full_ref;
uint32_t full_ref_len;
// Also has the data on where chromosomes appear in the reference
chrom *chromData;

// True if we want to turn off CIGAR string calculation (which can be slow)
bool noCIGAR = false;
// True if we want to turn off the unmapped read output
bool noUnmapped = false;
// True if we want to include unmapped reads in the SAM with appropriate flag
bool unmappedSAM = false;
// If positive, check the Index against the Reference (which can be very slow)
// If >1, only check 1/verifyIndex portion of the index.
int verifyIndex = false;
// Filename for where the FPGA bitfile is found
const char *bitfileFilename = NULL;

// How do we handle multiple good CALs for a read?
enum style { BEST, // All CALs per read that have the best score.
	     BEST_UNIQUE, // Only 1 CAL per read, just the best.
	     ALL // All CALs regardless of score
};
style filterStyle = ALL;
// All CALs with scores below this value will be discarded, regardless of style
int scoreCutoff = INT_MIN;
// How many CALs can we output for each read.  Any beyond this discarded.
int CALlimit = INT_MAX;
// How many N's we allow in an individual read.  Reads with more than this Ns are
// rejected.
int maxN = 2;
// Model of the Pico card in use.
uint32_t cardModel = 0;

// Masks that specify a node to run on for the even and odd processes.  Used to make sure that
// processes run on the same node as the memory they generally use.
struct bitmask *oddMask;
struct bitmask *evenMask;

// doIntThread sets this to the number of reads processed.  This is used for statistics.
uint64_t InThreadReadCount = -1;
uint64_t limitReads = -1;

// Memory boundry alignment in number of CALs.
int CALOffset = 0;
int CALAlignMask = 0;

// DEBUG TIMERS Timers for debug
//perf_timer ptIn, ptTable, ptCtrl, ptScore;
/*struct timespec sClockIn, sClockTable, sClockCtrl, sClockScores, eClockIn, eClockTable, eClockScores, eClockCtrl;
unsigned long long tClocksIn, tClocksTable, tClocksCtrl, tClocksScores, tCALIn, tCALTable, tCALCtrl, tCALScores;
void calcTime(struct timespec &sclock, struct timespec &eclock, unsigned long long &tclocks, unsigned long long &tcals, const char* name, unsigned int total) {
  clock_gettime(CLOCK_MONOTONIC, &eclock);
  tclocks += (eclock.tv_sec * 1000000000 + eclock.tv_nsec) - (sclock.tv_sec * 1000000000 + sclock.tv_nsec);
  tcals++;
  if (tcals >= total) {
    cout << "Thread " << name << " " << total << " CALs took " << tclocks / 1000000000.0 << "s, CALs/s " << tcals / (tclocks / 1000000000.0) << endl;
    tcals = tclocks = 0;
  }
  }*/

// InThread reads a Fastq file.  It sends each read to a pair of TableThreads, one on each
// socket, in a round-robin fashion.  The format is a read id (from 1 to N), then a packed read
// (see readFastqRecord in readFastq.cpp).  Once all the reads have been sent, it sends a readId
// of 0 with no read to designate the end of the file.
void * doInThread(void* ptr) {
  uint64_t id = (uint64_t)ptr;

  // Start processing the FASTQ file
  cout << "Opening reads file " << fastqFilename << endl;
  ifstream *fastqFile = openFastqFile(fastqFilename);

  // Get reads from the file, and send reads to tableThread pairs in a round-robin fashion
  uint64_t numRead=0;
  int pair_id=0;

  // DEBUG TIMERS
/*  ptInit(&ptIn, 100000);
    ptStartStopTest();*/

  while (1) {
//    ptStart(&ptIn);
    readDescr *readData = readFastqRecord(fastqFile, fastqFilename, ++numRead, maxN);
    if (readData == NULL) // Finished reading the file
      break; 
//    ptOp(&ptIn, "in");
    // Send data to the TableThreads for lookup in the index
    for (int pair=0; pair < 2; pair++) {
      sq_produce(in_table_qs[pair_id*2+pair],
		 (uint64_t)readData,
		 (sq_callback)InThread_flushAll,
		 (sq_cbData)NULL);
    }
    pair_id = (pair_id+1)%NUM_TABLETHREAD_PAIRS;

    if (numRead == limitReads) {
      numRead++;
      cout << "** Hit limitReads **\n";
      break;
    }
    
  }

  numRead--; // numRead is 1 greater than number of actual reads, since it
  // provides an ID for the failed read attempt at the end.

  fastqFile->close();
  delete fastqFile;

  // Send end of computation tag, then flush all queues
  for (int i=0; i<NUM_TABLETHREAD_PAIRS*2; i++) {
    printf("end of reads sent to table queue %d\n", i);
    sq_produce(in_table_qs[i],
	       EndOfAllReads,
	       (sq_callback)InThread_flushAll,
	       (sq_cbData)NULL);
    sq_flushQueue(in_table_qs[i]);
  }

  printf("finished\n");

  InThreadReadCount = numRead;
  //cout << "File reader handled " << numRead << " reads" << endl;
}

// Table thread - gets a set of reads on a input queue, looks them up in the tables on this socket,
// and sends the matching CAL locations out an output queue.
//
// The output queue gets the same read ID and readData as inThread produces, but adds reads.
// The format is a read id (from 1 to N), then a packed read
// (see readFastqRecord in readFastq.cpp).
// It then sends sets of CALs, where the bottom 32 bits are CALs, and the top 32 bits are:
//   0: read comes from forward direction of the reference.
//   1: read comes from the reverseComplement direction of the reference.
//   2: end of CAL list (the CAL is irrelevant)
//
// Once all the reads have been sent, it sends a readId
// of 0 with no read to designate the end of the file.
void * doTableThread(void* ptr) {
  uint64_t id = (uint64_t)ptr;
  int pair_id = id/2;     // ID specifying which pair I am in
  int pair_member = id%2; // Am I the odd or even member of this pair?

  ptrTableEntry *myPtrTable = (pair_member == 0) ? evenPtrTable : oddPtrTable;
  CALtableEntry *myCALTable = (pair_member == 0) ? evenCALtable : oddCALtable;

  // Filter to remove duplicate CALs
  CALfilter filter;

  /*
  stringstream message;
  message << "doTableThread[" << pair_id << "][" << pair_member << "]\n";
  cout << message.str();
  */

  // Make copy of queue structure for this thread - can help performance for some compilers
  //sw_queue_t inQueue = *in_table_qs[id];
  //sw_queue_t outQueue = *table_FPGAcontrol_qs[id];
  cout << "Table thread " << id << " started" << endl;

  // DEBUG TIMERS
//  ptInit(&ptTable, 100000);

  while (1) {
    // Process each read
    char identifier[MAX_FASTQ_LINE_LEN] = {0};
    char queryQuality[MAX_FASTQ_LINE_LEN] = {0};

    readDescr *readData
      = (readDescr *)sq_consume(in_table_qs[id],
				(sq_callback)TableThread_flushAll,
				(sq_cbData)id);

    sq_produce(table_FPGAcontrol_qs[id],
	       (uint64_t)readData,
	       (sq_callback)TableThread_flushAll,
	       (sq_cbData)id);
//    if (id == 0) ptStart(&ptTable);
    if (readData == EndOfAllReads) {// All reads done
        cout << "Table thread " << id << " got end" << endl;
      break;
    }
    //else cout << "ReadData: " << readData->getReadID() << "\n";

    uint64_t CALsSentForThisRead=0; // For debugging

    uint64_t *packedRead = readData->getPackedRead();
    // Generate the first seed for this read.
    uint64_t reverseSeed = packedRead[0] & (((1ULL<<seedLength)<<seedLength)-1ULL);
    reverseSeed ^= (((1ULL<<seedLength)<<seedLength)-1ULL);
    uint64_t targetPacked = packedRead[0];
    uint64_t forwardSeed=0;
    // Simplistic base-reverse.  If important, better can be found online.
    for(int firstSeed=0; firstSeed < seedLength; firstSeed++) {
      forwardSeed = (forwardSeed << 2) | (targetPacked & 3);
      targetPacked >>= 2;
    }

    // Go through all seeds, starting with the first one calculated above
    uint64_t lexSeed, hashedSeed;
    uint64_t isRev;
    for(int whichSeed = seedLength; whichSeed <= readLength; whichSeed++) {
#ifdef NOOVERLAP
      if (whichSeed % seedLength == 0) {
#endif // NOOVERLAP
      if (forwardSeed <= reverseSeed) {
	lexSeed = forwardSeed;
	isRev=0;
      } else {
	lexSeed = reverseSeed;
	isRev=1;
      }
      
      // Hash the seed.  Then use the top bit to pick the evenPtrTable/oddPtrTable
      hashedSeed = hashfunc(lexSeed, seedLength);
      uint64_t whichTable = (hashedSeed >> (2*seedLength-1ULL)) & 1ULL;
      hashedSeed &= ((1ULL << (2*seedLength-1)) - 1ULL);
      
      if (whichTable == pair_member) { // Only look up seeds in my half of the table.
	uint64_t startPointer;
	uint64_t endPointer;
	uint32_t key;
	seedToPointers(myPtrTable, hashedSeed & ((1ULL<<(2*seedLength-1)) - 1ULL), 
		       startPointer, endPointer, key);

	// Check each entry in CAL table.  Send ones that match
	while (startPointer < endPointer) {
	  CALtableEntry *entry = &(myCALTable[startPointer]);
	  if (getKey(entry) == key) {
	    uint32_t CAL = getCAL(entry);
	    uint64_t reversed = isRev ^ getIsRevComp(entry);
	    // adjust CAL to beginning of read
	    if (reversed) {
	      CAL -= (readLength-whichSeed);
	    } else { 
	      CAL -= (whichSeed-seedLength);
	    }
	    uint64_t payload = CAL | (reversed << 32);
	    
	    // Only send if we haven't seen it before
	    if (!filter.isDuplicate(payload, readData->getReadID())) {
//              if (id == 0) ptStop(&ptTable);
	      sq_produce(table_FPGAcontrol_qs[id],
			 payload,
			 (sq_callback)TableThread_flushAll,
			 (sq_cbData)id);
//              if (id == 0) ptStart(&ptTable);
	      CALsSentForThisRead++;
	    }
	  }
	  startPointer++;
	}
      }
#ifdef NOOVERLAP
      }
#endif //NOOVERLAP
      if (whichSeed % 32 == 0) {
	targetPacked = packedRead[whichSeed/32];
      }
      forwardSeed <<= 2;
      forwardSeed |= (targetPacked & 3);
      forwardSeed &= (((1ULL<<seedLength)<<seedLength)-1ULL);
      reverseSeed >>= 2;
      reverseSeed |= (((targetPacked & 3) ^ 3) << (2*(seedLength-1)));
      targetPacked >>= 2;
    }
//    if (id == 0) ptOp(&ptTable, "table");
    sq_produce(table_FPGAcontrol_qs[id],
	       EndOfRead,
	       (sq_callback)TableThread_flushAll,
	       (sq_cbData)id);
//    cout << "Read produced " << CALsSentForThisRead << " cals" << endl;
  }
  
  sq_produce(table_FPGAcontrol_qs[id],
	     EndOfAllReads,
	     (sq_callback)TableThread_flushAll,
	     (sq_cbData)id);
  sq_flushQueue(table_FPGAcontrol_qs[id]);
}

// Variables used for statistics and checking.
// The number of reads processed.
uint64_t FPGAcontrolThreadReadCount[SW_NUM];
// Total number of unique CALs sent on for processing, per FPGA
uint64_t FPGAcontrolUniqueCALsSent[SW_NUM];
// Total number of reads sent on for processing, per FPGA
uint64_t FPGAcontrolReadsSent[SW_NUM];

// Merges data from table threads, sends on to SmWa uints on the FPGAs
void * doFPGAcontrolThread(void* ptr) {
  uint64_t id = (uint64_t)ptr;
  cout << "control thread " << id << " starting" << endl;

  CALfilter filter;
  uint64_t readCount=0;
  int pair_id=id; // Only read from one table thread pair

  uint64_t calsCount=0;
  FPGAcontrolReadsSent[id]=0;
  FPGAcontrolThreadReadCount[id]=-1;

  // Debug timers DEBUG TIMERS
/*  struct timespec startClock, filterClock, revClock, alignClock, sendClock, endClock;
  unsigned long long tStartClocks, tFilterClocks, tRevClocks, tAlignClocks, tSendClocks, tEndClocks, calLoops;
  tStartClocks = tFilterClocks = tRevClocks = tSendClocks = tAlignClocks = tEndClocks = calLoops = 0;*/
//  ptInit(&ptCtrl, 100000);

  while (true) {
    readDescr *readData0
      = (readDescr *)sq_consume(table_FPGAcontrol_qs[pair_id*2+0],
				(sq_callback)FPGAcontrolThread_flushAll,
				(sq_cbData)NULL);
    readDescr *readData1
      = (readDescr *)sq_consume(table_FPGAcontrol_qs[pair_id*2+1],
				(sq_callback)FPGAcontrolThread_flushAll,
				(sq_cbData)NULL);
    if (readData0 == EndOfAllReads) {
      printf("FPGA control thread %lu received end of reads\n", id);
    }
    if (readData0 != readData1)
      cout << "*** Mismatched read id " << readData0->getReadID()
	   << " != " << readData1->getReadID() << " ***\n";

    sq_produce(FPGAcontrol_GetScores_qs[id],
               (uint64_t)readData0,
               (sq_callback)FPGAcontrolThread_flushAll,
               (sq_cbData)NULL);

//    ptStart(&ptCtrl);

    if (readData0 == EndOfAllReads) {// Last read recieved - end
      FPGAcontrolThread_flushAll(NULL);
      cout << "FPGA control thread " << id << " got end" << endl;
      break;
    }

    readCount++;
    if (readCount % 1000000 == 0)
      cout << readCount/1000000 << "M " << flush;

    vector<uint64_t> reverseCALs;
    // Whether we have sent the read to FPGA already.
    bool sentRead = false;

    for (int pair_member = 0; pair_member < 2; ++pair_member) {
      while (true) {
//        clock_gettime(CLOCK_MONOTONIC, &startClock);
//        ptStop(&ptCtrl);
	uint64_t packedCAL
	  = sq_consume(table_FPGAcontrol_qs[pair_id*2+pair_member],
		       (sq_callback)FPGAcontrolThread_flushAll,
		       (sq_cbData)NULL);
//        ptStart(&ptCtrl);
	if (packedCAL == EndOfRead)
	  break; // Finished processing this read - do next pair member

//	clock_gettime(CLOCK_MONOTONIC, &filterClock);
        if (!filter.isDuplicate(packedCAL, readData0->getReadID())) {
	  // Figure out which FPGA to send to.
	  uint64_t CAL = packedCAL & ((1ULL << 32ULL) - 1ULL);
	  uint64_t rev = packedCAL >> 32ULL;

	  CAL = (CAL < indelAllowance) ? 0 : CAL - indelAllowance;

	  // Now there is an FPGA control thread per FPGA, so swId is just id
	  int swId = id;
	  ++calsCount;

	  packedCAL = CAL | (rev << 32ULL); // Repack the CAL for GetScores
//          clock_gettime(CLOCK_MONOTONIC, &revClock);

	  // Queue reverse CAL for later
	  if (rev) {
	    reverseCALs.push_back(swId);
	    reverseCALs.push_back(packedCAL);
	    continue;
	  }
  
//          clock_gettime(CLOCK_MONOTONIC, &alignClock);
          if (! sentRead) {
            sws[swId]->setPackedRead(readData0->getPackedRead(), readLength, readData0->getReadID());
            FPGAcontrolReadsSent[swId]++;
            sentRead = true;
          }

	  int ref_length = readLength + (2*indelAllowance) + (CAL & CALAlignMask);
//          clock_gettime(CLOCK_MONOTONIC, &sendClock);
	  sws[swId]->alignCAL(CAL, ref_length, 0, 0, false);
//          ptStop(&ptCtrl);
	  sq_produce(FPGAcontrol_GetScores_qs[swId],
		     packedCAL,
		     (sq_callback)FPGAcontrolThread_flushAll,
		     (sq_cbData)id);
//          ptStart(&ptCtrl);
	}/* else {
          clock_gettime(CLOCK_MONOTONIC, &revClock);
          clock_gettime(CLOCK_MONOTONIC, &alignClock);
          clock_gettime(CLOCK_MONOTONIC, &sendClock);
        }
        clock_gettime(CLOCK_MONOTONIC, &endClock);
        tStartClocks += (filterClock.tv_sec * 1000000000 + filterClock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
        tFilterClocks += (revClock.tv_sec * 1000000000 + revClock.tv_nsec) - (filterClock.tv_sec * 1000000000 + filterClock.tv_nsec);
        tRevClocks += (alignClock.tv_sec * 1000000000 + alignClock.tv_nsec) - (revClock.tv_sec * 1000000000 + revClock.tv_nsec);
        tAlignClocks += (sendClock.tv_sec * 1000000000 + sendClock.tv_nsec) - (alignClock.tv_sec * 1000000000 + alignClock.tv_nsec);
        tSendClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (sendClock.tv_sec * 1000000000 + sendClock.tv_nsec);
        tEndClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
        calLoops++;
        if (calLoops >= 100000000) {
          cout << "calLoops " << calLoops << " took " << tEndClocks/1000000000.0 << "s, rate = " << calLoops / (tEndClocks / 1000000000.0) << " loops/sec, start " << tStartClocks / 1000000000.0 << "s, filter " << tFilterClocks / 1000000000.0 << "s, rev " << tRevClocks / 1000000000.0 << "s, align " << tAlignClocks / 1000000000.0 << "s, send " << tSendClocks / 1000000000.0 << "s" << endl;
          tStartClocks = tFilterClocks = tRevClocks = tSendClocks = tAlignClocks = tEndClocks = calLoops = 0;
          }*/
      }
    }

    // Dispatch queued reverse CALs
    vector<uint64_t>::const_iterator it;
    bool sentRevRead = false;

    for (it = reverseCALs.begin(); it != reverseCALs.end(); ++it) {
//      clock_gettime(CLOCK_MONOTONIC, &startClock);
      int swId = *it;
      ++it;
      uint64_t packedCAL = *it;
      uint64_t CAL = packedCAL & ((1ULL << 32) - 1ULL);
      
//      clock_gettime(CLOCK_MONOTONIC, &filterClock);
      if (! sentRevRead) {
        sws[swId]->setPackedRead(readData0->getPackedReadRevComp(), readLength, readData0->getReadID());
        FPGAcontrolReadsSent[swId]++;
        sentRevRead = true;
      }

      int ref_length = readLength + (2*indelAllowance) + (CAL & 0x7F);
      sws[swId]->alignCAL(CAL, ref_length, 0, 0, false);
//      clock_gettime(CLOCK_MONOTONIC, &sendClock);
//      ptStop(&ptCtrl);
      sq_produce(FPGAcontrol_GetScores_qs[swId],
                 packedCAL,
                 (sq_callback)FPGAcontrolThread_flushAll,
                 (sq_cbData)NULL);
//      ptStart(&ptCtrl);
/*      clock_gettime(CLOCK_MONOTONIC, &endClock);
      tStartClocks += (filterClock.tv_sec * 1000000000 + filterClock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
      tFilterClocks += (sendClock.tv_sec * 1000000000 + sendClock.tv_nsec) - (filterClock.tv_sec * 1000000000 + filterClock.tv_nsec);
      tSendClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (sendClock.tv_sec * 1000000000 + sendClock.tv_nsec);
      tEndClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
      calLoops++;
      if (calLoops >= 100000000) {
        cout << "calLoops " << calLoops << " took " << tEndClocks/1000000000.0 << "s, rate = " << calLoops / (tEndClocks / 1000000000.0) << " loops/sec, start " << tStartClocks / 1000000000.0 << "s, filter " << tFilterClocks / 1000000000.0 << "s, send " << tSendClocks / 1000000000.0 << "s" << endl;
        tStartClocks = tFilterClocks = tRevClocks = tSendClocks = tAlignClocks = tEndClocks = calLoops = 0;
      }*/
    }
//    ptOp(&ptCtrl, "ctrl");

    // Finish up the reverseComplement read.
    sq_produce(FPGAcontrol_GetScores_qs[id],
               EndOfRead,
               (sq_callback)FPGAcontrolThread_flushAll,
               (sq_cbData)NULL);
  }

/*
  // Drain the rest of the queues.
  for (int dindex = (pair_id+1)%(NUM_TABLETHREAD_PAIRS/SW_NUM);
       dindex != pair_id;
       dindex = (dindex+1)%(NUM_TABLETHREAD_PAIRS/SW_NUM)) {
    if (sq_consume(table_FPGAcontrol_qs[dindex*2+0],
		   (sq_callback)FPGAcontrolThread_flushAll,
		   (sq_cbData)NULL) != EndOfAllReads
	|| sq_consume(table_FPGAcontrol_qs[dindex*2+1],
		      (sq_callback)FPGAcontrolThread_flushAll,
		      (sq_cbData)NULL) != EndOfAllReads) {
      cout << "*** Missing EndOfAllReads at end of queue ***\n";
    }
  }
*/

  cout << "\nCAL counts sw[" << id << "]: " << calsCount << "\n";

  // Send the read count to the main thread.
  FPGAcontrolThreadReadCount[id] = readCount;
  FPGAcontrolUniqueCALsSent[id] = calsCount;
  //  cout << "Countrol thread handled " << readCount << " reads\n";
}

// Helper for doGetScores.  Returns the next readDescr, pulling it from all
// FPGAcontrol_GetScores_qs's.  If done with reads, will output EndOfAllReads, and
// will empty the rest of the queues.
Inline readDescr *getScores_nextReadDescr(uint64_t id) {
  readDescr *readData
    = (readDescr *)sq_consume(FPGAcontrol_GetScores_qs[id],
			      (sq_callback)getScores_flushAll,
			      (sq_cbData)NULL);

  return readData;
}

// Variables for statistics and checking.
// Total number of unique CALs recieved back, per FPGA
uint64_t GetScoresUniqueCALsRecieved[SW_NUM];

// CALs / read histogram
uint64_t GetScoresCALsPerRead[SW_NUM];
vector<uint64_t> CALsPerReadVec;//[SW_NUM];

// Helper for doGetScores.  Grabs all the scoredCALs for the read, putting them into
// filteredSet.  It consumes the EndOfRead on all FPGAcontrol_GetScores_qs at the end, so
// the queues are ready for the next call of the routine for the next read (if any).
Inline void getScores_grabScoredCALs(scoredSet *filteredSet, readDescr *readData, uint64_t id) 
{
  bool done = false; // True when done processing all CALs on that SmWa unit

  // We do round-robin reading of values from queues that aren't done.
  while (! done) {
    // Better branch predict via longer burst of reads.  But, doesn't seem to help much. Probably does nothing with getScore thread per FPGA
    for (int burst=0; !done && burst<1000; burst++) { 
//      ptStop(&ptScore);
      uint64_t packedCAL = sq_consume(FPGAcontrol_GetScores_qs[id],
                                      (sq_callback)getScores_flushAll,
                                      (sq_cbData)NULL);
//      ptStart(&ptScore);
      if (packedCAL == EndOfRead) {
	done = true;
        if (id == 0) {
          CALsPerReadVec.push_back(GetScoresCALsPerRead[id]);
          GetScoresCALsPerRead[id] = 0;
        }
        continue;
      } else {
        GetScoresCALsPerRead[id]++;
      }

      GetScoresUniqueCALsRecieved[id]++;

      // Uncomment the the next line, and set --noUnmapped to test without any getScores
      // work - tests rest of system's throughput.
//      sws[id]->trashScoredCAL(); continue;

      // Grab Score From FPGA
      scoredCAL *scoring = sws[id]->getScoredCAL(packedCAL);

      //scoring->printScoredCAL();

      // Skip this check if we are skipping the card, and just fix read ID
      if (SKIP_CARD) { 
        scoring->_ReadID = readData->getReadID();
      } else if (scoring->getReadID() != readData->getReadID()) {
        cerr << "ERROR: FPGA" << id
	     << " returned readID: " << scoring->getReadID()
	     << ", should be: " << readData->getReadID()
	     << "**\n" << flush;
        exit(EXIT_FAILURE);
      }
	
//      cout << "Pushing read from id " << scoring->getReadID() << " with score " << scoring->getScore()<<endl;
      filteredSet->push(scoring);
    }
  }
}

bool doGetScoresDone[SW_NUM]={false};
uint64_t doGetScoresCurrentReadID=0; /* For printing status in main thread.
				      * What read are we processing now?
                                      * Only doGetScores thread 0 prints this
				      */

// Gathers information from all FPGA units and outputs the appropriate
// results.
void *doGetScores(void *x) {
  uint64_t id = (uint64_t)x;

  char outFilename[15];
  const char *baseOutName = "output_000.sam";
  ofstream out;
  strcpy(outFilename, baseOutName);
  outFilename[7] += (id%1000)/100;
  outFilename[8] += (id%100) /10;
  outFilename[9] += (id%10);
  out.open(outFilename);
  if (out == NULL) {
    cerr << "** Cannot open output file " << outFilename << " **\n";
    exit(EXIT_FAILURE);
  }

  char unmappedFilename[19];
  const char *baseUnmappedName = "unmapped_000.fastq";
  ofstream unmapped;
  strcpy(unmappedFilename, baseUnmappedName);
  outFilename[9]  += (id%1000)/100;
  outFilename[10] += (id%100) /10;
  outFilename[11] += (id%10);
  unmapped.open(unmappedFilename);
  if (unmapped == NULL) {
    cerr << "** Cannot open unmapped file " << unmappedFilename << " **\n";
    exit(EXIT_FAILURE);
  }

  GetScoresUniqueCALsRecieved[id]=0;
  GetScoresCALsPerRead[id] = 0;

  // variable for creating cigar strings
  int cigarRefLen = readLength+3*indelAllowance+2*CALOffset;
  // cigarRefLen Larger than needed - but ensure take larger region than HW so
  // we see the same good path, or better.
  cigarGen cg(readLength, cigarRefLen);

  char unpackedRef[cigarRefLen+1];

  // CIGAR string for exact match/mismatch
  char noIndelStr[20];
  sprintf(noIndelStr, "%dM", readLength);

  // Read group for output reads to SAM
  string readGroup;

  // Unit for filtering matches.  Different types of units support different
  // filtering styles.
  scoredSet *filteredSet;
  switch (filterStyle) {
  case BEST:
    filteredSet = new scoredSet_allBest(scoreCutoff, readLength*C_MATCH);
    break;
  case BEST_UNIQUE:
    filteredSet = new scoredSet_uniqueBest(scoreCutoff, readLength*C_MATCH);
    break;
  case ALL:
    filteredSet = new scoredSet_all(scoreCutoff, readLength*C_MATCH);
    break;
  default:
    cout << "ERROR: Unknown filterStyle\n";
    exit(EXIT_FAILURE);
  }
  
  outputSAMHeader(out, refFilename, readGroup);

  // Should only have to do this once, if we had the starting CAL for the
  // read, but we have the ending CAL instead.  So,
  // 1.) We filter end CALs before spending time on software SmithWaterman
  // 2.) We filter start CALs at the end, to catch cases where two different
  //     end CALs have the same start CAL.
  set<uint64_t>preFilter;
  set<uint64_t>postFilter;

  // To measure performance of score CAL loop
//  struct timespec startClock, grabClock, procClock, endClock;
//  unsigned long long totalStartClocks = 0, totalGrabClocks = 0, totalProcClocks = 0, totalReads = 0;

  // DEBUG TIMERS
//  ptInit(&ptScore, 100000);

  while (true) {
//    clock_gettime(CLOCK_MONOTONIC, &startClock);
//    ptStart(&ptScore);

    // Get filters ready for processing next read.
    filteredSet->clear();
    preFilter.clear();
    postFilter.clear();
    
//    ptStop(&ptScore);
    readDescr *readData = getScores_nextReadDescr(id);
//    ptStart(&ptScore);
    if (readData == EndOfAllReads) { // We're done!
      break;
    }
    if (id == 0) {
      doGetScoresCurrentReadID = readData->getReadID();
    }

    //cout << "ReadDescr ID: " << readData->getReadID() << "\n";
//    clock_gettime(CLOCK_MONOTONIC, &grabClock);

    // Get all CALs for this read.
    getScores_grabScoredCALs(filteredSet, readData, id);

//    clock_gettime(CLOCK_MONOTONIC, &procClock); // Now that we have grabbed the CALs, start the clock

    // Now that all scoredCALs are in filteredSet, output the winners.
    uint64_t numWritten = 0; // Keep track of CALs/read, for CALlimit.
    if (filteredSet->empty()) {
      // In this case write to the unmapped file
      if (!noUnmapped) {
        // No mapping, send to unmapped file.
        unmapped << "@" << readData->getIdentifier() << "\n";
        unmapped << readData->getRead() << "\n";
        unmapped << "+\n";
        unmapped << readData->getQuality() << "\n";
      }
      // In this case write to the SAM output file, not that the options are
      // independent so both, one or neither can be used
      if (unmappedSAM) {
        // Set the flags correctly
        outputSAMline(out, readData, 0, NULL,
		      0, false, true,
		      "X", 0, NULL, NULL);
      }
    } else {
      while (!filteredSet->empty()) {
	scoredCAL *scoring = filteredSet->pop();
	uint64_t packedCAL = scoring->getPackedCAL();

	if (preFilter.find(packedCAL) != preFilter.end()) {
	  continue; // Duplicate packedCAL - skip it
	}
	preFilter.insert(packedCAL);
	
	uint64_t CAL = packedCAL & ((1ULL << 32) - 1ULL);
	uint64_t rev = packedCAL >> 32;
	int32_t score = scoring->getScore();
//	printf("%ld,%ld,%d\n", CAL, rev, score);
	const char *cigarStr, *mdStr;
	int editDist;

	char *thisUnpackedRead = (rev) ? readData->getReadCleanedRevComp()
	                               : readData->getReadCleaned();
	
	// Determine the alignment, and adjust the starting CAL
	if (noCIGAR) {
	  cigarStr = "*";
	  mdStr = NULL;
	  editDist = 0;
	  // Keep CAL as is
	} else if (scoring->getNoIndel() == 1) {
	  cigarStr = noIndelStr;
	  mdStr = NULL;
	  CAL = scoring->getLastBase() - readLength;
	  editDist = ((readLength * C_MATCH) - score)/(C_MATCH-C_MISMATCH);
	  if (editDist == 0) {
	    mdStr = cg.computeMDperfect();
	  } else {
	    unpackReadMultiWord(unpackedRef, full_ref, CAL+1,readLength);
	    mdStr = cg.computeMDnoIndel(thisUnpackedRead, unpackedRef);
	  }
	} else {
          //int64_t startPoint = CAL-indelAllowance-CALOffset;

          int64_t startPoint = CAL - indelAllowance;
          //printf("CAL = %lu base=%ld align=%ld\n", CAL, scoring->getLastBase() - startPoint, scoring->getLastBase());
          //printf("CAL = %lu offset = %d startpoint = %ld\n", CAL, CALOffset, startPoint);
	  uint64_t trueCAL; // Exactly where read matches, based on softwareSmWa
	  if (startPoint < 0) startPoint = 0;
	  unpackReadMultiWord(unpackedRef, full_ref,
			      startPoint, cigarRefLen);
          if (SKIP_CARD) {
            cigarStr = "*";
          } else {
            cigarStr = cg.computeCIGAR(thisUnpackedRead, unpackedRef,
                                       scoring->getLastBase() - startPoint,
                                       score,
                                       trueCAL, editDist, mdStr);
          }
	  CAL = trueCAL+startPoint;
	}
	
	if (postFilter.find(CAL) != postFilter.end()) {
	  continue; // Duplicate packedCAL - skip it
	}
	postFilter.insert(CAL);
	
	outputSAMline(out, readData, CAL, chromData,
		      score, rev, false,
		      cigarStr, editDist, mdStr,
                      readGroup.c_str());
	numWritten++;
        if (numWritten >= CALlimit) // Hit max - get rid of rest
          filteredSet->clear();
      }
    }
    delete readData; // Delete the readDescr so we don't leak memory

    // Compute the time, and inc reads
/*    clock_gettime(CLOCK_MONOTONIC, &endClock);
    totalStartClocks += (grabClock.tv_sec * 1000000000 + grabClock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
    totalGrabClocks += (procClock.tv_sec * 1000000000 + procClock.tv_nsec) - (grabClock.tv_sec * 1000000000 + grabClock.tv_nsec);
    totalProcClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (procClock.tv_sec * 1000000000 + procClock.tv_nsec);
    totalReads++;
    if (totalReads >= 200000) {
      unsigned long long totalClocks = totalStartClocks + totalGrabClocks + totalProcClocks;
      cout << totalReads << " reads took " << totalClocks /1000000000.0 << " seconds, for a rate of " << totalReads / (totalClocks/1000000000.0) << " reads/s" << endl;
      cout << "Individual times were start " << totalStartClocks/1000000000.0 << " grab " << totalGrabClocks/1000000000.0 << " proc "<< totalProcClocks/1000000000.0 << endl;
      totalReads = 0;
      totalStartClocks = 0;
      totalGrabClocks = 0;
      totalProcClocks = 0;
      }*/
//    ptOp(&ptScore, "score");
  }
  out.close();
  delete filteredSet;
  delete sws[id]; // Finishes up the threads inside the SmithWaterman objects
  doGetScoresDone[id]=true;
}

static void parse_cmd_args(int argc, char *argv[]);

// Hardcoded, picks the best fpga configuration to use.
const char *bitfileDirectory = "fpga/fpgaBit/";

const char *pickBitfile(int readLength)
{
  const char *filename = NULL;

  if (readLength <= 76)
    filename = "Aligner8_76.bit";
  else if (readLength <= 100)
    filename = "Aligner6_100.bit";
  else {
    cerr << "** No fpga bitfile exists for reads of length " << readLength << " **\n";
    exit(EXIT_FAILURE);
  }

  return combineStrings(bitfileDirectory, filename);
}

/////////////////
// MAIN METHOD //
/////////////////
int main(int argc, char* argv[]){
  time_t              startTime, endTime, midTime, tableCreated, processingTime;
  double              runTime;

  parse_cmd_args(argc, argv);
  initConversions();

  time (&startTime);

  // Make sure NUMA support is available
  if (numa_available() == -1) {
    cerr << "Attempting to use NUMA on a non-NUMA machine\n";
    exit(1);
  }
  if (numa_max_node() != 1) {
    cerr << "Machine should have exactly 2 nodes for NUMA testing\n";
    exit(1);
  }

  cout << "-=-=-=-=-=- Short Read Processing -=-=-=-=-=-\n";

  // Establish nodemasks for putting memory and threads on the right node (socket).
  oddMask = numa_allocate_nodemask();
  evenMask = numa_allocate_nodemask();
  numa_bitmask_setbit(oddMask, 1);
  numa_bitmask_setbit(evenMask, 0);

  // Instantiate the indexes
  cout << "Opening pointer table " << ptrTableFilename << "\n";
  initPtrTables(ptrTableFilename, oddMask, evenMask);

  time (&endTime);
  runTime = difftime (endTime,startTime);
  cout << "Pointer Table creation: " << runTime << " seconds\n";
  midTime = endTime;

  cout << "Opening CAL table " << CALtableFilename << "\n";
  //initCALtables(CALtableFilename, oddMask, evenMask);
  initCALtables_bin(CALtableFilename, oddMask, evenMask);

  time (&endTime);
  runTime = difftime (endTime,midTime);
  cout << "CAL Table creation: " << runTime << " seconds\n";
  midTime = endTime;

  // Create the inter-thread queues
  buildQueues();

  sws = new SmithWaterman*[SW_NUM];
  for (int i = 0; i < SW_NUM; ++i) {
      cout << "Creating SW " << i << " setting send_queue to " << &SmWa_send_qs[i] << " inx is " << SmWa_send_qs[i]->c_inx << " score queue " << &SmWa_score_qs[i] << " inx is " << SmWa_score_qs[i]->c_inx << endl;
     sws[i] = new SmithWaterman((1<<30), bitfileFilename, SmWa_send_qs[i], SmWa_score_qs[i]);
  }
  time (&endTime);
  runTime = difftime (endTime,midTime);
  cout << "SmithWaterman Init time: " << runTime << " seconds\n";
  midTime = endTime;

  if (sws[0]->getPicoCardModel() == 0x505) {
    CALOffset = 128;
    CALAlignMask = 0x7F;
  } else {
    CALOffset = 64;
    CALAlignMask = 0x3F;
  }

  chromData = new chrom();
  swFasta2ddr(sws, SW_NUM, refFilename, full_ref, full_ref_len, *chromData);

  if (verifyIndex) checkIndex(refFilename, chromData->refLength(), verifyIndex);

  //chromData->print();
  time (&endTime);
  runTime = difftime (endTime,midTime);
  cout << "SmithWaterman Setup: " << runTime << " seconds\n";
  midTime = endTime;

  // Create the threads
  // includes 1 input and 1 control thread, a pair for each tablethread_Pair
  // 2 input and 1 output for each FPGA in the SmWa units, and 1 GetScore for
  // each FPGA.
  cout << "Table thread pairs: " << NUM_TABLETHREAD_PAIRS
       << " Ttl threads: " << NUM_TABLETHREAD_PAIRS*2+2+2*(4) << "\n";
  pthread_t tableThreads[NUM_TABLETHREAD_PAIRS][2];

  numa_bind(evenMask);
  pthread_t inThread;
  pthread_create(&inThread, NULL, doInThread, (void *) 0);
  for (int i=0; i<NUM_TABLETHREAD_PAIRS; i++) {
    pthread_create(&tableThreads[i][0], NULL, doTableThread, (void *)(i*2+0ULL));
  }
  pthread_t getScores_Thread[SW_NUM];
  for (int i=0; i<SW_NUM; i++) {
    pthread_create(&getScores_Thread[i], NULL, doGetScores, (void*)(i+0ULL));
  } // It might be good to think about how to bind getScores and FPGAcontrol now

  numa_bind(oddMask);

  pthread_t FPGAcontrolThread[SW_NUM];
  for (int i=0; i<SW_NUM; i++) {
    pthread_create(&FPGAcontrolThread[i], NULL, doFPGAcontrolThread, (void *)(i+0ULL));
  }
  for (int i=0; i<NUM_TABLETHREAD_PAIRS; i++) {
    pthread_create(&tableThreads[i][1], NULL, doTableThread, (void *)(i*2+1ULL));
  }

  /* The following code can monitor queue status during execution */
  bool allGetScoresDone;
  do {
    usleep(100*1000*1000);
    //continue; // skip the rest of the loop

    printQueueInfo();  // Prints info on queue stalls
    time (&endTime);
    processingTime = difftime (endTime,midTime);
    uint64_t num_cals=0;
    for (int v=0; v<SW_NUM; v++) {
      num_cals += GetScoresUniqueCALsRecieved[v];
    }
    cout << "Execution time: " << processingTime
	 << " reads/sec: " << doGetScoresCurrentReadID/processingTime
	 << " CALs/sec: " << num_cals/processingTime
	 << " CALs/read: " << (1.0*num_cals)/doGetScoresCurrentReadID
	 << "\n";
    
    allGetScoresDone = true;
    for (int i=0; i < SW_NUM; i++) {
      if(!doGetScoresDone[i]) {
        allGetScoresDone = false;
        break;
      }
    }
  } while(!allGetScoresDone);

  ///////////////////////////
  // Join with final thread, so we wait until processing is over. //
  ///////////////////////////
  pthread_join(inThread, NULL);
  for (unsigned int i=0; i<SW_NUM; i++) {
    pthread_join(FPGAcontrolThread[i], NULL);
  }
  pthread_join(tableThreads[0][0], NULL);
  for (unsigned int i=0; i<SW_NUM; i++) {
    pthread_join(getScores_Thread[i], NULL); // Note: when ending, getScores also deletes the sws's.
  }

  time (&endTime);
  printQueueInfo();  // Prints info on queue stalls

  processingTime = difftime (endTime,midTime);

  cout << "\n\nExecution time: " << processingTime << " seconds\n";
  if (processingTime < 1) processingTime = 1;
  uint64_t memReadsPerSecond = InThreadReadCount*(readLength-seedLength+1)*2/processingTime;
  cout << "  Table accesses/second: " << setprecision(4) << (memReadsPerSecond/1000000) << "M"
       << " Reads/second: " << setprecision(4) << (InThreadReadCount/processingTime/1000000) << "M"
       << " thread pairs: " << NUM_TABLETHREAD_PAIRS << "\n";
  runTime = difftime (endTime,startTime);
  cout << "Total Runtime: " << runTime << " seconds\n";

  // Sum up the FPGAcontrol reads
  uint64_t FPGAcontrolReads = 0;
  for (int read_sw = 0; read_sw < SW_NUM; read_sw++)
      FPGAcontrolReads += FPGAcontrolThreadReadCount[read_sw];

  cout << "Reads processed: inThread: " << InThreadReadCount
       << " controlThreads: " << FPGAcontrolReads << endl;
  cout.flush();
  if (FPGAcontrolReads != InThreadReadCount) {
    cerr << "*** Read count mismatch - data is being lost in table lookup! ***\n"; exit(2);
  }

  cout << "CALs processed per FPGA\n";
  bool mismatch = false;
  for (int t=0; t<SW_NUM; t++) {
    cout << "  FPGA" << t << " sent: " << FPGAcontrolUniqueCALsSent[t]
	 << " CALs, recieved: " << GetScoresUniqueCALsRecieved[t] << " CALs\n";
    if (FPGAcontrolUniqueCALsSent[t] != GetScoresUniqueCALsRecieved[t])
      mismatch = true;
    // Dump CAL vectors:
    ofstream outfile("calvector_000.txt", ios::out | ios::trunc);
    for (unsigned int k=0; k < CALsPerReadVec.size(); k++) {
      outfile << CALsPerReadVec.at(k) << endl;
    }
    //ofstream outfile("calvector_000.txt", ios::out | ios::binary);
    //outfile.write((char*)&CALsPerReadVec[0], CALsPerReadVec.size()*sizeof(uint64_t));
  }
  cout.flush();

  if (mismatch) {
    cerr<< "*** CAL count mismatch - data is being lost in FPGA! ***\n"; exit(2);
  }

  
  uint64_t dataSent=0;
  uint64_t uniqueCALs=0;
  for (int t=0; t<SW_NUM; t++) {
    dataSent += FPGAcontrolReadsSent[t]*readLength/4 // 4 bases/byte
      + FPGAcontrolUniqueCALsSent[t]*4*2; // CALs + score responses
    uniqueCALs += FPGAcontrolUniqueCALsSent[t];
  }
  double throughput = dataSent/processingTime /(1024.0*1024.0);
  cout << "Estimated throughput, summed across FPGAs: "
       << (dataSent/1024.0/1024.0) << "MB/"
       << processingTime << "sec = "
       << throughput << "MB/sec\n";
  cout << "Average Unique CALs/read: "
       << (uniqueCALs / (1.0*InThreadReadCount))
       << "\n";
  
  delete chromData; // Might as well do some cleanup

  return 0;
}

static void usage(char *pgrm_name) {
  cout << "Usage: " << pgrm_name << endl;
  cout << endl;
  
  cout << "  --help, -h:          Print this usage message and then exit.\n";
  cout << endl;
  cout << "  --reads  <filename>: Set the short-reads input file to map.\n";
  cout << "  --ref    <filename>: Set the reference genome.\n";
  cout << "  --bitFile <filename>: Set the bit file.\n";
  cout << "  --ptrTbl <filename>: Set the indexing table file.\n";
  cout << "  --calTbl <filename>: Set the CAL table file pointed to by the index table." << endl;
  cout << "  --TblDir <dir>: Directory for ptrTbl, calTbl, and ref.fasta\n";
  cout << "       This option forces ptrTbl=<dir>/pointer_table.txt,\n";
  cout << "       calTbl=<dir>/cal_table.txt.bin, \n";
  cout << "       and ref=<dir>/reference.fasta\n";
  cout << endl;
  cout << "  --minScore    <value>: CALs with a score below this are discarded.\n";
  cout << "  --numMismatch <value>: sets minScore to tolerate up to this many mismatches.\n";
  cout << "  --maxN        <value>: rejects all reads with more than this many Ns.\n"; 
  cout << "  --bestUnique : output only one, best CAL for each read.\n";
  cout << "  --best : output all CALs for a read that have the highest score.\n";
  cout << "  --all : output all CALs above minScore for each read.\n";
  cout << "  --limitCALs <integer>: output at most this many CALs for each read\n";
  cout << endl;
  cout << "  --readLength <integer>" << endl;
  cout << "      set value := " << readLength << endl;
  cout << "  --seedLength <integer>: can be at most 32" << endl;
  cout << "      set value := " << seedLength << endl;
  cout << "  --limitReads <stop after this # of millions of reads>\n";
  cout << "  --verifyIndex <0: no, 1:yes, N>1: check only 1/nth" << endl;
  cout << "  --noCIGAR : don't produce CIGAR strings\n";
  cout << "  --noUnmapped : don't produce a file of unmapped reads\n";
  cout << "  --unmappedSAM : include unmapped reads in the SAM file\n";
  cout << endl;

  cout << "Files:" << endl;
#ifdef   USE_COMPRESSED_FASTQ
  cout << "  reads: The compressed fastQ formatted file of short-reads." << endl;  
#else  //USE_COMPRESSED_FASTQ
  cout << "  reads: The fastQ formatted file of short-reads." << endl;
#endif //USE_COMPRESSED_FASTQ
  cout << "    set value := "  << fastqFilename << endl;
  cout << "  ptrTbl: The index table from seeds to the CAL table." << endl;
  cout << "    set value := "  << ptrTableFilename << endl;
  cout << "  calTbl: The table of CALs." << endl;
  cout << "    set value := " << CALtableFilename << endl;
}

static void parse_cmd_args(int argc, char *argv[]) {
  int numMismatch = -1;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "--help") == 0 ||
	strcmp(argv[i], "-h") == 0) {
      usage(argv[0]);
      exit(EXIT_SUCCESS);
    } else if (strcmp(argv[i], "--numMismatch") == 0) {
      ++i;
      numMismatch = atoi(argv[i]);
    } else if (strcmp(argv[i], "--minScore") == 0) {
      ++i;
      scoreCutoff = atoi(argv[i]);
    } else if (strcmp(argv[i], "--maxN") == 0) {
      ++i;
      maxN = atoi(argv[i]);
    } else if (strcmp(argv[i], "--bestUnique") == 0) {
      filterStyle = BEST_UNIQUE;
    } else if (strcmp(argv[i], "--best") == 0) {
      filterStyle = BEST;
    } else if (strcmp(argv[i], "--all") == 0) {
      filterStyle = ALL;
    } else if (strcmp(argv[i], "--limitCALs") == 0) {
      ++i;
      CALlimit = atoi(argv[i]);
    } else if (strcmp(argv[i], "--noCIGAR") == 0) {
      noCIGAR = true;
    } else if (strcmp(argv[i], "--noUnmapped") == 0) {
      noUnmapped = true;
    } else if (strcmp(argv[i], "--unmappedSAM") == 0) {
      unmappedSAM = true;
    } else if (strcmp(argv[i], "--verifyIndex") == 0) {
      ++i;
      verifyIndex = atoi(argv[i]);
    } else if (strcmp(argv[i], "--calTbl") == 0) {
      ++i;
      CALtableFilename = argv[i];
    } else if (strcmp(argv[i], "--ptrTbl") == 0) {
      ++i;
      ptrTableFilename = argv[i];
    } else if (strcmp(argv[i], "--TblDir") == 0) {
      ++i;
      tableDirectory = argv[i];
    } else if (strcmp(argv[i], "--reads") == 0) {
      ++i;
      fastqFilename = argv[i];
    } else if (strcmp(argv[i], "--ref") == 0) {
      ++i;
      refFilename = argv[i];
    } else if (strcmp(argv[i], "--limitReads") == 0) {
      ++i;
      limitReads = atoi(argv[i])*1000*1000;
    } else if (strcmp(argv[i], "--readLength") == 0) {
      ++i;
      readLength = atoi(argv[i]);
      if (readLength < 1) {
	cerr << "Invalid readlength '" << argv[i] << "'" << endl;
	exit(2);
      }
    } else if (strcmp(argv[i], "--seedLength") == 0) {
      ++i;
      seedLength = atoi(argv[i]);
      if (seedLength < 1 || seedLength > 32) {
	cerr << "Invalid seedlength '" << argv[i] << "'" << endl;
	exit(EXIT_FAILURE);
      }
    } else if (strcmp(argv[i], "--bitfile") == 0) {
      ++i;
      bitfileFilename = argv[i];
    } else {
      cerr << "Unknown argument: " << argv[i] << endl;
      exit(EXIT_FAILURE);
    }
  }

  if (tableDirectory != NULL) {
    refFilename = combineStrings(tableDirectory, "/reference.fasta");
    CALtableFilename = combineStrings(tableDirectory, "/cal_table.txt.bin");
    ptrTableFilename = combineStrings(tableDirectory, "/pointer_table.txt");
  }

  if (scoreCutoff == INT_MIN && numMismatch == -1)
    numMismatch = readLength/20; // Default value if nothing is specified
  if (numMismatch > -1) {
    scoreCutoff = (readLength-numMismatch)*C_MATCH + numMismatch*C_GAP_OPEN;
  }

  cout << "ptrTableFilename=" << ptrTableFilename
       << "\nCALtableFilename=" << CALtableFilename
       << "\nrefFilename=" << refFilename
       << "\nreadsFilename=" << fastqFilename
       << "\nbitfile=" << bitfileFilename
       << "\nscoreCutoff=" << scoreCutoff
       << "\nnumMismatch=" << numMismatch
       << "\nmaxN=" << maxN
       << "\n\n";
}
