/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "conversions.h"

using namespace std;

// Arrays used for fast character conversion lookups.  Set up by initConversions()
int c2b[256]={255}; // What base corresponds to a given character.  b=255 means ERROR
char b2c[4]={'-'};  // What character corresponds to a given base.
char cleanChar[256]={'-'}; // Upper-case character corresponding to given base. N->A
char rb[256]={'-'}; // Given a base char, returns revComp of it.  keeps case. N->N

// Initialize the various lookup tables.
void initConversions()
{
  c2b['a']=0; c2b['A']=0; b2c[0]='A'; cleanChar['a']='A'; cleanChar['A']='A';
  c2b['c']=1; c2b['C']=1; b2c[1]='C'; cleanChar['c']='C'; cleanChar['C']='C';
  c2b['g']=2; c2b['G']=2; b2c[2]='G'; cleanChar['g']='G'; cleanChar['G']='G';
  c2b['t']=3; c2b['T']=3; b2c[3]='T'; cleanChar['t']='T'; cleanChar['T']='T';
  // N treated as C for simplicity
  c2b['n']=1; c2b['N']=1;             cleanChar['n']='C'; cleanChar['N']='C';

  rb['A']='T'; rb['T']='A'; rb['a']='t'; rb['t']='a';
  rb['C']='G'; rb['G']='C'; rb['c']='g'; rb['g']='c';
  rb['N']='N'; rb['n']='n';
}
