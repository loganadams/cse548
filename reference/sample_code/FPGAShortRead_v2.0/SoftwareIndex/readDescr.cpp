/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "common.h"
#include "readDescr.h"
#include "conversions.h"
#include <iostream>

using namespace std;

readDescr::~readDescr() {
  free(_read);
  free(_identifier);
  free(_quality);
  delete[] _packedRead;
  delete[] _packedReadRevComp;
  delete[] _readRevComp;
  delete[] _qualityRevComp;
  delete[] _readCleaned;
  delete[] _readCleanedRevComp;
}

// Takes a quality score (from a FASTQ file) and reverses it.
// source is the quality score to reverse, reversed is the output.
// Both source and reversed must point to buffers with readLength+1
// locations, where the final position is the '/0' at the end of the string.
void reverseQuality(char *reversed, char *source, int readLength)
{
  reversed[readLength]='\0';
  for (int i=0; i<readLength; i++)
    reversed[i] = source[readLength-i-1];
}

// Takes a read and reverseComplements it.
// source is the read to reverse, dest is the output.
// Both source and dest must point to buffers with readLength+1
// locations, where the final position is the '/0' at the end of the string.
void revCompUnpackedRead(char *source, char *dest, int readLength)
{
  dest[readLength]='\0';
  for (int i=0; i<readLength; i++)
    dest[i] = revBase(source[readLength-i-1]);
}

// Takes a FASTQ read and cleans it to use just AGCT.  Lower case goes
// to upper case, and N's go to A's. 
// Dest must be a buffer of the right size.  Readlength is the length of
// the source read, not including the final \0. 
void cleanString(char *dest, char *source, int readLength)
{
  for (int i=0; i<readLength; i++) {
    dest[i]=cleanedChar(source[i]);
  }
  dest[readLength]='\0';
}

void revComplPackedRead(const uint64_t read[], uint64_t *revCompl) {
  uint64_t length = readLength/32+1;
  memset(revCompl, 0, sizeof(uint64_t)*length);

  for (uint64_t i = 0; i < readLength; ++i) {
    uint64_t packedIndex = i/32;
    uint64_t baseIndex = 2*(i%32);
    uint64_t base = (read[packedIndex] & (3ULL << baseIndex)) >> baseIndex;

    uint64_t packedIndexCompl = (readLength-i-1)/32;
    uint64_t baseIndexCompl = 2*((readLength-i-1)%32);
    uint64_t baseCompl = base ^ 3ULL;

    revCompl[packedIndexCompl] |= (baseCompl << baseIndexCompl);
  }
}
