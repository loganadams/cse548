/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CALFILTER_H
#define CALFILTER_H 1

#include "common.h"

// The index table lookups will likely find the same CALs for multiple lookups, but sending
// this information, or processing it, is wasted effort.  Sorting the CALs can remove duplicates,
// but can be too time-consuming.  We use a simple table lookup instead.
//
// We have a table, indexed by func(CAL) (including reverse bit), that records the read
// and CAL last seen that matched that func(CAL).  If we find the same read and CAL at that
// location, we know we have found a duplicate.  If the read or CAL doesn't match, we can't be sure
// we haven't seen it before, but we send it on to be safe.  These false-negatives on matches are
// safe.  Note that this table could be made smaller by using smaller entries per table, but this
// version is simple, and likely good enough.

// FILTER_LENGTH_BITS in common.h controls the size of the tables -
// the tables are 2^FILTER_LENGTH_BITS long.
// Larger tables have fewer false-negatives, but consumes more space.
// Ideally, should be as large as possible while still fitting in the L1.
#define FILTER_LENGTH (1<<FILTER_LENGTH_BITS)

class CALfilter {
  uint64_t CAL[FILTER_LENGTH];
  uint64_t readID[FILTER_LENGTH];
  int CALtoIndex(uint64_t newCAL) {
    uint64_t temp = newCAL % (FILTER_LENGTH*FILTER_LENGTH-1);
    int index = (newCAL ^ (newCAL>>FILTER_LENGTH_BITS)) & (FILTER_LENGTH-1);
    return index;
  };
 public:
  CALfilter() {
    for(int i=0; i<FILTER_LENGTH; i++) {
      CAL[i]=0;
      readID[i]=0;
    }
  };
  // newCAL is a CAL, including the bit to say if it is from the reverse complement strand.
  // newReadID is a unique ID for this read.  You cannot use newReadID=0.  If you want to reuse
  // a newReadID, create a new CALfilter
  bool isDuplicate(uint64_t newCAL, uint64_t newReadID) {
    newCAL -= newCAL % indelAllowance; // Nearby CALs should map to same place
    int index = CALtoIndex(newCAL);
    bool result = ((CAL[index] == newCAL) && (readID[index] == newReadID));
    CAL[index] = newCAL;
    readID[index] = newReadID;
    return result;
  };
};

#endif
