/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdint.h>
#include "hashfunc.h"

// All hash functions are labelled hashfunc_X_Y, where X is the length of the value being hashed
// (generally seed length for us), and Y is the number of bottom bits that are XORed into the 
// top - the bottom Y bits of the output become the key for our CAL table, and the (X-Y) bits above
// that index into the ptr table. 

// A fast hash function.  Takes a 44-bit input, and converts to a 44-bit value, where the bottom 
// 12 bits are XORed into the upper 32 bits.
//
// We want a more general version, but this is a specific case useful as an example and a
// reference
uint64_t hashfunc_44_12(uint64_t in) {

  //masks were chosen to correspond to a 12 bit
  //to 32 bit function that meets the following
  //criteria:
  // 1. linear
  // 2. one-to-one
  // 3. high diffusion
  static const uint32_t mask_table1[16] = {
    0x00000000, 0x060c3b6b, 0xc893ca05, 0xce9ff16e,
    0x3d8645fa, 0x3b8a7e91, 0xf5158fff, 0xf319b494,
    0xb6cdfc5b, 0xb0c1c730, 0x7e5e365e, 0x78520d35,
    0x8b4bb9a1, 0x8d4782ca, 0x43d873a4, 0x45d448cf
  };
  static const uint32_t mask_table2[16] = {
    0x00000000, 0x2d9f30b4, 0xd9d0f582, 0xf44fc536,
    0xa18600d0, 0x8c193064, 0x7856f552, 0x55c9c5e6,
    0xa6047f32, 0x8b9b4f86, 0x7fd48ab0, 0x524bba04,
    0x07827fe2, 0x2a1d4f56, 0xde528a60, 0xf3cdbad4
  };
  static const uint32_t mask_table3[16] = {
    0x00000000, 0x6d648933, 0xb1200a71, 0xdc448342,
    0x2348dfbe, 0x4e2c568d, 0x9268d5cf, 0xff0c5cfc,
    0x9f602794, 0xf204aea7, 0x2e402de5, 0x4324a4d6,
    0xbc28f82a, 0xd14c7119, 0x0d08f25b, 0x606c7b68
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];

  
  accum <<= 12;
  return in ^ accum;
}

uint64_t hashfunc_44_16(uint64_t in) {
  static const uint32_t mask_table1[16] = {
    0x00000000, 0x05dcb2a3, 0x0f6e9761, 0x0ab225c2,
    0x0bfde90c, 0x0e215baf, 0x04937e6d, 0x014fccce,
    0x0687898b, 0x035b3b28, 0x09e91eea, 0x0c35ac49,
    0x0d7a6087, 0x08a6d224, 0x0214f7e6, 0x07c84545
  };
  static const uint32_t mask_table2[16] = {
    0x00000000, 0x09f35641, 0x0f057c30, 0x06f62a71,
    0x026141cc, 0x0b92178d, 0x0d643dfc, 0x04976bbd,
    0x09108d51, 0x00e3db10, 0x0615f161, 0x0fe6a720,
    0x0b71cc9d, 0x02829adc, 0x0474b0ad, 0x0d87e6ec
  };
  static const uint32_t mask_table3[16] = {
    0x00000000, 0x06b873ea, 0x0b483aec, 0x0df04906,
    0x0f4006b0, 0x09f8755a, 0x04083c5c, 0x02b04fb6,
    0x084ff610, 0x0ef785fa, 0x0307ccfc, 0x05bfbf16,
    0x070ff0a0, 0x01b7834a, 0x0c47ca4c, 0x0affb9a6
  };
  static const uint32_t mask_table4[16] = {
    0x00000000, 0x00578abc, 0x0bf29dcc, 0x0ba51770,
    0x05851539, 0x05d29f85, 0x0e7788f5, 0x0e200249,
    0x03482f1a, 0x031fa5a6, 0x08bab2d6, 0x08ed386a,
    0x06cd3a23, 0x069ab09f, 0x0d3fa7ef, 0x0d682d53
  };
  
  uint64_t temp, accum;
  
  temp = in;
  accum = 0;
  
  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  
  accum <<= 16;
  return in ^ accum;
}

uint64_t hashfunc_44_14(uint64_t in) {
  static const uint32_t mask_table1[16] = {
    0x00000000, 0x2d8a7ac2, 0x26d0b9f2, 0x0b5ac330,
    0x1f9f84aa, 0x3215fe68, 0x394f3d58, 0x14c5479a,
    0x38d0fa46, 0x155a8084, 0x1e0043b4, 0x338a3976,
    0x274f7eec, 0x0ac5042e, 0x019fc71e, 0x2c15bddc
  };
  static const uint32_t mask_table2[16] = {
    0x00000000, 0x1d4d264d, 0x34d4cda3, 0x2999ebee,
    0x172ba9ad, 0x0a668fe0, 0x23ff640e, 0x3eb24243,
    0x3713575c, 0x2a5e7111, 0x03c79aff, 0x1e8abcb2,
    0x2038fef1, 0x3d75d8bc, 0x14ec3352, 0x09a1151f
  };
  static const uint32_t mask_table3[16] = {
    0x00000000, 0x28f34ae5, 0x065bf1db, 0x2ea8bb3e,
    0x331782e4, 0x1be4c801, 0x354c733f, 0x1dbf39da,
    0x14ff1c8e, 0x3c0c566b, 0x12a4ed55, 0x3a57a7b0,
    0x27e89e6a, 0x0f1bd48f, 0x21b36fb1, 0x09402554
  };
  static const uint32_t mask_table4[4] = {
    0x00000000, 0x1f8b32d8, 0x32be3592, 0x2d35074a
  };
  uint64_t temp, accum;

  temp = in;
  accum = 0;
  
  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0x3 ];

  accum <<= 14;
  return in ^ accum;
}

uint64_t hashfunc_24_4(uint64_t in) {
  static const uint32_t mask_table1[16] = {
    0x00000000, 0x000e40f9, 0x000e6b91, 0x00002b68,
    0x000efb07, 0x0000bbfe, 0x00009096, 0x000ed06f,
    0x000a4373, 0x0004038a, 0x000428e2, 0x000a681b,
    0x0004b874, 0x000af88d, 0x000ad3e5, 0x0004931c
  };

  return in ^ (((uint64_t) mask_table1[ in & 0xf ]) << 4);
}

uint64_t hashfunc_null(uint64_t in) {
  return in;
}

uint64_t hashfunc_32_4(uint64_t in) {
  static const uint32_t mask_table1[16] = {
    0x00000000, 0xfeda9bad, 0x8d6936d8, 0x73b3ad75,
    0xadcd6082, 0x5317fb2f, 0x20a4565a, 0xde7ecdf7,
    0xaadb2106, 0x5401baab, 0x27b217de, 0xd9688c73,
    0x07164184, 0xf9ccda29, 0x8a7f775c, 0x74a5ecf1
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];

  accum <<= 4;
  return in ^ accum;
}

uint64_t hashfunc_40_12(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x7a2109bcb9, 0x6ba3a13fb4, 0x1182a8830d,
    0x398e034a8b, 0x43af0af632, 0x522da2753f, 0x280cabc986,
    0xc1bc73b2d3, 0xbb9d7a0e6a, 0xaa1fd28d67, 0xd03edb31de,
    0xf83270f858, 0x82137944e1, 0x9391d1c7ec, 0xe9b0d87b55
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x9f3e71a521, 0xb77cff66ab, 0x28428ec38a,
    0xb311650aa1, 0x2c2f14af80, 0x46d9a6c0a, 0x9b53ebc92b,
    0xa03b286259, 0x3f0559c778, 0x1747d704f2, 0x8879a6a1d3,
    0x132a4d68f8, 0x8c143ccdd9, 0xa456b20e53, 0x3b68c3ab72
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0xd7993879db, 0x447fc31a6d, 0x93e6fb63b6,
    0x8d63253809, 0x5afa1d41d2, 0xc91ce62264, 0x1e85de5bbf,
    0x97dd1f4d5d, 0x4044273486, 0xd3a2dc5730, 0x43be42eeb,
    0x1abe3a7554, 0xcd27020c8f, 0x5ec1f96f39, 0x8958c116e2
  };
  static const uint64_t mask_table4[16] = {
    0x00000000
  };
  
  uint64_t temp, accum;
  
  temp = in;
  accum = 0;
  
  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0x0 ];
  
  
  accum <<= 12;
  return in ^ accum;

}

uint64_t hashfunc_36_8(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x49642248b, 0xe6a9bcb38, 0xafcd9efb3,
    0x71d15715d, 0x38b5755d6, 0x9778eba65, 0xde1cc9eee,
    0xbd3cb575, 0x42b7e91fe, 0xed7a77e4d, 0xa41e55ac6,
    0x7a029c428, 0x3366be0a3, 0x9cab20f10, 0xd5cf02b9b
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x13446d1e3, 0xe38676941, 0xf0c21b8a2,
    0xa75edf67a, 0xb41ab2799, 0x44d8a9f3b, 0x579cc4ed8,
    0x89063d2d9, 0x9a425033a, 0x6a804bb98, 0x79c426a7b,
    0x2e58e24a3, 0x3d1c8f540, 0xcdde94de2, 0xde9af9c01
  };
  static const uint64_t mask_table3[16] = {
    0x00000000
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0x0 ];


  accum <<= 8;
  return in ^ accum;

}

uint64_t hashfunc_50_22(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x10281d3bc5889, 0x3fbd18f3d2aa4, 0x2f9505c81722d,
    0x22f5c2eb99e8f, 0x32dddfd05c606, 0x1d48da184b42b, 0xd60c7238eca2,
    0x312a1aa323794, 0x21020798e6f1d, 0xe970250f1d30, 0x1ebf1f6b345b9,
    0x13dfd848ba91b, 0x3f7c5737f192, 0x2c62c0bb683bf, 0x3c4add80adb36
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x20c904b038176, 0x54a99ff410b6, 0x25839d4f791c0,
    0x146c1a04237f, 0x218fc5107a209, 0x40c585f033c9, 0x24c55cef3b2bf,
    0x2340ec9671a5a, 0x389e82649b2c, 0x260a756930aec, 0x6c371d908b9a,
    0x22062d3633925, 0x2cf29860b853, 0x274cb4c972993, 0x785b0794a8e5
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0x1a2504b4f2045, 0xdfd2224b5af3, 0x17d8269047ab6,
    0x31a2fb28e5fa5, 0x2b87ff9c17fe0, 0x3c5fd90c50556, 0x267addb8a2513,
    0x37b4695e9313a, 0x2d916dea6117f, 0x3a494b7a26bc9, 0x206c4fced4b8c,
    0x616927676e9f, 0x1c3396c284eda, 0xbebb052c346c, 0x11ceb4e631429
  };
  static const uint64_t mask_table4[16] = {
    0x00000000, 0x32c477e214018, 0x29a1e09e97d99, 0x1b65977c83d81,
    0x2ef254352cf1, 0x302b52a146ce9, 0x2b4ec5ddc5168, 0x198ab23fd1170,
    0xf4de8aabc798, 0x3d899f48a8780, 0x26ec08342ba01, 0x14287fd63fa19,
    0xda2cde9eeb69, 0x3f66ba0bfab71, 0x24032d77796f0, 0x16c75a956d6e8
  };
  static const uint64_t mask_table5[16] = {
    0x00000000, 0xf07c4149224a, 0x2df6d87b3157c, 0x22f11c6fa3736,
    0x3e3151d8571f5, 0x313695ccc53bf, 0x13c789a366489, 0x1cc04db7f46c3,
    0x319bc17b46ade, 0x3e9c056fd4894, 0x1c6d190077fa2, 0x136add14e5de8,
    0xfaa90a311b2b, 0xad54b783961, 0x225c48d820e57, 0x2d5b8cccb2c1d
  };
  static const uint64_t mask_table6[4] = {
    0x00000000, 0x2d067962fc7f5, 0x1b691444f41e4, 0x366f6d2608611
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table6[ temp & 0x3 ];


  accum <<= 22;
  return in ^ accum;

}

uint64_t hashfunc_58_30(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x49273038507561b, 0x3d8aed1361a68ea, 0x1d3b464fc1c03ea,
0x0cf4fbd7d32d93d, 0x7c567afc1881df9, 0x6a111d6a094c6dc, 0x2d19550c01e3637,
0x00a5e8675888b78, 0x1719bb4731037ae, 0x48a19e901b0d0e2, 0x2b8c495fb1cde70,
0x6d36745ae001a08, 0x0580fabcc6626ae, 0x52b1c661fbf1bc2, 0x3be8a9bb7d7e137
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x36923a16343f375, 0x0346de6945263a0, 0x5750e68732e0686,
0x2d2e281dce098dc, 0x16a769893f90b5d, 0x0469251547a72f5, 0x1d8c6c82ddaf7b0,
0x60091d144a85951, 0x2bfe0f46fe0be2e, 0x27ee8257ea3ca4b, 0x63857b5ceb66dda,
0x20651c7cbda94a8, 0x10875a799d553f9, 0x65ebdee8ab6c986, 0x086b5626db895a6
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0x14f8ca2a0c1e59b, 0x2af86d5fb60d8ea, 0x6d40f83e0b4be72,
0x29718bb85fc44a4, 0x33354774a1116de, 0x2e559ff2beba87e, 0x131126fced82431,
0x28cc75f4ae40cc0, 0x3386f5f2ec10c98, 0x3a44142bef9fdf7, 0x2c44e3b77409b88,
0x027263c143e1aa5, 0x6d63b707acbdb67, 0x09f28f4c574e879, 0x738e50a0540ca21
  };
  static const uint64_t mask_table4[16] = {
    0x00000000, 0x179c26455687ef7, 0x7c4d9a81b66e0f3, 0x12617c2545e8f0d,
0x7305aa24d27c856, 0x1080114c7399aca, 0x04963f01e73d501, 0x1dfb8d776bc09e0,
0x5d498cd1668161e, 0x0fbff9523bb6892, 0x75ebadecf4a450b, 0x0e6d27c7f1fc727,
0x0c3c532d451121f, 0x214f4c661be02cb, 0x1c2ddcdd15b05cc, 0x1b6939a16abd365
  };
  static const uint64_t mask_table5[16] = {
    0x00000000, 0x3c86097da60fbaa, 0x314a4740747a8c2, 0x0736c2842c4ba2b,
0x0e2182a2d89e311, 0x135bd1ccdb44f59, 0x1517f0ee0915e22, 0x26ab30ce18b6be0,
0x1a2cadd8ab0e191, 0x138cb71b7d2ef87, 0x0c0b1f564c6d2ef, 0x03899cac13bb943,
0x37175433315ccce, 0x0be65fee7861980, 0x34dea038a530d4d, 0x3b0404f65d3d00f
  };
  static const uint64_t mask_table6[16] = {
    0x00000000, 0x2e4e16ca21248b5, 0x335b1eab55b7114, 0x301234335b15683,
0x2a5061e60b6d692, 0x23b80dcc0eedb91, 0x079f48f9afc61e0, 0x3d88c917351e447,
0x3762929dd59496d, 0x12c7521e81e6d8d, 0x2b0480df651a5fd, 0x14d4906b7b4df58,
0x61c1d87bc6e820e, 0x64dd4062d6b58cf, 0x1211eb73bfb0cf3, 0x59fa81f5c8939c0
  };
  static const uint64_t mask_table7[16] = {
    0x00000000, 0x2a3837a3c915c50, 0x2a3b81a6d7b3189, 0x5fc1f04dd8fee4c,
0x2e697de08f01185, 0x3ad27167b9dd8a9, 0x28e8d22971b31f8, 0x307638457391e5e,
0x6a339330cbd685f, 0x143280c010b7c1b, 0x4fe73dcf654d110, 0x03d7b7e690e21fe,
0x220fc23b6466e10, 0x516dac45eb6d72f, 0x0eeddae5d74b157, 0x2808cb14f3f65ea
  };
  static const uint64_t mask_table8[16] = {
    0x00000000, 0x5f57b87124ff296, 0x736be8760bb86d2, 0x1f55380b4ffdf4d,
0x2bb6fd785a18b6a, 0x782e6620cfa3bde, 0x1117b688b805a7b, 0x4fe023eaa2f720e,
0x24a4efee98f8715, 0x25c1a2307dc7528, 0x100bfe0ac8ef8a3, 0x6b9dfeca1da5f69,
0x2aec1251752d0f3, 0x1e5fe74954b21ee, 0x28aa57f39929672, 0x068cc543e92b273
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table6[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table7[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table8[ temp & 0x3 ];


  accum <<= 30;
  return in ^ accum;

}
