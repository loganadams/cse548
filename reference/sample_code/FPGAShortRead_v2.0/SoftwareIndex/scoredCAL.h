/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SCOREDCAL_H_
#define SCOREDCAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <iostream>

using namespace std;

/*
 * scoredCAL objects contain the information returned from the FPGA for a
 * given CAL.
 */

class scoredCAL {
 private:
//  uint32_t _ReadID;    // The ID identifying the READ
  int32_t _score;      // The score for this alignment
  bool _noIndel;       // True if the alignment involves only match & mismatch,
                       // no insert or delete
  uint64_t _lastBase;  // Position of last base in ref that matched this read.

  uint64_t _queryEnd;  // ???? Not sure - look at FPGA code to see what this is
                       // ???? probably last base in read that matched.
  uint64_t _packedCAL; // Storage of associated packedCAL.  Provided from
                       // elsewhere in code (not from FPGA).
 public:
  uint32_t _ReadID;    // The ID identifying the READ
  scoredCAL(uint32_t ReadID, int32_t score, bool noIndel, bool reverseComp,
	    uint64_t queryEnd, uint64_t lastBase, uint64_t packedCAL) {
    _ReadID = ReadID;
    _score = score;
    _noIndel = noIndel;
    _queryEnd = queryEnd;
    _lastBase = lastBase;
    _packedCAL = packedCAL;
    if (reverseComp) {
      std::cerr << "ERROR: revComp flag set to true on FPGA return\n";
      exit(EXIT_FAILURE);
    }
  }
  ~scoredCAL();
  
  // The ID identifying the READ
  uint32_t getReadID() { return _ReadID; }

  // The score for this alignment
  int32_t getScore() { return _score; }

  // True if the alignment involves only match & mismatch, no insert or delete
  bool getNoIndel() { return _noIndel; }

  // Position of last base in ref that matched this read.
  uint64_t getLastBase() { return _lastBase; }

  // Packed CAL associated with this core. [31..0] = CAL, [32] = revComp.
  uint64_t getPackedCAL() { return _packedCAL; }

  void printScoredCAL();
};

#endif /* SCOREDCAL_H_ */
