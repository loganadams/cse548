/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

int main(void) {
  ofstream outstrm;
  outstrm.open("SpeedTestFile.txt");
  if (outstrm == NULL) {
    cout << "Can't open file" << endl;
    return 1;
  }

  unsigned int timestowrite = 1024 * 1024 * 8;
  clock_t start, stop;

  cout << "Starting writes" << endl;
  start = clock();

  for(unsigned int i = 0; i < timestowrite; i++) {
    outstrm << "aOuttput " << i % 10
            << "bOuttput " << i % 10
            << "cOuttput " << i % 10
            << "dOuttput " << i % 10
            << "eOuttput " << i % 10
            << "fOuttput " << i % 10
            << "gOuttput " << i % 10
            << "hOuttput " << i % 10
            << "iOuttput " << i % 10
            << "jOuttput " << i % 10;
    outstrm << "jOuttput " << i % 10
            << "kOuttput " << i % 10
            << "lOuttput " << i % 10
            << "mOuttput " << i % 10
            << "nOuttput " << i % 10
            << "oOuttput " << i % 10
            << "pOuttput " << i % 10
            << "qOuttput " << i % 10
            << "rOuttput " << i % 10
            << "sOuttput " << i % 10
            << "tOuttput " << i % 10
            << "uOuttput " << i % 10
            << "vOuttput " << i % 10
            << "wOuttput " << i % 10;
    outstrm << "xOuttput " << i % 10
            << "Outtput " << i % 10
            << endl;
  }

  stop = clock();
  double sec = (stop - start) / (double) CLOCKS_PER_SEC;
  double speed = (260 * timestowrite / (1024*1024)) / sec;
  cout << "Finished " << timestowrite << " 260 byte writes, started at " << start << ", at " << stop << endl;
  cout << "Runtime was " << sec << "s" << endl;
  cout << "Speed was " << speed <<  "MB/s" << endl;

  outstrm.close();
  return 0;
}
