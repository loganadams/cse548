/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef READDESCR_H_
#define READDESCR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <iostream>

using namespace std;

// Takes a FASTQ read and cleans it to use just AGCT.  Lower case goes
// to upper case, and N's go to A's. 
// Dest must be a buffer of the right size.  Readlength is the length of
// the source read. 
void cleanString(char *dest, char *source, int readLength);

// Takes a quality score (from a FASTQ file) and reverses it.
// source is the quality score to reverse, reversed is the output.
// Both source and reversed must point to buffers with readLength+1
// locations, where the final position is the '/0' at the end of the string.
void reverseQuality(char *reversed, char *source, int readLength);

// Takes a read and reverseComplements it.
// source is the read to reverse, dest is the output.
// Both source and dest must point to buffers with readLength+1
// locations, where the final position is the '/0' at the end of the string.
void revCompUnpackedRead(char *source, char *dest, int readLength);

// Takes a packedRead and reverseComplements it.
// source is the read to reverse, dest is the output.
void revComplPackedRead(const uint64_t read[], uint64_t *revCompl);

/*
 * Holds the original data for each read, as well as related information for
 * printing this read in normal and reverse complemented versions.
 */

class readDescr {
 private:
  uint64_t _ReadID;      // The ID identifying the READ
  char *_read;           // The read itself, including any N's.
  char *_readCleaned;    // Read changed to just use AGCT
  char *_identifier;     // The FASTQ identifier string (the "@" line).
  char *_quality;        // The FASTQ quality line.
  uint64_t *_packedRead; // The read, in packed format
  uint64_t *_packedReadRevComp; // RevComp of packedRead

  char *_readRevComp;    // reversed complement of the read
  char *_readCleanedRevComp; // reverse comp cleaned to only use AGCT
  char *_qualityRevComp; // quality for reverse complement of read
 public:

  // Makes copies of all of the inputs.
  readDescr(uint64_t ReadID, char *identifier, char *read,
	    char *quality, uint64_t *packedRead, int readLength) {
    _ReadID = ReadID;
    _read = strdup(read);
    _identifier = strdup(identifier);
    _quality = strdup(quality);

    _packedRead = new uint64_t[readLength/32+1];
    _packedReadRevComp = new uint64_t[readLength/32+1];
    for (int i=0; i*32<readLength; i++) {
      _packedRead[i] = packedRead[i];
    }
    revComplPackedRead(packedRead, _packedReadRevComp);
    
    _readRevComp = new char[readLength+1];   
    revCompUnpackedRead(_read, _readRevComp, readLength);
    _qualityRevComp = new char[readLength+1];
    reverseQuality(_qualityRevComp, quality, readLength);

    _readCleaned = new char[readLength+1];
    cleanString(_readCleaned, _read, readLength);
    _readCleanedRevComp = new char[readLength+1];
    revCompUnpackedRead(_readCleaned, _readCleanedRevComp, readLength);
  }
  // Delete the locals for this read
  ~readDescr();
  
  // The ID identifying the READ
  uint64_t getReadID() { return _ReadID; }

  // The read itself, including any N's.
  char *getRead() { return _read; }

  // Return the read itself, but converted to just use AGCT.
  char *getReadCleaned() { return _readCleaned; }

  // The quality line from the FASTQ file
  char *getQuality() { return _quality; }

  // The identifier line from the FASTQ file
  char *getIdentifier() { return _identifier; }

  // The read, in packed form.
  uint64_t *getPackedRead() { return _packedRead; }

  // The read, in packed form.
  uint64_t *getPackedReadRevComp() { return _packedReadRevComp; }

  // reversed complement of the read
  char *getReadRevComp() { return _readRevComp; }

  // cleaned version of the reverse complement
  char *getReadCleanedRevComp() { return _readCleanedRevComp; }

  // quality for reverse complement of read
  char *getQualityRevComp() { return _qualityRevComp; } 

};

#endif /* READDESCR_H_ */
