/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include "allqueues.h"

using namespace std;

// *** Definitions of the inter-thread queues ***
//
// Queue from inThread to tableThreads.
// Indexed by id of the target tableThread.
SW_Queue *in_table_qs; 
//static const uint64_t EndOfAllReads = 0; //read_id that designates we are done.
// Protocol:
//   inThread can choose what pair of tableThreads to send a given read to.
//   Right now that is round-robin.
// Each in_table_qs:
//   Zero or more read records: {
//     <readdescr *>: Pointer to the readDescr object for this read.
//   }
//   EndOfAllReads: says all reads have been processed.
//
// ---------------------------------------------------------------------------
//
// Queue from tableThreads to FPGAcontrolThread.
// Indexed by id of the source tableThread.
SW_Queue *table_FPGAcontrol_qs;
// The queue value to signal the end of the CALs for a read.
//static const uint64_t EndOfRead = (2ULL << 32);
// Protocol:
//   Zero or more read records: {
//     <readdescr *>: Pointer to the readDescr object for this read.
//     Zero or more CAL records: {
//       <packedCAL>: 64-bit value
//                        [31..0]  CAL
//                        [32]     reverseComp bit
//                        [63..33] unused
//     }
//     EndOfRead: says no more CALs for this read are coming.
//   }
//   EndOfAllReads: says all reads have been processed.
//   
// ---------------------------------------------------------------------------
//
// Queue from FPGAcontrolThread to GetScores thread.
// Indexed by id of FPGA (there is a GetScores for each FPGA).
SW_Queue *FPGAcontrol_GetScores_qs;
// Protocol:
//   FPGAcontrolThread can choose what FPGA to send a given read to.
//   Right now that is based on specific reference regions to specific
//   FPGAs, but that is expected to change to provide better load
//   balancing.
// Each FPGAcontrol_GetScores_qs:
//   Zero or more read records: {
//     <readdescr *>: Pointer to the readDescr object for this read.
//     Zero or more CAL records, one for each CAL sent to corresponding FPGA: {
//       <packedCAL>: 64-bit value
//                        [31..0]  CAL
//                        [32]     reverseComp bit
//                        [63..33] unused
//     }
//     EndOfRead: says no more CALs for this read are coming.
//   }
//   EndOfAllReads: says all reads have been processed.
//   
// ---------------------------------------------------------------------------
//

// ---------------------------------------------------------------------------
//
// Internal SmWa send queues.
// Indexed by id of the SmithWaterman unit.
SW_Queue *SmWa_send_qs;
// Internal SmWa score queues.
// Indexed by id of the SmithWaterman unit.
SW_Queue *SmWa_score_qs;

// Routine that allocates and initializes the queues.
void buildQueues()
{
  in_table_qs
    = (SW_Queue *)malloc(sizeof(SW_Queue)*2*NUM_TABLETHREAD_PAIRS);
  table_FPGAcontrol_qs
    = (SW_Queue *)malloc(sizeof(SW_Queue)*2*NUM_TABLETHREAD_PAIRS);
  for (int i=0; i<NUM_TABLETHREAD_PAIRS*2; i++) {
    in_table_qs[i] = sq_createQueue();
    table_FPGAcontrol_qs[i] = sq_createQueue();
  }

  FPGAcontrol_GetScores_qs = (SW_Queue *) malloc(sizeof(SW_Queue)*SW_NUM);
  SmWa_send_qs = (SW_Queue *) malloc(sizeof(SW_Queue)*SW_NUM);
  SmWa_score_qs = (SW_Queue *) malloc(sizeof(SW_Queue)*SW_NUM);
  for (int i=0; i<SW_NUM; ++i) {
    FPGAcontrol_GetScores_qs[i] = sq_createQueue();
    SmWa_send_qs[i] = sq_createQueue();
    SmWa_score_qs[i] = sq_createQueue();
    cout << "Created queues " << i << " send is " << &SmWa_send_qs[i] << " inx is " << SmWa_send_qs[i]->c_inx << endl;
  }
}

// Routine that prints diagnostic information.  Requires the liberty queues to
// have INSTRUMENT_QUEUE_STALLS defined.
void printQueueInfo()
{
  cout << "\n";
  cout << "Queue Status------------------\n";
  for (int i=0; i<NUM_TABLETHREAD_PAIRS*2; i++) {
    cout << "  In->Table[" << i << "] ";
    sq_printQueueStalls(in_table_qs[i]);
    cout << "\n";
  }

  for (int i=0; i<NUM_TABLETHREAD_PAIRS*2; i++) {
    cout << "Table[" << i << "]->Ctrl ";
    sq_printQueueStalls(table_FPGAcontrol_qs[i]);
    cout << "\n";
  }
  for (int i=0; i<SW_NUM; ++i) {
    cout << "Ctrl[" << i << "]->GetSc ";
    sq_printQueueStalls(FPGAcontrol_GetScores_qs[i]);
    cout << "\n";
  }

  for (int i=0; i<SW_NUM; ++i) {
    cout << " SmWa[" << i << "]->FPGA ";
    sq_printQueueStalls(SmWa_send_qs[i]);
    cout << "\n";
  }

  for (int i=0; i<SW_NUM; ++i) {
    cout << " FPGA->SmWa[" << i << "] ";
    sq_printQueueStalls(SmWa_score_qs[i]);
    cout << "\n";
  }

  cout << "------------------------------\n";
}

