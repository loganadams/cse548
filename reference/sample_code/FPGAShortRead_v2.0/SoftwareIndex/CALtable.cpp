/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "common.h"
#include "CALtable.h"

using namespace std;

// The real CALtables, holding the data we want.
// oddCALtable is in memory on socket 1, and has the data relevant to oddPtrTable.
// evenCALtable is the same on socket 0.  The length of these tables are in evenCALtable_len and
// oddCALtable_len.
CALtableEntry *evenCALtable, *oddCALtable;

uint64_t evenCALtable_len=0, oddCALtable_len=0;

#ifdef CAL_TABLE_USE_HUGE_PAGES
#define CAL_TABLE_ALLOC(size) (CALtableEntry *)get_hugepage_region((size), GHR_STRICT | GHR_COLOR)
#else // CAL_TABLE_USE_HUGE_PAGES
#define CAL_TABLE_ALLOC(size) (CALtableEntry *)malloc((size))
#endif // CAL_TABLE_USE_HUGE_PAGES

// Initialize the CALtable data structures.  CALfile is the name of the file with the 
// CAL table.  The two masks are NUMA bitmasks used to make sure we allocate memories onto
// the appropriate socket's memory
//
// evenCALtable_len and oddCALtable_len must be set properly before these are set.
//
// The lines of the CALtable are <key> <CAL> <isRevComp>.  The key is portion of the hashed seed
// not used for ptrTable lookup (i.e. the portion we still need to match to find a seed's CALs in
// a given bucket).  The CAL is the address of the earliest base in the genome that matches that
// seed.  isRevComp is either 0 or 1.  If 1, the seed is actually from the revComp of the genome.
// HOWEVER, the CAL is still the earliest base in the forward direction.  So, a seed that matches
// at the revComp of the beginning of the genome has a CAL of 0.
void initCALtables(const char* CALfile, bitmask *oddMask, bitmask *evenMask)
{
  uint32_t key, CAL, isRevComp;
  uint32_t address = 0;

  if (evenCALtable_len == 0 || oddCALtable_len == 0) {
    cerr << "evenCALtable_len and oddCALtable_len must be set before calling initCALtables.\n";
    exit(EXIT_FAILURE);
  }

  // Allocate the table storage on the proper sockets
  numa_bind(oddMask);
  oddCALtable = CAL_TABLE_ALLOC(sizeof(CALtableEntry)*oddCALtable_len);

  numa_bind(evenMask);
  evenCALtable = CAL_TABLE_ALLOC(sizeof(CALtableEntry)*evenCALtable_len);
    
  if (oddCALtable == NULL || evenCALtable == NULL) {
    cerr << "Unable to allocate storage for the CAL table - are hugepages set up correctly?\n";
    exit(EXIT_FAILURE);
  }

  ifstream CALfile_stream (CALfile);
  if (!CALfile_stream){
    cout << "Unable to CALtable file " << CALfile << endl;
    exit(EXIT_FAILURE);
  }

  numa_bind(evenMask);
  for (address=0; address<evenCALtable_len; address++) {
    if (CALfile_stream.eof()) {
      cerr << "Ran out of entries in CALTable " << CALfile << " near entry " << address << endl;
      exit(EXIT_FAILURE);
    } 
    CALfile_stream >> key;
    CALfile_stream >> CAL;
    CALfile_stream >> isRevComp;
    if (isRevComp != 0 && isRevComp != 1) {
      cerr << "Malformed CAL table entry in CALTable " << CALfile << " near entry " << address << endl;
      exit(EXIT_FAILURE);
    }
    evenCALtable[address].key = key<<1 | isRevComp;
    evenCALtable[address].CAL = CAL;
  }

  numa_bind(oddMask);
  for (address=0; address<oddCALtable_len; address++) {
    if (CALfile_stream.eof()) {
      cerr << "Ran out of entries in CALTable " << CALfile 
	   << " near entry " << address+evenCALtable_len << endl;
      exit(EXIT_FAILURE);
    } 
    CALfile_stream >> key;
    CALfile_stream >> CAL;
    CALfile_stream >> isRevComp;
    if (isRevComp != 0 && isRevComp != 1) {
      cerr << "Malformed CAL table entry in CALTable " << CALfile
	   << " near entry " << address+evenCALtable_len << endl;
      exit(EXIT_FAILURE);
    }
    oddCALtable[address].key = key << 1 | isRevComp;
    oddCALtable[address].CAL = CAL;
  }

  uint32_t extra;
  if (CALfile_stream >> extra) {
    cerr << "*** Warnining: Extra entries in CALtable " << CALfile << "***\n";
  }

  CALfile_stream.close();
}

// Same as initCALtables, but the CALfile is assumed to be in compressed (binary)
// form, via compressCALtable
void initCALtables_bin(const char* CALfile, bitmask *oddMask, bitmask *evenMask)
{
  uint64_t key, CAL, isRevComp;
  uint64_t address = 0;

  if (evenCALtable_len == 0 || oddCALtable_len == 0) {
    cerr << "evenCALtable_len and oddCALtable_len must be set before calling initCALtables.\n";
    exit(EXIT_FAILURE);
  }

  // HACK ALERT: if you allocate tables with a numabind, and you allocate
  // too many to one processor, you can get BUS ERRORs.  We work around it
  // here by allocating the same size for each table.  Should research
  // this in future.
  uint64_t maxTable_len = evenCALtable_len;
  if (oddCALtable_len > evenCALtable_len) maxTable_len = oddCALtable_len;

  cout << "CALtable length - even: " << evenCALtable_len
       << " (" << fixed << setprecision(2) << (double)evenCALtable_len * sizeof(CALtableEntry)/(1024*1024*1024)
       << "GB), odd: " << oddCALtable_len
       << " (" << fixed << setprecision(2) << (double)oddCALtable_len * sizeof(CALtableEntry)/(1024*1024*1024)
       << "GB)\n" << flush;

  // Allocate the table storage on the proper sockets
  numa_bind(oddMask);
  // HACK ALERT: should be: oddCALtable = CAL_TABLE_ALLOC(sizeof(CALtableEntry)*oddCALtable_len);
  oddCALtable = CAL_TABLE_ALLOC(sizeof(CALtableEntry)*maxTable_len);

  numa_bind(evenMask);
  // HACK ALERT: should be: evenCALtable = CAL_TABLE_ALLOC(sizeof(CALtableEntry)*evenCALtable_len);
  evenCALtable = CAL_TABLE_ALLOC(sizeof(CALtableEntry)*maxTable_len);
  if (oddCALtable == NULL || evenCALtable == NULL) {
    cerr << "Unable to allocate storage for the CAL table - are hugepages set up correctly?\n";
    exit(EXIT_FAILURE);
  }
/*
  // If there are memory problems, can an aggressor memory access expose it?
  CALtableEntry *testTable = CAL_TABLE_ALLOC(sizeof(uint64_t)*1000);
  if (testTable == NULL) {
    cout << "\nBOGUS\n";
  }
  for (uint64_t x=0; x<evenCALtable_len; x++) {
    evenCALtable[x].key=1;
    evenCALtable[x].CAL=1;
  }
*/  

  ifstream CALfile_stream (CALfile);
  if (!CALfile_stream){
    cout << "Unable to open binary CALtable file " << CALfile << endl;
    exit(EXIT_FAILURE);
  }
  uint32_t readBuf[2];

  numa_bind(evenMask);
  for (address=0; address<evenCALtable_len; address++) {
    if (CALfile_stream.eof()) {
      cerr << "Ran out of entries in CALTable " << CALfile << " near entry " << address << endl;
      exit(EXIT_FAILURE);
    } 
    CALfile_stream.read((char *)readBuf, sizeof(uint32_t)*2);
    key = readBuf[0];
    CAL = readBuf[1];
    evenCALtable[address].key = key;
    evenCALtable[address].CAL = CAL;
  }
  
  numa_bind(oddMask);
  for (address=0; address<oddCALtable_len; address++) {
    if (CALfile_stream.eof()) {
      cerr << "Ran out of entries in CALTable " << CALfile 
	   << " near entry " << address+evenCALtable_len << endl;
      cerr << "Last entry - key: " << (key>>1) << " CAL: " << CAL << " revComp: " << (key & 1) << endl;
      exit(EXIT_FAILURE);
    } 
    CALfile_stream.read((char *)readBuf, sizeof(uint32_t)*2);
    key = readBuf[0];
    CAL = readBuf[1];
    oddCALtable[address].key = key;
    oddCALtable[address].CAL = CAL;
  }

  uint32_t extra;
  if (CALfile_stream >> extra) {
    cerr << "*** Warnining: Extra entries in CALtable " << CALfile << "***\n";
  }

  CALfile_stream.close();
}

void printCALtableEntry(CALtableEntry *table, uint64_t address)
{
  cout << "[" << getKey(&(table[address]))
       << ":" << table[address].CAL
       << ":" << getIsRevComp(&(table[address])) << "]\n";
}
