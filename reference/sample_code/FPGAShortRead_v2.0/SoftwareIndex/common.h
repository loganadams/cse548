/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef COMMON_H
#define COMMON_H 1

#include <stdint.h>
#include <string.h>

//#define SOFTWARE_CONFIRM

//Uncomment the following lines to write the query file for software testing.
//#define TESTBENCH_QUERY
//#define TESTBENCH_QUERY_FILE "query.txt"

// Pick only one of the follow definitions.  BFAST does overlapping 22base reads.
// MRFASTn does non-overlapping n-base seeds
//#define MRFAST12
//#define MRFAST14
//#define MRFAST16
//#define MRFAST18
#define MRFAST20
//#define MRFAST22
//#define MRFAST25
//#define MRFAST29
//#define BFAST
//Don't do more than one of MRFAST12..MRFAST25 or BFAST

#define SW_NUM 1

#define section "21"

#ifdef MRFAST12
#define addressBases 12
#define keyBases 0
#define NOOVERLAP
#elif defined MRFAST14
#define addressBases 14
#define keyBases 0
#define NOOVERLAP
#elif defined MRFAST16
#define addressBases 14
#define keyBases 2
#define NOOVERLAP
#elif defined MRFAST18
#define addressBases 14
#define keyBases 4
#define NOOVERLAP
#elif defined MRFAST20
#define addressBases 14
#define keyBases 6
#define NOOVERLAP
#elif defined MRFAST22
#define addressBases 14
#define keyBases 8
#define NOOVERLAP
#elif defined MRFAST25
#define addressBases 14
#define keyBases 11
#define NOOVERLAP
#elif defined MRFAST29
#define addressBases 14
#define keyBases 15
#define NOOVERLAP
#elif defined BFAST
#define addressBases 14
#define keyBases 8
#else // No definition
#error Must define exactly one of BFAST, MRFAST12 .. MRFAST25
#endif

// All MRFASTn set MRFAST, which does formatting and other things like MRFAST
#if defined MRFAST12 || defined MRFAST14 || defined MRFAST16 || defined MRFAST18 || defined MRFAST20 || defined MRFAST22 || defined MRFAST25 || defined MRFAST29
#define MRFAST
#endif

extern int seedLength;
extern int readLength;

#define addressBits (2*addressBases)
#define keyBits (2*keyBases)

// Number of entries in the ptrTable
#define numPtrTableEntries (1<<(2*addressBases))

// The files we access.
extern const char *fastqFilename;
extern const char *ptrTableFilename;
extern const char *CALtableFilename;
extern const char *refFilename;
extern const char *tableDirectory;

// If true, we assume compressed read files instead of normal FASTQ
//#define USE_COMPRESSED_FASTQ

// This must be a multiple of 64
#define MAX_FASTQ_LINE_LEN 1024
#define MAX_FASTQ_LINE_PACKED_LEN ((CHAR_BIT*MAX_FASTQ_LINE_LEN+63)/64)

// The number of pairs of tableThreads we use, must be multiple of number of FPGAS. (1 pair per FPGA default)
#define NUM_TABLETHREAD_PAIRS (SW_NUM * 1)

// FILTER_LENGTH_BITS controls the size of the CALfilter tables.
// Larger tables have fewer false-negatives, but consumes more space.
// Ideally, should be as large as possible while still fitting in the L1 with all other local data.
#define FILTER_LENGTH_BITS 10

// System Parameters
#define indelAllowance 10       // defines number of bases that we allow for insertions or deletions while comparing the read to the reference (total is 2x this value)

#define refBucketSize 192       // sets the size of the reference bucket that we will compare against while performing SW alignment
#define filterTableEntries 1024 // number of entries in the CAL filter table
#define keysPerMem 4            // number of key,CAL pairs that fit in one aligned entry of CAL table (256 bit aligned)
#define CALFIFOentries 64       // number of entries the CALFIFO can hold

// define the constant PTR_TABLE_USE_HUGE_PAGES if you want the ptrTable to make use of hugepages.
// This removes much of the overhead of virtual memory and TLB misses.
#define PTR_TABLE_USE_HUGE_PAGES
// Define the constant CAL_TABLE_USE_HUGE_PAGES if you want the CALtable to make use of hugepages.
// This removes much of the overhead of virtual memory and TLB misses.
#define CAL_TABLE_USE_HUGE_PAGES
// Define the constant REF_USE_HUGE_PAGES if you want the reference to make use of hugepages.
// This removes much of the overhead of virtual memory and TLB misses.
// Note: preliminary testing says to NOT use hugepages for the reference.
//#define REF_USE_HUGE_PAGES

const char *combineStrings(const char *c1, const char *c2);

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

#endif
