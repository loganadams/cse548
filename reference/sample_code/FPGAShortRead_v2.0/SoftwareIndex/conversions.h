/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CONVERSIONS_H
#define CONVERSIONS_H 1

// Routines that convert some values to others, particularly unencoded to encoded
// DNA strings and the like.
//
// Many conversions use lookup tables, that need to be initialized.  Thus, you MUST
// call initConversions() at the beginning of your program, before using any other of
// the conversions in this file.

// Initialize the various lookup tables.
void initConversions();

// Arrays used for fast character conversion lookups.  Set up by initConversions()
extern int c2b[256]; // What base corresponds to a given character.  b=255 means ERROR
extern char b2c[4];  // What character corresponds to a given base.
extern char cleanChar[256]; // Upper-case character corresponding to given base. N->A
extern char rb[256]; // Given a base char, returns revComp of it.  keeps case. N->N.

// Encodes a DNA char {AaGgCcTtNn} as a 2-bit number {0..3} for compact storage. 255
// means an invalid DNA character.
inline int CharToBase(char val)
{
  return c2b[val];
}

// Inverse of CharToBase.  Only returns capital letters.  Only the bottom 2 bits of base
// are used.
inline char BaseToChar(int base)
{
  int index = base & 3;
  return b2c[index];
}

// Returns a standardized upper-case character for every DNA character.  acgt->upper case,
// N->A. '-' designates an invalid character.
inline char cleanedChar(char src)
{
  return cleanChar[src];
}

// Given a base char, returns revComp of it.  Lowercase stays lowercase, uppercase
// stays uppercase.  N's stay as N's.  '-' designates an invalid character.
inline char revBase(char val)
{
  return rb[val];
}

#endif // CONVERSIONS_H
