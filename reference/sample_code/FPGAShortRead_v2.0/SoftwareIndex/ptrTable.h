/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef PTRTABLE_H
#define PTRTABLE_H 1

#include <numa.h>
#include "common.h"

// June 12, 2012 - pointer tables have gotten a major overhaul.  Instead of a compressed format
// that saves space, but limits the maximum number of CALs per seed, we are going to a 64-bit,
// flat structure.  In general the CAL tables are 40x bigger than the pointer table, so the change
// shouldn't matter much.
//
// A ptrTable is half of the hashing structure for the index of a genome.  The CALTable holds a 
// compressed dataset that lists CALs (positions in the genome) for each seed.  To find the portion
// of the dataset in the CALTable that corresponds to a specific seed, we use the ptrTable.  The
// prtTable supports an array that relates a subset of a seed to a set of locations
// in the CALTable.  Then, once the CALTable is accessed, the rest of the bits can be checked.
//
// Each entry in the ptrTable points to the first location in the CAL table that may match
// the given seed.  Thus, the given seed may match CAL locations from the point given in the
// array up to, but not including, the next entry in the array.  If consecutive locations have
// the same value, it means that seed is not found in the CAL table.
typedef uint64_t ptrTableEntry;

void printPtrTableEntry(ptrTableEntry *table, uint64_t address);

// The real ptrTables, holding the data we want
// oddTable is in memory on socket 1, evenTable on socket 0.  There are numPtrTableEntries/2 entries
// per table (defined in common.h).  evenPtrTable has entries for seeds with the top bit of 0,
// oddPtrTable has entries for seeds with the top bit of 1.
extern ptrTableEntry *oddPtrTable, *evenPtrTable;

// Initialized the ptrTable data structures.  ptrFile is the name of the file with the 
// pointer table.  The two masks are NUMA bitmasks used to make sure we allocate memories onto
// the appropriate socket's memory
//
// This also initializes the lengths of the CALtables.
void initPtrTables(const char* ptrFile, bitmask *oddMask, bitmask *evenMask);

// Function to look up a seed's values in the pointer table.  It is assumed that the right
// table (oddPtrTable, evenPtrTable) has already been picked for the seed and sent in.
// Table is the table we are indexing into.
// Seed is the seed we are looking up.  It should already have been hashed.
// startPointer is a pointer to the start of the bucket of CALs for this seed in the CALtable,
// endPointer is a pointer to the start of the next bucket.
// key is the key to use to look up values in that CAL table.
void seedToPointers(ptrTableEntry *Table, const uint64_t hashedSeed,
		    uint64_t &startPointer, uint64_t &endPointer, uint32_t &key);

void printSeed(uint64_t value, int padding);

#endif
