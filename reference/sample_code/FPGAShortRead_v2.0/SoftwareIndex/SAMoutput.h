/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SAMOUTPUT_H
#define SAMOUTPUT_H

#include <string>
#include "fpga/chrom.h"
#include "readDescr.h"

void outputSAMline(ofstream &outStream, readDescr *read,
                    uint64_t CAL, chrom *chromData,
                    int32_t score, bool rev, bool unmapped,
		    const char *cigarStr, int editDist,
                    const char *mdStr, const char *readGroup);
void setRefInput(const char *filename);
void outputSAMHeader(ofstream &outStream, const char *refFilename, string &readGroup);

#endif // SAMOUTPUT_H
