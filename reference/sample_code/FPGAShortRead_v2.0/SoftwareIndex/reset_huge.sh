#!/bin/bash
# Script to i
# Initialize the huge pages setup.  Must be run as root.  This takes a long time to run.
# be patient.

hugeadm --pool-pages-min DEFAULT:0G
hugeadm --pool-pages-max DEFAULT:0G
hugeadm --pool-list
