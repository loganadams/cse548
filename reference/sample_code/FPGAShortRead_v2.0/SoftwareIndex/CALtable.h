/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CALTABLE_H
#define CALTABLE_H 1

#include <numa.h>
#include "common.h"

extern "C" {
  #include <hugetlbfs.h>
}

// The CAL table is a compressed set of locations in genome, indexed by seeds into the ptrTable.  The
// ptrTable specifies buckets in the CAL table.  The buckets contain CALs for many different
// seeds.  To disambiguate the CALs, they are labelled with keys (which are the parts of seeds not
// used to find a value in the ptrTable.

// CAL table is constructed as an array of key-CAL pairs
typedef struct {
  uint32_t key; // key associated with a CAL, along with a bit saying if the value comes from the
                // reverse-complement of the genome.  The isRevComp is bit0, key is bits1+.
  uint32_t CAL; // CAL is a location in the reference genome
} CALtableEntry;

inline uint32_t getKey(CALtableEntry *entry) { return (entry->key >> 1); }
inline uint32_t getIsRevComp(CALtableEntry *entry) { return (entry->key & 0x1); }
inline uint32_t getCAL(CALtableEntry *entry) { return entry->CAL; }

// The real CALtables, holding the data we want.
// oddCALtable is in memory on socket 1, and has the data relevant to oddPtrTable.
// evenCALtable is the same on socket 0.  The length of these tables are in evenCALtable_len and
// oddCALtable_len.
extern CALtableEntry *evenCALtable, *oddCALtable;

extern uint64_t evenCALtable_len, oddCALtable_len;

// Initialize the CALtable data structures.  CALfile is the name of the file with the 
// CAL table.  The two masks are NUMA bitmasks used to make sure we allocate memories onto
// the appropriate socket's memory.
//
// evenCALtable_len and oddCALtable_len must be set properly before these are set.
void initCALtables(const char* CALfile, bitmask *oddMask, bitmask *evenMask);
void initCALtables_bin(const char* CALfile, bitmask *oddMask, bitmask *evenMask);

void printCALtableEntry(CALtableEntry *table, uint64_t address);

#endif
