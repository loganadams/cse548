/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SCOREDSET_H_
#define SCOREDSET_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <iostream>
#include <vector>

#include "scoredCAL.h"

using namespace std;

/*
 * scoredSet provides a way to organize incoming scoredCAL data.
 * The scoredCAL has results from the FPGA aligner on the quality of a
 * proposed alignment.  ScoredSet takes groups of these scoredCALs for a given
 * read, and provides ways to select sets of the resulting values.  This
 * includes finding the best scoring alignment, all alignments above a given
 * cutoff, etc.
 *
 * Each style of selection has it's own subclass.  Use the subclass that
 * matches the behavior you want.
 *
 * Those versions that hold multiple values will output them in score order, from
 * highest score to lowest.
 *
 * For subclasses that hold multiple values, they do NOT ensure the elements
 * contained are unique.  Since uniqueness is generally determined by the FIRST
 * base matching the reference, and finding that first base can be
 * time-consuming (for versions with indels), we postpone that to later
 * processing - filter these results first, then pay the cost of making
 * them unique.
 *
 * Any scoredCALs left in the set at a clear, or otherwise disposed of, will
 * be "delete"-edd.
 */

class scoredSet {
 protected:
  // Bounds on the scores this set will store.  Values under minScore are
  // deleted, while values above maxScore are an error.
  int32_t _minScore;
  int32_t _maxScore;

  // Number of elements currently in the set
  uint32_t count; 

 public:
  scoredSet(int32_t minScore, int32_t maxScore);
  ~scoredSet();

  virtual void push(scoredCAL *val)=0; // Adds the given entry to the structure.
  virtual scoredCAL *pop()=0; // Remove entry with best score.
  virtual scoredCAL *top()=0; // Access entry with best score without removing it.

  // True if there are no elements.
  bool empty() { return (count==0); };

  virtual void clear()=0;  // Empties the structure, deleting current contents.
};

/*
 * Keeps only the single best scored entry.  If there are multiple best 
 * entries, an arbitrary one is chosen.
 */
class scoredSet_uniqueBest : public scoredSet {
 private:
  scoredCAL *best; // Current best value

 public:
  scoredSet_uniqueBest(int32_t minScore, int32_t maxScore);
  ~scoredSet_uniqueBest();

  void push(scoredCAL *val); // Adds the given entry to the structure.
  scoredCAL *pop(); // Remove entry with best score.
  scoredCAL *top(); // Access entry with best score without removing it.

  void clear();  // Empties the structure, deleting the current contents.
};

/*
 * Keeps the best scored entries.
 * If there are multiple best, all are retained. 
 */
class scoredSet_allBest : public scoredSet {
 private:
  // All contents.
  vector<scoredCAL *> contents;
  int32_t bestScore; // Current best score of any elements in set.

 public:
  scoredSet_allBest(int32_t minScore, int32_t maxScore);
  ~scoredSet_allBest();

  void push(scoredCAL *val); // Adds the given entry to the structure.
  scoredCAL *pop(); // Remove entry with best score.
  scoredCAL *top(); // Access entry with best score without removing it.

  void clear();  // Empties the structure, deleting the current contents.
};

/*
 * Keeps all values, but returns them from highest to lowest quality.
 */
class scoredSet_all : public scoredSet {
 private:
  // All contents, indexed by (score-_minScore).
  vector<scoredCAL *> *contents;
  int32_t bestScore; // >= the best score of any element in set.
 public:
  scoredSet_all(int32_t minScore, int32_t maxScore);
  ~scoredSet_all();

  void push(scoredCAL *val); // Adds the given entry to the structure.
  scoredCAL *pop(); // Remove entry with best score.
  scoredCAL *top(); // Access entry with best score without removing it.

  void clear();  // Empties the structure, deleting the current contents.
};

#endif /* SCOREDSET_H_ */
