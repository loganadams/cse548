/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// perf_timer.h
// Performance structures and operations
// Used as a timer, based on monotonic clock
// Nathaniel McVicar: nmcvicar@uw.edu

#ifndef PERF_TIMER_H_
#define PERF_TIMER_H_

#include <iostream>
#include <time.h>

typedef struct perf_timer_struct {
  struct timespec tStart, tStop;
  unsigned long long tTime, tOps, tTotalOps, tSamples;
//  bool tStarted;
} perf_timer;

void ptInit (perf_timer *pt, unsigned long long ops) {
  pt->tTime = pt->tOps = pt->tSamples = 0;
  pt->tTotalOps = ops;
//  pt->tStarted = false;
}

void ptStart (perf_timer *pt) {
//  if (pt->tStarted) cout << "TIMER ALREADY STARTED!" << endl;
  clock_gettime(CLOCK_MONOTONIC, &(pt->tStart));
  pt->tSamples++;
//  pt->tStarted = true;
}

void ptStop (perf_timer *pt) {
//  if (! pt->tStarted) cout << "TIMER NOT STARTED!" << endl;
  clock_gettime(CLOCK_MONOTONIC, &(pt->tStop));
  pt->tTime += (pt->tStop.tv_sec * 1000000000 + pt->tStop.tv_nsec) - (pt->tStart.tv_sec * 1000000000 + pt->tStart.tv_nsec);
//  pt->tStarted = false;
}

void ptOp (perf_timer *pt, const char* name) {
  ptStop(pt);
  pt->tOps++;
  if (pt->tOps >= pt->tTotalOps) {
    cout << "Thread " << name << " " << pt->tOps << " ops took " << pt->tTime / 1000000000.0 << "s, ops/s " << pt->tOps / (pt->tTime / 1000000000.0) << " starts " << pt->tSamples << endl;
    pt->tTime = pt->tOps = pt->tSamples = 0;
  }
}

void ptStartStopTest () {
  cout << "Running 1,000,000 start stop test" << endl;
  perf_timer tester, testee;
  ptInit(&tester, 1);
  ptInit(&testee, 10);

  ptStart(&tester);
  for (unsigned int i = 0; i < 1000000; i++) {
    ptStart(&testee);
    ptStop(&testee);
  }
  ptOp(&tester, "start stop test");

  cout << "Start stop test complete" << endl;
}

#endif // PERF_TIMER_H_
