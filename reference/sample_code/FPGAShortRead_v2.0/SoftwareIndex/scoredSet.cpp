/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "common.h"
#include "scoredSet.h"
#include <iostream>

using namespace std;

/* minScore: The lowest score that is acceptable.
 *           Entries below this are filtered out.
 * maxScore: The highest score that could occur.  It is an error to push
 *           a value above this.
 */
scoredSet::scoredSet(int32_t minScore, int32_t maxScore) {
  if (minScore > maxScore) {
    cerr << "** Error: min allowed score is above max possible score. **\n";
    exit(EXIT_FAILURE);
  }
  _minScore = minScore;
  _maxScore = maxScore;
  count = 0;
}

scoredSet::~scoredSet() {
}

/******************************* uniqueBest routines **************************/

scoredSet_uniqueBest::scoredSet_uniqueBest(int32_t minScore, int32_t maxScore)
  : scoredSet(minScore, maxScore)
{
  best = NULL;
}

scoredSet_uniqueBest::~scoredSet_uniqueBest()
{
}

// Adds the given entry to the structure.
void scoredSet_uniqueBest::push(scoredCAL *val)
{
  if (val->getScore() < _minScore) {
    delete val;
  } else if (best == NULL || best->getScore() < val->getScore()) {
    if (best != NULL) delete best;
    best = val; count = 1;
  } else {
    delete val;
  }
}

// Remove entry with best score.
scoredCAL *scoredSet_uniqueBest::pop()
{
  if (count == 0) {
    cerr << "** Error: pop on an empty scoredSet **\n";
    exit(EXIT_FAILURE);
  }
  count = 0;
  scoredCAL *result = best;
  best = NULL;
  return result;
}

// Access entry with best score without removing it.
scoredCAL *scoredSet_uniqueBest::top()
{
  if (count == 0) {
    cerr << "** Error: top on an empty scoredSet **\n";
    exit(EXIT_FAILURE);
  }
  return best;
}

// Empties the structure, deleting the current contents.
void scoredSet_uniqueBest::clear()
{
  if (best != NULL)
    delete best;
  best = NULL;
  count = 0;
}

/******************************* allBest routines **************************/

scoredSet_allBest::scoredSet_allBest(int32_t minScore, int32_t maxScore)
  : scoredSet(minScore, maxScore)
{
  bestScore = minScore;
}

scoredSet_allBest::~scoredSet_allBest()
{
  clear();
}

// Adds the given entry to the structure.
void scoredSet_allBest::push(scoredCAL *val)
{
  //cout << "Push - score: " << val->getScore() << " bestscore: " << bestScore;
  if (val->getScore() < bestScore) {
    delete val;
  } else  if (val->getScore() == bestScore) {
    contents.push_back(val);
    count++;
  } else { // New best: val->getScore() > bestScore
    for (; count > 0; count--) { 
      delete contents.back();
      contents.pop_back();
    }
    count = 1;
    bestScore = val->getScore();
    contents.push_back(val);
  }

  //cout << " new count: " << count << "\n";
}

// Remove entry with best score.
scoredCAL *scoredSet_allBest::pop()
{
  //cout << "Pop - count: " << count << "\n";
  if (count == 0) {
    cerr << "** Error: pop on an empty scoredSet **\n";
    exit(EXIT_FAILURE);
  }
  count--;
  scoredCAL *result = contents.back();
  contents.pop_back();
  return result;
}

// Access entry with best score without removing it.
scoredCAL *scoredSet_allBest::top()
{
  if (count == 0) {
    cerr << "** Error: pop on an empty scoredSet **\n";
    exit(EXIT_FAILURE);
  }
  return contents.back();
}

// Empties the structure, deleting the current contents.
void scoredSet_allBest::clear()
{
  //cout << "Clear\n";
  for (; count > 0; count--) {
    delete contents.back();
    contents.pop_back();
  }
  bestScore = _minScore;
  count = 0;
}

/******************************* _all routines **************************/

scoredSet_all::scoredSet_all(int32_t minScore, int32_t maxScore)
  : scoredSet(minScore, maxScore)
{
  int delta = maxScore - minScore + 1;
  contents = new vector<scoredCAL *>[delta];
  bestScore = minScore;
}

scoredSet_all::~scoredSet_all()
{
  clear();
  delete[] contents;
}

// Adds the given entry to the structure.
void scoredSet_all::push(scoredCAL *val)
{
//  cout << "Pushing read from " << val->getReadID() << " with score " << val->getScore()<<endl;
  if (val->getScore() > _maxScore) {
    cerr << "** Pushing item with score " << val->getScore()
	 << " into scoredSet with maxScore " << _maxScore
	 << " **\n";
    exit(EXIT_FAILURE);
  }
  if (val->getScore() < _minScore) {
    delete val;
  } else { // Add it
    int pos = val->getScore() - _minScore;
    contents[pos].push_back(val);
    count++;
    bestScore = MAX(bestScore, val->getScore());
  }
}

// Remove entry with best score.
scoredCAL *scoredSet_all::pop()
{
  if (count == 0) {
    cerr << "** Error: pop on an empty scoredSet **\n";
    exit(EXIT_FAILURE);
  }
  while(contents[bestScore - _minScore].empty())
    bestScore--;
  count--;
  scoredCAL *result = contents[bestScore - _minScore].back();
  contents[bestScore - _minScore].pop_back();
  return result;
}

// Access entry with best score without removing it.
scoredCAL *scoredSet_all::top()
{
  if (count == 0) {
    cerr << "** Error: top on an empty scoredSet **\n";
    exit(EXIT_FAILURE);
  }
  while(contents[bestScore - _minScore].empty())
    bestScore--;
  return contents[bestScore - _minScore].back();
}

// Empties the structure, deleting the current contents.
void scoredSet_all::clear()
{
  for (int t = bestScore - _minScore; t >= 0; t--) {
    while(!contents[t].empty()) {
      delete contents[t].back();
      contents[t].pop_back();
    }
  }
  count=0;
  bestScore = _minScore;
}
