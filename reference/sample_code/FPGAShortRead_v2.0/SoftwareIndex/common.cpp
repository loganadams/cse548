/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>

#include "common.h"

using namespace std;

/////////////
// GLOBALS //
/////////////
FILE *tbDataFile;

/////////////
// METHODS //
/////////////

// *** Pick the reads file to use ***

#ifndef  USE_COMPRESSED_FASTQ
// Large set of reads.
const char *fastqFilename = "/usr/share/shortread/data/nickerson/reads1.fastq";
#else  //USE_COMPRESSED_FASTQ
// Large set of reads, in compressed format
const char *fastqFilename = "reads1.fastq.cmp";
#endif //USE_COMPRESSED_FASTQ

// Very small set of sample reads
//const char *fastqFilename = "TEST_READS.fastq";

// *** Pick the reference to use ***

// Full genome, with 54,448,967 reads
//const char *ptrTableFilename = "pointer_table_full_44_16.txt";
//const char *CALtableFilename = "cal_table_full_44_16.txt";
//const char *refFilename      = "/usr/share/shortread/data/nickerson/human_genome.fasta";

//chr21 test case - I think all the MRFAST data is bogus.
#if defined MRFAST 
const char *ptrTableFilename = "pointer_table_chr21_44_16.txt";
const char *CALtableFilename = "cal_table_chr21_44_16.txt";
const char *refFilename      = "chr21.fasta";
#elif defined BFAST
const char *ptrTableFilename = "pointer_table_chr21_24_0.txt";
const char *CALtableFilename = "cal_table_chr21_24_0.txt";
const char *refFilename      = "chr21.fasta";
#else
#error invalid case
#endif

// If tableDirectory is set, we get CAL, PTR, and reference genome from this
// directory.
const char *tableDirectory = NULL;

// Default length values.
#ifdef MRFAST12
int seedLength = 12;
int readLength = 100;
#elif defined MRFAST14
int seedLength = 14;
int readLength = 100;
#elif defined MRFAST16
int seedLength = 16;
int readLength = 100;
#elif defined MRFAST18
int seedLength = 18;
int readLength = 100;
#elif defined MRFAST20
int seedLength = 20;
int readLength = 100;
#elif defined MRFAST22
int seedLength = 22;
int readLength = 100;
#elif defined MRFAST25
int seedLength = 25;
int readLength = 100;
#elif defined MRFAST29
int seedLength = 29;
int readLength = 100;
#elif defined BFAST
int seedLength = 22;           // must be 32 bases or less
int readLength = 76;
#else
#error invalid case
#endif //BFAST

// Takes two strings, and returns a new string that is equal to c1 followed by c2.
const char *combineStrings(const char *c1, const char *c2)
{
  int c1_len = strlen(c1);
  int c2_len = strlen(c2);
  
  char *result = (char *)calloc(c1_len+c2_len+1, sizeof(char));
  strcpy(result, c1);
  strcat(result, c2);

  return result;
}
