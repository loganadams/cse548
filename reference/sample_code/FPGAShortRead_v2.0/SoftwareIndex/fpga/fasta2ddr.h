/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdint.h>
#include <stdio.h>

#include "Reference.h"
#include "Writer.h"

#include "SmithWaterman.h"

#include "chrom.h"

#include <picodrv.h>
#include <pico_errors.h>

//For debug
//#define FPGA_PRINT

//Methods
void binaryBasesGen(const char* referenceFileName,
		    const char *binFileName,
		    const char *chrFileName);

// Initialize the memories on the FPGA board.
// OUTPUTS:
// full_ref: pointer to a copy of the reference in host memory.
// full_ref_len: length of full_ref (i.e. full_ref[0]..full_ref[full_ref_len-1])
// chromData: pointer to hold the chromosome data from the reference.
void swFasta2ddr(SmithWaterman **sws, int swNum, 
		 const char* referenceFileName, uint64_t *&full_ref,
		 uint32_t &full_ref_len, chrom &chromData);

// Verifies that the memory has the same data as the binary reference file.
// sws - the objects corresponding to the smith-waterman engines (i.e. the FPGAs)
// swNum - the number of FPGAs.
// filename - name of the binary file holding the reference.
// full_ref - the internal version of the reference file, for error messages
void swVerifyDDR(SmithWaterman** sws, int swNum, const char* filename, uint64_t *full_ref);
