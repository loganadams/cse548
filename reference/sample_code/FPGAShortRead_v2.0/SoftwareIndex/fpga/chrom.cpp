/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * chrom.cpp
 *
 */

#include "chrom.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <assert.h>

using namespace std;

chrom::chrom() {
  chromNum=0;
  referenceLength=0;
  chromName=NULL;
  chromCAL=NULL;
}

chrom::chrom(uint64_t ref_len, int num_chromosomes,
	 vector<char *> &names, vector<uint64_t> &cals)
{
  assert(names.size() >= num_chromosomes);
  assert(cals.size() >= num_chromosomes);

  referenceLength = ref_len;
  chromNum = num_chromosomes;
  chromName = new char *[ref_len];
  chromCAL = new uint64_t[ref_len];
  for (int i=0; i<chromNum; i++) {
    chromName[i] = names[i];
    chromCAL[i] = cals[i];
    assert(cals[i]<ref_len);
  }
}

chrom::~chrom() {
  if (chromName != NULL)
    delete[] chromName;
  if (chromCAL != NULL)
    delete[] chromCAL;
}

// Initialize the chrom based upon the contents of the chrom file.
void chrom::read(const char *filename)
{
  FILE *chromFile = fopen(filename, "r");
  bool failed = false;

  if (chromFile == NULL) {
    cerr << "*** Missing chrom file " << filename << " ***\n";
    exit(EXIT_FAILURE);
  }
  if (fscanf(chromFile, "Reference length: %" PRIu64 "\n", &referenceLength)
      == EOF) failed = true;
  if (fscanf(chromFile, "# chromosomes: %d\n", &chromNum)
      == EOF) failed = true;

  // Make sure these are empty before making new ones
  if (chromName != NULL) {
    delete[] chromName;
    chromName = NULL;
  }
  if (chromCAL != NULL) {
    delete[] chromCAL;
    chromCAL = NULL;
  }
  chromName = new char *[chromNum];
  chromCAL = new uint64_t[chromNum];
  for (int i=0; i<chromNum && !failed; i++) {
    char buffer[500];
    if (fscanf(chromFile, "%s %" PRIu64 "\n", buffer, &chromCAL[i])
	== EOF) failed = true;
    chromName[i]=strdup(buffer);
  }
  //print();
  if (failed) {
    cerr << "*** Invalid .chr file " << filename << " ***\n";
    exit(EXIT_FAILURE);
  }
  fclose(chromFile);
}

// Write the contents of the object to the give filename
void chrom::write(const char *filename)
{
  FILE *chromFile = fopen(filename, "w");
  if (chromFile == NULL) {
    cerr << "*** Invalid chrom filename " << filename << " ***\n";
    exit(EXIT_FAILURE);
  }
  fprintf(chromFile, "Reference length: %" PRIu64 "\n", referenceLength);
  fprintf(chromFile, "# chromosomes: %d\n", chromNum);
  for (int i=0; i<chromNum; i++) {
    fprintf(chromFile, "%s %" PRIu64 "\n", chromName[i], chromCAL[i]);
  }
  fclose(chromFile);
}

// Print the contents of the object, for debugging
void chrom::print()
{
  printf("Reference length: %" PRIu64 "\n", referenceLength);
  printf("# chromosomes: %d\n", chromNum);
  for (int i=0; i<chromNum; i++) {
    printf("%s %" PRIu64 "\n", chromName[i], chromCAL[i]);
  }
}

// Given a CAL position, find the index of the chromosome containing that
// CAL.  The index can then be used for firstCAL and name functions.
int chrom::find(uint64_t cal)
{
  //fprintf(stderr, "cal = %ld, ref = %ld\n", cal, referenceLength);
  assert(cal>=0 && cal<referenceLength);
  
  // Binary search
  int minIndex = 0, maxIndex = chromNum;

  for (int i = (minIndex+maxIndex)/2;
       minIndex+1 != maxIndex;
       i = (minIndex+maxIndex)/2) {
    uint64_t iCAL = chromCAL[i];
    if (iCAL < cal) minIndex = i;
    else maxIndex = i;
  }
  
  return minIndex;
}
