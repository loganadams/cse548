/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Reference.cpp
 *
 *  Created on: Oct 21, 2011
 *      Author: Corey
 */

#include "Reference.h"
#include "../conversions.h"

#include <iostream>
#include <fstream>
#include "chrom.h"
#include <vector>
#include <string.h>


using namespace std;

// open the fasta file at the beginning
// set the number of chars in the current seed to 0
Reference::Reference(Writer* writer) {

    refLength = 0;

    // set the pointer to the writer
    scribe = writer;
}

// close the fasta reference file
Reference::~Reference() {
    if (refLength > 0){
        delete [] refData;
    }
}

// return the next base from the refData buffer
// if next base is '>', then this is the start of a new chromosome, and we should skip to the next line
// if next base is '\n', grab another base
// if at the end of the file, this returns 'EOF' (for End Of File)
char Reference::getNextBase(){

    currentCAL++;
    return refData[currentCAL-1];
}

// opens the reference file for parsing along with the ref info
//  file to tell how many valid seeds are in this reference
void Reference::openRefFile(const char* refFileName) {

    // open the reference file and check that it opened properly
    referenceFile = fopen(refFileName, "r");
    if (referenceFile == NULL){
      cerr << "Error opening reference file \"" << refFileName << "\"\n";
      exit(1);
    }

    // set the CAL (base of reference) and file pointer to 0
    currentCAL = 0;
}

// close the reference file and the ref info file
// closes the currently open reference file
void Reference::closeRefFile() {
    fclose(referenceFile);
}

// loads the reference file into a buffer
// If chromFileName is non-NULL, it is the name of a file to save chromosome
// positions and other support information about the file.
void Reference::loadReference(const char* refFileName, const char *chromFileName){
    long refSize;
    uint32_t currentCAL=0;
    int basesRead;
    int err;
    int number;

    // Used to keep track of the chromosome names & start positions
    vector<char *> chromName;
    vector<uint64_t> chromCAL;
    int chromNum=0;

    // open the reference file
    openRefFile(refFileName);

    // get the size of the reference file
    err = fseek(referenceFile , 0, SEEK_END);
    if (err != 0){
        fprintf(stderr, "Non-zero return value from fseek: %i\n", err);
        exit(1);
    }
    refSize = ftell(referenceFile);

    // create the reference buffer
    if (refSize >= 0){
        refData = new char[refSize+1];
    }else{
        perror ("The following error occurred");
        exit(1);
    }

    // reset the file pointer to the beginning
    rewind(referenceFile);

    // copy the file into the buffer
    // 1) read another line from the file
    // 2) discard newlines
    // 3) basesRead will tell how many bases were actually read
    while((basesRead = fscanf(referenceFile, "%[^\r\n]%n%*[\r\n]", &refData[currentCAL], &number)) != EOF){
        
        // don't increment the CAL number if we just read in a new chromosome header
        if(strstr(&refData[currentCAL], ">") == NULL){
            currentCAL += number;
        } else {
	  // Chromosome start - save name & position
	  //printf("%s @ %u\n", &(refData[currentCAL+1]), currentCAL);
	  chromName.push_back(strdup(&(refData[currentCAL+1])));
	  chromCAL.push_back(currentCAL);
	  chromNum++;
	}
    }

    // place an end of file marker in the last entry
    refData[currentCAL] = EOF;

    // set the total number of bases in the reference
    refLength = currentCAL;

    // close the reference file
    closeRefFile();

    if (chromFileName != NULL) {
      chrom c(currentCAL, chromNum, chromName, chromCAL);
      c.write(chromFileName);
    }
}

// print the contents of the reference buffer to the screen
void Reference::print(){
    printf("Ref Data = \n");
    uint32_t i = 0;
    uint32_t j;

    // print until the end of file marker is reached
    while(i < refLength){

        // print 50 bases per line
        for(j=i; j<i+50 && j<refLength; j++){
            printf("%c", refData[j]);
        }
        printf("\n");

        i = j;
    }
    printf("\n");
}

// writes the reference data in binary format to the index file via the scribe
// Note: this writes the bases of the reference in little-endian notation
void Reference::writeToFile(){

    uint32_t    currentCAL = 0;
    char        base;
    uint64_t    writeData = 0;
    int         baseIndex = 0;

    // reset reference file pointer to the beginning
    currentCAL = 0;

    // fill in bases into the write data (index 0 to 31)
    while(currentCAL < refLength){
        
        // grab the next base
        base = getNextBase();

        // add the base to the write data
        writeData += ((uint64_t) CharToBase(base)) << (2*baseIndex);

        // move on to the next base
        baseIndex++;
        currentCAL++;

        // write the data if this write data is populated
        if (baseIndex*2 >= 64){
            scribe->writeHalfLine(writeData);
            baseIndex = 0;
            writeData = 0;
        }
    }

    // print the leftover bases in the buffer to the file
    if (baseIndex > 0){
        scribe->writeHalfLine(writeData);
        writeData = 0;
    }

    // ensure that we have printed aligned to 256 bits
    while ((scribe->getBytesWritten() % 32) != 0){
        scribe->writeHalfLine(writeData);
    }
}
