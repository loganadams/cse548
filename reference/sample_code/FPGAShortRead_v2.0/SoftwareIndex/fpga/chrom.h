/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * chrom.h
 *
 * Holds information on which CALs are part of which chromosome or other contig
 *
 */

#ifndef CHROM_H_
#define CHROM_H_

#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define __STDC_FORMAT_MACROS 
#include <inttypes.h>

using namespace std;

class chrom {
 private:
  int chromNum; // # of chromosomes in reference
  uint64_t referenceLength; // # of bases in the reference

  // Contents are in two arrays, in order chromosomes appear in reference.
  char **chromName; // Name of chromosome
  uint64_t *chromCAL; // First CAL of chromosome.

 public:
  // constructor and destructor
  chrom(); // empty chrom
  ~chrom();

  // Construct & initialize the chrom
  chrom(uint64_t ref_len, int num_chromosomes, 
      vector<char *> &names, vector<uint64_t> &cals);

  // Initialize the chrom based upon the contents of the chrom file.
  void read(const char *filename);

  // Write the contents of the object to the give filename
  void write(const char *filename);

  // Print the contents of the object, for debugging
  void print();

  // Given a CAL position, find the index of the chromosome containing that
  // CAL.  The index can then be used for firstCAL and name functions.
  int find(uint64_t cal);

  // Find information on the chromosome at position index.  Index is the
  // position in the reference file (first chrom = 0, 2nd = 1, etc).
  uint64_t firstCal(int index) { return chromCAL[index]; }
  char *name(int index) { return chromName[index]; }

  // The number of bases in the reference.
  uint64_t refLength() { return referenceLength; }
};

#endif /* CHROM_H_ */
