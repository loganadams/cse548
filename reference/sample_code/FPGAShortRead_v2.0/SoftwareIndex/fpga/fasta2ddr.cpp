/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fasta2ddr.h"
#include <sys/timeb.h>
#include "../common.h"
#include "chrom.h"

#include <iostream>
#include <fstream>

extern "C" {
  #include <hugetlbfs.h>
}

using namespace std;

/***
 * binaryBasesGen takes in the name of a *.fasta and converts it to a binary file
 */

void binaryBasesGen(const char *myReferenceFileName,
		    const char *binFileName,
		    const char *chromFileName) {
  Reference* 	myRef;
  Writer* 	scribe;
  
  //Instantiation
  scribe = new Writer();
  myRef = new Reference(scribe);
  
  //Reading *.fasta
  printf("Reading *.fasta in %s...", myReferenceFileName);
  fflush(stdout);
  myRef->loadReference(myReferenceFileName, chromFileName);
  printf("done\n");
  
  //Make new binary file
  printf("Writing binary file to %s...\n", binFileName);
  scribe->openFile(binFileName);
  
  //Write content in *.fasta to binary file.
  printf("Writing reference data to file...");
  fflush(stdout);
  myRef->writeToFile();
  scribe->closeFile();
  printf("done\n");
  
  delete myRef;
  delete scribe;
  
  printf("\nDone creating binary bases!\n");
}

// We want a binary version of the reference.  If the user gives us a reference
// that is a fasta file (i.e. ends in ".fasta") we look for the binary file, or
// create it if it isn't there.
// If they don't give us a fasta file, we assume it is already a binary reference.
const char *binaryFilename = NULL;
// With binary files, we also have a chromFilename that lists chromosome positions
const char *chromFilename = NULL; 

#ifdef REF_USE_HUGE_PAGES
#define REF_ALLOC(size) (uint64_t *)get_hugepage_region((size), GHR_STRICT | GHR_COLOR)
#else // REF_USE_HUGE_PAGES
#define REF_ALLOC(size) (uint64_t *)malloc((size))
#endif // REF_USE_HUGE_PAGES

// Initialize the memories on the FPGA board.
// Outputs:
// full_ref: pointer to a copy of the reference in host memory.
// full_ref_len: length of full_ref (i.e. full_ref[0]..full_ref[full_ref_len-1])
// chromData: pointer to hold the chromosome data from the reference.
void swFasta2ddr(SmithWaterman** sws, int swNum,
		 const char* referenceFileName, uint64_t *&full_ref,
		 uint32_t &full_ref_len, chrom &chromData){
  FILE          *in, *chromIn;	
  long 		row;
  uint32_t 	binbuf[4], readbuf[4], readbuf2[4], adx, i,
    DDRWritePercent, DDRWriteDivide;
  size_t 		result;
  timeb		delay;
  
  //counters for multiple ram write
  uint32_t	ddrPtr, intercept, endPtr, curPtr;
  uint32_t	ddrStartPtr, ddrEndPtr;

  //const long memsizeDDR = 512*1024*1024; // Size of the memories on each FPGA
  const uint64_t memsizeDDR = 4l*1024l*1024l*1024l; // Memsize of a M505?

  int suffixPos = strlen(referenceFileName) - strlen(".fasta");

  // Get the reference file, converting to binary if needed.
  if (suffixPos >=0
      && strcmp(".fasta", &(referenceFileName[suffixPos])) == 0) {
    // Process a .fasta file
    binaryFilename = combineStrings(referenceFileName, ".bin");
    chromFilename = combineStrings(binaryFilename, ".chr");
    in = fopen(binaryFilename, "rb");
    if (in == NULL) {
      binaryBasesGen(referenceFileName, binaryFilename, chromFilename);
      in = fopen(binaryFilename, "rb");
    }
  } else {
    // Process a binary file
    binaryFilename = referenceFileName;
    chromFilename = combineStrings(binaryFilename, ".chr");
    in = fopen(binaryFilename, "rb");
    if(in == NULL) { 
      cerr << "Binary reference file \"" << binaryFilename << "\" cannot be opened\n";
      exit(1);
    }
  }

  chromData.read(chromFilename);
  uint64_t refBases = chromData.refLength();

  // Read the file into full_ref.
  const uint64_t basesPerUint64 = sizeof(uint64_t)*8/2;
  // Round up to nearest 64-bit boundary.
  full_ref_len = (refBases + (basesPerUint64-1))/basesPerUint64;
  // We add 16 to length to make the end obvious
  full_ref = REF_ALLOC((full_ref_len+16)*sizeof(uint64_t));
  for (int c=1; c<=16; c++)
    full_ref[c+full_ref_len]=c;
  
  result = fread(full_ref, sizeof(uint64_t), full_ref_len, in);
  if (result < full_ref_len) {
    cerr << "Error: reference should be " << full_ref_len
	 << " words long, but is actually " << result
	 << "\n";
    exit(EXIT_FAILURE);
  } else {
    // Debug printing
    cout << "full_ref_len " << full_ref_len << ", read length " << result << endl;
  }

  // Binary references are aligned to a 256-bit boundary.  Check to see if we
  // are within 256 bits of the end.
  uint64_t temp1[4];
  result = fread(temp1, sizeof(uint64_t), 4, in);
  if (result == 4) {
    cerr << "Error: reference file longer than expected, should be roughly "
	 << full_ref_len*sizeof(uint64_t)
	 << " bytes for a " << refBases
	 << " base reference.\n";
    exit(EXIT_FAILURE);
   }

  // // Determine which portions of the reference go to which units
  // sws[0]->setReferenceStart(0);
  // uint32_t basesPerUnit = refBases/swNum;
  // //basesPerUnit -= (basesPerUnit%64);
  // basesPerUnit -= (basesPerUnit%128); // Align to 256-bit boundaries
  // for (int i=1; i<swNum; i++) {
  //   sws[i]->setReferenceStart(i*basesPerUnit);
  //   sws[i-1]->setReferenceEnd(i*basesPerUnit-1);
  // }
  // sws[swNum-1]->setReferenceEnd(refBases-1);

  for (int i=0; i< swNum; i++) {
    sws[i]->setReferenceStart(0);
    sws[i]->setReferenceEnd(refBases-1);
  }
  // Need extra bases for CALs that match near the end of a region
  uint32_t marginBases = readLength + 2*indelAllowance + 512;
  //marginBases -=hj (marginBases%64);
  marginBases -= (marginBases%128); // Align to 256-bit boundaries

  //Writing to Memory
  cout << "Writing data to DDR..." << flush;

  // Write the data out to the FPGAs
  for (int j=0; j<swNum; j++) {
    // First location in full_ref that has bases for this unit.
    uint32_t start = sws[j]->getReferenceStart()/basesPerUint64;
    // First location in full_ref that has bases just beyond this unit.
    uint32_t end = (sws[j]->getReferenceEnd()+basesPerUint64)/basesPerUint64;
    // if (j!=swNum-1)
    //   end += marginBases/basesPerUint64;
    // else
/*    if (end & 0x1F != 0) {
      end |= 0x1F; // Get the last partial word in, NSM changed for 256-bit RAM
      end += 1;
      }*/
    uint32_t unitBases = (end-start)*basesPerUint64;
    if (unitBases / 4 > memsizeDDR) {
      cerr << "Not enough space in memory on FPGA " << j << "\n";
      exit(EXIT_FAILURE);
    }

/*
    // Debug, fill the RAM with junk
    cerr << "Sending junk data, unitBases " << unitBases << "\n";
    uint64_t junkData[1024];
    for (int l = 0; l < 1024; l++)
      junkData[l] = 0xA0A1A2A3A4A50000ull + l;
    for (int addr = 0; addr < (unitBases / 4); addr += 8192)
      sws[j]->writeram(junkData, addr, 8192);
*/

    // Write the RAM in sections to avoid errors TODO Fix this, is it Pico bug?
//    for (int addr = 0; addr < (unitBases / 4); addr += 32)
//      sws[j]->writeram(&(full_ref[start + (addr / 8)]), addr, 32);
    sws[j]->writeram(&(full_ref[start]), 0, unitBases/4);
/*    cout << "Some parts of ref, " << full_ref[(163436032/basesPerUint64) - 1] << ", " << full_ref[163436032/basesPerUint64] << ", " << full_ref[(163436032/basesPerUint64) + 1] << endl;
    cout << "Unitbases / 4 " << unitBases/4 << " end " << end << endl;
    uint32_t rambuf[16];
    sws[j]->readram(rambuf, (163436032/4)-32, 64);
    cout << "early " << rambuf[0] << ", " << rambuf[1] << ", " << rambuf[2] << endl << "late " << rambuf[5] << ", " << rambuf[6] << ", " << rambuf[7] << endl << "next slot " << rambuf[8] << ", " << rambuf[9] << ", " << rambuf[10] << endl;*/
  }
  cout << " done.\n" << flush;

#ifdef FPGA_PRINT
  for(int j = 0; j < swNum; j++) {
    printf("sw%d - refStart: 0x%x, refEnd 0x%x\n", j,
	   sws[j]->getReferenceStart(), sws[j]->getReferenceEnd());
  }
#endif
  
  swVerifyDDR(sws, swNum, binaryFilename, full_ref); 
}

// Verifies that the memory has the same data as the binary reference file.
// sws - the objects corresponding to the smith-waterman engines (i.e. the FPGAs)
// swNum - the number of FPGAs.
// filename - name of the binary file holding the reference.
// full_ref - the internal version of the reference file, for error messages
void swVerifyDDR(SmithWaterman** sws, int swNum, const char* filename, uint64_t *full_ref)
{
  const int BufLength = 1024; // Read buffer length
  uint32_t filebuf[BufLength];
  uint32_t rambuf[BufLength];
  uint32_t rambuf2[BufLength];
  uint32_t rambuf3[BufLength];

  FILE *in = fopen(filename, "rb");
  if (in == NULL) {
    cerr << "Missing binary reference in swVerifyDDR\n";
    exit(1);
  }
  for(int j = 0; j < swNum; j++) {
    uint32_t begin = sws[j]->getReferenceStart();
    uint32_t end = sws[j]->getReferenceEnd();
    cout << "Verifying data in DDR " << j
	 << ", CALs {" << begin << ".." << end << "}"
	 << "..."; cout.flush();
    fseek(in, begin/4, SEEK_SET); // Move file pointer to same spot as begin
    for (int cal = 0;
	 cal<=(end-begin); 
	 cal += 4*BufLength*sizeof(uint32_t)) { // There are 4 bases per byte
      size_t file_result = fread(filebuf, sizeof(uint32_t), BufLength, in);
      sws[j]->readram(rambuf, cal/64*16, sizeof(uint32_t)*BufLength);
      for (int k=0; k<BufLength; k++) {
	if(filebuf[k]!=rambuf[k]) {
	  if (cal+k*sizeof(uint32_t)*4 > end-begin)
	    break; // Reached end of memory segment
	  cerr << "\n***RAM mismatch***\n  RAM " << j
	       << " position (in CALs) " << begin+cal+k*sizeof(uint32_t)*4
	       << " Memory value: " << rambuf[k]
	       << " File value: " << filebuf[k] << "\n";
          cerr << "  Position calculated from cal " << cal << " k " << k << " begin " << begin << " sizeof(unit32_t) " << sizeof(uint32_t) << " end " << end << " BufLength " << BufLength << "\n";
	  cerr << "filebuf: ";
	  for (int e=0; e<BufLength; e++) {
	    cerr << filebuf[e] << ", ";
	  }
	  cerr << "\nrambuf: ";
	  for (int e=0; e<BufLength; e++) {
	    cerr << rambuf[e] << ", ";
	  }
	  for (int d=0; d<64; d++) rambuf2[d]=19;
	  sws[j]->readram(rambuf2, cal/64*16, sizeof(uint32_t)*BufLength);
	  cerr << "\nrambuf (duplicate): ";
	  for (int e=0; e<BufLength; e++) {
	    cerr << rambuf2[e] << ", ";
	  }
	  sws[j]->readram(rambuf3, cal/64*16, sizeof(uint32_t)*BufLength);
	  cerr << "\nrambuf (triplicate): ";
	  for (int e=0; e<BufLength; e++) {
	    cerr << rambuf3[e] << ", ";
	  }
	  cerr << "\nmainmem: ";
	  uint32_t *tmp = (uint32_t *)full_ref; // Index by 32-bit quants, to match others.
	  for (int e=0; e<BufLength; e++) {
	    cerr << tmp[(begin+cal)/4/4+e] << ", ";
	  }
	  cerr << "\n";
	  exit(1);
	}
      }
    }
    cout << " passed\n"; cout.flush();
  }
}
