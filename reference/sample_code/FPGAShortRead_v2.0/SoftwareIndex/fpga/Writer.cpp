/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Writer.cpp
 *
 *  Created on: Oct 24, 2011
 *      Author: Corey
 * Description: Writes data to the index file in binary format
 */

#include "Writer.h"

Writer::Writer() {

}

Writer::~Writer() {
}

// opens the specified filename for writing
void Writer::openFile(const char* fileName) {

    // open the file and verify it opened properly
    fileHandle = fopen(fileName, "wb");
    if (fileHandle == NULL) {
        perror("Error opening file in Writer\n");
        exit(1);
    }

    // reset the number of bytes written to this file
    bytesWritten = 0;
}

// closes the current file
void Writer::closeFile() {
    fclose(fileHandle);
}

// write 8 bits of data to the file
// note: this method assumes the output file is already opened correctly
void Writer::writeByte(uint8_t writeData) {
    bytesWritten += fwrite(&writeData, sizeof(uint8_t), 1, fileHandle) * sizeof(uint8_t);
}

// write 32 bits of data to the file
void Writer::writeWord(uint32_t writeData) {
    bytesWritten += fwrite(&writeData, sizeof(uint32_t), 1, fileHandle) * sizeof(uint32_t);
}

// writes the specified 64 bits of data to the file
void Writer::writeHalfLine(uint64_t writeData) {
    bytesWritten += fwrite(&writeData, sizeof(uint64_t), 1, fileHandle) * sizeof(uint64_t);
}

// write 128 bits of data to the file
void Writer::writeLine(line128 *writeData) {
    uint64_t dataToWrite;
    for (int i = 0; i < 2; i++) {
        dataToWrite = writeData->data[i];
        writeHalfLine(dataToWrite);
    }
}

// write the specified number of 128-bit lines to the file
void Writer::writeNumLines(line128* writeData, int numLines) {

    // write starting at the LS 128-bit line
    for (int i = 0; i < numLines; i++) {
        writeLine(&writeData[i]);
    }
}

// returns the number of bytes written to the index file thus far
uint64_t Writer::getBytesWritten() {
    return bytesWritten;
}
