/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Reference.h
 *
 *  Created on: Oct 21, 2011
 *      Author: Corey
 * Description: Interfaces with the fasta reference file and returns valid seeds
 */

#ifndef REFERENCE_H_
#define REFERENCE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Writer.h"

class Reference {
private:
    FILE*       referenceFile;          // reference file handle
    char*       refData;                // holds the reference data

    uint32_t    currentCAL;             // current location in the reference
    uint32_t    refLength;              // total number of bases in this reference genome (includes N)

    Writer*     scribe;                 // object used to write to the index file

    // methods
    char        getNextBase         ();

    void        openRefFile         (const char* refFileName);
    void        closeRefFile        ();
public:
    // constructor and destructor
    Reference                       (Writer* scribe=NULL);
    ~Reference                      ();

    // methods
    void        loadReference       (const char* refFileName, const char *chrFileName);
    void        print               ();
    void        writeToFile         ();
};

#endif /* REFERENCE_H_ */
