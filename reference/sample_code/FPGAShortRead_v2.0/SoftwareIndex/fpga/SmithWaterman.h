/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SMITH_WATERMAN_H
#define SMITH_WATERMAN_H 1

#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <picodrv.h>
#include <pico_errors.h>

#include "../scoredCAL.h"

extern "C" {
  #include "../queue/inline.h"
  #include "../queue/sw_queue_astream.h"
}

//Sizes buffers to SW_BUF_LEN 32-bit values.  Used to group values together
//on PCI transfers.  A larger value reduces PCI overhead, but wastes more 
//space in host memory.
#define SW_BUF_LEN 1000000

//#include "fasta2ddr.h"

/////////////////
// DEFINITIONS //
/////////////////

#define PICO_CARD           0x501

#define BITS_PER_BASE       2

#define BYTES_PER_SCORE     16

#define SCORE_BITS          11

#define SEND_STREAM_WIDTH   16  // width in bytes
#define SCORE_STREAM_WIDTH  16  // width in bytes

#define SEND_STREAM         1
#define SCORE_STREAM        1

#define SKIP_CARD           0

/////////////
// STRUCTS //
/////////////


///////////
// CLASS //
///////////

class SmithWaterman {
 private:

  // handle to the Pico card
  PicoDrv *pico;
  char ibuf [1024];
  int sendStream;
  int scoreStream;

  // queues for sending and receiving data    
  SW_Queue sendQueue;
  SW_Queue scoreQueue;

  // thread data
  pthread_t           sendThread;
  pthread_t           scoreThread;
  int                 sendThreadRet;
  int                 scoreThreadRet;
    
  // semaphores to specify that the sending and receiving threads should terminate
  // TODO: determine if this should be a single semaphore or one for each thread
  volatile bool       sendsCompleted;
  volatile bool       scoresCompleted;
   
  uint32_t            referenceStart;
  uint32_t            referenceEnd;
  bool                firstCAL;

  volatile uint64_t   numReferencesAligned;
  volatile uint64_t   numScoresReceived;

  uint32_t	sw_id;
  
  uint32_t      picoCardModel;

  FILE* queryFileTxt;

  struct timespec startClock, consume1Clock, consume2Clock, createClock, endClock;
  unsigned long long totalStartClocks, totalConsume1Clocks, totalConsume2Clocks, totalCreateClocks, totalStartEndClocks, totalCALs;
  unsigned int randCount;

 public:
  // Constructor and Destructor
  SmithWaterman   (int bufferBytes, const char* bitFileName,
		   SW_Queue send_queue, SW_Queue score_queue);
  ~SmithWaterman  ();
    
  // write data to ddr
  // Buf - pointer to a location with the data to send.
  // adx - address in the FPGA memory to store it at.
  // bytes - how many bytes to send.
  void writeram(uint64_t *buf, uint32_t adx, uint32_t bytes);

  void readram(uint32_t *mem, uint32_t startAddress, uint32_t bytes);

  // The value of the first CAL contained in the memory for this unit
  inline uint32_t getReferenceStart(void) {
    return referenceStart;
  }
  // The value of the last CAL contained in the memory for this unit.
  // This ignores the margin at the end used to overlap between memories
  // (i.e. to support a reference starting at CAL == getReferenceEnd,
  // we need values beyond this point).
  inline uint32_t getReferenceEnd(void) {
    return referenceEnd;
  }
  inline void setReferenceStart(uint32_t refStart) {
    referenceStart = refStart;
  }
  inline void setReferenceEnd(uint32_t refEnd) {
    referenceEnd = refEnd;
  }

  inline int getPicoCardModel() {
    return picoCardModel;
  }
	
  // blocking methods to send and receive data using the circular buffers
  void sendData();
  void receiveScores();
    
  // sends a CAL and its side-band info to the FPGA for alignment
  void alignCAL(uint32_t CAL, int target_length, int32_t query_start_clip, 
		int32_t query_end_clip, bool reverseComplement);

  void setPackedRead(uint64_t *query, int query_length,	uint32_t query_id);
    
  // blocking call to get the scoring information from the score buffer.
  // Note: query_end is only valid when query_end_clip == 1 for that target
  scoredCAL *getScoredCAL(uint64_t packedCAL);

  // Consume same data on the queues as a getScoredCAL call, but just trashes it.
  // Probably only useful during testing.
  void trashScoredCAL();
    
  // Flush the send queue.  Helpful at the end of processing.
  void flushQueue()
  {
    sq_flushQueue(sendQueue);
  }

};


////////////////////
// HELPER METHODS //
////////////////////

void*       startSendData       (void* sw);
void*       startReceiveScores  (void* sw);

#endif
