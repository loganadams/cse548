/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * File Name : SmithWaterman.cpp
 *
 * Description :  This class handles the packing of Smith-Waterman data into
 *                the correct format for sending to the FPGA.  It also
 *                receives scores from the FPGA and writes those
 *                results to a file.
 *
 * Creation Date : Thu 26 Jan 2012 11:36:21 AM PST
 *
 * Author : Corey Olson
 *
 * Last Modified : Thu 23 Feb 2012 01:34:10 PM PST
 *
 */
//#define C_DEBUG
#include "SmithWaterman.h"

#include "../common.h"
#include "../allqueues.h"

#include <stdio.h>

unsigned long long smwaqprod = 0;
unsigned long long smwaqcons = 0;

// constructor allocates the read, reference, and score buffers; downloads the bitfile to the FPGA;
//  initializes work completed semaphores; initializes head and tail pointers for the buffers
SmithWaterman::SmithWaterman(int bufferBytes, const char* bitFileName,
			     SW_Queue send_queue, SW_Queue score_queue){
  
  int err;
  
  //HL-May17th: ID variable, a tag for easier debug
  static uint32_t cur_id = 0;
  
  sw_id = cur_id++;
  cout << "SW object " << this << " is setting send_queue to " << &sendQueue << " inx is " << send_queue->c_inx << endl;
  // create the queues
  sendQueue = send_queue;
  scoreQueue = score_queue;

  // get a handle to the card
  if (bitFileName != NULL){
    printf("Loading bitfile %s\n", bitFileName);
    err             = RunBitFile(bitFileName, &pico);
  }else{
    printf("Connecting to already programmed FPGA\n");
    err             = FindPico(PICO_CARD, &pico);
  }
  
  if (err < 0){
    fprintf(stderr, "RunBitFile error: %s\n", 
	    PicoErrors_FullError(err, ibuf, sizeof(ibuf)));
    exit(1);
  }

  // Get Pico Card Model
  PICO_CONFIG picoConfig = {0};
  pico->GetPicoConfig(&picoConfig);
  picoCardModel = picoConfig.model;
  
  // create streams for writing data and reading scores
  sendStream          = pico->CreateStream(SEND_STREAM);
  if (sendStream < 0) {
    fprintf(stderr, "Create stream error: %s\n", PicoErrors_FullError(sendStream, ibuf, sizeof(ibuf)));
    exit(1);
  }
  
  scoreStream         = pico->CreateStream(SCORE_STREAM);
  if (scoreStream < 0) {
    fprintf(stderr, "Create stream error: %s\n", PicoErrors_FullError(scoreStream, ibuf, sizeof(ibuf)));
    exit(1);
  }

  //printf("&&& scoreStream %0x sendStream %0x &&&\n", scoreStream, sendStream);
  
  // initialize the number of references and scores processed
  numReferencesAligned= 0;
  numScoresReceived   = 0;
  
  // initialize that the work is not yet completed
  sendsCompleted      = false;
  scoresCompleted     = false;
  
  // spawn the sendRead, sendRef, and receiveScore threads
  sendThreadRet       = pthread_create(&sendThread,       NULL, startSendData,         (void*) this);
  scoreThreadRet      = pthread_create(&scoreThread,      NULL, startReceiveScores,    (void*) this);

#ifdef TESTBENCH_QUERY
  queryFileTxt = fopen(TESTBENCH_QUERY_FILE, "w");
#endif

  totalStartClocks = totalConsume1Clocks = totalConsume2Clocks = totalCreateClocks = totalStartEndClocks = totalCALs = 0;

  randCount = 0;
}

// destructor tells threads that work has been completed and frees up allocated memory for circular
//  buffers
SmithWaterman::~SmithWaterman(){
  // tell the FPGA communication threads that they need to end their work.
  // Note: this will get them to finish work quickly, and data in transit might be lost.
  sendsCompleted      = true;
  scoresCompleted     = true;

  // Wait for the FPGA communication threads to actually end by joining with them.
  pthread_join(sendThread, NULL);
  pthread_join(scoreThread, NULL);

  // Hardware testbench query file
#ifdef TESTBENCH_QUERY
  fclose(queryFileTxt);
#endif
}

// adds the data to the read buffer
// NOTE: the query_length parameter specifies the actual query length, but the query itself is
//       aligned to the beginning of the query array
void SmithWaterman::setPackedRead(uint64_t* query, int query_length,
				  uint32_t query_id) {
  
  uint64_t    txData;
  int         bytesSent = 0;
  int         bytesToSend = 16 * (1 + (((query_length * BITS_PER_BASE + 127) / 128)));
  
  // Designate new reads with 128 bits of 1 first
  txData = (uint64_t)-1;
  sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);
  sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);

  // push the read length into the read buffer
  txData = ((uint64_t) query_id << 32) | (uint64_t) query_length;
  sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);
  txData = 0;
  sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);
  bytesSent += 16;
  
  // send the packed read
  for(int i=0; i<(query_length/32+1); ++i) {
    txData = query[i];
    
    // add 64 bits to the queue
    sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);
    bytesSent += 8;
  }
  
  // ensure we send the proper amount of data per query
  txData = 0;
  while(bytesSent < bytesToSend){
    sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);
    bytesSent += 8;
  }

//  cout << "Sending read, id " << query_id << " length " << query_length << " data0: " << hex << query[0] << dec << endl;
  
  //HUNLAN RESET FIRSTCAL
  firstCAL = true;
}

// sends a CAL and its side-band information to the FPGA for alignment
void SmithWaterman::alignCAL       (uint32_t        CAL,                int     target_length,
                                    int32_t         query_start_clip,   int32_t query_end_clip,
                                    /*bool            firstTarget,*/    bool    reverseComplement){
  uint64_t    txData;
  
  // set the length of the target
  txData = (uint64_t) target_length;
  txData <<= 32;
  
  // set the CAL
  txData += (uint64_t) (CAL - referenceStart);
  if ( (int64_t)CAL - referenceStart < 0 ) {
    printf("problem!!\n got value = %ld\n", ((int64_t)CAL) - referenceStart);
  }
  sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL);
  // set the firstCAL, reverse complement, startclip, and endclip values
  txData = (query_end_clip << 3) + 
    (query_start_clip << 2) +
    (reverseComplement << 1) + 
    firstCAL;//firstTarget;
  if(firstCAL) {
    firstCAL = false;
  }
//  cout << "Sending CAL: " << hex << CAL << dec << " length " << target_length << " first " << firstCAL << endl;
  sq_produce(sendQueue, txData, (sq_callback)FPGAcontrolThread_flushAll, NULL); 
  
  // update the number of references aligned thus far
  numReferencesAligned++;
}

// Consume same data on the queues as a getScoredCAL call, but just trashes it.
// Probably only useful during testing.
void SmithWaterman::trashScoredCAL()
{
  uint64_t    data64a, data64b;
  
  // grab the score, reverse complement, and read ID
  data64a = sq_consume(scoreQueue, (sq_callback)getScores_flushAll, NULL);
  data64b = sq_consume(scoreQueue, (sq_callback)getScores_flushAll, NULL);
  smwaqcons += 2;
}

// blocking call to get the scoring information from the score buffer.
scoredCAL *SmithWaterman::getScoredCAL(uint64_t packedCAL)
{
  uint64_t query_end;
  uint64_t target_end;
  bool reverseComplement;
  bool noIndel;
  uint32_t id; 

  uint64_t    data64a, data64b;
  int32_t     score = 0;
  uint32_t    unsignedScore;  
  uint64_t    queryEndCell;   // 0-based
  uint64_t    targetEndCell;  // 0-based
  
  int32_t mask;

  scoredCAL* tempcal;
  
//  clock_gettime(CLOCK_MONOTONIC, &startClock);

  // grab the score, reverse complement, and read ID
  data64a = sq_consume(scoreQueue, (sq_callback)getScores_flushAll, NULL);
  unsignedScore = data64a & ((1<<SCORE_BITS)-1);
  reverseComplement = (data64a >> 31) & 1;
  noIndel = (data64a >> 30) & 1;
  id = (uint32_t) (data64a >> 32);
  
//  clock_gettime(CLOCK_MONOTONIC, &consume1Clock);

  // grab the reference base and the read base alignments
  data64b = sq_consume(scoreQueue, (sq_callback)getScores_flushAll, NULL);
  targetEndCell = (uint32_t) data64b;
  queryEndCell = (uint32_t) (data64b >> 32);

/*  smwaqcons += 2;
  if (smwaqcons % 1000000 == 0)
  cout << "Cons " << smwaqcons << endl;*/
  
//  clock_gettime(CLOCK_MONOTONIC, &consume2Clock);

  // convert the score from the stream to a signed score
  if (unsignedScore >> (SCORE_BITS-1)){
    score = -1 * (1<<(SCORE_BITS-1));
  }    
  score += unsignedScore & ((1<<(SCORE_BITS-1))-1);
  if (SKIP_CARD) {
    // 2 out of every hundred, get score 100
    if (randCount = 0) {
      score = 100;
      randCount = 50;
    } else {
      score = 0;
      randCount --;
    }
  }
  
  // set the query end position
  // Note: the query base is only valid when clip_end was asserted
  query_end = queryEndCell;
  
  // set the end position of the target
  target_end = targetEndCell + referenceStart;
  
  mask = ((score & 0x200) << 22) >> 22;

  //printf("%016lX %016lX\n", data64a, data64b);
//  cout << "Created cal with score " << (score|mask) << " premask " << score << endl;

//  clock_gettime(CLOCK_MONOTONIC, &createClock);

  tempcal =  new scoredCAL(id, (score | mask), noIndel, reverseComplement,
		       query_end, target_end, packedCAL);

/*  clock_gettime(CLOCK_MONOTONIC, &endClock);
  totalStartClocks += (consume1Clock.tv_sec * 1000000000 + consume1Clock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
  totalConsume1Clocks += (consume2Clock.tv_sec * 1000000000 + consume2Clock.tv_nsec) - (consume1Clock.tv_sec * 1000000000 + consume1Clock.tv_nsec);
  totalConsume2Clocks += (createClock.tv_sec * 1000000000 + createClock.tv_nsec) - (consume2Clock.tv_sec * 1000000000 + consume2Clock.tv_nsec);
  totalCreateClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (createClock.tv_sec * 1000000000 + createClock.tv_nsec);
  totalStartEndClocks += (endClock.tv_sec * 1000000000 + endClock.tv_nsec) - (startClock.tv_sec * 1000000000 + startClock.tv_nsec);
  totalCALs++;
  if (totalCALs >= 100000000) {
    unsigned long long totalClocks = totalStartClocks + totalConsume1Clocks + totalConsume2Clocks + totalCreateClocks;
    cout << totalCALs << " CALs took " << totalClocks /1000000000.0 << "s, for a rate of " << totalCALs / (totalClocks/1000000000.0) << " CALs/s. Start-end " << totalStartEndClocks /1000000000.0 << "s" << endl;
    cout << "Individual times were start " << totalStartClocks/1000000000.0 << " consume1 " << totalConsume1Clocks/1000000000.0 << " consume2 "<< totalConsume2Clocks/1000000000.0 << " create " << totalCreateClocks/1000000000.0 << endl;
    totalCALs = 0;
    totalStartClocks = 0;
    totalConsume1Clocks = 0;
    totalConsume2Clocks = 0;
    totalCreateClocks = 0;
    totalStartEndClocks = 0;
    }*/

  return tempcal;
}

// thread to send data to the FPGA from the send buffer
void SmithWaterman::sendData()
{
  uint64_t buffer[SW_BUF_LEN/2]; // Temp buffer.  Data goesQueue->buffer->FPGA
  int index; // Points to first empty location in the buffer.

  // Sends must be aligned to SEND_STREAM_WIDTH.  alignedIndex points to first
  // position of an empty or partial grouping (i.e. data from buffer[0] to
  // buffer[alignedIndex-1] are ready to send.
  int alignedIndex;

  int err;

  index=0;
  while (1) { // Transfer data forever
    while (!sq_canConsume(sendQueue) && !sendsCompleted) {
      // Loop forever waiting for data
      // Reverse flush the queue, may kill performance, but prevents deadlock
      sq_reverseFlush(sendQueue);
    }
    if (sendsCompleted) {
      // Exiting thread.  Check if there is buffered data that will be lost.
      if (index != 0 || sq_canConsume(sendQueue)) {
	cerr << "** WARNING: exiting sendData thread with pending data **\n" << flush;
      }
      return;
    }

    buffer[index++]
      = (uint64_t)sq_consume(sendQueue,
			     (sq_callback) sq_reverseFlush,
			     sendQueue); // Blocking call

    // Once you get an entry, get as much more as you can w/o blocking, to
    // bundle PCI sends while avoiding deadlock.
    while(sq_canConsume(sendQueue) && index < SW_BUF_LEN/2-1) { 
      buffer[index++]
	= (uint64_t)sq_consume(sendQueue,
			       (sq_callback) sq_reverseFlush,
			       sendQueue);
    }
    
    // Turn off bits in alignedIndex to align to SEND_STREAM_WIDTH bytes chunks
    alignedIndex = index & (~((SEND_STREAM_WIDTH/sizeof(uint64_t))-1));

    // Must have enough data to meet minimum send width
    if (alignedIndex < 1)
      continue;

#ifdef C_DEBUG
    printf("\nreads data:\n");
    for(int i=0; i<alignedIndex; i++){
      if (i%2==1) {
	printf("%016lX %016lX r%d\n", buffer[i], buffer[i-1], sw_id);
      }
    }
    if (alignedIndex%2 != 0) {
      printf("loner: %016lX r%d\n", buffer[alignedIndex-1], sw_id);
    }
#endif //C_DEBUG

#ifdef TESTBENCH_QUERY
    //Hardware testbench query file
    fprintf(queryFileTxt, "%X\n", alignedIndex);
    int i = 0;
    for (int i = 1; i<alignedIndex; i+=2) {
      fprintf(queryFileTxt, "%X %X\n", buffer[i], buffer[i - 1]);
    }
    if (alignedIndex%2 == 1) {
      fprintf(queryFileTxt, "0 %X\n", buffer[alignedIndex-1]);
    }
#endif //TESTBENCH_QUERY

    if (!SKIP_CARD) {
      err = pico->WriteStream(sendStream, (void*)buffer, 
                              alignedIndex*sizeof(uint64_t));
    } else {
      err = alignedIndex*sizeof(uint64_t);
    }
    if (err < 0){
      fprintf(stderr, "WriteStream error in sendData(): %s\n", 
	      PicoErrors_FullError(err, ibuf, sizeof(ibuf)));
      exit(1);
    }
    if (err != alignedIndex*sizeof(uint64_t)) {
      cerr << "ERROR: WriteStream should have sent "
	   << (alignedIndex*sizeof(uint64_t)) << " bytes, but it did send "
	   << err << " bytes\n";
      exit(EXIT_FAILURE);
    }
    
    //Move any partially aligned data to beginning of queue, and continue.
    for (int temp=alignedIndex; temp<index; temp++) {
      buffer[temp-alignedIndex]=buffer[temp];
    }
    index -= alignedIndex;
  }
}

// thread to receive score data from the FPGA and add it to the score buffer
void SmithWaterman::receiveScores(){
  //cout << "recScore " << sw_id << " send: " << sendStream << " score: " << scoreStream << "\n";
  uint64_t buffer[SW_BUF_LEN/2];

  int err;

  while (1){
    if (scoresCompleted) {
      // Signal to be done.  Should probably check to see if FPGA has unsent data.  Save that
      // for later.
      sq_flushQueue(scoreQueue); // Probably redundant, but just in case
      return;
    }
    // Number of bytes we might have, based on CALs sent so far.
    int bytesPossible = BYTES_PER_SCORE 
      * (numReferencesAligned - numScoresReceived);

    // Number of bytes we can fit into the buffer.
    int bufferMaxBytes = SW_BUF_LEN/2*sizeof(uint64_t);

    int bytesToGet = ((bytesPossible<bufferMaxBytes)
		      ? bytesPossible
		      : bufferMaxBytes);

    // Round to full transactions.
    bytesToGet &= (~(SEND_STREAM_WIDTH-1));

    // Repeat until we actually have data to get.
    if (bytesToGet == 0) {
      continue;
    }

    //bytesToGet = 128/8;
    //cout << "sw_id: " << sw_id
    //	 << " bytesToGet " << bytesToGet
    //	 << " numRefAl " << numReferencesAligned
    //	 << " numScoresRec " << numScoresReceived
    //	 << "\n";

    // read data from the stream
    if (!SKIP_CARD) {
      err = pico->ReadStream(scoreStream, (void*)buffer, bytesToGet);
    } else {
      err = bytesToGet;
    }
    
    if (err < 0){
      fprintf(stderr, "ReadStream error in receiveScores(): %s\n", 
	      PicoErrors_FullError(err, ibuf, sizeof(ibuf)));
      exit(1);
    } else if (err != bytesToGet) {
      cerr << "ERROR: ReadStream should have gotten " << bytesToGet
	   << " bytes, but actually got " << err << " bytes.\n";
      exit(EXIT_FAILURE);
    }
//    cout << "Bytes to get  " << err << endl;
    for (int i=0; i*sizeof(uint64_t)<err; i++) {
      if (SKIP_CARD) {
        buffer[i] = (i & 0x1) ? 0x3131d422b7LL : 0x5400000047LL; 
      }
//      cout << "Byte " << i << " is: " << hex << buffer[i] << dec <<endl;
      // Pico system seems to be putting out bogus entries sometimes.
      if (buffer[i]>>32LL == 0x4242dead) {
	printf("*** WARNING: 4242DEAD at %d/%lu ***\n", i, err/sizeof(uint64_t));
	cout << flush;
      }
      sq_produce(scoreQueue,
		 buffer[i],
		 (sq_callback) sq_flushQueue,
		 scoreQueue);
/*      smwaqprod++;
      if (smwaqprod % 1000000 == 0)
      cout << "prod " << smwaqprod << endl;*/
    }
    sq_flushQueue(scoreQueue); // Flush, so that next call to FPGA doesn't
                               // delay the rest of the system.
      
#ifdef C_DEBUG
    printf("\noutput:\n");
    int i;
    for(i=0; i*sizeof(uint64_t)<err; i++){
      if (i % 2 == 1){
	printf("%016lX, %016lX o%d\n", buffer[i], buffer[i-1], sw_id);
      }
    }
    if (err%2 != 0) {
      printf("loner: %016lX o%d\n", buffer[i-1], sw_id);
    }
#endif //C_DEBUG 
     
    // update the number of bytes that have been read
    numScoresReceived += err / BYTES_PER_SCORE;
  }
}

//write data to ddr
// Buf - pointer to a location with the data to send.
// adx - address in the FPGA memory to store it at.
// bytes - how many bytes to send.
void SmithWaterman::writeram(uint64_t *buf, uint32_t adx, uint32_t bytes)
{
  int err = pico->WriteRam(adx, buf, bytes, PICO_DDR3_0);
  if (err < 0) {
    cerr << "WriteRam error: "
	 << PicoErrors_FullError(err, ibuf, sizeof(ibuf))
	 << "\n";
    exit(EXIT_FAILURE);
  }
  
  if (0) {//err != bytes) {
    cerr << "WriteRam wrote " << err
	 << " bytes instead of the desired " << bytes << " bytes\n";
    exit(EXIT_FAILURE);
  }
  return;
}

//read ddr
void SmithWaterman::readram(uint32_t *mem, uint32_t startAddress,
			    uint32_t bytes)
{
  char ibuf[1024];
  
  int err = pico->ReadRam(startAddress, mem, bytes, PICO_DDR3_0);
  if (err < 0) {
    cerr << "ReadRam error: "
	 << PicoErrors_FullError(err, ibuf, sizeof(ibuf))
	 << "\n";
    exit(EXIT_FAILURE);
  }
  if (err != bytes) {
    cerr << "ReadRam read " << err
	 << " bytes instead of the desired " << bytes << " bytes\n";
    exit(EXIT_FAILURE);
  }
}

// function to kick off new thread sending reads
void* startSendData(void* ptr){
  SmithWaterman* sw = (SmithWaterman*) ptr;
  sw->sendData();
}

// function to kick off new thread receiving scores
void* startReceiveScores(void* ptr){
  SmithWaterman* sw = (SmithWaterman*) ptr;
  sw->receiveScores();
}
