/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stdint.h>
#include <stdint.h>
#include <string.h>
#include "readFastq.h"
#include "common.h"

using namespace std;

// Takes in a cal_table.txt file, and outputs a binary cal_table.
// This basically converts an ASCII form to 3 uint32_t values.

int main(int argc, char* argv[])
{
  if (argc != 3) {
    cerr << "Incorrect number of arguments: " << argv[0] << " <calTable filename> <compressed filename>\n";
    exit(EXIT_FAILURE);
  }
  cout << "-=-=-=-=-=- Compressing CAL_TABLE file -=-=-=-=-=-\n";
  
  const char *inFilename = argv[1];
  const char *outFilename = argv[2];

  cout << "Opening CAL_TABLE file " << inFilename << endl;
  ifstream inFile(inFilename);
  if (!inFile || !inFile.good()) {
    cerr << "Unable to open " << inFilename << endl;
    exit(EXIT_FAILURE);
  }

  cout << "Opening compressed output file " << outFilename << endl;
  ofstream outFile(outFilename);
  if (!outFile || !outFile.good()) {
    cerr << "Unable to open " << outFilename << endl;
    exit(EXIT_FAILURE);
  }

  uint32_t entry[2];
  uint32_t key, CAL, isRevComp;
  uint64_t address=0;

  while(!inFile.eof()) {
    address++;
    inFile >> key;
    inFile >> CAL;
    inFile >> isRevComp;
    if (isRevComp != 0 && isRevComp != 1) {
      cerr << "Malformed CAL table entry neary entry " << address << endl;
      exit(2);
    }
    entry[0]=key<<1 | isRevComp;
    entry[1]=CAL;
    outFile.write((char*)entry, sizeof (uint32_t) * 2);
  }
  cout << "Last entry - key: " << key << " CAL: " << CAL << " recComp: " << isRevComp << endl;
  inFile.close();
  outFile.close();
  
  return 0;
}
