/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "common.h"

#ifndef HASHFUNC_H
#define HASHFUNC_H 1

// All hash functions are labelled hash_X_Y, where X is the length of the value being hashed
// (generally seed length for us), and Y is the number of bottom bits that are XORed into the 
// top - the bottom Y bits of the output become the key for our CAL table, and the (X-Y) bits above
// that index into the ptr table. 
// 
// To generate hashes, we use the makeHash routines.  Our X is the SEEDWIDTH,
// while Y = SEEDWIDTH - BUCKETIDWIDTH.


// A fast hash function.  Takes a 44-bit input, and converts to a 44-bit value, where the bottom 
// 12 bits are XORed into the upper 32 bits.
//
// We want a more general version, but this is a specific case useful as an example and a
// reference
uint64_t hashfunc_44_12(uint64_t in);

// The primary BFAST hash, and MRFAST22
uint64_t hashfunc_44_16(uint64_t in);

// MRFAST length 16 seeds
uint64_t hashfunc_32_4(uint64_t in);

// MRFAST length 18 seeds
uint64_t hashfunc_36_8(uint64_t in);

// MRFAST length 20 seeds
uint64_t hashfunc_40_12(uint64_t in);

// MRFAST length 25 seeds
uint64_t hashfunc_50_22(uint64_t in);

// MRFAST length 29 seeds
uint64_t hashfunc_58_30(uint64_t in);

uint64_t hashfunc_24_4(uint64_t in);
uint64_t hashfunc_null(uint64_t in);

/* A simple hash to swap the C and T entries - helps balance the size of 
 * the first and second half of the CAL tables given the lexigraphic sort
 * A(00) -> A(00)
 * C(01) -> T(11)
 * G(10) -> G(10)
 * T(11) -> C(01)
 * Only affects the top base of the seed!
 */
inline uint64_t hashfunc_mr(uint64_t in, uint64_t seed_length) {
  uint64_t topBaseLSB = (in >> (2ULL*seed_length - 2ULL)) & 1ULL;
  uint64_t hashed = in ^ (topBaseLSB << (2ULL*seed_length - 1ULL));
  return hashed;
}

// Use the mode to pick the hashfunction.
// MRFASTn or BFAST defines in common.h pick the hash function
inline uint64_t hashfunc(uint64_t lexSeed, uint64_t seedLength) {
  uint64_t hashedSeed;

#if defined MRFAST12 || defined MRFAST14
  hashedSeed = hashfunc_mr(lexSeed, seedLength);
#elif defined MRFAST16
  hashedSeed = hashfunc_32_4(lexSeed);
#elif defined MRFAST18
  hashedSeed = hashfunc_36_8(lexSeed);
#elif defined MRFAST20
  hashedSeed = hashfunc_40_12(lexSeed);
#elif defined MRFAST22
  hashedSeed = hashfunc_44_16(lexSeed);
#elif defined MRFAST25
  hashedSeed = hashfunc_50_22(lexSeed);
#elif defined MRFAST29
  hashedSeed = hashfunc_58_30(lexSeed);
#elif defined BFAST
  hashedSeed = hashfunc_44_16(lexSeed);
#else
#error No valid hash function
#endif
  return hashedSeed;
}
#endif
