/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef READFASTQ_H
#define READFASTQ_H 1

#include <fstream>
#include <iostream>

using namespace std;

#include "common.h"
#include "readDescr.h"

// Opens a Fastq file, making it ready for subsequent calls to readFastqRecord.
ifstream *openFastqFile(const char *filename);

// Reads a record from the ifstream provided.  filename is the name of the file,
// strictly used for error messages.  numRead is the number of reads already
// read from this file.  maxN is a filter - any read with more than maxN
// N's in it is silently rejected, and the reader moves to the next read.
//
// The function returns a readDescr with the line's data if the read worked,
// or NULL if EOF is encountered.
//
// To pack the read, bases are converted by CharToBase into a 2-bit code.  The
// first base in the read is put into the bottom two bits of packedRead.
// The next base is in the next two bits.  After 32 bases, we move to the next
// entry in packedRead, again putting the 33rd base into the bottom two bits.
// In the common case that the read length is not a multiple of 32, the final
// value will have data in the lower bits, and 0's in the upper bits.
//
// Note that if the reads in the file are longer than the readLength constant,
// only the first "readLength" bases will be converted.  If the reads in the
// file are shorter, the missing bases are assumed to encode as 00.
readDescr *readFastqRecord(ifstream *readFile, const char *filename, 
			   const uint64_t numRead, const int maxN);

// FASTQ files are very big, with an ascii coding of values, quality scores, and other
// elements.  We alternatively use compressed files, where reads are packed as tightly
// as possible (i.e. in the packedRead format itself, as binary numbers instead of characters).

// Opens a Compressed Fastq file, making it ready for subsequent calls to readCompressedRecord.
ifstream *openCompressedFile(const char *filename);

// Reads a record from the ifstream provided.  filename is the name of the file, strictly used
// for error messages.  packedRead should be an array long enough to take the read in packed
// format. numRead is the number of reads already read from this file (for error messages)
//
// The function returns true if the read worked, or
// 0 if EOF is encountered 
bool readCompressedRecord(ifstream *readFile, const char *filename,
			  uint64_t *packedRead, const uint64_t numRead,
			  char *identifier, char *quality);

// Prints the first "length" set of bases represented by the specified value.
// Length will normally be 32, except for the final value of the set
void printPackedRead(uint64_t value, int length);
void printPackedRead(uint64_t value, int length, ostream &out);
void printPackedReadMultiWord(uint64_t *readArray, int length, ostream &out);

/*
 * Unpackes the read in "source" into the character buffer "targ".  It skips
 * the first "offset" bases from the beginning.  It then prints "numBases"
 * bases into the targ buffer.  It then null terminates the array.
 */
void unpackReadMultiWord(char *targ, uint64_t *source,
			 uint64_t offset, int numBases);

// Testing routine - converts a read in character array form into a packed
// read.
uint64_t *stringToPackedRead(char *str);

#endif
