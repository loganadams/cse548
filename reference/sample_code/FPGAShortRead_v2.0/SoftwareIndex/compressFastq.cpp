/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stdint.h>
#include <stdint.h>
#include <string.h>
#include "readFastq.h"
#include "common.h"

using namespace std;

// Takes in a fastq file, and outputs a compressed fastq file.  Comments are removed, quality scores
// are eliminated, and bases are packed tightly.  What is left is a short header, then packed reads
// (as described in readFastq.h).  As an example, /usr/share/shortread/data/nickerson/reads1.fastq
// is reduced down by a factor of 8. 

int main(int argc, char* argv[])
{
  if (argc != 3) {
    cerr << "Incorrect number of arguments: " << argv[0] << " <fastq filename> <compressed filename>\n";
    exit(EXIT_FAILURE);
  }
  cout << "-=-=-=-=-=- Compressing FASTQ file -=-=-=-=-=-\n";
  
  const char *inFilename = argv[1];
  const char *outFilename = argv[2];

  cout << "Opening FASTQ reads file " << inFilename << endl;
  ifstream *inFile = openFastqFile(inFilename);

  cout << "Opening compressed output file " << outFilename << endl;
  ofstream outFile(outFilename);
  if (!outFile || !outFile.good()) {
    cerr << "Unable to open " << outFilename << endl;
    exit(EXIT_FAILURE);
  }
  
  // Create a header for the file
  outFile << ">>SourceFastq:" << inFilename << "\n";
  outFile << ">>Readlength: " << readLength << "\n";

  // Get reads from the file, and send compressed reads to the output file.
  uint64_t numRead=0;

  uint32_t packedReadLength = readLength/32;
  if (packedReadLength*32<readLength)
    packedReadLength++;
  uint64_t packedRead[packedReadLength+1];
  char identifier[MAX_FASTQ_LINE_LEN];
  char queryQuality[MAX_FASTQ_LINE_LEN];
  while(readFastqRecord(inFile, inFilename, packedRead, numRead++, identifier, queryQuality)) {
    outFile << identifier << endl;
    outFile << queryQuality << endl;
    outFile.write((char*)packedRead, sizeof (uint64_t) * packedReadLength);
  }
  inFile->close(); delete inFile;
  outFile.close();
  
  return 0;
}
