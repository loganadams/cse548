/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <cassert>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <fstream>
#include <sstream>
#include <iostream>

#include "common.h"
#include "readFastq.h"
#include "SAMoutput.h"

using namespace std;

// TODO Get rid of these
static int refSize;
static char *ref;

static char *getFile(string filename, int *filesize) {
  int fd = open(filename.data(), O_RDONLY);
  assert(fd != -1);

  *filesize = lseek(fd, 0, SEEK_END);
  lseek(fd, 0, SEEK_SET);

  char *ret = (char*)mmap(0, *filesize, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(ret);
  assert(close(fd) == 0);

  return ret;
}

void setRefInput(const char *filename) {
  // mmap reference into memory for hamming dist of CALs
  ref = getFile("chr21.fasta.cmp", &refSize);
}

static void printRead(const uint64_t packedRead[], ostream &out) {
  for (int c=readLength, j=0; c>0; c-=32, j++) {
    printPackedRead(packedRead[j], (c>32)?32:c, out);
  }
}

void outputSAMline(ofstream &outStream, readDescr *read,
		   uint64_t CAL, chrom *chromData,
		   int32_t score, bool rev, bool unmapped,
		   const char *cigarStr,
		   int editDist, const char *mdStr,
                   const char *readGroup) {

  int flags;
  uint64_t chromIndex;
  const char *chrString;
  uint64_t chromPos;
  if (unmapped) {
    flags = 4; // rev and unmapped can't be set at the same time (flags = 20)
    chrString = "*";
    chromPos = -2;
  } else {
    flags = rev ? 16 : 0;
    chromIndex = chromData->find(CAL);
    chrString = chromData->name(chromIndex);
    chromPos = CAL - chromData->firstCal(chromIndex);
  }

  char *quality = (rev) ? read->getQualityRevComp() : read->getQuality();
  char *readStr = (rev) ? read->getReadRevComp() : read->getRead();

  outStream << read->getIdentifier()       // QNAME
	    << "\t" << flags               // FLAG
	    << "\t" << chrString           // RNAME
	    << "\t" << (chromPos+2)        // POS (one indexed, so add an extra)
	    << "\t255"                     // MAPQ  (TODO)
	    << "\t" << cigarStr            // CIGAR
	    << "\t*"                       // RNEXT (TODO)
	    << "\t0"                       // PNEXT (TODO)
	    << "\t0"                       // TLEN  (TODO)
	    << "\t" << readStr             // SEQ
	    << "\t" << quality             // QUAL
	    << "\tNM:i:" << editDist;
  if (mdStr != NULL)
    outStream << "\tMD:Z:" << mdStr;
  if (readGroup != NULL)
    outStream << "\tRG:Z:" << readGroup;
  outStream << "\tAS:i:" << score
	    << endl;
}

void outputSAMHeader(ofstream &outStream, const char *refFilename, string &readGroup) {
  // Write the @SQs to the output file, using the .fasta.fai file for info
  string faiLine, chrName, chrLength;
  string faiFilename(refFilename);
  faiFilename += ".fai";
  ifstream faiFile(faiFilename.c_str(), ifstream::in);

  // Check for the .fai file and attach the SQs if there is one
  if (!faiFile.is_open()) {
    cerr << "\nNo ascii reference index file " << faiFilename << " found\n";
  } else {
    // Read through the index file, and write each line out as an @SQ line
    while(faiFile.good()) {
      getline(faiFile, faiLine);
      // First thing in the line is the chr, next is the length
      istringstream faiStream(faiLine);
      getline(faiStream, chrName, '\t');
      getline(faiStream, chrLength, '\t');
      if(faiStream.good()) {
        outStream << "@SQ\tSN:" << chrName << "\tLN:" << chrLength << endl;
      }
    }
    faiFile.close();
  }

  // Attach any other header information that might be in the header file
  string headFilename(refFilename);
  headFilename += ".head";
  faiFile.open(headFilename.c_str(), ifstream::in);
  if (!faiFile.is_open()) {
    cerr << "\nNo ascii reference header file " << headFilename << " found\n";
  } else {
    // Read through the header file and copy all lines to the sam file
    while(faiFile.good()) {
      getline(faiFile, faiLine);
      // This is currently a wasteful way to do it, but for future processing
      if (faiFile.good()) { // Don't waste lines if read failed
        outStream << faiLine << endl;

        // If the line was a @RG, set the readGroup to it
        if(faiLine.find("@RG") != string::npos) {
          istringstream rgStream(faiLine);
          do {
            getline(rgStream, chrName, '\t'); // Read another section
            if (!rgStream.good()) {
              cerr << "\n** No valid ID: tag found in @RG line **\n";
              exit(EXIT_FAILURE);
            }
          } while(chrName.find("ID:") == string::npos);
          // Now that ID: is found, remove it and keep the read group
          chrName.erase(0, 3); // Erase the first three, which should be "ID:"
          readGroup = chrName;
        }
      }
    }
    faiFile.close();
  }
}
