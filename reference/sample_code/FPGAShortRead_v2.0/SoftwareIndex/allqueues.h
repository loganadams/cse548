/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef ALLQUEUES_H_
#define ALLQUEUES_H_

#include <stdio.h>

extern "C" {
  #include <hugetlbfs.h>
  #include "queue/inline.h"
  #include "queue/sw_queue_astream.h"
}

#include "common.h"
#include "shortread.h"

using namespace std;

// *** Definitions of the inter-thread queues ***
//
// Queue from inThread to tableThreads.
// Indexed by id of the target tableThread.
extern SW_Queue *in_table_qs; 
const uint64_t EndOfAllReads = 0; //read_id that designates we are done.
// Protocol:
//   inThread can choose what pair of tableThreads to send a given read to.
//   Right now that is round-robin.
// Each in_table_qs:
//   Zero or more read records: {
//     <readdescr *>: Pointer to the readDescr object for this read.
//   }
//   EndOfAllReads: says all reads have been processed.
//
// ---------------------------------------------------------------------------
//
// Queue from tableThreads to FPGAcontrolThread.
// Indexed by id of the source tableThread.
extern SW_Queue *table_FPGAcontrol_qs;
// The queue value to signal the end of the CALs for a read.
static const uint64_t EndOfRead = (2ULL << 32);
// Protocol:
//   Zero or more read records: {
//     <readdescr *>: Pointer to the readDescr object for this read.
//     Zero or more CAL records: {
//       <packedCAL>: 64-bit value
//                        [31..0]  CAL
//                        [32]     reverseComp bit
//                        [63..33] unused
//     }
//     EndOfRead: says no more CALs for this read are coming.
//   }
//   EndOfAllReads: says all reads have been processed.
//   
// ---------------------------------------------------------------------------
//
// Queue from FPGAcontrolThread to GetScores thread.
// Indexed by id of FPGA (there is a GetScores for each FPGA).
extern SW_Queue *FPGAcontrol_GetScores_qs;
// Protocol:
//   FPGAcontrolThread can choose what FPGA to send a given read to.
//   Right now that is based on specific reference regions to specific
//   FPGAs, but that is expected to change to provide better load
//   balancing.
// Each FPGAcontrol_GetScores_qs:
//   Zero or more read records: {
//     <readdescr *>: Pointer to the readDescr object for this read.
//     Zero or more CAL records, one for each CAL sent to corresponding FPGA: {
//       <packedCAL>: 64-bit value
//                        [31..0]  CAL
//                        [32]     reverseComp bit
//                        [63..33] unused
//     }
//     EndOfRead: says no more CALs for this read are coming.
//   }
//   EndOfAllReads: says all reads have been processed.
//   
// ---------------------------------------------------------------------------
//

// ---------------------------------------------------------------------------
//
// Internal SmWa send queues.
// Indexed by id of the SmithWaterman unit.
extern SW_Queue *SmWa_send_qs;
// Internal SmWa score queues.
// Indexed by id of the SmithWaterman unit.
extern SW_Queue *SmWa_score_qs;

// Routine that allocates and initializes the queues.
void buildQueues();

// Routine that prints diagnostic information.  Requires the liberty queues to
// have INSTRUMENT_QUEUE_STALLS defined.
void printQueueInfo();

/* Routines for flushing all queues attached to a given thread. */
Inline void InThread_flushAll(sq_cbData dummy) // Parameter unused
{
  for (int i=0; i<NUM_TABLETHREAD_PAIRS*2; i++) {
    sq_flushQueue(in_table_qs[i]);
  }
}

Inline void TableThread_flushAll(sq_cbData id) // Parameter is TableThread id
{
  uint64_t tableId = (uint64_t)id;
  sq_reverseFlush(in_table_qs[tableId]);
  sq_flushQueue(table_FPGAcontrol_qs[tableId]);
}

Inline void FPGAcontrolThread_flushAll(sq_cbData dummy) // Parameter unused
{
  for (int i=0; i<NUM_TABLETHREAD_PAIRS*2; i++) {
    sq_reverseFlush(table_FPGAcontrol_qs[i]);
  }

  for (int j=0; j<SW_NUM; j++) {
    sq_flushQueue(FPGAcontrol_GetScores_qs[j]);
    sq_flushQueue(SmWa_send_qs[j]);
  }
}

Inline void getScores_flushAll(sq_cbData dummy) // Parameter unused
{
  for (int j=0; j<SW_NUM; j++) {
    sq_reverseFlush(FPGAcontrol_GetScores_qs[j]);
    sq_reverseFlush(SmWa_score_qs[j]);
  }
}

#endif // ALLQUEUES_H_
