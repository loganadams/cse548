/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "common.h"
#include "CALtable.h"
#include "ptrTable.h"
#include "hashfunc.h"
#include "readFastq.h"
#include "conversions.h"

using namespace std;

/* Returns the next base in the reference file.  Can be aAgGcCtTnN, or * which means EOF
 */
char getNextRefChar(FILE *inFile)
{
  int val = fgetc(inFile);
  if (val == EOF)
    return '*';
  if (val == '>') {
    for (val = fgetc(inFile); val != '\n'; val = fgetc(inFile));
    return getNextRefChar(inFile);
  }
  if (val == '\n')
    return getNextRefChar(inFile);
  int check = CharToBase(val);
  if (check < 0 || check > 3) {
    cerr << "Unknown base\n";
    exit(EXIT_FAILURE);
  }
  return (char)val;
}

/* Looks through the reference, and makes sure that every value in the reference actually appears in the proper place in the index.
 * Note that the index can have extra bogus entries, but this will help find missing entries.
 * ref - a copy of the reference, in packed form.
 * ref_len - the length of the reference, in bases
 * nth - check only 1/nth of the index.  nth==1, check all, nth==2: check half, etc.
 */
void checkIndex(const char *refFilename, uint64_t ref_len, int nth)
{
  cout << "Checking Indexes against the reference" << flush;

  uint64_t ignored=0; // Number of seeds in reference containing an 'N', and thus ignored
  uint64_t missing=0; // Number of locations in reference not in table and not suppressed
  uint64_t suppressed=0; // Number of locations in reference that hits CAL table at empty location
  uint64_t matched=0; // Number of locations in reference we did find.

  uint64_t minMissing, maxMissing;
  uint64_t lastFound=0;

  uint64_t delta=0; // Just for display

  if (seedLength>32) {
    cerr << "\nCannot handle seed lengths of more than 32 bases\n";
    exit(EXIT_FAILURE);
  }
  if (ref_len < seedLength) {
    cerr << "\n** Reference too short **\n";
    exit(EXIT_FAILURE);
  }

  FILE *refFile = fopen(refFilename, "r");
  if (refFile == NULL) {
    cerr << "\n** Missing ascii reference file " << refFilename << "**\n";
    exit(EXIT_FAILURE);
  }

  uint64_t hasNs=0; // Non-zero if the current seed has an N in it
  uint64_t reverseSeed=0;
  uint64_t forwardSeed=0;

  // Generate the first seed for the reference.
  for(int firstSeed=0; firstSeed < seedLength; firstSeed++) {
    char val = getNextRefChar(refFile);
    if (val == '*') {
      cerr << "\n** Reference file " << refFilename << " is too short **\n";
      exit (EXIT_FAILURE);
    }
    uint64_t bitVal = CharToBase(val);
    uint64_t nVal = (val == 'N' || val == 'n') ? 1 : 0;
    forwardSeed <<= 2;
    forwardSeed |= (bitVal & 3);
    forwardSeed &= (((1ULL<<seedLength)<<seedLength)-1ULL);
    reverseSeed >>= 2;
    reverseSeed |= (((bitVal & 3) ^ 3) << (2*(seedLength-1)));
    hasNs >>= 2;
    hasNs |= ((nVal & 3) << (2*(seedLength-1)));
  }

  bool firstMiss = true;

  for (int64_t CAL=0; CAL<ref_len-seedLength; CAL++) {
    if (CAL%(10*1000*1000) == 0)
      cout << "." << flush;
    bool skipThis = !(CAL%nth == 0); // true if not part of 1/nth I'm checking 

    // Go through all seeds, starting with the first one calculated above
    uint64_t lexSeed, hashedSeed;
    uint64_t isRev;
    if (forwardSeed <= reverseSeed) {
      lexSeed = forwardSeed;
      isRev=0;
    } else {
      lexSeed = reverseSeed;
      isRev=1;
    }
    
    // Hash the seed.
    hashedSeed = hashfunc(lexSeed, seedLength);
    uint64_t whichTable = (hashedSeed >> (2*seedLength-1ULL)) & 1ULL;
    hashedSeed &= ((1ULL << (2*seedLength-1)) - 1ULL);

    ptrTableEntry *targPtrTable = (whichTable == 0) ? evenPtrTable : oddPtrTable;
    CALtableEntry *targCALTable = (whichTable == 0) ? evenCALtable : oddCALtable;

    uint64_t startPointer;
    uint64_t endPointer;
    uint32_t key;
    
    seedToPointers(targPtrTable, hashedSeed & ((1ULL<<(2*seedLength-1)) - 1ULL), 
		   startPointer, endPointer, key);

    bool seedMatch=false; // Is any matching seed found?
    bool calMatch=false; // Did we find this CAL?

    // Just for debugging now.  DELETE ME
    if (!skipThis && endPointer - startPointer > delta*2) {
      delta = endPointer - startPointer;
      cout << delta << " " << flush;
    }

    // Check each entry in CAL table.  Send ones that match
    while (!skipThis && !hasNs && startPointer < endPointer) {
      /* Binary seach for the value.  CAL table sorted by increasing key, then increasing CAL */
      uint64_t midPointer = (startPointer + endPointer)/2;
      CALtableEntry *entry = &(targCALTable[midPointer]);
      if (getKey(entry) < key) {
	startPointer = midPointer+1;
      } else if (getKey(entry) > key) {
	endPointer = midPointer;
      } else {
	seedMatch = true;
	if (getCAL(entry) < CAL) {
	  startPointer = midPointer+1;
	} else if (getCAL(entry) > CAL) {
	  endPointer = midPointer;
	} else {
	  calMatch=true;
	  break;
	}
      }
    }

    // Print diagnostics for the first real miss
    if (!skipThis && !hasNs && firstMiss && seedMatch && !calMatch) {
      cout << "\nFirst miss, CAL: " << CAL 
	   << "\nSeed: ";
      printPackedRead(forwardSeed, seedLength);
      cout << "\n Rev: ";
      printPackedRead(reverseSeed, seedLength);
      cout << "\n" << flush;
      firstMiss = false;
    }

    if (!skipThis) {
      if (hasNs != 0) {
	ignored++;
      } else if (seedMatch) {
	if (calMatch) {
	  matched++;
	  lastFound = CAL;
	} else {
	  if (missing == 1) {
	    minMissing = hashedSeed;
	    maxMissing = hashedSeed;
	  }
	  missing++;
	  if (missing > 1) {
	    if (hashedSeed < minMissing) minMissing = hashedSeed;
	    if (hashedSeed > maxMissing) maxMissing = hashedSeed;
	  }
	}
      } else {
	suppressed++;
      }
    }

    char val = getNextRefChar(refFile);
    if (val == '*') {
      cerr << "\n** Reference file " << refFilename << " ends too soon **\n";
      exit (EXIT_FAILURE);
    }
    uint64_t bitVal = CharToBase(val);
    uint64_t nVal = (val == 'N' || val == 'n') ? 1 : 0;
    forwardSeed <<= 2;
    forwardSeed |= (bitVal & 3);
    forwardSeed &= (((1ULL<<seedLength)<<seedLength)-1ULL);
    reverseSeed >>= 2;
    reverseSeed |= (((bitVal & 3) ^ 3) << (2*(seedLength-1)));
    hasNs >>= 2;
    hasNs |= ((nVal & 3) << (2*(seedLength-1)));
  }

  if (getNextRefChar(refFile) != '*') {
    cout << "\n** Warning: reference file is longer than expected **\n";
  }

  cout << matched*100*nth/ref_len << "% correct, "
       << missing*100*nth/ref_len << "% missing, "
       << suppressed*100*nth/ref_len << "% suppressed, "
       << ignored*100*nth/ref_len << "% ignored.\n" << flush;

  if (missing > 0) {
    cout << "Smallest missing seed: ";
    printPackedRead(minMissing, seedLength);
    cout << "\nLargest missing seed: ";
    printPackedRead(maxMissing, seedLength);
    cout << "\n" << flush;
  }
  cout << "Last found: " << lastFound 
       << ", should be roughly: " << ref_len
       << "\n" << flush;
}
