/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <stdint.h>
#include <numa.h>

extern "C" {
  #include <hugetlbfs.h>
}

//#include "/home/hauck/libhugetlbfs/hugetlbfs.h"

using namespace std;

#include "throughput.h"

// The large table.  Put here so threads can access it
// Since we cannot create one 20GB table, we create multiple smaller ones.
// num_tables is the number of these subtables.  size is the size of each of those subtables
const int lg_num_tables = 2;
const int num_tables = 1<<lg_num_tables;
const int lg_table_size = 28; // 30=16GB, 29=8GB, etc
const uint64_t table_size = 1 << lg_table_size;
int *table[num_tables];

// The number of threads we use.
const int num_threads = 16;
// A well-randomized starting point for each thread.  The thread then uses a fast randomizer from there
uint64_t start_random[num_threads];
// The return values from the threads
int return_vals[num_threads];

// The number of reads per thread we perform
const uint64_t reads_per_thread = 1024*1024*1024LL;

// Masks that specify a node to run on for the even and odd processes.  Used to make sure that
// processes run on the same node as the memory they generally use.
struct bitmask *oddMask;
struct bitmask *evenMask;

/////////////////
// MAIN METHOD //
/////////////////
// testing the memory throughput of the CPU on Grendel
int main(int argc, char* argv[]){
  int                 readThreadRet[num_threads];
  pthread_t           readThreads[num_threads];
  int                 err, return_val;
  time_t              startTime, endTime, tableCreated;
  double              runTime;

  // Make sure NUMA support is available
  if (numa_available() == -1) {
    cerr << "Attempting to use NUMA on a non-NUMA machine\n";
    exit(1);
  }
  if (numa_max_node() != 1) {
    cerr << "Machine should have exactly 2 nodes for NUMA testin\n";
    exit(1);
  }

  cout << "-=-=-=-=-=- NUMA -=-=-=-=-=-\n";

  // Establish nodemasks for putting memory and threads on the right node (socket).
  oddMask = numa_allocate_nodemask();
  evenMask = numa_allocate_nodemask();
  numa_bitmask_setbit(oddMask, 1);
  numa_bitmask_setbit(evenMask, 0);  
  //numa_set_localalloc(); // Allocate to memory on the same node.

  if (0) { // Force everything to be on one socket, for testing purposes.
    oddMask = evenMask;
  }

  /////////////////////////////////////
  // Build a large table to use as target //
  /////////////////////////////////////
  time (&startTime);
  cout << "Per Table size: " << table_size << " ints, sizeof(int): " << sizeof(int)
       << " Total: " << table_size * sizeof(int) / (1024*1024*1024LL) << "GB" 
       << endl;

  cout << "Sizeof(uint64_t): " << sizeof(uint64_t) << endl;

  cout << "Total Table size: " << table_size*num_tables << " ints, number of tables: " << num_tables
       << " Total: " << table_size*num_tables * sizeof(int) / (1024*1024*1024LL) << "GB" 
       << endl;

  // Create the table so that table[j] with j odd is on Node 1, and j even on Node 0.
  // This binding is set up to associate memory with associated threads efficiently.

  const int UseHugePages = 1;

  if (UseHugePages) {
    cout << "Huge pagesize: " << gethugepagesize() << "\n";
  } else {
    cout << "Huge pages disabled\n";
  }

  numa_bind(evenMask);
  for (int j=0; j<num_tables; j+=2) {
    if (UseHugePages)
      table[j] = (int *)get_hugepage_region(sizeof(int)*table_size, GHR_STRICT | GHR_COLOR);
    else
      table[j] = new int[table_size];
    for (uint64_t i=0; i<table_size; i++) {
      table[j][i] = i;
    }
  }

  numa_bind(oddMask);
  for (int j=1; j<num_tables; j+=2) {
    if (UseHugePages)
      table[j] = (int *)get_hugepage_region(sizeof(int)*table_size, GHR_STRICT | GHR_COLOR);
    else
      table[j] = new int[table_size];
    for (uint64_t i=0; i<table_size; i++) {
      table[j][i] = i;
    }
  }

  time (&tableCreated);
  runTime = difftime (tableCreated,startTime);  
  cout << "Table Creation Runtime " << runTime << " seconds" << endl;

  /////////////////////////
  // Set up random numbers for starting points - we use a quick randomizer in the threads,
  // but we use a good one here to get them started.
  ////////////////////////
  srandom(1);
  for (int i=0; i<num_threads; i++) {
     start_random[i] = random();
     start_random[i] = (start_random[i] << 32) ^ (uint64_t)random();
  }

  ////////////////////////////
  // Create the Threads     //
  ////////////////////////////
  cout << "Number of threads: " << num_threads << endl;
  cout << "Reads per thread: " << reads_per_thread << endl;

  // This can swap or change the processor binding, to make sure we see performance drops.
  if (0) {
    cout << "--> SWAPPING NUMA - mismatch of CPU and Memory allocations!\n";
    struct bitmask *swapMask = oddMask;
    oddMask = evenMask;
    evenMask = swapMask;
  }
  if (0) {
    cout << "--> NUMA OVERLOAD - putting all CPUs on one socket!\n";
    oddMask = evenMask;
  }

  numa_bind(oddMask);
  for (uint64_t i=1; i<num_threads; i+=2) {
    //readThreadRet[i] = pthread_create(&readThreads[i], NULL, readMemory, (void*) i);
    readThreadRet[i] = pthread_create(&readThreads[i], NULL, numa_readMemory, (void*) i);
    //readThreadRet[i] = pthread_create(&readThreads[i], NULL, numa_prefetch_readMemory, (void*) i);
    //readThreadRet[i] = pthread_create(&readThreads[i], NULL, numa_prefetch_readMemory_slowed, (void*) i);
  }

  numa_bind(evenMask);
  for (uint64_t i=0; i<num_threads; i+=2) {
    //readThreadRet[i] = pthread_create(&readThreads[i], NULL, readMemory, (void*) i);
    readThreadRet[i] = pthread_create(&readThreads[i], NULL, numa_readMemory, (void*) i);
    //readThreadRet[i] = pthread_create(&readThreads[i], NULL, numa_prefetch_readMemory, (void*) i);
    //readThreadRet[i] = pthread_create(&readThreads[i], NULL, numa_prefetch_readMemory_slowed, (void*) i);
  }
  time (&startTime);

  ///////////////////////////
  // Join with Read Thread //
  ///////////////////////////
  for (int i=0; i<num_threads; i++) {
    pthread_join(readThreads[i], NULL);
  }
  time (&endTime);
  
  runTime = difftime (endTime,startTime);
  cout << "Table: " << table_size*num_tables * sizeof(int) / (1024*1024*1024LL) << "GB" 
       << " Reads/thread: " << reads_per_thread/(1000*1000) << "M"
       << " # threads: " << num_threads
       << " readMemory " << runTime << " seconds"
       << " Throughput: " << reads_per_thread*num_threads/runTime/(1000*1000) << "M reads/second" << endl;
  return 0;
}

uint64_t LFSR_36(uint64_t state) {
  // a 36-bit LFSR can be built from a system that XORs positions 35,24 back to the bottom bit
  // Meant as a very fast, random-enough-to-fool-a-prefetcher function
  const uint64_t mask = ((uint64_t)1 << 36) - 1;

  uint64_t bit = ((state>>35) ^ (state>>24)) & 1;
  uint64_t result = ((state << 1) | bit) & mask;
  
  return result;
}

// Test thread that reads memory randomly
void * readMemory(void* ptr){
  uint64_t id = (uint64_t)ptr;

  uint64_t state = LFSR_36(start_random[id]);
  uint64_t count;

  stringstream message;
  message << "Running subthread " << id << endl;
  cout << message.str();

  int max = 0;
  for (state = LFSR_36(state), count = reads_per_thread;
       count > 0;
       state = LFSR_36(state), count--) {
    //if ((count % 1000000000) == 0) cout << count/1000000000 << "G" << endl;

    uint64_t index = state & ((1LL<<lg_table_size)-1);
    int table_index = (state>>lg_table_size) & ((1<<lg_num_tables)-1);
    int value = table[table_index][index];
    max += value;
    //uint64_t index_old = state % table_size;
    //int table_index_old = (state/table_size) % num_tables;
  }

  // Need to have side-effects, or it will remove this part of the code.
  //cout << max;
  return_vals[id] = max;
  /*for (state = LFSR_36(state); state != first; state = LFSR_36(state), period++) {
    //cout << state << endl;
    if ((period % 1000000000) == 0) cout << period/1000000000 << "G" << endl;
  }

  cout << "Period: " << period << endl;
  */
}

// Test thread that reads memory randomly.  It does use the numa policy - an even thread only
// accesses table[even entries], and an odd thread only accesses table[odd entries].
void * numa_readMemory(void* ptr){
  uint64_t id = (uint64_t)ptr;
  uint isOdd = id & 1;

  uint64_t state = LFSR_36(start_random[id]);
  uint64_t count;

  stringstream message;
  message << "Running subthread " << id << " prefers " << numa_preferred() << endl;
  cout << message.str();

  int max = 0;
  for (state = LFSR_36(state), count = reads_per_thread;
       count > 0;
       state = LFSR_36(state), count--) {
    //if ((count % 1000000000) == 0) cout << count/1000000000 << "G" << endl;

    uint64_t index = state & ((1LL<<lg_table_size)-1);
    int table_index = (state>>lg_table_size) & ((1<<lg_num_tables)-1);
    table_index = (table_index & 126) + isOdd;
    int value = table[table_index][index];
    max += value;
    //uint64_t index_old = state % table_size;
    //int table_index_old = (state/table_size) % num_tables;
  }

  // Need to have side-effects, or it will remove this part of the code.
  //cout << max;
  return_vals[id] = max;
  /*for (state = LFSR_36(state); state != first; state = LFSR_36(state), period++) {
    //cout << state << endl;
    if ((period % 1000000000) == 0) cout << period/1000000000 << "G" << endl;
  }

  cout << "Period: " << period << endl;
  */
}

// To prefetch, we figure out the addresses in advance, prefetch them, and store
// the addresses in the buffer.  Then, we start using the addresses from the buffer.
const int prefetch_depth = 16; // Must be a power of 2

// Test thread that reads memory randomly.  It does use the numa policy - an even thread only
// accesses table[even entries], and an odd thread only accesses table[odd entries].
// Instead of reading immediately, it pipelines the operation.  That is, the address calculator
// races ahead prefetching locations.  We don't currently stash those addresses, but could (it
// is easier here to just recalculate the target).
void * numa_prefetch_readMemory(void* ptr){
  uint64_t id = (uint64_t)ptr;
  uint isOdd = id & 1;

  uint64_t state = LFSR_36(start_random[id]);
  uint64_t state_prefetched = state;
  uint64_t count;

  stringstream message;
  message << "Running prefetching subthread " << id << " prefers " << numa_preferred() << endl;
  cout << message.str();

  // Jump the prefetch state ahead of the normal accesser.  Could actually prefetch these,
  // but simpler to ignore these startup issues.
  for(int dist=0; dist<prefetch_depth; dist++)
    state_prefetched = LFSR_36(state_prefetched);

  int max = 0;
  for (state = LFSR_36(state), state_prefetched = LFSR_36(state_prefetched), count = reads_per_thread;
       count > 0;
       state = LFSR_36(state), state_prefetched = LFSR_36(state_prefetched), count--) {

    uint64_t index = state & ((1LL<<lg_table_size)-1);
    int table_index = (state>>lg_table_size) & ((1<<lg_num_tables)-1);
    table_index = (table_index & 126) + isOdd;
    int value = table[table_index][index];
    max += value;

    uint64_t index_pref = state_prefetched & ((1LL<<lg_table_size)-1);
    int table_index_pref = (state_prefetched>>lg_table_size) & ((1<<lg_num_tables)-1);
    table_index_pref = (table_index_pref & 126) + isOdd;
    __builtin_prefetch(&table[table_index_pref][index_pref],0,0);
  }

  return_vals[id] = max;
}

const int slowFactor = 10; // >= 1

// Run the randomizer multiple times in a row, based on slowFactor.  Designed to see if/when we
// become compute-bound, via numa_prefetch_readMemory_slowed
uint64_t LFSR_36_slowed(uint64_t state) {
  for (int i=0; i<slowFactor; i++)
    state = LFSR_36(state);
  return state;
}

// Same as numa_prefetch_readMemory EXCEPT we double-call the randomizer.  If this slows the
// computation down, we are compute-bound.  If not, we are memory-bound.
void * numa_prefetch_readMemory_slowed(void* ptr){
  uint64_t id = (uint64_t)ptr;
  uint isOdd = id & 1;

  uint64_t state = LFSR_36_slowed(start_random[id]);
  uint64_t state_prefetched = state;
  uint64_t count;

  stringstream message;
  message << "Running prefetching subthread " << id << " prefers " << numa_preferred() << endl;
  cout << message.str();

  // Jump the prefetch state ahead of the normal accesser.  Could actually prefetch these,
  // but simpler to ignore these startup issues.
  for(int dist=0; dist<prefetch_depth; dist++)
    state_prefetched = LFSR_36_slowed(state_prefetched);

  int max = 0;
  for (state = LFSR_36_slowed(state), state_prefetched = LFSR_36_slowed(state_prefetched), count = reads_per_thread;
       count > 0;
       state = LFSR_36_slowed(state), state_prefetched = LFSR_36_slowed(state_prefetched), count--) {

    uint64_t index = state & ((1LL<<lg_table_size)-1);
    int table_index = (state>>lg_table_size) & ((1<<lg_num_tables)-1);
    table_index = (table_index & 126) + isOdd;
    int value = table[table_index][index];
    max += value;

    uint64_t index_pref = state_prefetched & ((1LL<<lg_table_size)-1);
    int table_index_pref = (state_prefetched>>lg_table_size) & ((1<<lg_num_tables)-1);
    table_index_pref = (table_index_pref & 126) + isOdd;
    __builtin_prefetch(&table[table_index_pref][index_pref],0,0);
  }

  return_vals[id] = max;
}
