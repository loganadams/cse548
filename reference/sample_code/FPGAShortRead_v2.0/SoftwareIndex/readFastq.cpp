/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <iostream>
#include <fstream>

using namespace std;

#include "readFastq.h"
#include "conversions.h"

// Opens a Fastq file, making it ready for subsequent calls to readFastqRecord.
ifstream *openFastqFile(const char *filename)
{
  ifstream *fastqFile = new ifstream(filename);
  if (!fastqFile || !fastqFile->good()){
    cerr << "Unable to open " << filename << endl;
    exit(EXIT_FAILURE);
  }
  return fastqFile;
}
    
// Reads a record from the ifstream provided.  filename is the name of the file,
// strictly used for error messages.  numRead is the number of reads already
// read from this file.  maxN is a filter - any read with more than maxN
// N's in it is silently rejected, and the reader moves to the next read.
//
// The function returns a readDescr with the line's data if the read worked,
// or NULL if EOF is encountered.
//
// To pack the read, bases are converted by CharToBase into a 2-bit code.  The
// first base in the read is put into the bottom two bits of packedRead.
// The next base is in the next two bits.  After 32 bases, we move to the next
// entry in packedRead, again putting the 33rd base into the bottom two bits.
// In the common case that the read length is not a multiple of 32, the final
// value will have data in the lower bits, and 0's in the upper bits.
//
// Note that if the reads in the file are longer than the readLength constant,
// only the first "readLength" bases will be converted.  If the reads in the
// file are shorter, the missing bases are assumed to encode as 00.
readDescr *readFastqRecord(ifstream *readFile, const char *filename,
			   const uint64_t numRead, const int maxN)
{
  // The four lines of a record in FASTQ format:
  // identifier (output param)
  char identifier[MAX_FASTQ_LINE_LEN];
  char read[MAX_FASTQ_LINE_LEN];
  char line3[MAX_FASTQ_LINE_LEN]; // generally just a + symbol, or padding;
  char quality[MAX_FASTQ_LINE_LEN];
  uint64_t packedRead[readLength/32+1]; 
  uint64_t *packedReadPtr;
  int numNs;
  // quality (output param)

  do {
    /////////////////////////////////////////////////////////////
    // fastq file is 4 lines per read, read is on the 2nd line //
    /////////////////////////////////////////////////////////////
    readFile->getline(identifier, MAX_FASTQ_LINE_LEN);
    if (readFile->eof())
      return NULL;
    readFile->getline(read, MAX_FASTQ_LINE_LEN);
    readFile->getline(line3, MAX_FASTQ_LINE_LEN);
    readFile->getline(quality, MAX_FASTQ_LINE_LEN);

    if (readFile->fail()) {
      cerr << "Fastq file " << filename << " at line " << numRead*4+1 << " has an incomplete record\n";
      exit(EXIT_FAILURE);
    }

    // Error checking
    if (identifier[0] != '@') {
      cerr << "Fastq file " << filename << " at line " << numRead*4+1 << " should have an identifier "
           << "line starting with '@'.\n"
           << "  Actual line: " << identifier;
      exit(EXIT_FAILURE);
    }
    if (line3[0] != '+') {
      cerr << "Fastq file " << filename << " at line " << numRead*4+1 << " should have a "
           << "line starting with '+'.\n"
           << "  Actual line: " << identifier;
      exit(EXIT_FAILURE);
    }

    if (strlen(read) != readLength) {
      cerr << "Fastq file " << filename << " at line " << numRead*4+2
           << " has read length " << strlen(read)
           << ", should be " << readLength << "\n";
      exit(EXIT_FAILURE);
    }

    uint64_t packed=0;
    int pos=0;
    int i=0;
    numNs=0;
    packedReadPtr = packedRead;
    for(i=0; i<readLength; i++) {
      if (read[i]=='n' || read[i]=='N') numNs++;
      uint64_t value = CharToBase(read[i]) & 3;
      packed |= (value << pos);
      pos += 2;
      if (i%32 == 31) {
        pos = 0;
        *packedReadPtr = packed;
        packed = 0;
        packedReadPtr++;
      }
    }
    if (i%32 != 0)
      *packedReadPtr = packed; // Save last fraction of a read

  } while (numNs > maxN); // Skip reads with too many N's
//      cout << "Too many Ns " << numNs << " max is " << maxN << " numRead " << numRead << " read is " << read << endl;
//    return readFastqRecord(readFile, filename, numRead, maxN);
//  }
//  else
  return new readDescr(numRead, &(identifier[1]), read, quality,
                       packedRead, readLength);
}

// FASTQ files are very big, with an ascii coding of values, quality scores, and other
// elements.  We alternatively use compressed files, where reads are packed as tightly
// as possible (i.e. in the packedRead format itself, as binary numbers instead of characters).

// Opens a Compressed Fastq file, making it ready for subsequent calls to readCompressedRecord.
ifstream *openCompressedFile(const char *filename)
{
  // Open the file
  ifstream *compFile = new ifstream(filename);
  if (!compFile || !compFile->good()){
    cerr << "Unable to open " << filename << endl;
    exit(EXIT_FAILURE);
  }
  
  // Consume the header
  char buffer[1000];
  compFile->getline(buffer, 1000); // Read the source line
  if (strncmp(buffer, ">>SourceFastq:", 14) != 0) {
    cerr << "Invalid compressed FASTQ file " << filename << "\n";
    exit (EXIT_FAILURE);
  }
  compFile->getline(buffer, 1000); // Read the Readlength line
  if (strncmp(buffer, ">>Readlength: ", 14) != 0) {
    cerr << "Invalid compressed FASTQ file " << filename << "\n";
    exit (EXIT_FAILURE);
  }

  // Make sure the file has reads of the same length as we expect
  long fileReadlength = atol(&(buffer[14]));
  if (fileReadlength != readLength) {
    cerr << "Invalid read length, expected " << readLength << "\n";
    cerr << "  File " << filename << " has reads of length " << fileReadlength << "\n";
    exit (EXIT_FAILURE);
  }

  return compFile;
}
    
// Reads a record from the ifstream provided.  filename is the name of the file, strictly used
// for error messages.  packedRead should be an array long enough to take the read in packed
// format. numRead is the number of reads already read from this file (for error messages)
//
// The function returns true if the read worked, or
// 0 if EOF is encountered 
bool readCompressedRecord(ifstream *readFile, const char *filename,
                          uint64_t *packedRead, const uint64_t numRead,
                          char *identifier, char *queryQuality)
{
  if (readFile->eof())
    return false;

  uint32_t packedReadLength = readLength/32;
  if (packedReadLength*32<readLength)
    packedReadLength++;

  readFile->getline(identifier, MAX_FASTQ_LINE_LEN);
  readFile->getline(queryQuality, MAX_FASTQ_LINE_LEN);
  readFile->read((char *)packedRead, sizeof (uint64_t) * packedReadLength);

  /*
  // Print the read
  int c, j;

  if (numRead >= 54448960) {
    cout << numRead << "{";
    for (c=readLength, j=0; c>0; c-=32, j++) {
      printPackedRead(packedRead[j],(c>32)?32:c);
    }
    cout << "} " << readFile->gcount() << "\n";
  }
  */

  if (readFile->gcount() == 0)
    return false;

  if (readFile->gcount() != sizeof (uint64_t) * packedReadLength) {
    cerr << "Error reading compressed FASTQ file " << filename << " at read: " << numRead << endl;
    exit(EXIT_FAILURE);
  }

  return true;
}

// Prints the first "length" set of bases represented by the specified value.
// Length will normally be 32, except for the final value of the set
void printPackedRead(uint64_t value, int length, ostream &out)
{
  while (length>0) {
    out << BaseToChar(value & 3);
    length--;
    value = value >> 2;
  }
}

void printPackedRead(uint64_t value, int length) {
  printPackedRead(value, length, cout);
}

void printPackedReadMultiWord(uint64_t *readArray, int length, ostream &out) {
  while (length>=33) {
    printPackedRead(readArray[0], 32, out);
    length -= 32;
    readArray++;
  }
  printPackedRead(readArray[0], length, out);
}

/* Helper for unpackReadMultiWord */
void unpackRead(char *targ, uint64_t value, int length)
{
  while (length>0) {
    targ[0] = BaseToChar(value & 3);
    length--;
    value = value >> 2;
    targ++;
  }
}

/*
 * Unpackes the read in "source" into the character buffer "targ".  It skips
 * the first "offset" bases from the beginning.  It then prints "numBases"
 * bases into the targ buffer.  It then null terminates the array.
 */
void unpackReadMultiWord(char *targ, uint64_t *source,
			 uint64_t offset, int numBases)
{
  targ[numBases] = '\0';

  if (offset >= 32) {
    source += offset/32;
    offset = offset % 32;
  }

  unpackRead(targ, (source[0])>>(2*offset), 32-offset);
  numBases -= (32-offset);
  source++;
  targ += 32-offset;
  
  while (numBases>32) {
    unpackRead(targ, source[0], 32);
    numBases -= 32;
    source++;
    targ += 32;
  }
  if (numBases > 0)
    unpackRead(targ, source[0], numBases);
}

// Testing routine - converts a read in character array form into a packed
// read.
uint64_t *stringToPackedRead(char *str)
{
  int len = strlen(str);
  int readSize = len/32;
  if (readSize*32 < len) readSize++;
  
  uint64_t *result = new uint64_t[readSize];
  uint64_t *packedRead = result;

  uint64_t packed=0;
  int pos=0;
  int i=0;
  for(i=0; i<len; i++) {
    uint64_t value = CharToBase(str[i]) & 3;
    packed |= (value << pos);
    pos += 2;
    if (i%32 == 31) {
      pos = 0;
      *packedRead = packed;
      packed = 0;
      packedRead++;
    }
  }
  if (i%32 != 0)
    *packedRead = packed; // Save last fraction of a read
  return result;
}
