/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include "common.h"
#include "ptrTable.h"
#include "CALtable.h"
#include "conversions.h"

extern "C" {
  #include <hugetlbfs.h>
}

using namespace std;

// The real ptrTables, holding the data we want
// oddPtrTable is in memory on socket 1, evenPtrTable on socket 0.
// There are numPtrTableEntries/2 entries per table (defined in common.h), plus 1 for the
// "end" of the final entry in each table (i.e. you want the start and end CALtable position for
// each seed, so you need an extra entry for the final seed in each table).
ptrTableEntry *oddPtrTable, *evenPtrTable;
uint64_t entriesPerTable = 0; // length of oddPtrTable & evenPtrTable, including the extra 1 at end.

#ifdef PTR_TABLE_USE_HUGE_PAGES
#define PTR_TABLE_ALLOC(size) (ptrTableEntry *)get_hugepage_region((size), GHR_STRICT | GHR_COLOR)
#else // PTR_TABLE_USE_HUGE_PAGES
#define PTR_TABLE_ALLOC(size) (ptrTableEntry *)malloc((size))
#endif // PTR_TABLE_USE_HUGE_PAGES

// Helper for initPtrTable.  Reads compressed entries and decompresses as it loads them.
// Note that it will double-write entries, since the compressed form holds data[0]..data[16],
// while the next entry holds data[16]..data[32], etc.
void readPtrTableEntry(ifstream &pointerFile, ptrTableEntry *Table, uint64_t address)
{
  uint64_t pointer, offset;

  pointerFile >> pointer;
  Table[address] = pointer;
  for(int i=1;i<=16;i++){
    pointerFile >> offset;
    Table[address+i] = pointer+offset;
  }
}

// Initialized the ptrTable data structures.  ptrFile is the name of the file with the 
// pointer table.  The two masks are NUMA bitmasks used to make sure we allocate memories onto
// the appropriate socket's memory
//
// This also initializes the lengths of the CALtables.
void initPtrTables(const char* ptrFile, bitmask *oddMask, bitmask *evenMask)
{
  uint64_t address;
  entriesPerTable = numPtrTableEntries/2 + 1; // Add one for endptr of final seed bucket

  cout << "Creating pointer tables, each "
       << (sizeof(ptrTableEntry)*numPtrTableEntries/2)
       << endl;

  /*
  cout << "Testing huge pages:\n"; cout.flush();
  numa_bind(oddMask);
  cout << "Odd CPU 2.0GB... "; cout.flush();
  if (PTR_TABLE_ALLOC((size_t)1024*1024*1024*2)) cout << "Worked!\n"; else cout << "Failed!\n";
  */

  // create a new hash table with the correct # of entries
  numa_bind(oddMask);
  oddPtrTable = PTR_TABLE_ALLOC(sizeof(ptrTableEntry)*entriesPerTable);

  numa_bind(evenMask);
  evenPtrTable = PTR_TABLE_ALLOC(sizeof(ptrTableEntry)*entriesPerTable);

//cout << "ptrTableEntry start: " << (uint64_t)evenPtrTable << " %1024: " << (uint64_t)evenPtrTable%1024 << endl;

  if (evenPtrTable == NULL || oddPtrTable == NULL) {
    cerr << "Unable to allocate storage for the pointer table - are hugepages set up correctly?\n";
    exit(EXIT_FAILURE);
  }

  // populate the ptrTable from file
  ifstream pointerFile(ptrFile);
  if (!pointerFile){
    cerr << "Unable to open pointerTable file " << ptrFile << "\n";
    exit(EXIT_FAILURE);
  } 
  
  numa_bind(evenMask);
  for (address=0; address<entriesPerTable-1; address+=16) {
    if (pointerFile.eof()) {
      cerr << "Ran out of entries in pointerTable " << ptrFile << " near entry " << address << endl;
      exit(EXIT_FAILURE);
    } 
    readPtrTableEntry(pointerFile, evenPtrTable, address);
  }

  numa_bind(oddMask);
  for (address=0; address<entriesPerTable-1; address+=16) {
    if (pointerFile.eof()) {
      cerr << "Ran out of entries in pointerTable " << ptrFile 
	   << " near entry " << address+entriesPerTable << endl;
      exit(EXIT_FAILURE);
    }
    readPtrTableEntry(pointerFile, oddPtrTable, address);
  }

  // Use the data from the ptrTables to figure out the length of the CAL tables.
  evenCALtable_len = evenPtrTable[entriesPerTable-1];
  oddCALtable_len = oddPtrTable[entriesPerTable-1] - evenCALtable_len;

  // Adjust the oddPtrTable start pointers, given that the CAL table is split into odd
  // and even portions.
  uint64_t test=0;
  for (address=0; address<entriesPerTable; address++) {
    if (oddPtrTable[address] < test) {
      cerr << "** PtrTable not monotonically increasing.  OddPtr[" << (address-1)
	   << "] = " << test
	   << ", OddPtr[" << address
	   << "] = " << oddPtrTable[address]
	   << " **\n";
      exit(EXIT_FAILURE);
    }
    test = oddPtrTable[address];
    oddPtrTable[address] -= evenCALtable_len;
  }
  
  uint64_t pointer;
  if (pointerFile >> pointer) {
    cerr << "Extra entries in pointerTable " << ptrFile << endl;
    exit(EXIT_FAILURE);
  } 
  
  pointerFile.close();
}

// Prints a seed given in the value field.  Seeds have their last base at the bottom bits, first
// base at the upper bits, in order.  Padding is a number of spaces to add to the front.
void printSeed(uint64_t value, int padding)
{
  int index;
  for (index = padding; index>0; index--)
    cout << " ";
  for (index=0; index<seedLength; index++) {
    cout << BaseToChar((int) ((value>>(2*(seedLength-1))) & 3));
    value <<= 2;
  }
}

// Function to look up a seed's values in the pointer table.  It is assumed that the right
// table (oddPtrTable, evenPtrTable) has already been picked for the seed and sent in.
// Table is the table we are indexing into.
// Seed is the seed we are looking up.  It should already have been hashed.
// startPointer is a pointer to the start of the bucket of CALs for this seed in the CALtable,
// endPointer is a pointer to the start of the next bucket.
// key is the key to use to look up values in that CAL table.
void seedToPointers(ptrTableEntry *Table, const uint64_t hashedSeed,
		    uint64_t &startPointer, uint64_t &endPointer, uint32_t &key)
{
  key = hashedSeed & ((1ULL<<keyBits)-1ULL);
  uint64_t address = hashedSeed >> (1ULL*keyBits);

  // Temporary check
  /*
  if (!(address < entriesPerTable-1)) {
    cout << "Error: addr: " << address
	 << ", ePerT: " << entriesPerTable
	 << " hashedSeed: " << hashedSeed
	 << " keyBits: " << keyBits
	 << "\n";
  }
  */
  assert(address < entriesPerTable-1);

  startPointer = Table[address];
  endPointer = Table[address+1];

  //cout << "Value: " << hashedSeed << " Addr: " << address << " Key: " << key << endl;
}
