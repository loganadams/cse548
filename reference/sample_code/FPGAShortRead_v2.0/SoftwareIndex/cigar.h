/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Software aligner to create CIGAR strings (says how a read and
 * reference were aligned in detail).
 * Based on Corey Olson's code.
 */

#ifndef CIGAR_H_
#define CIGAR_H_

#define C_GAP_EXTEND -1
#define C_GAP_OPEN -2
#define C_MATCH 2
#define C_MISMATCH -2

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <string>

#define MAX_READ_LENGTH 500
#define MAX_REF_LENGTH (MAX_READ_LENGTH+100)

/*
 * CIGAR strings are alignment information for the SAM/BAM format.  Details
 * in http://samtools.sourceforge.net/SAM1.pdf
 *
 * Basically, there is a string of numbers and letters M, D, I.  For example
 * "8M2I4M1D3M" means 8 bases are a matched (i.e. are the same, or are different
 * but the best alignment assumes they are a mismatch/substitution), then 2 are
 * insertions in the read not found in the reference, 4 match/mismatch,
 * 1 deletion in the read of a base that was in the reference, and 3
 * match/mismatch.  There are other things in the format, but that is the
 * subset we use.
 */

/* cigar is a reusable object for creating cigar strings.  Creating the
 * object sets up the proper data structures.
 */

class cigarGen {
 private:
  // The length of the reads and references this unit will process
  int readlen, reflen;

  // Similarity is the main SmithWaterman score
  short similarity[MAX_READ_LENGTH+1][MAX_REF_LENGTH+1];

  short readGap[MAX_READ_LENGTH+1][MAX_REF_LENGTH+1];
  short refGap[MAX_READ_LENGTH+1][MAX_REF_LENGTH+1];
  // Indicates whether the cell is considered an I, M, or D.  * means the start
  // of an alignment.  Instead of M, we use = for match, X for mismatch.
  // '.' is a bogus value for things outside of the band for banded alignment.
  // \0 is an unset value
  char direction[MAX_READ_LENGTH+1][MAX_REF_LENGTH+1];

  // We keep a buffer to hold all cigarStrings, so we don't have to keep
  // allocating it.
  char *cigarString;
  char *cigarStringEnd; // Points to the \0 at the end

  // Same as cigarString, but for MD strings
  char *mdString;

  char *perfectMDstring;

  // Lists the per-base operations that get compressed into an MD string.
  // During CIGAR creation, we push individual operations into the transcript,
  // in reverse order (since backtrace does the storing):
  // M - match
  // A/C/G/T - mismatch, giving the base
  // A^/C^/G^/T^ - delete from reference (2-character operation
  // Insertions not included - MD doesn't use those.
  char *transcript;

  // Initializes all scoring matrices to a good known value.  Called by the
  // constructor, and likely never needs to be called again.
  void initMatrices();

  // Helper for computeCIGAR.  Outputs the MD string for a matching with
  // indels.  Uses the transcript to guide it, so the transcript should be
  // filled in first (see header for details).
  //
  // transIndex - input, points to the position 1 past where the transcript
  // data ends.
  const char *computeMDindels(char *transIndex);

  //Helper function for alignRead
  void computeSquare(int i, int j, char *read, char *reference);

  // aligns the current read against the current reference.
  // Assumes the entries [0][*] and [*][0] are already set via initMatrices(),
  // but multiple alignReads calls can use the same initial initMatrices(). 
  // 
  // NOTE: read and ref must use the same character set, and only exact
  // matches will be counted.  So, if there is a mix of upper and lower
  // case values ("agCCTA") it will not match a=A.  Also, N characters if
  // they are present will be matched to other N's, and nothing else.
  void alignRead(char *read, char *reference);

  // Same as alignRead, but faster because it is banded - by knowing the
  // best answer position and score, we compute what portion of the alignment
  // array is actually needed to find the alignment.
  void alignReadBanded(char *read, char *reference,
		       int position, int score);

 public:
  // The read_length and ref_length fix the size of elements compared in this
  // cigar generator.
  cigarGen(int read_length, int ref_length);
  ~cigarGen();
  
  // Compute and return the CIGAR string.  Score must
  // be the score of aligning the result with the read - code can use
  // the score to band the portion of the alignment it must do.
  //
  // read and reference are character arrays containing strings to compare.
  // Generally they will be sequences of {'A','G','C','T'}, though this code
  // simply says that if ref[i]==read[i] it is a match, and a mismatch
  // otherwise.
  //
  // endBase is an input that says which base of the reference is the last one
  // that aligns with the last base of the read.  If 0, we compute it from
  // scratch, but if 1 or more we can do a faster calculation using this info.
  // The position is relative to start of the reference (i.e. 1 is the second
  // base of the reference).
  //
  // score is the score found by the hardware for this alignment.  It is used
  // to calculate the band in banded alignment, and for error checking. If
  // the software finds a higher score, it updates the score parameter to 
  // this new value.
  //
  // startBase returned as the position (o..strlen(reference)-1) in the
  // reference parameter that is the start of the CIGAR alignment.  Thus,
  // if this code says N, then first base of the read aligns to reference[N].
  //
  // editDist is an output of the edit distance along the CIGAR alignment.  Each
  // mismatch, insertion, or deletion counts as one. 
  //
  // MDstring: returns the optional MD string for SAM output
  //
  // NOTE: the char string returned, and the MDstring, will be reused for
  // further calls to computeCIGAR on this object, so copy or otherwise finish
  // using the string before calling this again.
  //
  // NOTE2: Can increase the score if the SW finds a better score than the HW
  // (can happen if SW sees a larger portion of the reference than the HW)
  // 
  // NOTE3: read and ref must use the same character set, and only exact
  // matches will be counted.  So, if there is a mix of upper and lower
  // case values ("agCCTA") it will not match a=A.  Also, N characters if
  // they are present will be matched to other N's, and nothing else.
  const char *computeCIGAR(char *read, char *reference, int64_t endBase,
			   int32_t &score, uint64_t &startBase,
			   int &editDist, const char *&MDstring);
  
  // Print internal status of the generator.  For debugging.
  void printMatrices(char *read, char *reference);

  // For cases where there are no indels, this routine returns the corresponding
  // MD string.  Doesn't need to do alignment, just base-by-base comparison.
  //
  // NOTE: only does EXACT matches, so A and a don't match, and N only matches
  // N.
  // NOTE2: reuses the MDstring buffer with other calls, so string will be
  // overwritten by subsequent calls to computeMDnoIndel or computeCIGAR
  const char *computeMDnoIndel(char *read, char *reference);

  // Output the MD string for a perfect matching (i.e. "76" for 76-base reads)
  const char *computeMDperfect() {
    return perfectMDstring;
  }
};

#endif /* CIGAR_H_ */
