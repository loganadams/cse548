#!/bin/tcsh
#

sed -n '1~4p' $1 | awk -F"1:N:" '{print substr($1,2) 0}' > temp_um.reads
awk -F"1:N:" '{print $1}' $2 > temp.reads
awk -F"AS:i:" '{print $2}' $2 > temp.scores
paste -d" " temp.reads temp.scores > temp.scored
cat temp.scored temp_um.reads | sort
