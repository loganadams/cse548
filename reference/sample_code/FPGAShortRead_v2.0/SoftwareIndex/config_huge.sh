#!/bin/bash
# Script to i
# Initialize the huge pages setup.  Must be run as root.  This takes a long time to run.
# be patient.

hugeadm --pool-pages-min DEFAULT:4G
hugeadm --pool-pages-max DEFAULT:4G
hugeadm --pool-list
