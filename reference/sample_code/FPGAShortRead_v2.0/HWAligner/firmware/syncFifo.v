/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// simple parameterized synchronous fifo
module syncFifo #(
    parameter WIDTH = 1,
    parameter DEPTH = 2 // must be a power of 2
    )
    (
    input               clk,
    input               rst,

    input   [WIDTH-1:0] din,
    input               wr_en,
    output              full,

    output  [WIDTH-1:0] dout,
    input               rd_en,
    output              empty
    );
    
    ///////////////
    // FUNCTIONS //
    ///////////////
    // computes ceil( log( x ) ) 
    function integer clogb2;
        input [31:0] value;
        begin
            value = value - 1;
            for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1) begin
                value = value >> 1;
            end
        end
    endfunction
    
    //////////////////
    // LOCAL PARAMS //
    //////////////////
    localparam LOG_DEPTH = clogb2(DEPTH);

    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////
    reg     [LOG_DEPTH:0]   wrPtr;
    reg     [LOG_DEPTH:0]   rdPtr;
    reg     [WIDTH-1:0]     data    [0:DEPTH-1];

    integer                 i;

    ///////////
    // LOGIC //
    ///////////
    assign empty    = (wrPtr == rdPtr);
    assign full     = (wrPtr[LOG_DEPTH] != rdPtr[LOG_DEPTH]) && 
                      (wrPtr[LOG_DEPTH-1:0] == rdPtr[LOG_DEPTH-1:0]);
    assign dout     = data[rdPtr[LOG_DEPTH-1:0]];

    // update the read pointer for a read
    always @ (posedge clk) begin
        if (rst) begin
            rdPtr       <= 0;
        end else if (rd_en && !empty) begin
            rdPtr       <= rdPtr + 1;
        end
    end

    // perform a write
    always @ (posedge clk) begin
        if (rst) begin
            wrPtr                       <= 0;
        end else if (wr_en && !full) begin
            data[wrPtr[LOG_DEPTH-1:0]]  <= din;
            wrPtr                       <= wrPtr + 1;
        end
    end

endmodule
