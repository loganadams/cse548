/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : SWController.v
*
* Description :  Controls the operation of a single SW module.  Loads the reference shifter and
*                stalls if the score fifo is full.
*
* Creation Date : Fri 06 Jan 2012 03:08:46 PM PST
*
* Author : Corey Olson
*
* Last Modified : Tue 28 Feb 2012 04:01:12 PM PST
*
*/
module SWController #(
        parameter READ_POS_BITS         = 32,
        parameter REF_POS_BITS          = 32,
        parameter BASES_PER_TRANSFER    = 64,
        parameter REF_BASES_PER_TRANSFER = 64,
        parameter REF_ALIGN_LENGTH = 6
    )
    (
        input       clk,
        input       rst,

        input       lastRefValid,   // signal from the lastRef fifo to signify that all reference data
                                    //  has been loaded into the reference fifo
        input       updateRead,     // asserted when a new read should be loaded for this ref data

        input [READ_POS_BITS-1:0]   readLength,    // length of the read (in bases)
        output reg  readValid,      // tells the S-W unit to load a new read
        output reg  firstRead,      // tells the SmWaArray that we are sending a new read
        output      lastRead,       // tells the array that this is the last section of read data
        input       readReady,      // S-W unit is ready to accept a new read
        input       readInValid,    // input read fifo has a valid read that is ready to transmit
                                    //  to the S-W array when this signal is asserted

        output reg  refsComplete,   // asserted when all refs for a read have been completed
        output reg  firstRef,       // tells the SmWaArray that we are starting a new reference score
        output reg  sendFirstRef,
        output      lastRef,        // tells the shifter that this is the last section of ref data
        input [REF_POS_BITS-1:0]    refLength,     // length of the reference (in bases)
        output reg  refValid,       // load the shifter when this is asserted
        input       refReady,       // tells that the shifter is empty
        input       scoreReady      // score fifo has available entries when this is asserted
    );

    // internal signals
    reg             loadRefCount;
    reg [5:0]       refCount;
    reg             decrementCount;
    
    reg             loadReadCount;
    reg [3:0]       readCount;
    reg             decrementReadCount;

    reg [2:0]       state;
    reg [2:0]       nextState;

    // counter to tell which section of the reference we are currently sending
    always @ (posedge clk) begin
        if (rst) begin
            firstRef    <= 0;
            refCount    <= 0;
        end else if (loadRefCount) begin
            firstRef    <= 1;
            //refCount    <= (refLength + BASES_PER_TRANSFER - 1) / BASES_PER_TRANSFER;
            refCount <= (refLength + REF_BASES_PER_TRANSFER -1) / REF_BASES_PER_TRANSFER;
        end else if (decrementCount) begin
            firstRef    <= 0;
            refCount    <= refCount - 1;
        end
    end

    assign lastRef = (refCount == 1);
    
    // counter to tell which section of the read we are currently sending
    always @ (posedge clk) begin
        if (rst) begin
            firstRead   <= 0;
            readCount   <= 0;
        end else if (loadReadCount) begin
            firstRead   <= 1;
            readCount   <= (readLength + BASES_PER_TRANSFER - 1) / BASES_PER_TRANSFER;
        end else if (decrementReadCount) begin
            firstRead   <= 0;
            readCount   <= readCount - 1;
        end
    end
    
    assign lastRead = (readCount == 1);

    // states
    // WAIT: wait for all reference data to be loaded into the input fifos, reset the counter
    // SEND_READ: tell the S-W unit to grab new read data from the read fifo
    // SEND_REF: send reference data to the shifter in chunks, decrement the counter
    localparam  WAIT        = 0,
                SEND_READ   = 1,
                SEND_REF    = 2,
                DELAY_REF   = 3,
                LOAD_READ   = 4;

    // FSM
    always @ (posedge clk) begin
        if (rst) begin
            state <= WAIT;
        end else begin
            state <= nextState;
        end
    end

    // next state logic
    always @ (*) begin

        refsComplete = 0;
        loadRefCount = 0;
        decrementCount = 0;
        refValid = 0;
        loadReadCount = 0;
        decrementReadCount = 0;
        readValid = 0;
        nextState = state;
        
        case (state)
            
            // wait for the score fifo to be ready and for the last reference to be loaded into the
            //  reference fifo
            WAIT: begin
                loadRefCount = 1;
                refsComplete = 1;
                if (lastRefValid && scoreReady) begin
                    // note: if this has troubles meeting timing, could always go to the send read
                    //  state and check the condition of the updateRead signal in that state
                    if (updateRead) begin
                        nextState = LOAD_READ;
                    end else begin
                        nextState = DELAY_REF;
                    end
                end
            end

            // load the length information about the read so we know how much data to send
            LOAD_READ: begin
                refsComplete = 1;
                loadReadCount = 1;
                if (readInValid) begin
                    nextState = SEND_READ;
                end
            end

            // tell the S-W array to grab new read
            // note: must wait for the S-W array to be ready for a read before asserting send,
            //  because the SmWaArray will never assert readReady if we do it that way
            SEND_READ: begin
                refsComplete = 1;
                if (readInValid) begin
                    readValid = 1;
                    if(readReady) begin
                        decrementReadCount = 1;
                        if(lastRead) begin
                            nextState = SEND_REF;
                        end
                    end
                end
            end
            
            // need to have >= 1 cycle of delay between different references
            DELAY_REF: begin
                if (refReady) begin
                    nextState = SEND_REF;
                end
            end

            // send reference data until it has all been sent
            // assert first for the first piece of reference data and last for the last piece of
            //  reference data
            SEND_REF: begin
                if (refReady) begin
                    refValid = 1;
                    decrementCount = 1;
                    if (lastRef) begin
                        nextState = WAIT;
                    end
                end
            end

            // this state should never be entered
            default: begin
                nextState = WAIT;
            end
        endcase
    end

endmodule
