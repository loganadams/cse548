/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : SWTop.v
*
* Description: This module is the top level module for the Smith Waterman Units, controller, and
*              reference shift register.  The controller is responsible for the flow
*              in the S-W unit.  The memory interface reads the requested reference data.  The  S-W
*              compute units run the Smith-Waterman like algorithm to score each short read at each
*              of the CALs.  Lastly, the track module tracks the best score for each target (but
*              can track the best score for each short read with a few small changes).
*
* Creation Date : Tue 18 Oct 2011 02:56:28 PM PDT
*
* Author : Corey Olson
*
* Last Modified : Fri 02 Mar 2012 05:14:10 PM PST
*
*/
module SWTop #(

    parameter C0_C_S_AXI_ID_WIDTH   = 8,    // AXI ID width for this module
    parameter C0_C_S_AXI_ADDR_WIDTH = 32,   // AXI address width
    parameter C0_C_S_AXI_DATA_WIDTH = 128,  // AXI data width

    parameter CAL_BITS              = 32,
    parameter READ_ID_BITS          = 32,
    parameter SCORE_BITS            = 9,
    
    parameter REF_POS_BITS          = 8,
    parameter READ_POS_BITS         = 32,
    parameter MAX_READ_LENGTH       = 76,
    parameter MAX_REF_LENGTH        = 1024,
    parameter BITS_PER_BASE         = 2,
    parameter SW_UNITS              = 2,

    parameter STREAM_WIDTH          = 128,
    parameter BASES_PER_TRANSFER    = 64,
    // DO NOT MODIFY BELOW THIS LINE
    parameter REFERENCE_WIDTH       = C0_C_S_AXI_DATA_WIDTH,
    parameter LOG2_SW_UNITS         = clogb2(SW_UNITS),
    parameter REF_ALIGN_LENGTH      = 6
    )
    (

    input                               clk,
    input                               rst,

    input                               clkSW,              // Smith-Waterman clock
    input                               rstSW,              // Smith-Waterman reset

    input   [STREAM_WIDTH-1:0]          streamInData,       // Data from input stream
    input                               streamInValid,      // Data is valid
    output                              streamInReady,      // Ready for stream data

/* NSM - These are no longer unused
    // CALFinder/CALFilter Data
    input   [CAL_BITS-1:0]              CALIn,              // CAL for alignment of read to
                                                            //  reference
    input   [REF_POS_BITS-1:0]          refLengthIn,        // length of the reference (in bases)
    input                               firstCALIn,         // load the SW unit with new read if
                                                            //  asserted
    input                               revCompIn,          // reference should be reverse
                                                            //  complemented before alignment if 
                                                            //  this bit is asserted for a CAL
    input                               clipStartIn,        // allow clipping at the start of the
                                                            //  alignment
    input                               clipEndIn,          // allow clipping at the end of the
                                                            //  alignment
    input                               CALInValid,         // CAL is valid
    output                              CALInReady,         // this module is ready to accept CAL

    // read that is getting aligned
    input   [READ_POS_BITS-1:0]         readLengthIn,       // read length (in bases)
    input   [READ_ID_BITS-1:0]          readIDIn,           // Read ID data from input stream
    input   [STREAM_WIDTH-1:0]          readIn,             // Read data from input stream
    input                               readInValid,        // Read is valid
    output                              readInReady,        // taking Read 
*/    
    // Outputs to Host
    output  [READ_ID_BITS-1:0]          readIDOut,          // id of the aligned read
    output  [CAL_BITS-1:0]              alignmentOut,       // alignment of the specified read 
    output  [READ_POS_BITS-1:0]         readBaseOut,        // last base of the read that aligned
    output  [SCORE_BITS-1:0]            scoreOut,           // score of the aligned read
    output                              revCompOut,         // asserted if the read aligned to the
                                                            //  reverse complement reference
    output										 					indelOut,						//HL-May20th: 1 if sequence alignment only involve
																														// substitution and matches.
	 output                              scoreOutValid,      // alignment and score for a read are valid
    input                               scoreOutReady,      // output is ready to accept alignments
    
    // DDR3 Interface
    output                              c0_s3_axi_clk,      // clock for master 3

    // AXI write address channel signals
    input                               c0_s3_axi_awready,  // Indicates slave is ready to accept a 
    output [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_awid,     // Write address ID
    output [C0_C_S_AXI_ADDR_WIDTH-1:0]  c0_s3_axi_awaddr,   // Write address
    output [7:0]                        c0_s3_axi_awlen,    // Write Burst Length
    output [2:0]                        c0_s3_axi_awsize,   // Write Burst size
    output [1:0]                        c0_s3_axi_awburst,  // Write Burst type
    output                              c0_s3_axi_awlock,   // Write lock type
    output [3:0]                        c0_s3_axi_awcache,  // Write Cache type
    output [2:0]                        c0_s3_axi_awprot,   // Write Protection type
    output [3:0]                        c0_s3_axi_awqos,    // Write Quality of Service Signaling
    output                              c0_s3_axi_awvalid,  // Write address valid
     
    // AXI write data channel signals
    input                               c0_s3_axi_wready,   // Write data ready
    output [C0_C_S_AXI_DATA_WIDTH-1:0]  c0_s3_axi_wdata,    // Write data
    output [C0_C_S_AXI_DATA_WIDTH/8-1:0]c0_s3_axi_wstrb,    // Write strobes
    output                              c0_s3_axi_wlast,    // Last write transaction   
    output                              c0_s3_axi_wvalid,   // Write valid
     
    // AXI write response channel signals
    input  [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_bid,      // Response ID
    input  [1:0]                        c0_s3_axi_bresp,    // Write response
    input                               c0_s3_axi_bvalid,   // Write reponse valid
    output                              c0_s3_axi_bready,   // Response ready
     
    // AXI read address channel signals
    input                               c0_s3_axi_arready,  // Read address ready
    output [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_arid,     // Read ID
    output [C0_C_S_AXI_ADDR_WIDTH-1:0]  c0_s3_axi_araddr,   // Read address
    output [7:0]                        c0_s3_axi_arlen,    // Read Burst Length
    output [2:0]                        c0_s3_axi_arsize,   // Read Burst size
    output [1:0]                        c0_s3_axi_arburst,  // Read Burst type
    output                              c0_s3_axi_arlock,   // Read lock type
    output [3:0]                        c0_s3_axi_arcache,  // Read Cache type
    output [2:0]                        c0_s3_axi_arprot,   // Read Protection type
    output                              c0_s3_axi_arvalid,  // Read address valid
    output [3:0]                        c0_s3_axi_arqos,    // Read Quality of Service Signaling 
     
    // AXI read data channel signals   
    input  [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_rid,      // Response ID
    input  [1:0]                        c0_s3_axi_rresp,    // Read response
    input                               c0_s3_axi_rvalid,   // Read reponse valid
    input  [C0_C_S_AXI_DATA_WIDTH-1:0]  c0_s3_axi_rdata,    // Read data
    input                               c0_s3_axi_rlast,    // Read last
    output                              c0_s3_axi_rready    // Read Response ready
    );
    
    ///////////////
    // FUNCTIONS //
    ///////////////
    // computes ceil( log( x ) ) 
    function integer clogb2;
        input [31:0] value;
        begin
            value = value - 1;
            // want log2(0) = 1
            if (value == 0) begin
                value = 1;
            end
            for (clogb2 = 0; value > 0; clogb2 = clogb2 + 1) begin
                value = value >> 1;
            end
        end
    endfunction

    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////

    // registered resets
    reg                                 rst_q=0;
    reg                                 rstSW_q=0;
   
    // handshake signals to the inputs of the Ram CAL Fifo and Track CAL Fifo
    wire                                CALInToBothValid;
    wire                                CALInToTrackFIFOValid;
    wire                                CALInToRamReady;
    wire                                CALInToTrackReady;
    
    // signals from the controller to the SW engines
    wire    [STREAM_WIDTH-1:0]          read;
    wire    [READ_POS_BITS-1:0]         readLength;
    wire                                lastRead;
    wire    [SW_UNITS-1:0]              readValid;
    wire    [SW_UNITS-1:0]              readReady;

    wire    [SW_UNITS-1:0]              refValid;
    wire    [SW_UNITS-1:0]              refReady;
    
    // signals from the memory interface to the SW engines and the controller
    wire    [REFERENCE_WIDTH-1:0]       reference;
    wire    [REF_POS_BITS-1:0]          refLength;
    wire    [REF_ALIGN_LENGTH-1:0]      firstRefAlign;
    wire                                referenceFirstCAL;
    wire                                referenceLastCAL;
    wire                                lastReference;
    wire                                revComp;
    wire                                clipStart;
    wire                                clipEnd;
    wire                                referenceValid;
    wire                                referenceReady;

    // signals from the controller to the track FIFOs
    wire    [READ_ID_BITS-1:0]          readID;
	wire                                readFifoValid;
    wire                                readFifoReady;
    
    // signals from the Track CAL Fifo to the Track module
    wire	[CAL_BITS-1:0]				CALToTrack;
    wire	[REF_POS_BITS-1:0]          refLengthToTrack;
    wire								firstCALToTrack;
    wire								revCompToTrack;
    wire								CALToTrackValid;
    wire								CALToTrackReady;

	// signals from the track read Fifo to the track module
    wire    [READ_POS_BITS-1:0]         readLengthToTrack;
	wire	[READ_ID_BITS-1:0]			readIDToTrack;
	wire								readToTrackValid;
	wire								readToTrackReady;
	
	// signals from SW engines to score mux
    wire    [READ_POS_BITS-1:0]         readBaseArray       [0:SW_UNITS-1];
	wire    [REF_POS_BITS-1:0]          positionArray       [0:SW_UNITS-1];
    wire    [SCORE_BITS-1:0]            scoreArray          [0:SW_UNITS-1];
		wire																indelArray					[0:SW_UNITS-1];
    wire    [SW_UNITS-1:0]              scoreArrayValid;
	reg     [SW_UNITS-1:0]              scoreArrayReady;
  
    // signals from score mux to Track module 
    reg     [READ_POS_BITS-1:0]         readBase; 
    reg     [REF_POS_BITS-1:0]          position;
    reg     [SCORE_BITS-1:0]            score;
		reg																	indel;
    reg     [LOG2_SW_UNITS-1:0]         activeSW;
    reg                                 scoreValid;
    wire								scoreReady;    
    
    // These are replacing the CAL and read stream inputs
    wire    [CAL_BITS-1:0]              CALIn;
    wire    [REF_POS_BITS-1:0]          refLengthIn;
    wire                                firstCALIn;
    wire                                lastCALIn;
    wire                                revCompIn;
    wire                                clipStartIn;
    wire                                clipEndIn;
    wire                                CALInValid;
    wire                                CALInReady;
    // read that is getting aligned
    wire    [READ_POS_BITS-1:0]         readLengthIn;
    wire    [READ_ID_BITS-1:0]          readIDIn;
    wire    [STREAM_WIDTH-1:0]          readIn;
    wire                                readInValid;
    wire                                readInReady;
    // Stream splitting signals
    reg                                 currentStreamCALs;
    reg                                 lastCALArrived;
    wire                                readInDone;

    // NSM Split the input stream out to the modules
    always @ (posedge clk)
      if (rst_q) begin
         currentStreamCALs <= 1'b1;
      end else 
        if (currentStreamCALs) begin
           // If accepting input and the last CAL is received, switch modes
           if (CALInReady && streamInValid && streamInData[65]) begin // Bit 65 is the last CAL bit
              currentStreamCALs <= 1'b0;
           end
        end else begin
           if (readInReady && streamInValid) begin
              // If accepting input and the reads are done, return to waiting for CALs
              if (readInDone)
                currentStreamCALs <= 1'b1;
           end
        end
    // Do the splitting
    assign streamInReady = currentStreamCALs ? CALInReady : readInReady;
    assign CALIn = streamInData[CAL_BITS-1:0];
    assign refLengthIn = streamInData[REF_POS_BITS+31:32];
    assign firstCALIn = streamInData[64];
    assign lastCALIn = streamInData[65];
    assign revCompIn = 0;
    assign clipStartIn = streamInData[66];
    assign clipEndIn = streamInData[67];
    // The end CAL word is now a valid CAL, Controller must throw it out
    assign CALInValid = streamInValid && currentStreamCALs;
    assign readLengthIn = streamInData[READ_POS_BITS-1:0];
    assign readIDIn = streamInData[READ_ID_BITS+31:32];
    assign readIn = streamInData;
    assign readInValid = streamInValid && !currentStreamCALs;

    /////////////////////////
    // Register the Resets //
    /////////////////////////
    always @ (posedge clk) rst_q <= rst;
    always @ (posedge clkSW) rstSW_q <= rstSW;

    ////////////////
    // Controller //
    ////////////////
    Controller #(
        .SW_UNITS           (SW_UNITS), 
        .LOG2_SW_UNITS      (LOG2_SW_UNITS),
        .LENGTH_BITS        (READ_POS_BITS),
        .STREAM_WIDTH       (STREAM_WIDTH),
        .BITS_PER_BASE      (BITS_PER_BASE),
        .BASES_PER_TRANSFER (BASES_PER_TRANSFER),
        .READ_ID_BITS       (READ_ID_BITS),
        .MAX_READ_LENGTH    (MAX_READ_LENGTH)
    ) controller (
        .clk                (clk), 
        .rst                (rst_q), 
        
        .referenceValid     (referenceValid),
        .referenceReady     (referenceReady),
        .firstCAL           (referenceFirstCAL),
        .lastCAL            (referenceLastCAL),
        .lastReference      (lastReference),

        .updateRead         (updateRead),
        .refValid           (refValid),
        .refReady           (refReady),

        .read               (read),
        .readID             (readID),
        .readLength         (readLength),
        .lastRead           (lastRead),
        .readValid          (readValid),
        .readReady          (readReady),
        
        .readIn             (readIn),
        .readIDIn           (readIDIn),
        .readLengthIn       (readLengthIn),
        .readInValid        (readInValid),
        .readInReady        (readInReady),
        .readInDone         (readInDone),

        .readFifoOutValid   (readFifoValid),
        .readFifoOutReady   (readFifoReady)
    );
    
    // must wait for track CAL fifo and ram controller to be ready before processing a new cal
    assign CALInReady = CALInToRamReady & CALInToTrackReady;
    assign CALInToBothValid = CALInValid & CALInReady;
    assign CALInToTrackFIFOValid = CALInToBothValid  && !lastCALIn;
    
    ////////////////////
    // RAM Controller //
    ////////////////////
    RamController #(
        .C0_C_S_AXI_ID_WIDTH    (C0_C_S_AXI_ID_WIDTH),    
        .C0_C_S_AXI_ADDR_WIDTH  (C0_C_S_AXI_ADDR_WIDTH),   
        .C0_C_S_AXI_DATA_WIDTH  (C0_C_S_AXI_DATA_WIDTH),
        .CAL_SIZE               (CAL_BITS),
	.STREAM_WIDTH           (REFERENCE_WIDTH),
        .MAX_REF_LENGTH         (MAX_REF_LENGTH), 
        .LENGTH_BITS            (REF_POS_BITS),
        .BASE_SIZE              (BITS_PER_BASE),
        .REFERENCE_WIDTH        (REFERENCE_WIDTH),
        .REF_ALIGN_LENGTH       (REF_ALIGN_LENGTH)
    ) RAMcontrol (
        .clk                    (clk),
        .rst                    (rst_q),

        .CAL                    (CALIn),
        .refLength              (refLengthIn),
        .firstCAL               (firstCALIn),
        .lastCAL                (lastCALIn),
        .revComp                (revCompIn),
        .clipStart              (clipStartIn),
        .clipEnd                (clipEndIn),
        .CALValid               (CALInToBothValid),       
        .CALReady               (CALInToRamReady),  // these CALs get written to the CAL fifo in
                                                    //  the Track module too, so CALInValid must 
                                                    //  be sent to the track module, and CALInReady 
                                                    //  is a combination of this ready signal and 
                                                    //  the ready signal from the track fifo

        .reference              (reference),
        .lastReference          (lastReference),
        .refLengthOut           (refLength),
        .revCompOut             (revComp),
        .clipStartOut           (clipStart),
        .clipEndOut             (clipEnd),
        .firstCALOut            (referenceFirstCAL),
        .lastCALOut             (referenceLastCAL),
        .referenceValid         (referenceValid),
        .referenceReady         (referenceReady),
        .firstRefAlignOut       (firstRefAlign),
                        
        .c0_s3_axi_clk          (c0_s3_axi_clk),

        .c0_s3_axi_awready      (c0_s3_axi_awready),
        .c0_s3_axi_awid         (c0_s3_axi_awid),
        .c0_s3_axi_awaddr       (c0_s3_axi_awaddr),
        .c0_s3_axi_awlen        (c0_s3_axi_awlen),
        .c0_s3_axi_awsize       (c0_s3_axi_awsize),
        .c0_s3_axi_awburst      (c0_s3_axi_awburst),
        .c0_s3_axi_awlock       (c0_s3_axi_awlock),
        .c0_s3_axi_awcache      (c0_s3_axi_awcache),
        .c0_s3_axi_awprot       (c0_s3_axi_awprot),
        .c0_s3_axi_awqos        (c0_s3_axi_awqos),
        .c0_s3_axi_awvalid      (c0_s3_axi_awvalid),
     
        .c0_s3_axi_wready       (c0_s3_axi_wready),
        .c0_s3_axi_wdata        (c0_s3_axi_wdata),
        .c0_s3_axi_wstrb        (c0_s3_axi_wstrb),
        .c0_s3_axi_wlast        (c0_s3_axi_wlast),
        .c0_s3_axi_wvalid       (c0_s3_axi_wvalid),
     
        .c0_s3_axi_bid          (c0_s3_axi_bid),
        .c0_s3_axi_bresp        (c0_s3_axi_bresp),
        .c0_s3_axi_bvalid       (c0_s3_axi_bvalid),
        .c0_s3_axi_bready       (c0_s3_axi_bready),
     
        .c0_s3_axi_arready      (c0_s3_axi_arready),
        .c0_s3_axi_arid         (c0_s3_axi_arid),
        .c0_s3_axi_araddr       (c0_s3_axi_araddr),
        .c0_s3_axi_arlen        (c0_s3_axi_arlen),
        .c0_s3_axi_arsize       (c0_s3_axi_arsize),
        .c0_s3_axi_arburst      (c0_s3_axi_arburst),
        .c0_s3_axi_arlock       (c0_s3_axi_arlock),
        .c0_s3_axi_arcache      (c0_s3_axi_arcache),
        .c0_s3_axi_arprot       (c0_s3_axi_arprot),
        .c0_s3_axi_arvalid      (c0_s3_axi_arvalid),
        .c0_s3_axi_arqos        (c0_s3_axi_arqos),
     
        .c0_s3_axi_rid          (c0_s3_axi_rid),
        .c0_s3_axi_rresp        (c0_s3_axi_rresp),
        .c0_s3_axi_rvalid       (c0_s3_axi_rvalid),
        .c0_s3_axi_rdata        (c0_s3_axi_rdata),
        .c0_s3_axi_rlast        (c0_s3_axi_rlast),
        .c0_s3_axi_rready       (c0_s3_axi_rready)
    );

    ///////////////
    // SWArray ////
    ///////////////
    genvar s;
    generate
        for (s=0; s<=SW_UNITS-1; s=s+1) begin: SW_ARRAYS   

            SmWaModule #(
                .MAX_READ_SIZE      (MAX_READ_LENGTH),
                .STREAM_WIDTH       (STREAM_WIDTH), 
                .BASE_SIZE          (BITS_PER_BASE),
                .POS_SIZE           (REF_POS_BITS), 
                .READ_POS_SIZE      (READ_POS_BITS), 
                .BASES_PER_TRANSFER (BASES_PER_TRANSFER),
                .SCORE_SIZE         (SCORE_BITS),
		          .REFERENCE_WIDTH    (REFERENCE_WIDTH),
                .REF_ALIGN_LENGTH   (REF_ALIGN_LENGTH)
            ) swmodule (
                .clk            (clk), 
                .rst            (rst_q),

                .clkSW          (clkSW),
                .rstSW          (rstSW_q),
                
                .clipStartIn    (clipStart),
                .clipEndIn      (clipEnd),
                .refLengthIn    (refLength),
                .updateReadIn   (updateRead),
                .lastRefIn      (lastReference),    
                .refIn          (reference), 
                .refInValid     (refValid[s]), 
                .refInReady     (refReady[s]),
                .firstRefAlignIn(firstRefAlign),

                .readIn         (read),
                .lastReadIn     (lastRead),
                .readLengthIn   (readLength),
                .readInValid    (readValid[s]), 
                .readInReady    (readReady[s]), 
                
                .readBaseOut    (readBaseArray[s]), 
                .positionOut    (positionArray[s]), 
                .scoreOut       (scoreArray[s]),
								.indelOut				(indelArray[s]), //HL-May20th 
                .scoreOutValid  (scoreArrayValid[s]),
                .scoreOutReady  (scoreArrayReady[s])
            );
        end
    endgenerate

    //////////////////
    // MUX S-W data //
    //////////////////
    // round-robin through all the SW-units
    always @(posedge clk) begin    
        if(rst_q) begin
            readBase            <= 0;
            position            <= 0;
            score               <= 0;
						indel								<= 0;//HL-May20th
            activeSW            <= 0;
            scoreValid          <= 0;
        end else if (!scoreValid) begin
            readBase            <= readBaseArray    [activeSW];
            position            <= positionArray    [activeSW];
            score               <= scoreArray       [activeSW];
						indel								<= indelArray				[activeSW];//HL-May20th
            scoreValid          <= scoreArrayValid  [activeSW];
            if (scoreArrayValid[activeSW]) begin
                if(activeSW == SW_UNITS-1) begin
                    activeSW    <= 0;
                end else begin
                    activeSW    <= activeSW + 1;
                end
            end
        end else if (scoreReady) begin
            scoreValid          <= 0;
        end
    end
  
    // mux valid to the Track and demux ready signal back to SW unit
    always @ (*) begin
        scoreArrayReady = 0;
        scoreArrayReady[activeSW] = !scoreValid;
    end
    
    ////////////////////
    // TRACK CAL FIFO //
    ////////////////////
    assign CALInToTrackReady = ~trackCALFull;
    assign CALToTrackValid = ~trackCALEmpty;
    asyncFifoBRAM #(
        .WIDTH  (CAL_BITS + 2 + REF_POS_BITS)
    ) trackCALFifo (
        .wr_clk (clk),
        .wr_rst (rst_q),
        .din    ({CALIn, firstCALIn, revCompIn, refLengthIn}),
        .wr_en  (CALInToTrackFIFOValid), // NSM Don't send last CAL to track
        .full   (trackCALFull),
        
        .rd_clk (clk),
        .rd_rst (rst_q),
        .dout   ({CALToTrack, firstCALToTrack, revCompToTrack, refLengthToTrack}),
        .rd_en  (CALToTrackReady),
        .empty  (trackCALEmpty)
    );
    
    /////////////////////
    // TRACK READ FIFO //
    /////////////////////
    assign readToTrackValid = ~readFifoEmpty;
    assign readFifoReady = ~readFifoFull;
    asyncFifoBRAM #(
        .WIDTH  (READ_POS_BITS + READ_ID_BITS)
    ) trackReadFifo (
        .wr_clk (clk),
        .wr_rst (rst_q),
        .din    ({readLength, readID}),
        .wr_en  (readFifoValid),
        .full   (readFifoFull),
        
        .rd_clk (clk),
        .rd_rst (rst_q),
        .dout   ({readLengthToTrack, readIDToTrack}),
        .rd_en  (readToTrackReady),
        .empty  (readFifoEmpty)
    );
    
    ////////////////////// 
    // TRACK BEST SCORE //
    //////////////////////
    Track #(
        .BITS_PER_BASE  (BITS_PER_BASE),
        .CAL_SIZE       (CAL_BITS), 
        .READ_ID_SIZE   (READ_ID_BITS), 
        .REF_POS_BITS   (REF_POS_BITS), 
        .READ_POS_BITS  (READ_POS_BITS), 
        .SCORE_SIZE     (SCORE_BITS),
		  .STREAM_WIDTH	(REFERENCE_WIDTH)
    ) track (
        .clk            (clk),
        .rst            (rst_q),

        .score          (score),
				.indel					(indel),//HL-May20th
        .readBase       (readBase),
        .alignment      (position),
        .scoreValid     (scoreValid),
        .scoreReady     (scoreReady),

        .readLengthIn   (readLengthToTrack),
        .readIDIn       (readIDToTrack),
        .readInValid    (readToTrackValid),
        .readInReady    (readToTrackReady),

        .CALIn          (CALToTrack),
        .refLengthIn    (refLengthToTrack),
        .firstCALIn     (firstCALToTrack),
        .revCompCALIn   (1'b0),//revCompToTrack),
        .CALInValid     (CALToTrackValid),
        .CALInReady     (CALToTrackReady),

        .readIDOut      (readIDOut),
        .readBaseOut    (readBaseOut),
        .alignmentOut   (alignmentOut),
        .scoreOut       (scoreOut),
				.indelOut				(indelOut),//HL-May20th
        .revCompOut     (revCompOut),
        .scoreOutValid  (scoreOutValid),
        .scoreOutReady  (scoreOutReady)
    );

endmodule 
