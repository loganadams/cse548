/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Module:      SmWaArray
 * 
 * Description: This module is the Smith Waterman Array, consisting of N Smith-Waterman units, 
 *                  where N is the number of short read bases.
 * 
 * Ports:
 *       <dir> <signal>         -<description>
 *          I   clk             - system clock
 *          I   rst             - system reset
 *
 *          I   SRCharIn        - The shortread.  Bits 0-1 are the first part of the sequence, 2-3 the next, etc.
 *          I   RefCharIn       - The input characters, shifted in one base a cycle.
 *
 *          I   clipStartIn     - flag to allow clipping at the start of the short read
 *          I   clipEndIn       - flag to allow clipping at the end of the short read
 * 
 *          I   ReadSendIn      - asserted when a new short read sequence is sent   
 *          I   RefSendIn       - asserted when a new reference sequence is sent 
 *          I   RefsComplete    - asserted when all references for a read have been sent
 * 
 *          O   ReadReady       - true when entire reference sequence is out of the SmWaArray, which is ready the next one
 *          I   DoneIn          - asserted when current reference base equals read base
 *          O   DoneOut         - true when done processing a score (one reference and one read sequence)
 *          O   Position        - best position found so far.  Is final best match when done==1.
 *          O   Score           - best score found.  Is final best score when done==1.
 *          O   ReadBase        - base of the read with the best score (only valid when clipEnd==1)
 * 
 * Author:      Corey Olson, Scott Hauck, and Maria Kim
 * Date:        1-11-2011
 */

module SmWaArray (SRCharIn,RefCharIn,ReadSendIn,RefSendIn,clk,rst,DoneIn,ReadReady,RefsComplete,
                    DoneOut,Position,Score, IndelOut,//HL-May20th: Indel bit
                    clipStartIn, clipEndIn, ReadBase, SRLengthIn, firstSRCharIn);
    
    ////////////////
    // PARAMETERS //
    ////////////////
    parameter   MAX_READ_SIZE = 75,
                BASE_SIZE = 2,
                POS_SIZE = 8,
                READ_POS_SIZE = 8,
                STREAM_WIDTH = 128,
                SCORE_SIZE = 9;

    ///////////
    // PORTS //
    ///////////
    input clk;
    input rst;
   
    input                           clipStartIn;
    input                           clipEndIn;
    input [READ_POS_SIZE-1:0]       SRLengthIn; 
    
    input                           firstSRCharIn;
    input [STREAM_WIDTH-1:0]        SRCharIn;
    input [BASE_SIZE-1:0]           RefCharIn;
    input                           ReadSendIn; 
    input                           RefSendIn;
    input                           DoneIn;
    
    input                           RefsComplete;
    output reg                      ReadReady;
    
    output                          DoneOut;
    output [READ_POS_SIZE-1:0]      ReadBase;
    output [POS_SIZE-1:0]           Position; 
    output [SCORE_SIZE-1:0]         Score;
		output													IndelOut; //HL-May20th: Indel bit

    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////

    reg [MAX_READ_SIZE*BASE_SIZE-1:0]   SRChar;                             // SRChar for SmWa units
    reg [9:0]                           SRBitPos;                           // bit position of the 
                                                                            //  short read to be 
                                                                            //  loaded next
    reg [MAX_READ_SIZE-1:0]             SRCharEn;                           // enable mask s.t.
                                                                            //  only read bases 
                                                                            //  with this enable 
                                                                            //  set to 1 contribute 
                                                                            //  to the score
    reg [READ_POS_SIZE-1:0]             SRLength;                           // registered length of
                                                                            //  the read - 1
    wire [READ_POS_SIZE-1:0]            ReadLength;                         // length of the read
                                                                            //  from clipEndFifo

    wire [MAX_READ_SIZE-1:0]            clipStarts;                         // allows the start of
                                                                            //  the read to be
                                                                            //  clipped by setting
                                                                            //  the first column of
                                                                            //  the scoring matrix
                                                                            //  to 0

    wire                                clipEnd;                            // allows the end of
                                                                            //  the read to be
                                                                            //  clipped by tracking the
                                                                            //  highest score anywhere
                                                                            //  in the matrix and the
                                                                            //  read base where it
                                                                            //  aligned

    wire [BASE_SIZE-1:0]                RefChars    [0:MAX_READ_SIZE-1];    // RefChars
    wire [MAX_READ_SIZE-1:0]            RefSends;                           // RefSend
    
    wire [SCORE_SIZE-1:0]               similarity  [0:MAX_READ_SIZE-1];    // similarity array of 
                                                                            //  scoring wires, 
                                                                            //  where row[i] drives 
                                                                            //  similarity[i]
		wire [SCORE_SIZE-1:0]								indel				[0:MAX_READ_SIZE-1];		//											HL-May20th
    wire [SCORE_SIZE-1:0]               refGap      [0:MAX_READ_SIZE-1];    // reference gap Score
    wire [SCORE_SIZE-1:0]               readGap     [0:MAX_READ_SIZE-1];    // read gap Score
    
    wire [SCORE_SIZE-1:0]               bestScore   [0:MAX_READ_SIZE-1];    // best similarity
                                                                            //  found in read bases
                                                                            //  up to this base
		wire [SCORE_SIZE-1:0]								bestIndel		[0:MAX_READ_SIZE-1];		//											HL-May20th
    wire [READ_POS_SIZE-1:0]            bestScorePos[0:MAX_READ_SIZE-1];    // base number of the
                                                                            //  best score (0 
                                                                            //  indexed)
   
    reg  [MAX_READ_SIZE:0]              doneShifter;

    integer                             base;

    // Done Logic - maintain a shift register equal to the length of the read
    //              DoneIn shifts into the left of the shift register, DoneOut shifts out the right
    //              note: DoneIn will be asserted with the last base, which has to travel
    //                    through all bases of the read + the bottom cell = MAX_READ_SIZE+1
    assign DoneOut = doneShifter[0];
    always @ (posedge clk) begin
        // to take advantage of the SRL32, don't use the reset logic
        doneShifter <= {DoneIn, doneShifter[MAX_READ_SIZE:1]};
    end

    // ReadReady - ready for a new read once the whole reference has shifted out
    always @(posedge clk) begin
        if(rst) begin
            ReadReady <= 1;
        end else if(RefSendIn || DoneIn) begin
            ReadReady <= 0;
        end 
        // if a new reference has loaded, we cannot assert readReady until all references in flight
        //  have been aligned
        //else if(DoneOut && RefsComplete && clipEndFifoEmpty) begin
        else if(RefsComplete && clipEndFifoEmpty) begin
            ReadReady <= 1;
        end
    end
    
    // Update SRChar - load the read on the readSend signal
    // keep track of what the bit position should be for the short read chars
    always @(posedge clk) begin
        if(rst) begin
            SRChar <= 0;
            SRBitPos <= 0;
            SRLength <= 0;
        end else if(ReadReady && ReadSendIn) begin
            if (firstSRCharIn) begin
                SRChar <= SRCharIn;
                SRBitPos <= STREAM_WIDTH;
                SRLength <= SRLengthIn - 1;
            end else begin
                SRChar <= SRChar | (SRCharIn << SRBitPos);
                SRBitPos <= SRBitPos + STREAM_WIDTH;
            end
        end 
    end

    // fifo to hold the clipEnd values
    // note: must be able to hold multiple clipEnd values because it is not used until the bottom
    //  cell, and the clipEnd may have already been overwritten with the next references clipEnd bit
    syncFifo #(
        .WIDTH  (READ_POS_SIZE + 1),
        .DEPTH  (8)
    ) clipEndFifo (
        .clk    (clk),
        .rst    (rst),

        .din    ({clipEndIn, SRLength[READ_POS_SIZE-1:0]}),
        .wr_en  (RefSendIn),

        .dout   ({clipEnd, ReadLength}),
        .rd_en  (DoneOut),
        .empty  (clipEndFifoEmpty)
    );

    // create a mask, such that only read bases where the mask bit is a 1 contribute to the score
    //  of the read
    always @ (posedge clk) begin
        if (rst) begin
            SRCharEn <= 0;
        end else begin
            for(base=0; base<MAX_READ_SIZE; base=base+1) begin
                if (SRLength >= base) begin
                    SRCharEn[base] <= 1'b1;
                end else begin
                    SRCharEn[base] <= 1'b0;
                end
            end
        end
    end

    // First Row units
    // Note: assume that the first row is always enabled
    SmWaUnit #(.BASE_SIZE(BASE_SIZE), .SCORE_SIZE(SCORE_SIZE),
               .READ_POS_SIZE(READ_POS_SIZE), .BASE_NUMBER(0)) 
    u0      (.similarityUp({(SCORE_SIZE){1'b0}}), .similarityOut(similarity[0]),
             .indelUpIn(1'b1), .indelOut(indel[0]),    //HL-May20th
             .SRCharIn(SRChar[BASE_SIZE-1:0]), .SRCharEn(1),
             .RefCharIn(RefCharIn), .RefCharOut(RefChars[0]),
             .RefSendIn(RefSendIn), .RefSendOut(RefSends[0]), 
             //.refGapIn({(SCORE_SIZE){GAP_EXTEND_COST - GAP_OPEN_COST}}),
             .refGapIn({(SCORE_SIZE){9'b111111111}}), .refGapOut(refGap[0]),
             .readGap(readGap[0]), .rst(rst), .clk(clk), 
             .clipStartIn(clipStartIn), .clipStartOut(clipStarts[0]),
             .bestIndelIn(1), .bestScoreIn(0), .bestScoreInBase(0),
             .bestIndelOut(bestIndel[0]), .bestScoreOut(bestScore[0]),
             .bestScoreOutBase(bestScorePos[0]));
    
    // Rest of the units
    genvar i;    
    generate 
        for (i=1; i<(MAX_READ_SIZE); i=i+1) begin : MAKE_UNITS
            SmWaUnit #(.BASE_SIZE(BASE_SIZE), .SCORE_SIZE(SCORE_SIZE),
                       .READ_POS_SIZE(READ_POS_SIZE), .BASE_NUMBER(i)) 
            u1      (.similarityUp(similarity[i-1]), .similarityOut(similarity[i]),
                     .indelUpIn(indel[i-1]),	.indelOut(indel[i]),  //HL-May20th
                     .SRCharIn(SRChar[BASE_SIZE*i+1:BASE_SIZE*i]), .SRCharEn(SRCharEn[i]),
                     .RefCharIn(RefChars[i-1]), .RefCharOut(RefChars[i]),
                     .RefSendIn(RefSends[i-1]), .RefSendOut(RefSends[i]), 
                     .refGapIn(refGap[i-1]), .refGapOut(refGap[i]),
                     .readGap(readGap[i]),.rst(rst), .clk(clk), 
                     .clipStartIn(clipStarts[i-1]), .clipStartOut(clipStarts[i]),
                     .bestIndelIn(bestIndel[i-1]), .bestScoreIn(bestScore[i-1]), .bestScoreInBase(bestScorePos[i-1]),//HL-May20th
                     .bestIndelOut(bestIndel[i]),  .bestScoreOut(bestScore[i]),  .bestScoreOutBase(bestScorePos[i]));//HL-May20th
        end
    endgenerate
    
    // Finds highest score for one CAL
    //       0 : track best score of bottom cell
    //       1 : track best score of all cells in the array
    SmWaBottom  #(.POS_SIZE(POS_SIZE), .SCORE_SIZE(SCORE_SIZE), .READ_POS_SIZE(READ_POS_SIZE))
        bot     (.Score(similarity[MAX_READ_SIZE-1]), .IndelIn(indel[MAX_READ_SIZE-1]),//HL-May20th
                .RefSend(RefSends[MAX_READ_SIZE-1]), .clk(clk), .rst(rst),
								//HL-May20th
                .BestIndelIn(bestIndel[MAX_READ_SIZE-1]), .BestScoreIn(bestScore[MAX_READ_SIZE-1]), .BestScoreInBase(bestScorePos[MAX_READ_SIZE-1]),
                .BestPosition(Position), .BestScore(Score), .BestIndelOut(IndelOut), //HL-May20th
                .clipEnd(clipEnd), .ReadBase(ReadBase), .ReadLength(ReadLength));
endmodule



/*
 * Module:      SmWaUnit
 * 
 * Description: This module computes the Smith-Waterman score for one base of the reference and read.
 * 
 * Ports:
 *       <dir> <signal>         -<description>
 *          I   clk             - system clock
 *          I   rst             - system reset
 * 
 *          I   SRCharIn        - ShortRead Character. 
 *          I   SRCharEn        - enable signal. SRChar is only valid when this enable is a 1 
 *          I   RefCharIn       - Stream of characters from the reference string. 
 *          O   RefCharOut      - passes RefChar to next unit
 *
 *          I   RefSendIn       - asserted when a new reference sequence is sent 
 *          O   RefSendOut      - passes RefSend signal to next unit
 *
 *          I   refGapIn        - extend score from the cell above
 *          O   refGapOut       - passes computed refGap score to next unit
 *
 *          O   readGap         - debug signal to see what the read gap score is
 *
 *          I   similarityUp    - score from the cell above (1 SRChar earlier in the shortread)
 *          O   similarityOut   - score of this cell
 *
 *          I   clipStart       - allow the start of the read to be clipped by setting the first
 *                                column to 0
 *
 * Author:      Scott Hauck and Maria Kim
 * Date:        1-11-2011
 */

module SmWaUnit (clk, rst, similarityUp, indelUpIn, SRCharIn, SRCharEn, RefCharIn, RefSendIn, refGapIn,//HL-May20th
                 RefSendOut, refGapOut, readGap, similarityOut, indelOut, RefCharOut, //HL-May20th
                 clipStartIn, clipStartOut, bestIndelIn, bestIndelOut, 								//HL-May20th
		 bestScoreIn, bestScoreOut, bestScoreInBase, bestScoreOutBase);

    ////////////////
    // PARAMETERS //
    ////////////////
    parameter   SCORE_SIZE = 9,
                BASE_SIZE = 2,
//                FIRST_ROW = 0,
                READ_POS_SIZE = 8,
                BASE_NUMBER = 0;
    
    ///////////
    // PORTS //
    ///////////
    input clk;
    input rst; 

    input [SCORE_SIZE-1:0]      similarityUp;
    input 			indelUpIn;		//HL-May20th
    input [BASE_SIZE-1:0]       SRCharIn;
    input                       SRCharEn;
    input [BASE_SIZE-1:0]       RefCharIn;
    input                       RefSendIn;
    input [SCORE_SIZE-1:0]      refGapIn;
    input                       clipStartIn;
    output reg                  clipStartOut;
    
    input 			bestIndelIn;	//HL-May20th
    output 			bestIndelOut;	//HL-May20th
    input [SCORE_SIZE-1:0]      bestScoreIn;
    output [SCORE_SIZE-1:0]     bestScoreOut;
    input  [READ_POS_SIZE-1:0]  bestScoreInBase;
    output [READ_POS_SIZE-1:0]  bestScoreOutBase;
     
    output reg                  RefSendOut;
    output [SCORE_SIZE-1:0]     refGapOut;
    output [SCORE_SIZE-1:0]     similarityOut;
    output 			indelOut;			//HL-May20th
    output [SCORE_SIZE-1:0]     readGap;
    output reg [BASE_SIZE-1:0]  RefCharOut;
    
    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////
    wire 			match;                 		//Ref and Read bases are equivalent
    reg [SCORE_SIZE-1:0] 	Diag;  		//Score from diagonal
    reg 			indelDiag;	//HL-May20th: Indel from diagonal
    
    // match is asserted if the bases being compared are the same
    assign match = (RefCharIn == SRCharIn);
    
    // registering the refSend signal to pass to the bottom cell (zipper
    // resets the systolic arrays)
    // registering the diagonal score
    // registering the reference base
    always @(posedge clk) begin
        if(rst) begin
            RefCharOut      <= 0;
            RefSendOut      <= 0;
            Diag            <= 0;
	    indelDiag       <= 0;	//HL-May20th:
            clipStartOut    <= 0;
        end else begin
            RefCharOut      <= RefCharIn;       // Pass ref sequence along.
            RefSendOut      <= RefSendIn;       // Pass RefSend signal
            Diag            <= similarityUp;    // Next cycle's diagonal cell is this cycle's up cell.
	    indelDiag	    <= indelUpIn;	//HL-May20th: Next cycle's diagonal cell is this cycel's up cell.
            clipStartOut    <= clipStartIn;     // clipStart flag for the next cell
        end
    end
    
    // Generate affine gap computation modules (first-row and otherwise)
    // generate
    //    if(FIRST_ROW)
    //      FirstRowComputeScore #(.SCORE_SIZE(SCORE_SIZE), .POS_SIZE(READ_POS_SIZE),
    //                             .BASE_NUMBER(BASE_NUMBER))
    //    score0  (.clk(clk), .rst(rst), .RefSend(RefSendIn), .match(match),
    //             .refGap(refGapOut), .similarity(similarityOut),.readGap(readGap),
    //             .clipStart(clipStartIn), 
    //     	.indelOut(indelOut),	.bestIndelOut(bestIndelOut),//HL-May20th
    //     	.bestScoreOut(bestScoreOut), .bestScoreOutBase(bestScoreOutBase));
    //    else
         ComputeScore #(.SCORE_SIZE(SCORE_SIZE), .POS_SIZE(READ_POS_SIZE),
                        .BASE_NUMBER(BASE_NUMBER))
       score0  (.clk(clk), .rst(rst), .RefSend(RefSendIn), .match(match), .en(SRCharEn),
                .similarityUpIn(similarityUp), .similarityDiagIn(Diag), 
		.indelUpIn(indelUpIn),	.indelDiagIn(indelDiag), //HL-May20th
                .refGapIn(refGapIn), .refGap(refGapOut), .readGap(readGap),
                .similarity(similarityOut), .indelOut(indelOut),	.clipStart(clipStartIn), //HL-May20th
                .bestIndelIn(bestIndelIn), .bestScoreIn(bestScoreIn), .bestScoreInBase(bestScoreInBase), 				//HL-May20th
                .bestIndelOut(bestIndelOut), .bestScoreOut(bestScoreOut), .bestScoreOutBase(bestScoreOutBase)); //HL-May20th
   // endgenerate 
   
endmodule

/*
 * Module:      SmWaBottom
 * 
 * Description: This module computes the best score and position for a short read sequence.
 * 
 * Ports:
 *       <dir> <signal>         -<description>
 *          I   clk             - system clock
 *          I   rst             - system reset
 * 
 *          I   RefSend         - RefSend to reset and compute a new position for a different CAL
 *          I   clipEnd         - when asserted, this module should track the best score anywhere
 *                                in the matrix, which effectively clips the end of the short read
 *          
 *          I   BestScoreIn     - best score from any cell in the matrix
 *          I   BestScoreInBase - read base associated with best score in the matrix
 *
 *          I   Score           - score from the cell above (1 SRChar earlier in the shortread)
 *          O   BestScore       - score  of best position seen so far.
 *
 *          O   BestPosition    - position of best score seen so far.  
 *          O   ReadBase        - base in the read with the best score (only valid when clipEnd==1)
 *          I   ReadLength      - length of the current read being aligned - 1
 *
 * Author:      Scott Hauck and Maria Kim
 * Date:        1-11-2011
 */
module SmWaBottom (clk, rst, Score, IndelIn, RefSend, BestPosition, BestScore, BestIndelOut, clipEnd, //HL-May20th: indelIn = from above
                    BestIndelIn, BestScoreIn, BestScoreInBase, ReadBase, ReadLength);			//HL-May20th: BestIndelIn = from entire matrix

    ////////////////
    // PARAMETERS //
    ////////////////
    parameter   SCORE_SIZE          = 9;
    parameter   POS_SIZE            = 8;
    parameter   READ_POS_SIZE       = 8;
    
    ///////////
    // PORTS //
    ///////////
    input                           clk;
    input                           rst;

    input [SCORE_SIZE-1:0]          Score;
		input														IndelIn; //HL-May20th: Indel bit
    input                           RefSend;
    
    input                           clipEnd;
		input														BestIndelIn; //HL-May20th: Indel bit
    input [SCORE_SIZE-1:0]          BestScoreIn;    
    input [READ_POS_SIZE-1:0]       BestScoreInBase;

    input [READ_POS_SIZE-1:0]       ReadLength;
    output reg [READ_POS_SIZE-1:0]  ReadBase;
    output reg [POS_SIZE-1:0]       BestPosition;
    output reg [SCORE_SIZE-1:0]     BestScore;   
		output reg											BestIndelOut; //HL-May20th: Indel bit 
    
    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////
    reg [POS_SIZE-1:0] Position;    // Counter for current position being analyzed. 
    
    // Best Score and Position
    always @(posedge clk) begin
        if(rst) begin
            BestPosition            <= 0;
            BestScore               <= 0;
            ReadBase                <= 0;
						BestIndelOut						<= 0; //HL-May20th: Indel bit
        end 
        // reset the best score when loading this SW unit
        else if(RefSend) begin
            BestScore               <= Score;
            BestPosition            <= 0;
            ReadBase                <= 0;
						BestIndelOut						<= IndelIn; //HL-May20th: TODO: Is this true?
        end
        // update the best score and position if clipEnd is asserted
        else if (clipEnd) begin
            if($signed(BestScoreIn) >= $signed(BestScore) && 
               $signed(BestScoreIn) >= $signed(Score)) begin
                if ($signed(BestScoreIn) > $signed(BestScore) || 
                   (BestScoreInBase < ReadBase)) begin
                    BestScore       <= BestScoreIn;
                    BestPosition    <= Position;
                    ReadBase        <= BestScoreInBase;
										BestIndelOut		<= BestIndelIn; //HL-May20th: Indel bit
                end
            end else if ($signed(Score) > $signed(BestScore)
			                || ($signed(Score) == $signed(BestScore)
					              && !BestIndelOut)) begin
                BestScore           <= Score;
                BestPosition        <= Position;
                ReadBase            <= ReadLength;
								BestIndelOut				<= IndelIn;		//HL-May20th: Indel bit
            end
        end 
        // check to see if the current score is better than the best
		  // We do >= because if there's a mismatch in the last base, we want the
		  // later score (the mismatch of the last base) instead of the earlier
		  // (the insertion of a base) - aligners seem to prefer this, and means
		  // we may get an indel-free score.  But, don't overwrite a score that has
		  // no indels.
        else if ($signed(Score) > $signed(BestScore)
			        || ($signed(Score) == $signed(BestScore)
					      && !BestIndelOut)) begin
            BestScore               <= Score; 
            BestPosition            <= Position;
            ReadBase                <= ReadLength;
						BestIndelOut						<= IndelIn;		//HL-May20th: Indel bit
        end        
    end
    
    // Position counter
    always @(posedge clk) begin
        if(rst) 
            Position <= 0;
        else if (RefSend)
            Position <= 0;
        else 
            Position <= Position + 1;            
    end
endmodule

