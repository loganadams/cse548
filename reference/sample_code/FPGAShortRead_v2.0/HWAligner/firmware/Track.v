/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : Track.v
*
* Description: This module outputs a score for every score that it receives.  It expects to output
*              a score for every CAL.  Therefore, CALs associated with the firstCAL flag should not
*              be garbage CALs.
*
* Creation Date : Wed Oct 19 22:41:23 2011
*
* Author : Corey Olson
*
* Last Modified : Thu 01 Mar 2012 10:53:33 AM PST
*
*/
module Track#(
    parameter BITS_PER_BASE     = 2,
    parameter CAL_SIZE          = 32,
    parameter READ_ID_SIZE      = 32,
    parameter REF_POS_BITS      = 8,
    parameter READ_POS_BITS     = 8,
    parameter SCORE_SIZE        = 9,
	 parameter STREAM_WIDTH		  = 128//HL-May24th
    // DO NOT MODIFY BELOW THIS LINE
    )
    (
    input                               clk,
    input                               rst,
    
    input [SCORE_SIZE-1:0]              score,          // score from the S-W engines for this read
		input																indel,					//HL-May20th
    input [READ_POS_BITS-1:0]           readBase,       // base of the read for this alignment
    input [REF_POS_BITS-1:0]            alignment,      // alignment from the S-W engines for this
                                                        //  read (must be added with CAL to get 
                                                        //  global alignment)
    input                               scoreValid,
    output reg                          scoreReady,
   
    input [READ_POS_BITS-1:0]           readLengthIn,   // length of the current read 
    input [READ_ID_SIZE-1:0]            readIDIn,       // read ID being aligned
    input                               readInValid,    //
    output reg                          readInReady,    //
   
    input [CAL_SIZE-1:0]                CALIn,          // CAL being aligned
    input   [REF_POS_BITS-1:0]           refLengthIn,    // length of the reference being aligned
    input                               firstCALIn,     // flag to signify this is the first CAL
                                                        //  aligned for a read (this is a junk CAL)
    input                               revCompCALIn,   // asserted if this CAL is for the reverse
                                                        //  complement reference strand
    input                               CALInValid,     // 
    output reg                          CALInReady,     //

    
    output reg  [READ_ID_SIZE-1:0]      readIDOut,      // id of read that was aligned
    output reg  [READ_POS_BITS-1:0]     readBaseOut,    // aligned base of the read
    output reg  [CAL_SIZE-1:0]          alignmentOut,   // final global alignment for a read
    output reg  [SCORE_SIZE-1:0]        scoreOut,       // final score for the alignment
		output reg													indelOut,				//HL-May20th
    output reg                          revCompOut,     // aligned CAL was the revComp strand
    output reg                          scoreOutValid,  //
    input                               scoreOutReady   //
    );
   
    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////

    reg     [1:0]                       state;
    reg     [1:0]                       nextState;
    
    // just do the math to add the CAL to the final computed alignment (accounting for reverse
    //  complement alignments of course)
    always @ (posedge clk) begin
        readBaseOut         <= readBase;
        scoreOut            <= score;
				indelOut						<= indel;//HL-May20th
        revCompOut          <= revCompCALIn;
        // Note: this outputs the global alignment, not just the alignment within the target
        if (revCompCALIn) begin
            alignmentOut    <= CALIn + (refLengthIn-1) - (alignment - (readLengthIn-1));
        end else begin
            //alignmentOut    <= {CALIn[CAL_SIZE-1:1], 1'b0} + alignment;
            alignmentOut <= CALIn + alignment;
				//alignmentOut	 <= (CALIn & ~(STREAM_WIDTH/BITS_PER_BASE - 1)) + alignment;
        end
    end

    // register the read data on the first CAL of a new read
    always @ (posedge clk) begin
        if (rst) begin
            readIDOut <= 0;
        end else if (readInReady) begin
            readIDOut <= readIDIn;
        end
    end

    /////////
    // FSM //
    /////////
    localparam  FIRST_CAL       = 0,    // need to register the read data on the first CAL
                SCORING         = 1,    // receive scores from the S-W compute engines
                SEND_RESULT     = 2;    // output the alignment, score, readID, readBase 
    
    always @ (posedge clk) begin
        if (rst) begin
            state <= SCORING;
        end else begin
            state <= nextState;
        end
    end

    // next state logic
    always @ (*) begin

        CALInReady = 0;
        scoreReady = 0;
        readInReady = 0;
        scoreOutValid = 0;

        nextState = state;

        case (state)

            // register the read data on the first CAL of a new read
            FIRST_CAL: begin
                readInReady = 1;
                if (readInValid) begin
                    nextState = SEND_RESULT;
                end
            end

            // while we are scoring, look for a firstCAL signal
            // if firstCAL, need to load the new read, otherwise move on to sending the result
            SCORING: begin
                if (scoreValid && CALInValid) begin
                    if (firstCALIn) begin
                        nextState = FIRST_CAL;
                    end else begin
                        nextState = SEND_RESULT;
                    end
                end
            end
            
            // in this state, we need to output the score, alignment, and readID
            SEND_RESULT: begin
                scoreOutValid = 1;
                if (scoreOutReady) begin
                    scoreReady = 1;
                    CALInReady = 1;
                    nextState = SCORING;
                end
            end
            
            default: begin
                nextState = SCORING;
            end
        endcase
    end

endmodule
