/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : Controller.v
*
* Description : This module controls the flow of data within the Aligner.  It directs the loading
*               of reads, from the input, to one of the S-W compute units by asserting a readValid
*               signal.  It watches the firstCAL line, at the output of the RamController, and when
*               firstCAL is asserted, it loads new read data from the input into a local buffer.
*
*               The controller passes the firstCAL bit from the RamController to the SmWaModule,
*               and reference data flows from the RamController to the S-W compute units.  Only the
*               S-W unit specified by 'target' accepts the new reference data.
*               
* Creation Date : Wed 19 Oct 2011 04:07:30 PM PDT
*
* Author : Corey Olson
*
* Last Modified : Fri 02 Mar 2012 05:20:08 PM PST
*
*/
module Controller #(
    parameter SW_UNITS              = 1,
    parameter LOG2_SW_UNITS         = 2,
    parameter LENGTH_BITS           = 32,
    parameter STREAM_WIDTH          = 128,
    parameter BITS_PER_BASE         = 2,
    parameter BASES_PER_TRANSFER    = 64,
    parameter READ_ID_BITS          = 32,
    parameter MAX_READ_LENGTH       = 512
    )
    (

    input                         clk, // clock for this system
    input                         rst, // reset for this system

    input                         referenceValid, // valid signal for reference data from the
                                                        //  RamController
    output reg                    referenceReady, // ready signal back to the RamController
    input                         firstCAL, // firstCAL from the RamController, if
                                                        //  asserted, update the read FIFO, write to 
                                                        //  the output read FIFO
    input                         lastCAL,       // This is set on the dummy final CAL
    input                         lastReference, // asserted on CALs that are not firstCAL
                                                        //  because the reference data is transmitted 
                                                        //  in multiple cycles
    
    output reg                    updateRead, // S-W engine needs to grab new read before
                                                        //  doing this S-W computation when asserted
    output reg [SW_UNITS-1:0]     refValid, // send the reference data to a S-W engine
    input [SW_UNITS-1:0]          refReady, // S-W engine ready to accept reference

    output [STREAM_WIDTH-1:0]     read, // read to be loaded into the S-W engine
    output reg [READ_ID_BITS-1:0] readID, // id for this read
    output reg [LENGTH_BITS-1:0]  readLength, // read length to the S-W engines
    output                        lastRead, // asserted for the last read transfer to a
                                                        //  S-W engine
    output reg [SW_UNITS-1:0]     readValid, // send read data to a S-W engine
    input [SW_UNITS-1:0]          readReady, // S-W engine ready to accept read
   
    input [STREAM_WIDTH-1:0]      readIn, // read data in
    input [READ_ID_BITS-1:0]      readIDIn, // id for this read
    input [LENGTH_BITS-1:0]       readLengthIn, // length of the current read
    input                         readInValid, // input read fifo has valid data
    output reg                    readInReady, // read enable signal for input read fifo
    output reg                    readInDone, // Used to switch stream between reads and CALs
     
    output reg                    readFifoOutValid, // write enable signal for output read fifo
    input                         readFifoOutReady    // output read fifo can accept a new read
    );

    /////////////////////
    // STATE VARIABLES //
    /////////////////////
    localparam  WAIT                = 0,                // wait for reference data
                SEND_READ           = 1,                // check if first round through SWArrays
                SEND_REF            = 2,                // waits for RefValid signal
                WRITE_READ          = 3,                // write the read to the output read fifo
                UPDATE_READ         = 4,                // update the input read 
                LOAD_READ_COUNT     = 5,                // load a new read count from the input
                WAIT_REF            = 6;                // wait for a valid reference
	 
    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////
    reg [LOG2_SW_UNITS-1:0]         target;             // Keeps track of target SWArray
    reg [SW_UNITS-1:0]              firstRound;         // First round through SWArray
    reg [SW_UNITS-1:0]              nextFirstRound;
    
    reg [STREAM_WIDTH-1:0]          localRead       [0:((MAX_READ_LENGTH+BASES_PER_TRANSFER-1)/BASES_PER_TRANSFER)-1];
                                                        // local copy of the read for loading the
                                                        //  read into multiple S-W engines
    reg [7:0]                       readCount;          // count the number of transfers per read
    reg                             loadReadLength;
    reg                             loadReadCount;
    reg                             decrementReadCount;
    reg                             loadRead;

    reg [2:0]                       state;
    reg [2:0]                       nextState;

    reg                             readInReadyLocal;
    
    integer                         i;

    // round-robin through the SW units
    // target is the number of the target S-W unit (not the one-hot encoding)
    always @ (posedge clk) begin
        if (rst) begin
            target <= 0;
        end else if (refValid[target] && refReady[target] && lastReference) begin
            if (target == SW_UNITS-1) begin
                target <= 0;
            end else begin
                target <= target + 1;
            end
        end
    end
    
    // count the number of loads remaining until the read buffer contains valid data
    always @ (posedge clk) begin
        if (rst) begin
            readCount   <= 0;
        end else if (loadReadCount) begin
            readCount   <= ((readLength + BASES_PER_TRANSFER - 1) / BASES_PER_TRANSFER);
        end else if (decrementReadCount) begin
            readCount   <= readCount - 1;
        end 
    end

    // load the read length from the input, need to cache it for all the S-W engines
    // pass the read length to the SmWaModule with the last transfer of a read (signals that all
    //  read bases have been added to the read fifo)
    always @ (posedge clk) begin
        if (rst) begin
            readLength  <= 0;
            readID      <= 0;
        end else if (loadReadLength) begin
            readLength  <= readLengthIn;
            readID      <= readIDIn;
        end
    end

    // load the read into a local buffer, max number of bases is 512, 
    always @ (posedge clk) begin
        if (loadRead) begin
            localRead[readCount-1]      <= readIn;
        end
    end

    // when loading the read into S-W units, now just read from the local buffer
    assign read = localRead[readCount-1];
   
    // when readCount == 1, we are sending the last read transfer
    assign lastRead = (readCount == 1);

    // NSM Register readInReady for timing, read is no longer ready upon changing state
    // don't need to reset because always in CAL mode during reset anyway
    always @ (posedge clk)
      readInReady <= readInReadyLocal && (state == nextState);

    /////////
    // FSM //
    /////////
    always @ (posedge clk) begin
        if (rst) begin 
            state           <= WAIT;
            firstRound      <= {SW_UNITS{1'b1}};
        end else begin
            state           <= nextState;
            firstRound      <= nextFirstRound;
        end
    end

    // Next State Logic //
    always @ (*) begin
     
        nextState = state; 
        nextFirstRound = firstRound;

        loadReadLength = 0;
        loadReadCount = 0;
        decrementReadCount = 0;
        loadRead = 0;

        readInReadyLocal = 0;
        readFifoOutValid = 0;

        readValid = 0;
        refValid = 0;
        updateRead = 0;

        referenceReady = 0;

        readInDone = 0;
       
        case(state)
            
            // wait until you have valid data from the RamController
            // if this is the firstCAL, we need to do some updating for the next read
            // else move on to sending the read and the reference
            // ref with firstCAL signal is junk, but we need to output the read to the read track fifo
            WAIT: begin
                if (referenceValid) begin
                    if (lastCAL) begin
                        loadReadLength = 1;
                        readInReadyLocal = 1;
                        if (readInValid && readInReady) begin
                            nextState = LOAD_READ_COUNT;
                        end
                    end else begin
                        nextState = SEND_READ;
                    end
                end
            end

            // load the read count and then read in the new read data
            LOAD_READ_COUNT: begin
                loadReadCount = 1;
                nextState = UPDATE_READ;
            end

            // load the next read into local storage
            UPDATE_READ: begin
                loadRead = 1;
                readInReadyLocal = 1;
                if (readInValid && readInReady) begin
                    decrementReadCount = 1;
                    if (lastRead) begin
                        loadReadCount = 1;
                        readInDone = 1;
                        nextState = WRITE_READ;
                    end
                end
            end
            
            // write read to the output FIFO and reset all the firstRound bits for the S-W units
            WRITE_READ: begin
                nextFirstRound = {SW_UNITS{1'b1}};
                readFifoOutValid = 1;
                if (readFifoOutReady) begin
                    nextState = WAIT_REF;
                end
            end
            
            // consume the lastCAL dummy, then return to WAIT
            // NSM This could be cleaned up to save a few cycles
            WAIT_REF: begin
                if (referenceValid) begin
                    referenceReady = 1;
                    nextState = WAIT;
                end
            end

            // see if the S-W unit needs the read to be loaded by checking if firstRound is 1
            // if so, send the read (in multiple transactions) 
            SEND_READ: begin
                if (firstRound[target]) begin
                    readValid[target] = 1;
                    if (readReady[target]) begin
                        decrementReadCount = 1;
                        if (lastRead) begin
                            loadReadCount = 1;
                            nextState = SEND_REF;
                        end
                    end
                end else begin
                    nextState = SEND_REF;
                end
            end
            
            // write the reference data to the target S-W unit
            // note: writing reference data takes multiple transactions, so must wait for the
            //       lastReference signal
            SEND_REF: begin
                if (referenceValid) begin
                    refValid[target] = 1;
                    updateRead = firstRound[target];
                    if (refReady[target]) begin
                        referenceReady = 1;
                        if (lastReference) begin
                            nextFirstRound[target] = 1'b0;
                            nextState = WAIT;
                        end
                    end
                end
            end
            
            default: begin 
               nextState = WAIT;
            end
        endcase
    end
    
endmodule 
