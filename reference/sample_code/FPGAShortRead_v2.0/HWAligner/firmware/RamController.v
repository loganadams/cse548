/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : RamController.v
*
* Description: This module is used to read from the reference.  It accepts CALs and a
*              reverse complement bit, and it looks up the reference data at the specified CAL in
*              the DDR3.  If the reverse complement bit is set for a CAL, it returns the reverse
*              complemented reference.
*
*              This also reads a variable length reference.  In order to reverse complement a
*              variable-length reference, we write the reverse-complemented reference data
*              (received on the read data port) into a block RAM, then read the data out of that
*              block RAM in a reverse order.  e.g. write revComp(data[0]) into location 0,
*              revComp(data[1]) into location 1, read from location 1 and send to SmWaModule, then
*              read from location 0 and send to SmWaModule.
*
*              This module simply passes clipStart and clipEnd to the SmWaModule
*
* Creation Date : Wed 19 Oct 2011 10:39:29 AM PDT
*
* Author : Corey Olson
*
* Last Modified : Fri 02 Mar 2012 05:11:55 PM PST
*
*/
`include "axi_defines.v"
// TODO: handle CAL that is not aligned to the boundary; i.e. if the CAL specifies a base that is
//       not aligned to the memory boundary, we need to read the DDR aligned to the boundary, but
//       we need to ignore the first number of bases when doing that alignment (also the length of
//       the DDR read may need to change
module RamController #(
    parameter C0_C_S_AXI_ID_WIDTH   = 8,    // AXI ID width for this module
    parameter C0_C_S_AXI_ADDR_WIDTH = 32,   // AXI address width
    parameter C0_C_S_AXI_DATA_WIDTH = 128,  // AXI data width

    parameter MEM_OFFSET            = 0,    // offset to the start of the reference data
    parameter CAL_SIZE              = 32,
    parameter STREAM_WIDTH          = 128,  // bus width for the reference data
    parameter MAX_REF_LENGTH        = 1024,
    parameter LENGTH_BITS           = 32,
    parameter BASE_SIZE             = 2,
    parameter REFERENCE_WIDTH       = C0_C_S_AXI_DATA_WIDTH,
    parameter REF_ALIGN_LENGTH      = 7
    // DO NOT MODIFY BELOW THIS LINE
    )
    (
    input                               clk,
    input                               rst,

    input   [CAL_SIZE-1:0]              CAL,
    input   [LENGTH_BITS-1:0]           refLength,
    input                               firstCAL,
    input                               lastCAL,
    input                               revComp,
    input                               clipStart,
    input                               clipEnd,
    input                               CALValid,
    output reg                          CALReady,

    output  [REFERENCE_WIDTH-1:0]       reference,
    output  [LENGTH_BITS-1:0]           refLengthOut,
    output                              firstCALOut,
    output                              lastCALOut,
    output                              revCompOut,
    output                              clipStartOut,
    output                              clipEndOut,
    output                              referenceValid,
    input                               referenceReady,
    output                              lastReference,      // since the reference is transferred
                                                            //  128 bits at a time, this signifies 
                                                            //  the last transfer of the reference

    output [REF_ALIGN_LENGTH-1:0]        firstRefAlignOut,
     
    // DDR3 Interface
    output                              c0_s3_axi_clk,      // clock for master 3

    // AXI write address channel signals
    input                               c0_s3_axi_awready,  // Indicates slave is ready to accept a 
    output [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_awid,     // Write address ID
    output [C0_C_S_AXI_ADDR_WIDTH-1:0]  c0_s3_axi_awaddr,   // Write address
    output [7:0]                        c0_s3_axi_awlen,    // Write Burst Length
    output [2:0]                        c0_s3_axi_awsize,   // Write Burst size
    output [1:0]                        c0_s3_axi_awburst,  // Write Burst type
    output                              c0_s3_axi_awlock,   // Write lock type
    output [3:0]                        c0_s3_axi_awcache,  // Write Cache type
    output [2:0]                        c0_s3_axi_awprot,   // Write Protection type
    output [3:0]                        c0_s3_axi_awqos,    // Write Quality of Service Signaling
    output                              c0_s3_axi_awvalid,  // Write address valid
     
    // AXI write data channel signals
    input                               c0_s3_axi_wready,   // Write data ready
    output [C0_C_S_AXI_DATA_WIDTH-1:0]  c0_s3_axi_wdata,    // Write data
    output [C0_C_S_AXI_DATA_WIDTH/8-1:0]c0_s3_axi_wstrb,    // Write strobes
    output                              c0_s3_axi_wlast,    // Last write transaction   
    output                              c0_s3_axi_wvalid,   // Write valid
     
    // AXI write response channel signals
    input  [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_bid,      // Response ID
    input  [1:0]                        c0_s3_axi_bresp,    // Write response
    input                               c0_s3_axi_bvalid,   // Write reponse valid
    output                              c0_s3_axi_bready,   // Response ready
     
    // AXI read address channel signals
    input                               c0_s3_axi_arready,  // Read address ready
    output reg [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_arid,     // Read ID
    output reg [C0_C_S_AXI_ADDR_WIDTH-1:0]  c0_s3_axi_araddr,   // Read address
    output reg [7:0]                    c0_s3_axi_arlen,    // Read Burst Length
    output [2:0]                        c0_s3_axi_arsize,   // Read Burst size
    output [1:0]                        c0_s3_axi_arburst,  // Read Burst type
    output                              c0_s3_axi_arlock,   // Read lock type
    output [3:0]                        c0_s3_axi_arcache,  // Read Cache type
    output [2:0]                        c0_s3_axi_arprot,   // Read Protection type
    output reg                          c0_s3_axi_arvalid,  // Read address valid
    output [3:0]                        c0_s3_axi_arqos,    // Read Quality of Service Signaling 
     
    // AXI read data channel signals   
    input  [C0_C_S_AXI_ID_WIDTH-1:0]    c0_s3_axi_rid,      // Response ID
    input  [1:0]                        c0_s3_axi_rresp,    // Read response
    input                               c0_s3_axi_rvalid,   // Read reponse valid
    input  [C0_C_S_AXI_DATA_WIDTH-1:0]  c0_s3_axi_rdata,    // Read data
    input                               c0_s3_axi_rlast,    // Read last
    output reg                          c0_s3_axi_rready    // Read Response ready
    );
    
    ///////////////
    // FUNCTIONS //
    ///////////////

    // finds the complement of a reference and reverses it
    // full reference
    function [C0_C_S_AXI_DATA_WIDTH-1:0] reverseComplement;
        input [C0_C_S_AXI_DATA_WIDTH-1:0] ref;
        
        reg    [C0_C_S_AXI_DATA_WIDTH-1:0] tmp;
        integer i;
        begin
            tmp = ref;
            for(i=0; i<C0_C_S_AXI_DATA_WIDTH; i=i+BASE_SIZE) begin
                reverseComplement = reverseComplement >> BASE_SIZE;
                reverseComplement[STREAM_WIDTH-1:STREAM_WIDTH-BASE_SIZE] = 
                    complementBase(tmp[STREAM_WIDTH-1:STREAM_WIDTH-BASE_SIZE]);
                tmp = tmp << BASE_SIZE;
            end
        end
    endfunction

    // complements a single base
    // complements:
    // A - T
    // C - G
    // encodings:
    // A = 0
    // C = 1
    // G = 2
    // T = 3
    function [BASE_SIZE-1:0] complementBase;
        input [BASE_SIZE-1:0] base;
        begin
            if (base == 0) begin
                complementBase = 3;
            end else if (base == 1) begin
                complementBase = 2;
            end else if (base == 2) begin
                complementBase = 1;
            end else begin
                complementBase = 0;
            end
        end
    endfunction
   
    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////

    // registered input signals
    reg                                 firstCAL_q;
    reg                                 lastCAL_q;
    reg                                 revComp_q;
    reg                                 CALValid_q;
    reg     [LENGTH_BITS-1:0]           refLength_q;
    reg                                 clipStart_q;
    reg                                 clipEnd_q;
    reg [REF_ALIGN_LENGTH-1:0]          firstRefAlign_q;

    // signals for the fifo between the read address and read data ports
    reg                                 fifoWriteEn;
    reg                                 fifoReadEn;

    // state machines
    reg     [1:0]                       rdataState;
    reg     [1:0]                       nextRdataState;
    
    reg     [1:0]                       addrState;
    reg     [1:0]                       nextAddrState;

    // block RAM to store the reference data
    // TODO: generalize the number of entries in this BRAM; i.e. we need to be able to store 1024
    //       bases of data
    reg     [C0_C_S_AXI_DATA_WIDTH-1:0] refData [0:15];
    reg     [3:0]                       refWritePtr;
    reg     [3:0]                       refReadPtr;
    reg     [3:0]                       newRefReadPtr;
    reg     [3:0]                       newLastRefPtr;

    // control signals
    reg                                 resetWritePtr;
    reg                                 resetReadPtr;
    wire                                writeRefData;
    wire                                readRefData;
    wire    [C0_C_S_AXI_DATA_WIDTH-1:0] ref;
    reg     [3:0]                       lastRefPtr;

    reg                                 lastRef;
    wire                                loadFirstRef;
    wire                                refAlignInReady;
    wire                                readFirstRef;
    reg                                 refValid;
    reg                                 firstReference;
    reg  nextFirstRef;

    integer								i;
	
    //////////////////////
    // UNUSED AXI PORTS //
    //////////////////////
    assign c0_s3_axi_awvalid = 0;
    assign c0_s3_axi_wvalid = 0;
    assign c0_s3_axi_bready = 0;

    // DDR3 clock
    assign c0_s3_axi_clk = clk;

    /////////////////
    // ARADDR PORT //
    /////////////////

    // read size, length, burst, lock, cache, prot don't change
    assign c0_s3_axi_arburst    = `INCREMENTING;
    assign c0_s3_axi_arlock     = `NORMAL_ACCESS;
    assign c0_s3_axi_arcache    = `BUF_ONLY;
    assign c0_s3_axi_arprot     = `DATA_SECURE_NORMAL;
    assign c0_s3_axi_arqos      = `NOT_QOS_PARTICIPANT;
    if (C0_C_S_AXI_DATA_WIDTH == 128) begin
        assign c0_s3_axi_arsize = `SIXTEEN_BYTES;
    end else begin
        assign c0_s3_axi_arsize = `THIRTY_TWO_BYTES;
    end
    
    // use lfsr for read id
    always @ (posedge clk) begin
        if (rst) begin
            c0_s3_axi_arid <= 1;
        end else if (c0_s3_axi_arvalid && c0_s3_axi_arready) begin
            c0_s3_axi_arid <= {^c0_s3_axi_arid[1:0],
                              c0_s3_axi_arid[C0_C_S_AXI_ID_WIDTH-1:1]};
        end
    end
   
    // compute the address and register the other inputs
    // Note: this will not issue read requests for junk firstCALs 
    always @ (posedge clk) begin
        if (rst) begin
            c0_s3_axi_araddr    <= 0;
            c0_s3_axi_arlen     <= 0;
            revComp_q           <= 0;
            firstCAL_q          <= 0;
            lastCAL_q           <= 0;
            CALValid_q          <= 0;
            refLength_q         <= 0;
            clipStart_q         <= 0;
            clipEnd_q           <= 0;
            firstRefAlign_q     <= 0;
        end else if (CALReady) begin
            c0_s3_axi_araddr    <= (((CAL / 8) * BASE_SIZE) + MEM_OFFSET) & 32'hFFFFFFE0;
            //c0_s3_axi_araddr <= ((CAL * BASE_SIZE) / 8) + MEM_OFFSET;
            c0_s3_axi_arlen     <= (((refLength + (REFERENCE_WIDTH/BASE_SIZE) - 1) / 
                                    (REFERENCE_WIDTH / BASE_SIZE))) - 1;
            firstRefAlign_q     <= CAL[REF_ALIGN_LENGTH-1:0];
            revComp_q           <= revComp;
            firstCAL_q          <= firstCAL;
            lastCAL_q           <= lastCAL;
            CALValid_q          <= CALValid;
            refLength_q         <= refLength;
            clipStart_q         <= clipStart;
            clipEnd_q           <= clipEnd;
        end
    end

    /////////////////////////////
    // READ ADDR STATE MACHINE //
    /////////////////////////////

    // states
    localparam  WAIT    = 0,
                FIFO    = 1,
                VALID   = 2;

    // FSM
    always @ (posedge clk) begin
        if (rst) begin
            addrState <= WAIT;
        end else begin
            addrState <= nextAddrState;
        end
    end

    // next state logic
    always @ (*) begin

        c0_s3_axi_arvalid = 0;
        fifoWriteEn = 0;
        CALReady = 0;
        nextAddrState = addrState;

        case(addrState) 

            // wait for valid data on the input
            WAIT: begin
                if (CALValid_q) begin
                    if (lastCAL_q)
                       nextAddrState = FIFO;
                    else
                       nextAddrState = VALID;
                end else begin
                    CALReady = 1;
                end
            end

            // this means we have a valid request for reference data
            // -assert valid on the AXI port and write this request data to the read fifo
            VALID: begin
                c0_s3_axi_arvalid = 1;
                if (c0_s3_axi_arready) begin
                    nextAddrState = FIFO;
                end
            end
            
            // write sideband info to the fifo
            // -must ensure that the read fifo is not full before writing
            FIFO: begin
                fifoWriteEn = 1;
                if (!readFifoFull) begin
                    CALReady = 1;
                    nextAddrState = WAIT;
                end
            end

            // this should never be entered, but if it is, return to the wait state
            default: begin
                nextAddrState = WAIT;
            end

        endcase
    end
 
    ////////////////////////
    // ADDR-TO-RDATA FIFO //
    ////////////////////////

    // fifo holds data during the reading of the reference
    asyncFifoBRAM #(
        .WIDTH  (LENGTH_BITS + 4 + REF_ALIGN_LENGTH)
    ) readFifo (
        .wr_clk (clk),
        .wr_rst (rst),
        // NSM The revComp spot is now used for lastCAL, since revComp is unused
        .din    ({refLength_q, firstCAL_q, lastCAL_q, clipStart_q, clipEnd_q, firstRefAlign_q}),
        .wr_en  (fifoWriteEn),
        .full   (readFifoFull),
        
        .rd_clk (clk),
        .rd_rst (rst),
        .dout   ({refLengthOut, firstCALOut, lastCALOut, clipStartOut, clipEndOut, firstRefAlignOut}),
        .rd_en  (fifoReadEn),
        .empty  (readFifoEmpty)
    );
    assign revCompOut = 0;

    ////////////////
    // RDATA PORT //
    ////////////////
    
    // this writes received data into a block RAM after conditionally reverse complementing it

    // states
    localparam  READING = 1,    // read from the rdata port and dump reference data into BRAM
                SENDING = 2,    // send data from the BRAM to the output
                LASTCAL = 3,    // send out the last CAL
                WAITING = 0;    // wait for FIFO data to be available
    
    // FSM
    always @ (posedge clk) begin
        if (rst) begin
            rdataState <= WAITING;
        end else begin
            rdataState <= nextRdataState;
        end
    end

    // next state logic
    always @ (*) begin
        
        refValid = 0;
        c0_s3_axi_rready = 0;
        fifoReadEn = 0;
        resetWritePtr = 0;
        resetReadPtr = 0;
        nextRdataState = rdataState;

        case(rdataState)

            // wait for FIFO data to become available, and skip read if it is dummy last CAL
            WAITING: begin
                if (!readFifoEmpty) begin
                    if (lastCALOut)
                        nextRdataState = LASTCAL;
                    else
                        nextRdataState = READING;
                end
            end

            // accept data from the read port and place the forward or reverse complement data in
            //  the correct location of the reference register
            READING: begin
                resetReadPtr = 1;
                c0_s3_axi_rready = 1;
                if (c0_s3_axi_rvalid && c0_s3_axi_rlast) begin
                    nextRdataState = SENDING;
                end
            end

            // send the assembled reference data to the target SW unit
            SENDING: begin
                resetWritePtr = 1;
                refValid = 1;
                if (referenceReady && lastReference) begin
                    fifoReadEn = 1;
                    nextRdataState = WAITING;
                end
            end

            // send out the dummy last CAL
            LASTCAL: begin
                refValid = 1;
                if (referenceReady && referenceValid) begin
                    fifoReadEn = 1;
                    nextRdataState = WAITING;
                end
            end
        endcase
    end

    // Block RAM to hold the reverse complemented reference data
    // TODO: this only works for the M-501 right now with the DDR interface = 128 bits; i.e. this
    //       assumes the DDR interface width is the same as the width of the streaming interface
    always @ (posedge clk) begin
        if (rst) begin
            for(i=0; i<16; i=i+1) begin
                refData[i]              <= 0;
            end
        end else if (writeRefData) begin
            if (revCompOut) begin
                refData[refWritePtr]    <= reverseComplement(c0_s3_axi_rdata);
            end else begin
                refData[refWritePtr]    <= c0_s3_axi_rdata;
            end

        end
    end

    // counter to tell how many entries were written into the block RAM
    always @ (posedge clk) begin
        if (rst) begin
            refWritePtr <= 0;
        end else if (resetWritePtr) begin
            refWritePtr <= 0;
        end else if (writeRefData) begin
            refWritePtr <= refWritePtr + 1;
        end
    end

    // read the ordered data (possibly reverse complemented) from the Block RAM and send to the SW units
    always @ (posedge clk)
        refReadPtr <= newRefReadPtr;

    always @* begin
        if (rst) begin
            newRefReadPtr      <= 0;
        end else if (resetReadPtr) begin
            if (revCompOut) begin
                newRefReadPtr  <= refWritePtr;
            end else begin
                newRefReadPtr  <= 0;
            end
        end else if (readRefData) begin
            if (revCompOut) begin
                newRefReadPtr  <= refReadPtr - 1;
            end else begin
                newRefReadPtr  <= refReadPtr + 1;
            end
        end else
           newRefReadPtr <= refReadPtr;
    end

    // track the entry of the reference BRAM that is the last entry to be sent
    always @ (posedge clk)
        lastRefPtr <= newLastRefPtr;

    always @* begin
        if (rst) begin
            newLastRefPtr <= 0;
        end else if (revCompOut) begin
            newLastRefPtr <= 0;
        end else if (writeRefData) begin
            newLastRefPtr <= refWritePtr;
        end else 
            newLastRefPtr <= lastRefPtr;
    end

    // reference data comes straight from the block RAM
   //assign reference = firstReference ? refData[refReadPtr] << (firstRefAlign << BASE_SIZE)
   //                                   : refData[refReadPtr];
    // NSM registered for timing, reset not required
    always @ (posedge clk) begin
        lastRef <= (newRefReadPtr == newLastRefPtr);
    end


    always @ (posedge clk) begin
       firstReference <= (rdataState != SENDING) && (nextRdataState == SENDING);
       nextFirstRef <= firstReference;
    end
    //assign firstReference = (rdataState != SENDING) && (nextRdataState == SENDING);

    // controls to write and read the block RAM
    assign writeRefData = c0_s3_axi_rvalid & c0_s3_axi_rready;
   assign readRefData = refValid & refAlignInReady;

   //assign lastReference = lastRef;
   //assign referenceValid = refValid;

   // First Reference Aligner //
   FirstRefAligner #(
        .REFERENCE_WIDTH (REFERENCE_WIDTH),
        .BASE_SIZE (BASE_SIZE),
        .REF_ALIGN_LENGTH(REF_ALIGN_LENGTH)
   ) firstRefAligner (
        .clk (clk),
        .rst (rst),
        .refSend (readRefData),
        .firstRef (firstReference),
        .lastRefIn (lastRef),
        .refIn (refData[refReadPtr]),
        .firstRefAlignIn ({firstRefAlignOut[REF_ALIGN_LENGTH-1:1], 1'b0}), //firstRefAlign),
        .referenceReady (referenceReady),

        .refOut (reference),
        .refInReady (refAlignInReady),
        .refOutReady (referenceValid),
        .lastRefOut (lastReference)
   );

endmodule
