/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * Module:      ComputeScore
 * 
 * Description: This module uses the affine gap model to compute a Smith Waterman score
 *                     for one read base and one reference base.
 *
 * Ports: 
 *       <dir> <signal>         -<description>
 *          O   refGap          - output score for a reference gap
 *          O   similarity      - final score for this cell
 *          
 *          I   clk             - system clock
 *          I   rst             - system reset
 *          
 *          I   ReadSend        - asserted when a new short read is sent
 *          I   RefSend         - asserted when a new reference sequence is sent  
 *          I   match           - asserted when current reference base equals read base
 *          I   en              - score is only updated if this enable is asserted, else
 *                                similarityUpIn is assumed to be the similarity score 
 *                                for this cell
 *          I   similarityUpIn  - the above score (short read base before current)
 *          I   refGapIn        - the extended gap score from above
 *          I   similarityDiagIn- the diagonal score
 *
 *          I   clipStart       - cells should be reset to 0 instead of negative values when
 *                                asserted, which effectively allows the start of the short 
 *                                read to be clipped for free       
 *
 *          I   bestScoreIn     - best score of any cell before this one
 *          O   bestScoreOut    - best score of any cell including this one (but not cells after
 *                                this one)
 *
 *          I   bestScoreInBase - index of the read base with the best score
 *          O   bestScoreOutBase- index of the read base with the best score after this cell is
 *                                included (this will either take the value of bestScoreInBase or
 *                                BASE_NUMBER)
 *                                Note: this is 0-indexed
 *
 * Author:      Corey Olson
 * Date:        6-20-2011
 */

module ComputeScore #(
    
    ////////////////
    // PARAMETERS //
    ////////////////
    parameter SCORE_SIZE        = 9,
    parameter GAP_OPEN_COST     = 2,
    parameter GAP_EXTEND_COST   = 1,
    parameter MISMATCH_PENALTY  = 2,
    parameter MATCH_BONUS       = 2,
    parameter BASE_NUMBER       = 0,    // which base this currently is
    parameter POS_SIZE          = 8     // number of bits to use to store the read base with the best score
    )
    
    (
    ///////////
    // PORTS //
    ///////////
    input                       clk,
    input                       rst,
    
    input                       RefSend,
    input                       match, 
    input                       en,
    input                       clipStart,
    
    input [SCORE_SIZE-1:0]      similarityUpIn,
    input [SCORE_SIZE-1:0]      refGapIn,
    input [SCORE_SIZE-1:0]      similarityDiagIn,
    input                       indelUpIn, //HL-May20th: Indel bit from Up, for !en & bestUpIn. TODO: is this really needed?
    input                       indelDiagIn, //HL-May20th: Indel bit from Diag
    
    output reg [SCORE_SIZE-1:0] refGap, 
    output reg [SCORE_SIZE-1:0] readGap,
    output reg [SCORE_SIZE-1:0] similarity,
    output reg                  indelOut, //HL-May20th: Indel bit out

    input                       bestIndelIn, //HL-May20th
    output reg                  bestIndelOut,

    input [SCORE_SIZE-1:0]      bestScoreIn,
    output reg [SCORE_SIZE-1:0] bestScoreOut,
    
    input [POS_SIZE-1:0]        bestScoreInBase,
    output reg [POS_SIZE-1:0]   bestScoreOutBase
    );

    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////
    
    // E-out/Left gap, SRgap
    wire [SCORE_SIZE-1:0]       extendReadGap;
    wire [SCORE_SIZE-1:0]       newReadGap;
    wire [SCORE_SIZE-1:0]       extendRefGap;
    wire [SCORE_SIZE-1:0]       newRefGap;
    
    // next values for the ref and read gaps
    reg  [SCORE_SIZE-1:0]       nextRefGap;
    reg  [SCORE_SIZE-1:0]       nextReadGap;
    
    // Max of Up and Left (the gap score)
    reg  [SCORE_SIZE-1:0]       nextGapMax;

    // Value of diagonal match/mismatch score
    reg  [SCORE_SIZE-1:0] 	similarityDiagScore;
   
   
    // next value for the similarity score
    reg  [SCORE_SIZE-1:0]       nextSimilarity;

    // next value for the indel bit
    reg                          nextIndelOut;   //HL-May20th
    
    // next value for the best indel bit, best score and base number
    reg                           nextBestIndelOut;       //HL-May20th
    reg  [SCORE_SIZE-1:0]       nextBestScoreOut;
    reg  [POS_SIZE-1:0]         nextBestScoreOutBase;
   
    // compute cost of opening new gaps or extending gaps
    assign extendReadGap        = readGap           - GAP_EXTEND_COST;
    assign newReadGap           = similarity        - GAP_OPEN_COST;
    assign extendRefGap         = refGapIn          - GAP_EXTEND_COST;
    assign newRefGap            = similarityUpIn    - GAP_OPEN_COST;
    
    // compute the next scores
    always @ (*) begin
        nextReadGap = 0;
        nextRefGap = 0;
        nextSimilarity = 0;
        similarityDiagScore = 0;
       
        // compute the next read gap score
        if($signed(extendReadGap) > $signed(newReadGap)) begin
            nextReadGap         = extendReadGap;
        end else begin
            nextReadGap         = newReadGap;
        end

        // compute the next ref gap score
        if ($signed(extendRefGap) > $signed(newRefGap)) begin
            nextRefGap          = extendRefGap;
        end else begin
            nextRefGap          = newRefGap;
        end

            // compute the diagonal match/mismatch score
            if (match) begin
                similarityDiagScore  = similarityDiagIn + MATCH_BONUS;
            end else begin
                similarityDiagScore  = similarityDiagIn - MISMATCH_PENALTY;
            end

            // compute the max of the read and reference gaps
            if ($signed(nextRefGap) > $signed(similarityDiagScore)) begin
                nextGapMax          = nextRefGap;
	        nextIndelOut        = 0;
            end else begin
                nextGapMax          = similarityDiagScore;
	        nextIndelOut        = indelDiagIn;
            end

	   
            // take the gap score if it is greater than the match/mismatch score
            if ($signed(nextGapMax) > $signed(nextReadGap)) begin
                nextSimilarity      = nextGapMax;
            end else begin 
                nextSimilarity      = nextReadGap;
                nextIndelOut        = 0;          //HL-May20th
            end


        // compute the next similarity score
        // only compute a new similarity score if this cell is enabled
        if (!en) begin
            nextSimilarity      = similarityUpIn;
            nextIndelOut        = indelUpIn;                    //HL-May20th
        end

        // compute the next best score out and position
        if (!en) begin
            //not enabled
            nextBestIndelOut    = bestIndelIn;          //HL-May20th
            nextBestScoreOut    = bestScoreIn;
            nextBestScoreOutBase= bestScoreInBase;
        end else if ($signed(similarityUpIn) > $signed(bestScoreIn)) begin 
            nextBestIndelOut    = indelUpIn;                    //HL-May20th
            nextBestScoreOut    = similarityUpIn;
            nextBestScoreOutBase= BASE_NUMBER-1;
        end else begin
            nextBestIndelOut    = bestIndelIn;          //HL-May20th
            nextBestScoreOut    = bestScoreIn;
            nextBestScoreOutBase= bestScoreInBase;
        end
    end

    // Systolic cell created here
    // only store readGap, refGap, and similarity
    // added: track the score and position of the best score anywhere in the matrix
    always @(posedge clk) begin
        if (rst) begin
            readGap             <= 0;
            refGap              <= 0;
            similarity          <= 0;
            indelOut            <= 0;                   //HL-May20th
            bestIndelOut        <= 0;                   //HL-May20th
            bestScoreOut        <= 0;
            bestScoreOutBase    <= 0;
        end else if (RefSend) begin
            if (clipStart) begin
                readGap         <= 0;
                refGap          <= 0;
                similarity      <= 0;
                indelOut        <= 0;                   //HL-May20th
                bestIndelOut            <= 0; //HL-May20th: TODO: Oh no! It is clipped... can't tell
                bestScoreOut    <= 0;
                bestScoreOutBase<= BASE_NUMBER;
            end else begin
                readGap         <= 0 - (BASE_NUMBER * GAP_EXTEND_COST + GAP_OPEN_COST);
                refGap          <= 0 - (BASE_NUMBER * GAP_EXTEND_COST + GAP_OPEN_COST);
                similarity      <= 0 - (BASE_NUMBER * GAP_EXTEND_COST + GAP_OPEN_COST);
                indelOut        <= 0;                   //HL-May20th
                bestIndelOut    <= 0 ;                  //HL-May20th
                bestScoreOut    <= 0 - (BASE_NUMBER * GAP_EXTEND_COST + GAP_OPEN_COST);
                bestScoreOutBase<= BASE_NUMBER;
            end
        end else begin
            readGap             <= nextReadGap;
            refGap              <= nextRefGap;
            similarity          <= nextSimilarity;
            indelOut            <= nextIndelOut;       //HL-May20th
            bestIndelOut        <= nextBestIndelOut;    //HL-May20th
            bestScoreOut        <= nextBestScoreOut;
            bestScoreOutBase    <= nextBestScoreOutBase;
        end
    end
           
endmodule 

/*
 * Module:      FirstRowComputeScore
 * 
 * Description: This module uses the affine gap model to compute a Smith Waterman score
 *                     for one read base and one reference base, for the first short read base (first row)
 * 
 * Ports: 
 *       <dir> <signal>         -<description>
 *          O   refGap          - output score for a reference gap
 *          O   similarity      - final score for this cell
 *          
 *          I   clk             - system clock
 *          I   rst             - system reset
 *          
 *          I   ReadSend        - asserted when a new short read is sent
 *          I   RefSend         - asserted when a new reference sequence is sent  
 *          I   match           - asserted when current reference base equals read base
 *
 *          I   clipStart       - cells should be reset to 0 instead of negative values when
 *                                asserted, which effectively allows the start of the short 
 *                                read to be clipped for free 
 *
 *          O   bestScoreOut    - best score of any cell including this one (but not cells after
 *                                this one)
 *          O   bestScoreOutBase- index of the read base with the best score after this cell is
 *                                included (this will either take the value of bestScoreInBase or
 *                                BASE_NUMBER)
 *
 * Author:      Corey Olson
 * Date:        6-20-2011
 */
// module FirstRowComputeScore #(
    
//     ////////////////
//     // PARAMETERS //
//     ////////////////
//     parameter SCORE_SIZE        = 9,
//     parameter GAP_OPEN_COST     = 2,
//     parameter GAP_EXTEND_COST   = 1,
//     parameter MISMATCH_PENALTY  = 2,
//     parameter MATCH_BONUS       = 2,
//     parameter BASE_NUMBER       = 0,    // which base this currently is
//     parameter POS_SIZE          = 8     // number of bits to use to store the read base with the best score
//     )
    
//     (
//     ///////////
//     // PORTS //
//     ///////////
//     input                       clk,
//     input                       rst,
    
//     input                       RefSend,
//     input                       match,
//     input                       clipStart,
    
    
//     output reg [SCORE_SIZE-1:0] refGap,    
//     output reg [SCORE_SIZE-1:0] readGap,
//     output reg [SCORE_SIZE-1:0] similarity,
//                 output reg                                                                      indelOut,                       //HL-May20th: Indel bit

//                 output                                                                                  bestIndelOut, //HL-May20th: Indel bit
//     output     [SCORE_SIZE-1:0] bestScoreOut,
//     output     [POS_SIZE-1:0]   bestScoreOutBase
//     );

//     //////////////////////
//     // INTERNAL SIGNALS //
//     //////////////////////
    
//     // E-out/Left gap, SRgap
//     wire [SCORE_SIZE-1:0]       extendReadGap;
//     wire [SCORE_SIZE-1:0]       newReadGap;
    
//     // next values for the ref and read gaps
//     reg  [SCORE_SIZE-1:0]       nextReadGap;
//     wire [SCORE_SIZE-1:0]       nextRefGap;
    
//     // Max of Up and Left (the gap score)
//     reg  [SCORE_SIZE-1:0]       nextGapMax;

//     // next value for the similarity score
//     reg  [SCORE_SIZE-1:0]       nextSimilarity;

//                 // HL-May20th: next value for the Indel bit
//                 reg                                                                                                     nextIndelOut;

//     // compute cost of opening new gaps or extending gaps
//     assign extendReadGap        = readGap       - GAP_EXTEND_COST;
//     assign newReadGap           = similarity    - GAP_OPEN_COST;
//     assign nextRefGap           = 0             - GAP_OPEN_COST;
    
//     // compute the next scores
//     always @ (*) begin
//         nextReadGap = 0;
//         nextSimilarity = 0;
//                                 nextIndelOut     = 0; //HL-May20th

//         // compute the next read gap score
//         if($signed(extendReadGap) > $signed(newReadGap)) begin
//             nextReadGap         = extendReadGap;
//         end else begin
//             nextReadGap         = newReadGap;
//         end

//         // compute the max of the read and reference gaps
//         if ($signed(nextRefGap) > $signed(nextReadGap)) begin
//             nextGapMax          = nextRefGap;
//         end else begin
//             nextGapMax          = nextReadGap;
//         end

//         // compute the next similarity score
//         // compute the diagonal match/mismatch score
//         if (match) begin
//             nextSimilarity      = MATCH_BONUS;
//         end else begin
//             nextSimilarity      = - MISMATCH_PENALTY;
//         end
//         // take the gap score if it is greater than the match/mismatch score
//         if ($signed(nextGapMax) > $signed(nextSimilarity)) begin
//             nextSimilarity      = nextGapMax;
//         end else begin
//            //Score is from substitution or match
//            nextIndelOut         = 1;                    //HL-May20th: Indel bit
//         end
//     end

//     // Systolic cell created here
//     // only store readGap, and similarity
//     // added: track the score and position of the best score anywhere in the matrix
//     always @(posedge clk) begin
//         if (rst) begin
//             readGap             <= 0;
//             similarity          <= 0;
//             refGap              <= 0;
//                                                 indelOut                                                <= 0;                   //HL-May20th: Indel bit
//         end else if (RefSend) begin
//             if (clipStart) begin
//                 readGap         <= 0;
//                 similarity      <= 0;
//                 refGap          <= 0;
//                                                                 indelOut                                <= 0;                   //HL-May20th: TODO: Oh No! Seq Clipped...
//             end else begin
//                 readGap         <= nextRefGap;
//                 similarity      <= nextRefGap;
//                 refGap          <= nextRefGap;
//                                                                 indelOut                                <= 1;                   //HL-May20th: new reference coming in.
//             end
//         end else begin
//             readGap             <= nextReadGap;
//             similarity          <= nextSimilarity;
//             refGap              <= nextRefGap;
//                                                 indelOut                                                <= nextIndelOut;
//         end
//     end
    
//     // best score out is just the similarity
//                 assign bestIndelOut                     = indelOut;
//     assign bestScoreOut     = similarity;
//     assign bestScoreOutBase = BASE_NUMBER;

// endmodule
