/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : SmWaModule.v
*
* Description: This module is the Smith Waterman Array, consisting of N Smith-Waterman units, 
*               where N is the number of short read bases.  The array is isolated in it's own
*               clock region by FIFOs (read data, reference data, score data).  The SWController
*               controls the flow of data within this module (tells reads to be loaded into the
*               systolic array (SmWaArray), tells references to be loaded into the reference
*               shifter, tells SmWaArray when to start a new reference).
*
*               Note: the purpose of the last reference fifo and last read fifo are to know when
*               the entire reference/read has been loaded into the respective reference or read
*               fifo.  This is because we cannot stall an alignment in the SmWaArray once an
*               alignment has begun, so we do not start the alignment until we have all the data.
*               
* Creation Date : Thu 20 Oct 2011 11:54:03 AM PDT
*
* Author : Corey Olson
*
* Last Modified : Fri 02 Mar 2012 05:23:09 PM PST
*
*/
// TODO: if target is revComp, need to align the last 'refLength' bases of the reference, not the
//       first
module SmWaModule #(
    parameter MAX_READ_SIZE         = 76,
    parameter STREAM_WIDTH          = 128,
    parameter BASE_SIZE             = 2,
    parameter POS_SIZE              = 8,
    parameter READ_POS_SIZE         = 8,
    parameter SCORE_SIZE            = 9,
    parameter BASES_PER_TRANSFER    = 64,
    parameter REFERENCE_WIDTH       = 128,
    parameter REF_ALIGN_LENGTH      = 7
    // DO NOT MODIFY BELOW THIS LINE
    )
    (
    
    input                               clk,
    input                               rst,
    
    input                               clkSW,
    input                               rstSW,
   
    // Target controls 
    input                               updateReadIn,   // should grab the next read from the read
                                                        //  fifo when asserted
    input                               lastRefIn,      // signal is asserted for the last
                                                        //  reference transmission
    input                               clipStartIn,    // flag to allow clipping at start of read
    input                               clipEndIn,      // flag to allow clipping at end of read
    input   [POS_SIZE-1:0]              refLengthIn,    // length of the reference (in bases) 

    // Target data
    input [REFERENCE_WIDTH-1:0]         refIn,          // reference data to the ref shift register
    input                               refInValid,     // valid reference data for the shift register
    output                              refInReady,     // shift register accepting ref data
    input [REF_ALIGN_LENGTH-1:0]        firstRefAlignIn,
    
    // Query data
    input                               readInValid,    // read data to the SW array is valid
    output                              readInReady,    // SW array is accepting read data
    input [STREAM_WIDTH-1:0]            readIn,         // read data to the SW array
    input                               lastReadIn,     // asserted with the last transfer of a read
    input   [READ_POS_SIZE-1:0]         readLengthIn,   // length of the short read (in bases)

    // Alignment data
		output															indelOut,																									//HL-May20th
    output [POS_SIZE-1:0]               positionOut,    // position of last base for best alignment 
                                                        //  with respect to the CAL
    output [SCORE_SIZE-1:0]             scoreOut,       // score for best alignment
    output [READ_POS_SIZE-1:0]          readBaseOut,    // base of the read with the best alignemnt
                                                        //  (only valid when clipEnd==1)
    output                              scoreOutValid,  // best alignment is valid
    input                               scoreOutReady   // output is accepting this alignment
    
    );

    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////

    wire                    updateRead; // should grab new short read from the SR fifo when asserted
    wire                    clipStart;  // allow clipping at the start of the read
    wire                    clipEnd;    // allow clipping at the end of the read
    wire    [POS_SIZE-1:0]  refLength;  // length of the reference (in bases)
    wire [REF_ALIGN_LENGTH-1:0] firstRefAlign;
    wire [REF_ALIGN_LENGTH-1:0] firstRefAlignShift;
    wire [READ_POS_SIZE-1:0]    readLength; // length of the short read (in bases)

    wire [REFERENCE_WIDTH-1:0] reference;  // 128 bit reference data parallel-loaded into the shifter
    wire [STREAM_WIDTH-1:0] readSW;     // short read data from the read fifo

    wire [BASE_SIZE-1:0]    refBase;    // Single reference base
    wire                    refDone;    // Done signal from RefShiftRegister to SmWaArray
    wire                    refValid;
    wire                    loadFirstRef;
    
    wire [SCORE_SIZE-1:0]   score;
    wire [POS_SIZE-1:0]     position;
		wire										indel;											//HL-May20th
    wire [READ_POS_SIZE-1:0]readBase;   // base of the read with the best alignment
    reg                     rst_q;
    reg                     rstSW_q;

    // register reset
    always @ (posedge clk)      rst_q   <= rst;
    always @ (posedge clkSW)    rstSW_q <= rstSW;

    /////////////////////////
    // last reference fifo //
    /////////////////////////
    assign writeLastRef = refInValid & refInReady & lastRefIn;
    assign readLastRef = referenceValid & lastReference;
    asyncFifoBRAM #(
        .WIDTH  (1+1+1+POS_SIZE+REF_ALIGN_LENGTH)
    ) lastReferenceFifo (
        .wr_clk (clk),
        .wr_rst (rst_q),
        .din    ({updateReadIn, clipStartIn, clipEndIn, refLengthIn, firstRefAlignIn}),
        .wr_en  (writeLastRef),
        .full   (lastRefFifoFull),
        
        .rd_clk (clkSW),
        .rd_rst (rstSW_q),
        .dout   ({updateRead, clipStart, clipEnd, refLength, firstRefAlign}),
        .rd_en  (readLastRef),
        .empty  (lastRefFifoEmpty)
    );
    
    ////////////////////
    // reference fifo //
    ////////////////////
    assign refInReady = ~refFifoFull;
    assign writeRef = refInValid & refInReady;
    assign readRef = referenceValid;
    asyncFifoBRAM #(
        .WIDTH  (REFERENCE_WIDTH)
    ) referenceFifo (
        .wr_clk (clk),
        .wr_rst (rst_q),
        .din    (refIn),
        .wr_en  (writeRef),
        .full   (refFifoFull),
        
        .rd_clk (clkSW),
        .rd_rst (rstSW_q),
        .dout   (reference),
        .rd_en  (readRef)
    );

   // First Reference Aligner //
/*   FirstRefAligner #(
        .REFERENCE_WIDTH (REFERENCE_WIDTH),
        .BASE_SZIE (BASE_SIZE),
        .REF_ALIGN_LENGTH(REF_ALIGN_LENGTH)
   ) firstRefAligner (
        .clk (clk),
        .rst (rst_q),
        .refSend (writeRef),
        .firstRef (firstRef),
        .refIn (refIn),
        .firstRefAlignIn (firstRefAlignIn),

        .refOut (refOut),
        .refInReady (refAlignInReady),
        .refOutReady (refAlignOutReady)
   );*/

    ///////////////////////////
    // SW loading controller //
    ///////////////////////////
    SWController #(
        .REF_POS_BITS       (POS_SIZE),
        .READ_POS_BITS      (READ_POS_SIZE),
        .BASES_PER_TRANSFER (BASES_PER_TRANSFER),
        .REF_BASES_PER_TRANSFER (REFERENCE_WIDTH / BASE_SIZE),
        .REF_ALIGN_LENGTH (REF_ALIGN_LENGTH)
    ) swcontrol (
        .clk            (clkSW),
        .rst            (rstSW_q),
        
        .lastRefValid   (~lastRefFifoEmpty),
        .updateRead     (updateRead),

        .firstRead      (firstRead),
        .lastRead       (lastRead),
        .readValid      (readValid),
        .readReady      (readReady),
        .readLength     (readLength),
        .readInValid    (~readFifoEmpty & ~lastReadFifoEmpty),

        .firstRef       (firstReference),
        .sendFirstRef   (sendFirstRef),
        .lastRef        (lastReference),
        .refsComplete   (refsComplete),
        .refLength      (refLength),
        .refValid       (referenceValid),
        .refReady       (referenceReady),

        .scoreReady     (scoreReady)
    );
        

    ////////////////////////
    // Ref Shift Register //
    ////////////////////////
    RefShiftRegister  #(
        .REF_SIZE       (REFERENCE_WIDTH / BASE_SIZE),
        .BASE_SIZE      (BASE_SIZE),
        .POS_SIZE       (POS_SIZE),
        .BASES_PER_TRANSFER (REFERENCE_WIDTH / BASE_SIZE),
        //.BASES_PER_TRANSFER (BASES_PER_TRANSFER)
        .REF_ALIGN_LENGTH (REF_ALIGN_LENGTH)
    ) refShifter (
        .clk            (clkSW),
        .rst            (rstSW_q),

        .Ref            (reference),
        .firstRef       (firstReference),
        .lastRef        (lastReference),
        .numRefBases    (refLength),
        .firstRefAlign  ({firstRefAlign[REF_ALIGN_LENGTH-1:1], 1'b0}),

        .RefSend        (referenceValid),
        .RefReady       (referenceReady),

        .RefBaseOut     (refBase),
        .Done           (refDone)
    );

    ////////////////////
    // last read fifo //
    ////////////////////
    assign writeLastRead = readInValid & readInReady & lastReadIn;
    assign readLastRead = readValid & readReady & lastRead;
    asyncFifoBRAM #(
        .WIDTH  (READ_POS_SIZE)
    ) lastReadFifo (
        .wr_clk (clk),
        .wr_rst (rst_q),
        .din    (readLengthIn),
        .wr_en  (writeLastRead),
        .full   (lastReadFifoFull),

        .rd_clk (clkSW),
        .rd_rst (rstSW_q),
        .dout   (readLength),
        .rd_en  (readLastRead),
        .empty  (lastReadFifoEmpty)
    );

    ///////////////
    // read fifo //
    ///////////////
    assign readInReady = ~readFifoFull;
    asyncFifoBRAM #(
        .WIDTH  (128)
    ) readFifo (
        .wr_clk (clk),
        .wr_rst (rst_q),
        .din    (readIn),
        .wr_en  (readInValid),
        .full   (readFifoFull),
        
        .rd_clk (clkSW),
        .rd_rst (rstSW_q),
        .dout   (readSW),
        .rd_en  (readValid & readReady),
        .empty  (readFifoEmpty)
    );
    
    reg referenceValid_prev;
    always @(posedge clkSW) begin
         if (rstSW_q) begin
            referenceValid_prev <= 0;
         end else begin
            referenceValid_prev <= referenceValid & firstReference & firstRefAlign[0];
         end
    end
/*    always @(posedge clk) begin
      if (rst) begin
         referenceValid_prev <= 0;
      end else begin
         referenceValid_prev <= referenceValid & firstReference;
      end
    end*/
    
    ///////////////
    // SmWaArray //
    ///////////////
    SmWaArray  #( 
        .MAX_READ_SIZE  (MAX_READ_SIZE), 
        .BASE_SIZE      (BASE_SIZE),
        .POS_SIZE       (POS_SIZE),
        .READ_POS_SIZE  (READ_POS_SIZE), 
        .STREAM_WIDTH   (STREAM_WIDTH),
        .SCORE_SIZE     (SCORE_SIZE)
    ) sw0 (
        .clk            (clkSW), 
        .rst            (rstSW_q), 
        
        .ReadSendIn     (readValid), 
        .ReadReady      (readReady), 
        .SRCharIn       (readSW), 
        .SRLengthIn     (readLength),
        .firstSRCharIn  (firstRead),

        .clipStartIn    (clipStart),
        .clipEndIn      (clipEnd),

        .RefCharIn      (refBase), 
        .RefSendIn      ((referenceValid & firstReference & ~firstRefAlign[0]) |
                         (referenceValid_prev)),
        .RefsComplete   (refsComplete), 
        
        .DoneIn         (refDone), 
        .DoneOut        (scoreValid), 
        .Position       (position), 
        .ReadBase       (readBase),
				.IndelOut				(indel), //HL-May20th
        .Score          (score)
    );

    ////////////////
    // score FIFO //
    ////////////////
    assign scoreReady = ~scoreFifoFull;
    assign scoreOutValid = ~scoreFifoEmpty;
    scoreFifoBRAM #(
        .WIDTH  (POS_SIZE + SCORE_SIZE + READ_POS_SIZE + 1) //HL-May20th: + 1 of indel, TODO: consider paramerterize
    ) scoreFifo (
        .wr_clk (clkSW),
        .wr_rst (rstSW_q),
        .din    ({score, position, readBase, indel}),
        .wr_en  (scoreValid),
        .full   (scoreFifoFull),
        
        .rd_clk (clk),
        .rd_rst (rst_q),
        .dout   ({scoreOut, positionOut, readBaseOut, indelOut}),
        .rd_en  (scoreOutReady),
        .empty  (scoreFifoEmpty)
    );

endmodule
