/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
* File Name : RefShiftRegister.v
*
* Description: This module holds a parallel-load, serial-shift register. 
*               It also computes the RefReady signal and passes a Done 
*               signal to the SmWaArray.  That Done signal is outputted
*               with the last valid base of the reference.
*
* Creation Date : Wed 23 Nov 2011 10:05:08 AM PST
*
* Author : Corey Olson
*
* Last Modified : Tue 28 Feb 2012 04:03:17 PM PST
*
*/
module RefShiftRegister #(
    parameter REF_SIZE              = 192,
    parameter BASE_SIZE             = 2,
    parameter POS_SIZE              = 8,
    parameter BASES_PER_TRANSFER    = 64,
    parameter REF_ALIGN_LENGTH      = 7
    )
    (
    
    input                           clk,
    input                           rst,
    
    input                           RefSend,
    input [BASE_SIZE*REF_SIZE-1:0]  Ref,
    input                           firstRef,
    input                           lastRef,
    input   [POS_SIZE-1:0]          numRefBases,
    input  [REF_ALIGN_LENGTH-1:0]   firstRefAlign,
    
    output [(BASE_SIZE-1):0]        RefBaseOut,
    output reg                      RefReady,
    output reg                      Done
    
    );
    
    //////////////////////
    // INTERNAL SIGNALS //
    //////////////////////
    reg [BASE_SIZE*REF_SIZE-1:0]    RefBases;
    reg                             last; 
    reg [POS_SIZE-1:0]              basesInLastRef;
    
    // counter for done, needs to be at least LOG2(REF_SIZE) = POS_SIZE
    // if currentRefBase == REF_SIZE-1, the first base is currently outputted
    // if currentRefBase == 0, the last base is currently outputted
    reg [POS_SIZE-1:0]              currentRefBase;
    
    // determine the number of bases in the last transfer
    always @ (posedge clk) begin
        if (rst) begin
            basesInLastRef <= 0;
        end else if (numRefBases % BASES_PER_TRANSFER == 0) begin
            basesInLastRef <= BASES_PER_TRANSFER;
        end else begin
            basesInLastRef <= numRefBases % BASES_PER_TRANSFER;
        end
    end

    // LS 'BASE_SIZE' bits of refBases are the output of this shifter
    assign RefBaseOut = RefBases[BASE_SIZE-1:0];

    // RefReady and Done 
    always @(posedge clk) begin
        if(rst) begin
            RefReady <= 1;
            Done <= 0;
        end 
        // if the first reference transfer only has one base
        else if (RefSend && firstRef && (firstRefAlign == (BASES_PER_TRANSFER - 1))) begin
            RefReady <= 1;
            Done <= 0;
        end
        // if loading a single base in (which is the last base)
        else if (RefSend && lastRef && (basesInLastRef == 1)) begin
        //else if (RefSend && RefReady && lastRef && (basesInLastRef == 1)) begin
            RefReady <= 1;
            Done <= 1;
        end 
        // load the SR on RefSend and reset the base counter
        else if (RefSend && RefReady) begin
        //else if (RefSend) begin
            RefReady <= 0;
            Done <= 0;
        end 
        // if shifting the last base out
        else if(currentRefBase == 1) begin
            RefReady <= 1;
            if (last) begin
                Done <= 1;
            end else begin
                Done <= 0;
            end
        end
        // waiting for new reference
        else begin
            RefReady <= RefReady;
            Done <= 0;
        end
    end
   
    // this tracks if the current section of the reference is the last section
    always @ (posedge clk) begin
        if (rst) begin
            last <= 0;
        end else if (RefSend && lastRef) begin
        //end else if (RefSend && RefReady && lastRef) begin
            last <= 1;
        end else if (RefSend) begin
        //end else if (RefSend && RefReady) begin
            last <= 0;
        end
    end

    // Reference shifter
    // note: reference is loaded into the shifter with the first base in the LS bit position, so
    //       shift bases to the right
    always @(posedge clk) begin
        if(rst) begin
            RefBases <= 0;
            currentRefBase <= 0;
        end else if (RefSend) begin
        //end else if (RefSend && RefReady) begin
            RefBases <= Ref;
            if (firstRef && lastRef) begin
               currentRefBase <= basesInLastRef - firstRefAlign - 1;
            end  else if (firstRef) begin
                currentRefBase <= BASES_PER_TRANSFER - firstRefAlign - 1;
            end else if (lastRef) begin
                currentRefBase <= basesInLastRef - 1;
            end else begin
                currentRefBase <= BASES_PER_TRANSFER - 1;
            end
        end else if (currentRefBase > 0) begin
            RefBases <= RefBases >> BASE_SIZE;
            currentRefBase <= currentRefBase - 1;
        end
    end

endmodule 
