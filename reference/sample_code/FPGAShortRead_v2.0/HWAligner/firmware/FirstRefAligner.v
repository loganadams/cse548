/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * File Name : FirstRefAligner.v
 *
 * Description: This module removes the excess base pairs in the first references
 *              due to memory alignment.
 *
 * Creation Date : Wed 16 Feb 2012
 *
 */

module FirstRefAligner #(
        parameter REFERENCE_WIDTH = 192,
        parameter BASE_SIZE = 2,
        parameter REF_ALIGN_LENGTH = 7
        )
   (

    input clk,
    input rst,

    input refSend,
    input firstRef,
    input lastRefIn,
    input [REFERENCE_WIDTH-1:0] refIn,
    input [REF_ALIGN_LENGTH-1:0] firstRefAlignIn,
    input referenceReady,

    output [REFERENCE_WIDTH-1:0] refOut,
    output refInReady,
    output refOutReady,
    output lastRefOut
    );
    localparam WAIT=2'd0, SHIFT=2'd1, SEND=2'd2;
       
    reg [REF_ALIGN_LENGTH-1:0] refAlign;
    reg [REFERENCE_WIDTH-1:0] reference;
    reg lastRef;
    reg [1:0] refAlignState;
    
    reg lastOut;
    reg refReady;
    reg [1:0] nextRefAlignState;
    reg [REF_ALIGN_LENGTH-1:0] nextRefAlign;
    reg [REFERENCE_WIDTH-1:0] nextReference;
    reg nextLastRef;
    reg nextRefShift;
    
    //assign refOut = firstRef ? refIn << (firstRefAlignIn * BASE_SIZE) : refIn;
    assign refOut = reference;//(refAlignState == WAIT) ? refIn : reference;
    assign refOutReady = refReady;
    assign lastRefOut = lastOut;
    assign refInReady = (refAlignState == WAIT); //&& referenceReady;

always @ (*) begin
   nextRefAlignState = refAlignState;
   nextReference = reference;
   nextRefAlign = refAlign;
   nextLastRef = lastRef;
   refReady = 0;
   lastOut = 0;
   case(refAlignState)
      WAIT: begin
         if (refSend) begin         
            nextReference = refIn;
            nextLastRef = lastRefIn;
            if (firstRef) begin
               nextRefAlignState = SHIFT;
               nextRefAlign = firstRefAlignIn;
            end else begin
               nextRefAlignState = SEND;
            end
         end
      end
      SHIFT: begin
         if (refAlign & 64) begin
            nextRefAlign = refAlign - 64;
            nextReference = reference >> (64 * BASE_SIZE);
         end else if (refAlign & 48) begin
            nextRefAlign = refAlign - 16;
            nextReference = reference >> (16 * BASE_SIZE);
         end else if (refAlign & 12) begin
            nextRefAlign = refAlign - 4;
            nextReference = reference >> (4 * BASE_SIZE);
         end else if (refAlign & 2) begin
            nextRefAlign = refAlign - 2;
            nextReference = reference >> (2 * BASE_SIZE);
         end
         if ( ~(nextRefAlign & 126) ) begin
            nextRefAlignState = SEND;
         end
      end
      SEND: begin
         refReady = 1;
         lastOut = lastRef;
         if (referenceReady) begin
            nextRefAlignState = WAIT;
         end
      end
      default: nextRefAlignState = WAIT;
   endcase
end

always @ (posedge clk) begin
   if (rst) begin
      refAlignState <= WAIT;
      reference <= 0;
      refAlign <= 0;
      lastRef <= 0;
   end else begin
      refAlignState <= nextRefAlignState;
      reference <= nextReference;
      lastRef <= nextLastRef;
      refAlign <= nextRefAlign;
   end
end

endmodule // FirstRefAligner
