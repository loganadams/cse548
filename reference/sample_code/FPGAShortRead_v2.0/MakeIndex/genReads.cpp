/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "genReads.h"

using namespace std;

char * referenceFilePath = (char*)DEFAULT_REFERENCE;
//char * indexFilePath =(char*)DEFAULT_INDEX;
char * outputFilePath = (char*)DEFAULT_OUTPUT;
int numShortReads = DEFAULT_NUM_SR;
bool reverseCompliment = false;
bool wild_file = false;
bool verbose = false;
bool ranges_set = false;
int NORMAL_PROB = 96;
int INSERTION_PROB = 1;
int DELETION_PROB = 1;
int MUTATION_PROB = 1;
int INSERTION_LEN = 8;
int DELETION_LEN = 8; 
int MUTATION_LEN = 8; 

param_ranges ranges;


int main(int argc, char* argv[])
{
    argp_parse (&argp, argc, argv, 0, 0, NULL);
    srand(time(NULL));

    Reference *reference = new Reference();
    FILE * referenceFile = fopen(referenceFilePath, "rb");
    /*
    PointerTable * pointerTable = new PointerTable();
    FILE * inputFile = fopen(indexFilePath, "rb");
    
    header_t inputHeader;
    fread(&inputHeader, sizeof(header_t), 1,inputFile);
    printf("Size of Reference (bytes): %llu\n", inputHeader.ref_length);
    pointerTable->read(inputFile);
    reference->read(inputFile, inputHeader.ref_length);    
    */

    //reference->readReference(referenceFile);
    reference->readReference(referenceFilePath);
    uint64_t ref_size = reference->getNumBases();  
    char * char_ref = reference->getCharReference();  

    //uint64_t * ref = reference->getBinaryReference();
    //uint64_t ref_size = reference->getReferenceSize() * 4; // num bytes times num bases per byte

    printf("Creating %d short reads\n", numShortReads);
    printf("Opening output file: %s\n", outputFilePath);
    FILE * outputFile = fopen(outputFilePath, "w");
    if (outputFile == NULL) {
        fprintf(stderr, "Error opening file %s\n", outputFilePath);
        exit(1);
    }

    char stringRead[READ_LENGTH+1];
    char * terminate = (char*)"\n+\n############################################################################\n";
    for(int i = 0; i < numShortReads; i++)
    {
        
        //const char * sr_info = getRandomRead(reference, ref_size, reverseCompliment, stringRead);
        const char * sr_info = getRandomRead(char_ref, ref_size, stringRead);
        
        fwrite(sr_info, strlen(sr_info), 1, outputFile);

        stringRead[READ_LENGTH] = '\0';
        if(verbose)
        {
            printf("Read finalized: %s\n\n", stringRead);
        }        
        fwrite(stringRead, READ_LENGTH, 1, outputFile);
        fwrite(terminate, sizeof(char)*80, 1, outputFile);
    }

    if(outputFile != NULL)    
    {
        printf("closing output file: %s\n", outputFilePath);
        fclose(outputFile);
    }
}

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
    /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    char * params;
    int paramters[7];
    switch (key)
    {
    case 'n':
        numShortReads = atoi(arg);
        break;
    case'r':
        reverseCompliment = true;
        printf("Producting reverse compliment short reads\n");
        break;
/*
    case 'i':
        indexFilePath = arg;
        printf("Now using index file : %s\n",arg);
        break;
*/
    case 'o':
        printf("Now writing output file : %s\n",arg);
        outputFilePath = arg;
        break;
    case 'w':
        printf("Producing wild short reads file!\n");
        wild_file = true;
        break;
    case 'v':
        printf("Producing verbose output\n");
        verbose = true;
        break;
    case 'p':
        printf("New input parameters\n");
        params = strtok(arg,",");
        NORMAL_PROB = atoi(params);
        params = strtok(NULL, ",");
        INSERTION_PROB = atoi(params);
        params = strtok(NULL, ",");
        DELETION_PROB = atoi(params);
        params = strtok(NULL, ",");
        MUTATION_PROB = atoi(params);
        params = strtok(NULL, ",");
        INSERTION_LEN = atoi(params);
        params = strtok(NULL, ",");
        DELETION_LEN = atoi(params);
        params = strtok(NULL, ",");
        MUTATION_LEN = atoi(params);
        printf("NORMAL_PROB = %d, INSERTION_PROB = %d, DELETION_PROB = %d, MUTATION_PROB = %d\n", NORMAL_PROB, INSERTION_PROB, DELETION_PROB, MUTATION_PROB);
        printf("INSERTION_LEN = %d, DELETION_LEN = %d, MUTATION_LEN = %d\n", INSERTION_LEN, DELETION_LEN, MUTATION_LEN);
        break;
    case ARGP_KEY_ARG:
        if (state->arg_num >= 4)
        /* Too many arguments. */
        argp_usage (state);
        break;
    case ARGP_KEY_END:
        if (state->arg_num < 0)
        /* Not enough arguments. */
        argp_usage (state);
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

const char * getRandomRead(char * reference, uint64_t ref_size, char * dest)
{
    uint64_t randomIndex;
    std::ostringstream sr_info;
    randomIndex = rand() % (ref_size - READ_LENGTH);
    
    sr_info << randomIndex;
    if (verbose) { printf("Read index: %lu\n", randomIndex); }

    if (wild_file == false)
    {
        for(int i = 0; i < READ_LENGTH; i++)
        {
            dest[i] = reference[randomIndex+i];
        }
    
        if(reverseCompliment)
        {
            // I believe this method only works with even lengths
            convert_to_revcomp(dest, READ_LENGTH);
            sr_info << " reverse\n";
        }
        else
        {
            sr_info << " forward\n";
        }
    }
    else
    {
        char * shortRead = new char[READ_LENGTH+1]();
        shortRead[READ_LENGTH] = '\0';
        
        int read_direction = rand() % 2;
        if(read_direction == reverse)
        {
            if (verbose) { printf("Reverse read"); }
            sr_info << " reverse";
        }
        else
        {
            if (verbose) { printf("Forward read"); }
            sr_info << " forward";
        }
      	
        int nextCharIndex = randomIndex;
        int readIndex = 0;
        while(readIndex < READ_LENGTH)        
        {
            int i, base, length;
            int char_type = getCharType();
            switch(char_type)
            {
            case normal:
                shortRead[readIndex] = reference[nextCharIndex];
                readIndex++;
                nextCharIndex++;
                break;
            case insertion:
                length = rand() % INSERTION_LEN + 1;
                sr_info << ", ";
                sr_info << length;
                sr_info << " base long insertion at ";
                sr_info << readIndex;
                if (verbose) { printf(", %d base long insertion at %u", length, readIndex); }
                base = rand() % 4;
                for(i = 0; i < length; i++)
                {
                    if(readIndex >= READ_LENGTH) { break; }
                    shortRead[readIndex] = bases[base];
                    readIndex++;
                }
                break;
            case deletion:
                length = rand() % DELETION_LEN + 1;
                sr_info << ", ";
                sr_info << length;
                sr_info << " base long deletion at ";
                sr_info << readIndex;
                if (verbose) { printf(", %d base long deletion at %u", length, readIndex); }
                nextCharIndex += length;
                break;
            case mutation:
                length = rand() % MUTATION_LEN + 1;
                sr_info << ", ";
                sr_info << length;
                sr_info << " base long mutation at ";
                sr_info << readIndex;
                if (verbose) { printf(", %d base long mutation at %u", length, readIndex); }
                for(i = 0; i < length; i++)
                {
                    if(readIndex >= READ_LENGTH) { break; }
                    do{
                        base = rand() % 4;
                    } while(reference[nextCharIndex] == bases[base]);
                    shortRead[readIndex] = bases[base];
                    readIndex++;
                    nextCharIndex++;
                }
                break;
            default:
                printf("ERROR: probability error\n");
                break;
            }
        }

        sr_info << "\n";
        if(verbose) { printf("\n"); }
        
        if(read_direction == reverse)
        {
            convert_to_revcomp(shortRead, READ_LENGTH);
        }

        memcpy(dest, shortRead, READ_LENGTH);

        if(verbose) {
            // output unchanged read
            for(int i = 0; i < READ_LENGTH; i++)
            {
                shortRead[i] = reference[randomIndex+i];
            }
        
            if(read_direction == reverse)
            {
                convert_to_revcomp(dest, READ_LENGTH);
            }
            printf("Read unchanged: %s\n", shortRead);
        }
    }

    return sr_info.str().c_str();
}

int getCharType()
{
	int type = rand() % (NORMAL_PROB + INSERTION_PROB + DELETION_PROB + MUTATION_PROB);
    //printf("type num: %d\n", type);
	if (type >= 0 && type < NORMAL_PROB)
	{
		return normal;	
	}
	else if (type >= NORMAL_PROB && type < (NORMAL_PROB + INSERTION_PROB))
	{
		return insertion;
	}
	else if (type >= (NORMAL_PROB + INSERTION_PROB) && type < (NORMAL_PROB + INSERTION_PROB + DELETION_PROB))
	{
		return deletion;
	}
	else if (type >= (NORMAL_PROB + INSERTION_PROB + DELETION_PROB) && type < (NORMAL_PROB + INSERTION_PROB + DELETION_PROB + MUTATION_PROB))
	{
		return mutation;
	}
	else
	{
		return -1;
	}
}


//*****************************//
// old version, no longer used //
//*****************************//
const char * getRandomRead(Reference * reference, uint64_t size, bool revComp, char * dest)
{
    uint64_t randomIndex;
    std::ostringstream sr_info;
    randomIndex = rand() % (size - READ_LENGTH);
    
    sr_info << randomIndex;

    if (wild_file == false)
    {
        uint64_t * binaryRead = new uint64_t[(READ_LENGTH/sizeof(uint64_t))+1]();

        reference->getBinarySubstring(binaryRead, randomIndex, READ_LENGTH, revComp);

        binaryToString(binaryRead, READ_LENGTH, dest);
    
        if(revComp)
        {
            sr_info << " reverse\n";
        }
        else
        {
            sr_info << " forward\n";
        }
    } 
    else
    {
        int read_direction = rand() % 2;
        int read_type = rand() % 4;
        char * shortRead = new char[READ_LENGTH+2]();

        uint64_t * binaryRead = new uint64_t[(READ_LENGTH/sizeof(uint64_t))+1]();

        reference->getBinarySubstring(binaryRead, randomIndex, READ_LENGTH+1, false);

        binaryToString(binaryRead, READ_LENGTH+1, shortRead);

        if(read_direction == reverse)
        {
            if (verbose) { printf("Reverse read:\n"); }
            sr_info << " reverse";
        }
        else
        {
            if (verbose) { printf("Forward read:\n"); }
            sr_info << " forward";
        }

        shortRead[READ_LENGTH+1] = '\0';
        if (verbose) { printf("Read: %s\n", shortRead); }

        int base, switch_index;
        switch(read_type)
        {
        case normal:
            if (verbose) { printf("normal\n"); }
            sr_info << " normal";
            break;   
        case insertion:
            switch_index = rand() % READ_LENGTH;
            sr_info << " insertion at ";
            sr_info << switch_index+1;
            if (verbose) { printf("insertion at %d:\n", switch_index); }
            for(int i = READ_LENGTH - 1; i > switch_index; i--)
            {
                shortRead[i] = shortRead[i-1];
            }
            base = rand() % 4;
            shortRead[switch_index] = bases[base];
            if (verbose) { printf("Read: %s\n", shortRead); }
            break;
        case deletion:
            switch_index = rand() % READ_LENGTH;
            sr_info << " deletion at ";
            sr_info << switch_index+1;
            if (verbose) { printf("deletion at %d:\n", switch_index); }
            for(int i = switch_index; i < READ_LENGTH+1; i++)
            {
                shortRead[i] = shortRead[i+1];
            }
            if (verbose) { printf("Read: %s\n", shortRead); }
            break;
        case mutation:
            switch_index = rand() % READ_LENGTH;
            sr_info << " mutation at ";
            sr_info << switch_index+1;
            if (verbose) { printf("mutation at %d:\n", switch_index); }
            do{
                base = rand() % 4;
            } while(shortRead[switch_index] == bases[base]);
            shortRead[switch_index] = bases[base];
            if (verbose) { printf("Read: %s\n", shortRead); }
            break;
        }

        sr_info << "\n";

        if(read_direction == reverse)
        {
            convert_to_revcomp(shortRead, READ_LENGTH);
        }

        memcpy(dest, shortRead, READ_LENGTH);
    }
    return sr_info.str().c_str();
}
