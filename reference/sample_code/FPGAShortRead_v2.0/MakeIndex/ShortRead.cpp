/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "ShortRead.h"


ShortRead::ShortRead(char * read){
    shortRead = new uint64_t [(READ_LENGTH*BITS_PER_BASE+(sizeof(uint64_t)*8-1))/(sizeof(uint64_t)*8)](); 
    charToBinary(read, shortRead, READ_LENGTH);
    currentSeed = 0;
}

ShortRead::ShortRead(FILE * shortReadFile){
    currentSeed = 0;
    char buffer[MAX_READ_LINE_LENGTH];
    shortRead = new uint64_t [(READ_LENGTH*BITS_PER_BASE+(sizeof(uint64_t)*8-1))/(sizeof(uint64_t)*8)](); 
    while(!feof(shortReadFile)){
    char * returned = fgets(buffer, MAX_READ_LINE_LENGTH,shortReadFile);
    if(returned == NULL){
        valid = false;
        break;
    }
    if(!feof(shortReadFile)){
        // if the first character can be interpreted as a base
        // then we have found a short read
        if(isBase(buffer[0])){ 
        charToBinary(buffer, shortRead, READ_LENGTH);
        //printf("Current Short Read: %s\n", buffer);
        valid = true;
        break;
        }
    }
    }
}
// This needs to be fixed. You can't return stack allocated structs
/*read_table_line ShortRead::getShortRead(){
    read_table_line sr;
    for(int i = 0; i < ((READ_LENGTH*BITS_PER_BASE+(sizeof(uint64_t)*8-1))/(sizeof(uint64_t)*8)); i++){
        sr.data[i] = shortRead[i];
    }
    return sr;
    }*/
uint64_t * ShortRead::getShortRead(){
    return shortRead;
}


uint64_t ShortRead::getCurrentPosition(){
    return currentSeed-1;
}

bool ShortRead::isValid(){
    return valid;
}

ShortRead::~ShortRead(){
    delete shortRead;
}

void ShortRead::toString(char * dest){
    binaryToString(shortRead, READ_LENGTH, dest);
}

Seed * ShortRead::getNextSeed(){
    if(currentSeed <= READ_LENGTH-SEED_LENGTH && shortRead != NULL){
    return getSeedAtIndex(currentSeed++);
    }else{
    return NULL;
    }
}

Seed * ShortRead::getSeedAtIndex(int index){
    uint64_t value = 0;
    getBinarySubstring(shortRead,&value, index, SEED_LENGTH, 0);
    return new Seed(value&SEED_MASK);
}
