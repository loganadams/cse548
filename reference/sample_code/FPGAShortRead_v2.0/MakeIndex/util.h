/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef UTIL_H_
#define UTIL_H_
/**
 * @file util.h
 * @author Dustin Richmond
 *
 * @version 1.0
 * util.h is the header file for util.cpp.
 *
 * util.h/cpp defines a collection of utility functions that are used
 * for building and reading the reference
 */

#include <stdio.h>
#include <stdint.h>
#include "defines.h"
/**
 * set_base sets a single binary base in a binary reference string
 *
 * @param string the array of uint64_t's that represents the reference string
 * @param index the index of the base to set in the binary string
 * @param value the value that the index in the string will be set to
 * */
void set_base(uint64_t *string,uint64_t index , uint64_t value);

/**
 * get_base returns the value of a single base in a binary reference string
 *
 * @param string the array of uint64_t's that represents the reference string
 * @param index the index of the base to read in the binary string
 *
 */
uint64_t get_base(uint64_t *string, uint64_t index);

/**
 * reverseBinaryString reverses and complements
 * a string of binary bases. 
 *
 * @param source A pointer to the binary string of bases
 * @param numBases the number of bases (length) of the binary string
 *
 * */
void reverseCompBinaryString(uint64_t * source, uint64_t numBases);

void getBinarySubstring(uint64_t* source, uint64_t * dest, uint64_t start, uint64_t numBases, bool revComp);
/**
 * charToBinary converts a character string of bases num_bases long
 * into its corresponding binary representation and puts 
 * the result in dest
 *
 * @param ascii_buffer A pointer to the character string of bases
 * @param dest A pointer to where the binary reference should be stored
 * @param num_bases The number of bases in the ascii buffer
 */
void    charToBinary(char * ascii_buffer, uint64_t *dest, uint64_t num_bases);

/**
 * convert_to_revcomp converts a character string of DNA bases (A,C,T,G) to it's 
 * reverse complement. The method executes in place.
 *
 * @param data A pointer to the character string of bases
 * @param length The length of the character string pointed to by data
 */
void    convert_to_revcomp(char * data, uint64_t length);

/**
 * charToBase converts a single character base to it's binary representation
 * 
 * @param base The character base to be converted
 * @return the binary representation of a single base
 */
int    charToBase(char base);

/**
 * baseToChar takes a single binary base and returns the character value. It performs
 * the pseudo-inverse (see warning) of charToBase
 *
 * Warning, A and N map to the same binary representation. All N's in binary will
 * be interpreted as A's
 *
 * @param base The binary representation of a single base
 * @return the character representation of the binary base that was provided
 */
char    baseToChar(int base);

/**
 * getComplement takes a single character base and returns its complement.
 * 
 * Note: to convert a single BINARY base, just use the ~ operator
 * 
 * @param base The character base to be complemented
 * @return base The complement of the base that was provided
 */
char    getComplement(char base);

/**
 * binaryToString takes a string of bases in binary representation and converts the
 * string into a character string of bases located in dest
 *
 * 
 * @param data A pointer to the binary string of bases
 * @param dest A pointer to where the character reference should be stored
 * @param length The number of bases in the binary buffer
 *
 */
void    binaryToString(uint64_t * data,uint64_t length, char * dest);

/**
 * isBase returns true if the base passed to the method is A, C, T, G, or N
 *
 * @param base the character base to be tested
 * @return an int/bool. 1/True if the character is a base, 0/False if not.
 */
int    isBase(char base);

/**
* countNumReads returns the number of short reads in a short reads file
*
* @param shortReadFile the file handle for the short reads file
* @return a uint64_t the number of short reads found in the file
*/
uint64_t countNumReads(FILE * shortReadFile);
#endif
