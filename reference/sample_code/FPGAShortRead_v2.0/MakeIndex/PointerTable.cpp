/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file PointerTable.cpp
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * PointerTable.cpp implements the methods described in PointerTable.h
 * that comprise the PointerTable object
 *
 */

#include "PointerTable.h"

/**
 * PointerTable is the constructor for a PointerTable object. It allocates a large table
 * that will be filled by the temporary CALTable if the index file is being built, or
 * filled by binary data if the index is being read.
 *
 * @returns a PointerTable object
 * 
 * */
PointerTable::PointerTable(){
    srand(time(NULL));
    table = new ptrTable_t[1LL<<IDX_BITS]();
}

/**
 * ~PointerTable is the desrtructor for a PointerTable object. It deletes the large
 * pointer table that was allocated during initialization
 *
 * 
 * */
PointerTable::~PointerTable(){
    delete table;
}

/**
 * read reads the pointer table from a previously opened FILE pointer
 * into the pointer table that was allocated when
 * the PointerTable object was constructed.
 *
 * An error will occur and the program will exit if the number of
 * entries read does not equal the number of entries expected
 * 
 * @param source the previously opened FILE pointer where the binary table data will be
 * read from
 *
 * */
void PointerTable::read(FILE * source){
    uint64_t numExpected = (1LL<<IDX_BITS);
    uint64_t numRead =  fread(table, sizeof(ptrTable_t),1LL<<IDX_BITS, source);
    if(numRead != numExpected){
        fprintf(stderr, 
                "Error: Expected to read %lu entries from index file for Pointer Table, got %lu\n",
                numExpected,
                numRead);
        exit(1);
    }
}

/**
 * write writes the current pointer table into previously opened FILE pointer
 *
 * An error will occur and the program will exit if the number of
 * entries written does not equal the number of entries expected
 * 
 * @param destination previously opened FILE pointer where the binary table data will be
 * written to
 *
 * */
void PointerTable::write(FILE * destination){
    uint64_t numExpected = (1LL<<IDX_BITS);
    uint64_t numWritten = fwrite(table, sizeof(ptrTable_t), 1LL<<IDX_BITS,destination);
    if(numWritten != numExpected){
        fprintf(stderr, 
                "Error: Expected to write %lu entries to reference file for Pointer Table. Wrote %lu\n",
                numExpected,
                numWritten);
        exit(1);
    }
}

void PointerTable::writeToTextFile(const char * textFile){
  uint64_t numEntries = (1LL<<IDX_BITS);
  FILE * ptrTextFile = fopen(textFile, "w");
  if(ptrTextFile == NULL){
    fprintf(stderr, "Could not open %s for output\n", textFile);
    exit(1);
  }
  //Header!
  //fprintf(ptrTextFile, "%d\n", IDX_BITS);
  //fprintf(ptrTextFile, "%d\n", OFFSET_BITS);
  uint64_t prevStart=0, startPtr, prevOffset, nextOffset;
  for(uint64_t currentEntry=0; currentEntry<numEntries; currentEntry++){
    startPtr = table[currentEntry].startPtr;
    if (startPtr != prevStart) {
      printf("\n** Pointer Table failed consistency checks **\n");
      exit(EXIT_FAILURE);
    }
    fprintf(ptrTextFile, "%lu ", startPtr);
    prevOffset = 0;
    for (int i=0; i<(1<<TAG_BITS); i++) {
      nextOffset = getOffset(&table[currentEntry], i);
      if (nextOffset < prevOffset) {
	printf("\n** Pointer table offsets failed consistency checks **\n");
	exit(EXIT_FAILURE);
      }
      fprintf(ptrTextFile, "%lu ", nextOffset);
      prevOffset = nextOffset;
    }
    fprintf(ptrTextFile, "\n");
    prevStart = startPtr + prevOffset;
  }
  fclose(ptrTextFile);
}

void diffPtrTables(ptrTable_t *a, ptrTable_t* b){
    for(int i =0 ; i  < (1LL<<IDX_BITS); i++){
        if(memcmp(&a[i], &b[i], sizeof(ptrTable_t))){
            printf("a:");printLine(a, i);
            printf("b:");printLine(b, i);
        }
    }

}

/**
 * buildPointerTable builds the pointer table from the temporary CALTable that was
 * created by a CALTable object
 *
 * @param tempTable The temporary CAL table constructed by a CALTable object
 * @param numTableEntries The number of VALID entires in the temporary table
 * (Invalid entries' seed field points to NULL)
 *
 * */
void PointerTable::buildPointerTable(FILE*  tempTable){
    //    ptrTable_t * table = new ptrTable_t[1LL<<IDX_BITS]();
    memset( table, 0, sizeof(ptrTable_t)*(1LL<<IDX_BITS));
    uint64_t currentPTIndex = 0;
    uint64_t currentPTTag = 0;
    uint64_t runningSum = 0;
    Seed currentSeed;
    uint64_t overallIndex =0;
    // Scan the temporary table until a valid seed is found
    // Record the valid seed's index and tag
    clearerr(tempTable);
    fseek(tempTable, 0 , SEEK_SET);
    fread(&currentSeed, sizeof(Seed),1,tempTable);
    currentPTIndex = currentSeed.getIndex();
    currentPTTag = currentSeed.getTag();
    do{
        // The Current seed's index has chaged, and we need to fill in the
        // Remaining offsets for what was the current line, and the start pointers
        // for all of the lines between the current index and the new index
        if(currentPTIndex != currentSeed.getIndex()){
            uint64_t newIndex = currentSeed.getIndex();
            //fill in remaining tags for this line in the pointer table
            for(int writePTTag = currentPTTag ; writePTTag < 1<<TAG_BITS; writePTTag++){
                setOffset(&table[currentPTIndex],writePTTag, runningSum);
            }
            currentPTIndex++;
            for(;currentPTIndex <= newIndex;currentPTIndex++){
                table[currentPTIndex].startPtr = overallIndex;
            }
            currentPTIndex = newIndex;
            currentPTTag = currentSeed.getTag();
            runningSum=1;
        }else if(currentPTTag != currentSeed.getTag()){
            // The current seed's tag has changed, and we need to fill in the 
            // offsets between the old tag and the new tag
            for(int writePTTag = currentPTTag;
                writePTTag < currentSeed.getTag(); writePTTag++){
                setOffset(&table[currentPTIndex],writePTTag, runningSum);
            }        
            currentPTTag = currentSeed.getTag();
            runningSum++;
        }else{ // Neither the index or the offset has changed.
            runningSum++;
        }
        if(runningSum > OFFSET_MASK){
            printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
            exit(1);
        }
        fread(&currentSeed, sizeof(Seed),1,tempTable);
        overallIndex++;
    }while(!feof(tempTable));
    // Fill in the remaining offsets for the current pointer table entry
    for(int writePTTag = currentPTTag ; writePTTag < (1LL<<TAG_BITS); writePTTag++){
        setOffset(&table[currentPTIndex],writePTTag, runningSum);
    }
    currentPTIndex++;
    // fill in the remaining entries in the Pointer Table. All offsets will be 0.
    for(;currentPTIndex < (1LL<<IDX_BITS); currentPTIndex++){
        table[currentPTIndex].startPtr = overallIndex;
    }
    //diffPtrTables(this->table, table);
    //printPointerTable(table);
}

/**
 * buildPointerTable builds the pointer table from the temporary CALTable that was
 * created by a CALTable object
 *
 * @param tempTable The temporary CAL table constructed by a CALTable object
 * @param numTableEntries The number of VALID entires in the temporary table
 * (Invalid entries' seed field points to NULL)
 *
 * */
void PointerTable::buildPointerTable(Seed **  tempTable, uint64_t numTableEntries){
    uint64_t currentPTIndex = 0;
    uint64_t currentPTTag = 0;
    uint64_t runningSum = 0;
    uint64_t overallIndex = 0 ;
    uint64_t currentIndex = 0;
    // Scan the temporary table until a valid seed is found
    while(tempTable[currentIndex]== NULL){ currentIndex++;}
    // Record the valid seed's index and tag
    currentPTIndex = tempTable[currentIndex]->getIndex();
    currentPTTag = tempTable[currentIndex]->getTag();
    for(; currentIndex < numTableEntries; currentIndex++){
        if(tempTable[currentIndex] != NULL){
            // The Current seed's index has chaged, and we need to fill in the
            // Remaining offsets for what was the current line, and the start pointers
            // for all of the lines between the current index and the new index
            if(currentPTIndex != tempTable[currentIndex]->getIndex()){
                uint64_t newIndex = tempTable[currentIndex]->getIndex();
                //fill in remaining tags for this line in the pointer table
                for(int writePTTag = currentPTTag ; writePTTag < 1<<TAG_BITS; writePTTag++){
                    setOffset(&table[currentPTIndex],writePTTag, runningSum);
                }
                currentPTIndex++;
                for(;currentPTIndex <= newIndex;currentPTIndex++){
                    table[currentPTIndex].startPtr = overallIndex;
                }
                currentPTIndex = newIndex;
                currentPTTag = tempTable[currentIndex]->getTag();
                runningSum=1;
            }else if(currentPTTag != tempTable[currentIndex]->getTag()){
                // The current seed's tag has changed, and we need to fill in the 
                // offsets between the old tag and the new tag
                for(int writePTTag = currentPTTag;
                    writePTTag < tempTable[currentIndex]->getTag(); writePTTag++){
                    setOffset(&table[currentPTIndex],writePTTag, runningSum);
                }        
                currentPTTag = tempTable[currentIndex]->getTag();
                runningSum++;
            }else{ // Neither the index or the offset has changed.
                runningSum++;
            }
            if(runningSum > OFFSET_MASK){
                printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
                exit(1);
            }
            overallIndex++;
        }
    }
    // Fill in the remaining offsets for the current pointer table entry
    for(int writePTTag = currentPTTag ; writePTTag < (1LL<<TAG_BITS); writePTTag++){
        setOffset(&table[currentPTIndex],writePTTag, runningSum);
    }
    currentPTIndex++;

    // fill in the remaining entries in the Pointer Table. All offsets will be 0.
    for(;currentPTIndex < (1LL<<IDX_BITS); currentPTIndex++){
        table[currentPTIndex].startPtr = overallIndex;
    }
    printf("overall index: %lu\n", overallIndex);
    //printPointerTable(table);
}

void PointerTable::printStatistics(){
    if(table == NULL){
        printf("Pointer table was null during statistics printing");
        return;
    }
    FILE * ptrTableStats  = fopen("ptrTableStats.csv", "w");
    int histogram[OFFSET_MASK+1];
    memset(histogram,0,sizeof(histogram));
    for(uint64_t i = 0 ; i < 1LL<<IDX_BITS; i ++){
        histogram[getOffset(&table[i],TAG_MASK)]++;
    }
    fprintf(ptrTableStats,"Pointer Table CALs Per Line Histogram\n");
    for(int i =0; i < OFFSET_MASK+1; i ++){
        fprintf(ptrTableStats,"%d,%d\n",i,histogram[i]);
    }
    fprintf(ptrTableStats, "Pointer Table Start Ptr vs. Index\n");
    for(uint64_t i =0 ; i < 1<<IDX_BITS; i++){
        fprintf(ptrTableStats,"%lu,%lu\n",i, table[i].startPtr);
    }
    
    fclose(ptrTableStats);
    FILE * calTableStats  = fopen("CALTableStats.csv", "w");
    memset(histogram,0,sizeof(histogram));
    for(uint64_t i =0 ; i < 1<<IDX_BITS; i++){
        for( uint64_t j = 0; j < TAG_MASK; j++){
            if(j == 0){
                histogram[getOffset(&table[i],j)]++;
            }else{
                histogram[getOffset(&table[i],j)-getOffset(&table[i],j-1)]++;
            }
        }
    }
    fprintf(calTableStats,"CALs per bucket Histogram\n");
    for(int i =0; i < OFFSET_MASK+1; i ++){
        fprintf(calTableStats,"%d,%d\n",i,histogram[i]);
    }
    fclose(calTableStats);
}


void printPointerTableStarts(ptrTable_t *table){
    for(uint64_t i =0 ; i < 1<<IDX_BITS; i++){
        printf("%lu", table[i].startPtr);
    }
}


/**
 * printPointerTable prints all of the lines in a given pointer table
 * 
 *
 * @param table A pointer table to print
 * */
void printPointerTable(ptrTable_t * table){
    //    for(uint64_t i =0 ; i < 10; i++){
    for(uint64_t i =0 ; i < 1<<IDX_BITS; i++){
        printLine(table, i);
    }
}

/**
 * printLine prints a single line from a given index in a given pointer table 
 * 
 *
 * @param table A pointer table where the line should be read from
 * @param index The index of the line to be printed
 * 
 * */
void printLine(ptrTable_t * table, uint64_t index){
    printf("Index: %lx CALStartIndex: %lx ",index, table[index].startPtr);
    for(int j = 0 ; j< 1<<TAG_BITS; j++){
        printf("Off %x: %lx ", j,getOffset(&table[index],j));
    }
    printf("\n");
}


/**
 * getRandomFromPointerTable produces a random (seed) index, (seed)tag, index and
 * length (metadata) for the CALTable. This is used for random verification of the CALtable.
 * 
 *
 * @param index   a pointer to where the randomly generated index will be stored
 * @param tag     a pointer to where the randomly generated tag will be stored
 * @param cal_ptr a pointer to where the cal_table start index (given by the randomly
 * generated index and tag)will be stored
 * @param length  a pointer to where the value representing the number of entries
 * after the start pointer will be stored
 * 
 * */
void PointerTable::getRandomFromPointerTable(uint64_t* index, uint64_t* tag, uint64_t *cal_ptr, uint64_t * length)
{
    uint64_t random;
    ptrTable_t *entry;
    uint64_t numEntries = 0;
    int i;
    do{
        random = rand() % ((1<<IDX_BITS)/ sizeof(ptrTable_t));
        entry = getIndex(random);
        for( i =0 ; i < TAG_MASK; i  ++){
            numEntries = getOffset(entry, i);
            if(numEntries !=0){
                break;
            }
        }
    } while (numEntries == 0);
    *index = random;
    *tag = i;
    if (i == 0){
        *cal_ptr = entry->startPtr;
    }else{
        *cal_ptr = (entry->startPtr + getOffset(entry, i-1));
    }
    *length = numEntries;    
}

/**
 * getCALMetadata takes a given index and tag from a seed and uses them to look up the start pointer
 * and number of potential cals in the CALTable. This data is written into the CALmetaData_t struct
 * poined to by dest.
 * 
 * 
 * @param pointerTableIndex the index into the pointer table.
 * @param tag The tag from the seed. The tag indexes a single pointer table line
 * @param dest A pointer to the metaData struct, where this method will write the start index
 * in the CALTable and the number of CAL entries to examine
 * */
void PointerTable::getCALMetadata(uint64_t pointerTableIndex, uint64_t tag, CALmetaData_t * dest){
    ptrTable_t * line = getIndex(pointerTableIndex);
    if( tag == 0){
        dest->startIndex = line->startPtr;
        dest->length = getOffset(line, 0);
    }else{
        if(tag > TAG_MASK){
            fprintf(stderr,"Error: requested %lu tag outside of tag range in PointerTable\n", tag);
            exit(1);
        }
        dest->startIndex =  line->startPtr + getOffset(line, tag-1);
        dest->length =  getOffset(line, tag) - getOffset(line, tag-1);
    }
}

/**
 * getIndex returns a pointer to the entry at a given index in the pointer table.
 *
 * @param index The index of the entry in the pointer table
 * @return a pointer to a ptrTable_t struct which is an entry in the pointer table
 *
 *
 * */
ptrTable_t * PointerTable::getIndex(uint64_t index){
    return &table[index];
}


/**
 * getPointerTable returns a pointer to this objects pointer table
 *
 * @returns a pointer to a ptrTable_t struct, which is used treated as the pointer table
 * for this object
 *
 * */
ptrTable_t * PointerTable::getPointerTable(){
    return table;
}



