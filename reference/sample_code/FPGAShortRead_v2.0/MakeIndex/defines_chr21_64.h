/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_64_H
#define CHR21_64_H

#define TYPE "CHR21_64"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 64
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/chr21_64.index"
#define DEFAULT_REFERENCE "../../data/chr21.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 32   // number of bits used for the start pointer
#define IDX_BITS       18   // number of bits of the seed used as address
#define OFFSET_BITS        8   // number of bits used for each offset
#define TAG_BITS           2    // number of bits of the seed used as tag
#define SEED_LENGTH 22 // number of bases used for the seed

#define MAX_SAME_SEED 8         // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
  uint64_t startPtr : START_POINTER_BITS;
  uint64_t offset0 : OFFSET_BITS;
  uint64_t offset1 : OFFSET_BITS;
  uint64_t offset2 : OFFSET_BITS;
  uint64_t offset3 : OFFSET_BITS;
} ptrTable_t;

#define setOffset(name, index, value)                    \
    switch (index) {                            \
    case (0) : (name)->offset0 = (value); break;            \
    case (1) : (name)->offset1 = (value); break;            \
    case (2) : (name)->offset2 = (value); break;            \
    case (3) : (name)->offset3 = (value); break;            \
    default : {fprintf(stderr, "setOffset index = %d\n", index); exit(1);} \
    }

#define getOffset(name, index)                    \
    (((index) == 0) ? (name)->offset0 :                \
     ((index) == 1) ? (name)->offset1 :                \
     ((index) == 2) ? (name)->offset2 :                \
     ((index) == 3) ? (name)->offset3 :                \
     (fprintf(stderr, "setOffset index = %d\n", index), 0))

inline uint64_t do_hash(uint64_t in) {

   uint32_t mask_table1[16] = {
    0x00000000, 0x05dcb2a3, 0x0f6e9761, 0x0ab225c2,
    0x0bfde90c, 0x0e215baf, 0x04937e6d, 0x014fccce,
    0x0687898b, 0x035b3b28, 0x09e91eea, 0x0c35ac49,
    0x0d7a6087, 0x08a6d224, 0x0214f7e6, 0x07c84545
  };
   uint32_t mask_table2[16] = {
    0x00000000, 0x09f35641, 0x0f057c30, 0x06f62a71,
    0x026141cc, 0x0b92178d, 0x0d643dfc, 0x04976bbd,
    0x09108d51, 0x00e3db10, 0x0615f161, 0x0fe6a720,
    0x0b71cc9d, 0x02829adc, 0x0474b0ad, 0x0d87e6ec
  };
   uint32_t mask_table3[16] = {
    0x00000000, 0x06b873ea, 0x0b483aec, 0x0df04906,
    0x0f4006b0, 0x09f8755a, 0x04083c5c, 0x02b04fb6,
    0x084ff610, 0x0ef785fa, 0x0307ccfc, 0x05bfbf16,
    0x070ff0a0, 0x01b7834a, 0x0c47ca4c, 0x0affb9a6
  };
   uint32__t mask_table4[16] = {
    0x00000000, 0x00578abc, 0x0bf29dcc, 0x0ba51770,
    0x05851539, 0x05d29f85, 0x0e7788f5, 0x0e200249,
    0x03482f1a, 0x031fa5a6, 0x08bab2d6, 0x08ed386a,
    0x06cd3a23, 0x069ab09f, 0x0d3fa7ef, 0x0d682d53
  };
  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];

  accum <<= 16;
  return in ^ accum;

}

#endif

