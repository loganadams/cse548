/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FILTER_H_
#define FILTER_H_

#define FILTER_TABLE_ENTRIES (1<<10)

#include <stdint.h>
#include "defines.h"

typedef struct tableEntry{
    uint32_t    CAL;
    uint32_t    version;
    bool        revComp;
} filterTableEntry;

// this class filters CALs for a short read
class Filter {

private:
    // data
    filterTableEntry*   table;
    uint32_t            currentVersion;
    
    // methods
    bool compareEntries(filterTableEntry* entry1, filterTableEntry* entry2);
                                                // return true if the entries are different, 
                                                //  false if they contain the same data
    uint32_t getCurrentVersion();               // returns the current version number
    filterTableEntry* getEntry(uint32_t num);   // returns a pointer to the entry specified by the
                                                //  index
    uint32_t hashCAL(uint32_t CAL);             // returns the hashed CAL, which is then used as
                                                //  the index into the table
    void copyIntoTable(filterTableEntry* newData, uint32_t tableIndex);
                                                // copies data from the new entry into the table
public:
    // constructor and destructor
    Filter();                                   // allocate the space for the filter table
                                                //  and set the version number to 0
    ~Filter();                                  // free up any allocated memory

    // methods
    void nextRead();                            // updates the version number
    bool addCAL(uint32_t CAL, bool revComp);    // add the current CAL information to the table
                                                //  and returns true if the CAL should be
                                                //  aligned
};

#endif
