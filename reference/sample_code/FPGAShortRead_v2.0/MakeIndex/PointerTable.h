/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef POINTERTABLE_H
#define POINTERTABLE_H
/**
 * @file PointerTable.h
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * PointerTable.h defines PointerTable class and its associated fields and methods
 *n
 */

#include <stdio.h>
#include "defines.h"
#include "CALTable.h"
#include <time.h>
#include <string.h>


/**
 * the tableData_t structure is used to simplify the passing of meta data related to
 * the start index and number of CAL's in the CAL table. This data is looked up in the
 * pointer table
 * */
typedef struct tableData_t {
    uint64_t    startIndex;
    uint64_t    length;
}CALmetaData_t;

class PointerTable{
 private:
    ptrTable_t *    table; /// table is the pointer to the pointerTable used by this class
 public:
    /**
     * PointerTable is the constructor for a PointerTable object. It allocates a large table
     * that will be filled by the temporary CALTable if the index file is being built, or
     * filled by binary data if the index is being read.
     *
     * @returns a PointerTable object
     * 
     * */
    PointerTable();

    /**
     * ~PointerTable is the desrtructor for a PointerTable object. It deletes the large
     * pointer table that was allocated during initialization
     *
     * 
     * */
    ~PointerTable();

    /**
     * write writes the current pointer table into previously opened FILE pointer
     *
     * An error will occur and the program will exit if the number of
     * entries written does not equal the number of entries expected
     * 
     * @param destination previously opened FILE pointer where the binary table data will be
     * written to
     *
     * */
    void write(FILE * destination);
    void writeToTextFile(const char * textFile);
    void printStatistics();
    /**
     * read reads the pointer table from a previously opened FILE pointer
     * into the pointer table that was allocated when
     * the PointerTable object was constructed.
     *
     * An error will occur and the program will exit if the number of
     * entries read does not equal the number of entries expected
     * 
     * @param source the previously opened FILE pointer where the binary table data will be
     * read from
     *
     * */
    void        read(FILE * source);

    /**
     * buildPointerTable builds the pointer table from the temporary CALTable that was
     * created by a CALTable object
     *
     * @param tempTable The temporary CAL table constructed by a CALTable object
     * @param numTableEntries The number of VALID entires in the temporary table
     * (Invalid entries' seed field points to NULL)
     *
     * */
    void        buildPointerTable(Seed **  tempTable, uint64_t numTableEntries);
    void buildPointerTable(FILE*  tempTable);
    /**
     * getRandomFromPointerTable produces a random (seed) index, (seed)tag, index and
     * length (metadata) for the CALTable. This is used for random verification of the CALtable.
     * 
     *
     * @param index   a pointer to where the randomly generated index will be stored
     * @param tag     a pointer to where the randomly generated tag will be stored
     * @param cal_ptr a pointer to where the cal_table start index (given by the randomly
     * generated index and tag)will be stored
     * @param length  a pointer to where the value representing the number of entries
     * after the start pointer will be stored
     * 
     * */
    void        getRandomFromPointerTable(uint64_t * index, uint64_t * tag, uint64_t * cal_ptr, uint64_t * length);

    /**
     * getCALMetadata takes a given index and tag from a seed and uses them to look up the start pointer
     * and number of potential cals in the CALTable. This data is written into the CALmetaData_t struct
     * poined to by dest.
     * 
     * 
     * @param pointerTableIndex the index into the pointer table.
     * @param tag The tag from the seed. The tag indexes a single pointer table line
     * @param dest A pointer to the metaData struct, where this method will write the start index
     * in the CALTable and the number of CAL entries to examine
     * */
    void        getCALMetadata(uint64_t pointerTableIndex, uint64_t tag, CALmetaData_t * dest);

    /**
     * getIndex returns a pointer to the entry at a given index in the pointer table.
     *
     * @param index The index of the entry in the pointer table
     * @return a pointer to a ptrTable_t struct which is an entry in the pointer table
     *
     *
     * */
    ptrTable_t *    getIndex(uint64_t index);

    /**
     * getPointerTable returns a pointer to this objects pointer table
     *
     * @returns a pointer to a ptrTable_t struct, which is used treated as the pointer table
     * for this object
     *
     * */
    ptrTable_t *    getPointerTable();
};
/**
 * printPointerTable prints all of the lines in a given pointer table
 * 
 *
 * @param table A pointer table to print
 * */
void            printPointerTable(ptrTable_t * table);

/**
 * printLine prints a single line from a given index in a given pointer table 
 * 
 *
 * @param table A pointer table where the line should be read from
 * @param index The index of the line to be printed
 * 
 * */
void            printLine(ptrTable_t * table, uint64_t index);
#endif
