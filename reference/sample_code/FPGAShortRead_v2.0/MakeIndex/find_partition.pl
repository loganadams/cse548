# Perl script to only print the lines associated with the current partition

# example usage:
# perl find_partition.pl 4 chr_hashed_seeds_sorted_by_CAL.txt full_cal_array.txt

# check for correct number of parameters
if($#ARGV != 2){
    die "Required arguments:\n\t1) Number of Partitions\n\t2) File containing reduced seeds and CALs base name (sorted by CAL)\n\t3) Output file base name (contains seeds and CALs for a partition)\n";
}

# CONSTANTS
my $numPartitions = shift;
my $sourceFile = shift;
my $outFileName = shift;

# VARIABLES
my $numLines;
#my $numLines = 2484729151;
my $lineCnt = 0;
my $currPart;
my $upper = 0;
my $name;

# to count the number of lines in the CAL array file
$numLines=0;
open INFILE, '<', $sourceFile or die "Unable to open $sourceFile for reading\n";
while (<INFILE>){
  $numLines++;
}
close INFILE;
print "DEBUG: $numLines CALs found in $sourceFile\n";

# open the input file
open INFILE, '<', $sourceFile or die "Unable to open $sourceFile for reading\n";

# cycle through all partitions
for ($currPart = 0; $currPart < $numPartitions; $currPart++){

    # update the limits for moving to the next partition
    $upper = $numLines * ($currPart + 1) / $numPartitions;
   
    # open the output file for this partition
    $name = $outFileName . "." . $currPart;
    open OUTFILE, '>', $name or die "Unable to open $name for writing\n";
    
    print "Printing section $currPart of $sourceFile to $name\n";
    while(($line = <INFILE>) && ($lineCnt<$upper)) {
        print OUTFILE $line;
        $lineCnt++;
    }
    close OUTFILE;
}
close INFILE;
