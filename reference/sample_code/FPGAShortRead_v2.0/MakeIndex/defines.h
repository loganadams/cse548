/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef DEFINES_H
#define DEFINES_H
#include <stdint.h>
// The following files define the Start pointer bits, address bits, 
// offset bits, and tag bits for specific tests
#ifdef SIM21_64
#include "defines_sim21_64.h"
#elif defined  CHR21_64
#include "defines_chr21_64.h"
#elif defined  CHR21_256
#include "defines_chr21_256.h"
#elif defined  CHR21_64_FG
#include "defines_chr21_64_fg.h"
#elif defined  CHR21_256_12BP
#include "defines_chr21_256_12bp.h"
#elif defined  CHR21_256_12BP_1000
#include "defines_chr21_256_12bp_1000.h"
#elif defined  CHR21_256_14BP
#include "defines_chr21_256_14bp.h"
#elif defined  CHR21_256_16BP
#include "defines_chr21_256_16bp.h"
#elif defined  CHR21_256_18BP
#include "defines_chr21_256_18bp.h"
#elif defined  CHR21_256_20BP
#include "defines_chr21_256_20bp.h"
#elif defined  CHR21_256_22BP
#include "defines_chr21_256_22bp.h"
#elif defined  CHR21_256_25BP
#include "defines_chr21_256_25bp.h"
#elif defined  CHR21_256_29BP
#include "defines_chr21_256_29bp.h"
#elif defined  CHR21_256_32BP
#include "defines_chr21_256_32bp.h"
#elif defined  DEFAULT_256
#include "defines_default_256.h"
#else 
#error A TYPE must be defined (see defines.h)
#endif

#define REF_LENGTH 128
// Amount of indels suspected
#define INDEL_OFFSET          10

#define READ_ID_BITS  32        // Size of Read IDs

#define READ_LENGTH   76

#define BITS_PER_BASE 2LL
#define BITS_PER_SEED (SEED_LENGTH * BITS_PER_BASE)
#define BASE_MASK     (((uint64_t)1 << BITS_PER_BASE)-1)

// CAL table definition
#define KEY_BITS           (2*SEED_LENGTH-IDX_BITS-TAG_BITS)
                                // number of bits of the seed used as key
                                // note: key bits must be < 32
#define TAG_MASK  ((1LL<<TAG_BITS)-1)
#define KEY_MASK  ((1LL<<KEY_BITS)-1)
#define IDX_MASK  ((1LL<<IDX_BITS)-1)
#define SEED_MASK  ((1LL<<(SEED_LENGTH*BITS_PER_BASE))-1)
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define CAL_BITS           32   // Size of CAL

#define MAX_READ_LINE_LENGTH 1024
typedef  uint32_t cal_t;

#if KEY_BITS==0
// CAL table entry - this will always be 64 bits and waste some space
typedef struct CALTable_t {
    cal_t cal;
    uint32_t revComp : 1;
    uint32_t key : (sizeof(uint64_t)*8 - 1 - sizeof(cal_t)*8); // needed so that data is initialized correctly.
} calTable_t;
#else
// CAL table entry - this will always be 64 bits and waste some space
typedef struct CALTable_t {
    cal_t cal;
    uint32_t key : KEY_BITS;
    uint32_t revComp : 1;
    uint32_t _slack : (sizeof(uint64_t)*8 - KEY_BITS - 1 - sizeof(cal_t)*8); // needed so that data is initialized correctly.
} calTable_t;
#endif

// For gold files -- this should be a packed struct eventually....
#define READ_ID_SIZE 23;
#define ALIGNMENT_SIZE 32;
#define SCORE_SIZE 8;
typedef struct sw_gold_t{
    uint64_t readId: READ_ID_SIZE;
    uint64_t alignment: ALIGNMENT_SIZE
    uint64_t score: SCORE_SIZE;
    uint64_t revComp: 1;
}sw_gold_t;
 
typedef struct header_t {
    uint32_t block_bits; // bits per memory acccess
    uint32_t seed_length; // bases per seed
    uint32_t index_bits; // number of bits used to index the pointer table (also the log of the number of entries in the pointer table)
    uint32_t tag_bits; // number of bits used to index the offset in the pointer table
    uint32_t offset_bits; // Number of bits in the offset field
    uint32_t calTableLength; // number of bytes in the cal table 
    uint32_t ref_length; // number of bytes in the binary reference
    uint32_t unused;
}header_t;

typedef struct read_t{
  uint64_t data[(READ_LENGTH*BITS_PER_BASE+(sizeof(uint64_t)*8-1))/(sizeof(uint64_t)*8)];
}read_table_line;

#endif // DEFINES_H
