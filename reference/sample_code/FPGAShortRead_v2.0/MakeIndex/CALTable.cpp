/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file CALTable.cpp
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * CALTable.cpp implements the class methods for the CALTable
 * class defined in CALTable.h
 *
 */

#include "CALTable.h"

CALTable::CALTable(){
    // nothing to allocate at initialization
    numCALs =0;
    srand(time(NULL));
}

CALTable::~CALTable(){
    if(table!= NULL){
        delete table;
    }
}

void CALTable::read(FILE* source, uint64_t size){
    table = (CALTable_t*) new char[size];
    int numBytesRead = fread(table,sizeof(char),size,source);
    numCALs = numBytesRead/sizeof(CALTable_t);
    if(numBytesRead != size){
        fprintf(stderr, "Error: Expected to read %lu bytes from reference file for CALTable, got %d\n",
                size,
                numBytesRead);
        exit(1);
    }
}

void CALTable::write(FILE * destination){
    uint64_t numExpected = numCALs;
    uint64_t numWritten = fwrite(table, sizeof(CALTable_t), numCALs,destination);
    if(numWritten != numExpected){
        fprintf(stderr, 
                "Error: Expected to write %lu bytes to reference file for CALTable. Wrote %lu\n",
                numExpected,
                numWritten);
        exit(1);
    }
}

void CALTable::writeToTextFile(const char * textFile){
    FILE * calTextFile = fopen(textFile, "w");
    if(calTextFile == NULL){
        fprintf(stderr, "Could not open %s for output\n", textFile);
        exit(1);
    }
    for (uint64_t i =0; i < numCALs; i++) {
          fprintf(calTextFile, "%u %u %u\n",
                  table[i].key, table[i].cal, table[i].revComp);
    }
    fclose(calTextFile);
}

void CALTable::createCALTable(FILE * tempTable){
    table = new CALTable_t[numCALs]();
    Seed tempSeed;
    fseek(tempTable, 0, SEEK_SET);
    clearerr(tempTable);
    fread(&tempSeed, sizeof(Seed),1, tempTable);
    uint64_t i = 0;
    while(!feof(tempTable)){
        table[i].key = tempSeed.getKey();
        table[i].revComp = tempSeed.getRevComp();
        table[i].cal = tempSeed.getCAL();
        fread(&tempSeed, sizeof(Seed),1, tempTable);
        i++;
    }
}

/**
* Finds and returns the maximum value in the given array with the given length.
* */
uint64_t findMax(uint64_t* arr, uint64_t len){
    uint64_t max = arr[0];
    for(int i = 1 ; i < len; i ++){
        if(max < arr[i]){
            max = arr[i];
        }
    }
    return max;
}

int compareCALEntries(const void *a,const void *b){
    Seed * CAL_a = (Seed*)a;
    Seed * CAL_b = (Seed*)b;
    uint64_t seedAVal = CAL_a->getHashedValue();
    uint64_t seedBVal = CAL_b->getHashedValue();
    // Compare the hashed value of the seeds
    if (seedAVal > seedBVal) {
        return 1;
    } else if (seedAVal == seedBVal) {
        uint64_t CALAVal = CAL_a->getCAL();
        uint64_t CALBVal = CAL_b->getCAL();
        // Compare the cal value of the seeds
        if(CALAVal > CALBVal) {
            return 1;
        } else if(CALAVal == CALBVal) {
            return 0;
        } else {	
            return -1;
        }
    } else {
        return -1;
    }
}

/**
* Sorts the seeds in all of the files.
*
* @param tempFiles An array of all the files
* @param fileLengths An array of file lengths
* */
void sortFiles(FILE** tempFiles, uint64_t * fileLengths){
    uint64_t maxlen = findMax(fileLengths, 1<<LOG_NUM_TEMP_FILES);
    Seed * tempTable = (Seed*)calloc(maxlen, sizeof(Seed));
    for(int i =0; i < 1<<LOG_NUM_TEMP_FILES; i ++){
        // Go to the beginning of the file and read it into a temporary table
        fseek(tempFiles[i] , 0 , SEEK_SET);
        fread(tempTable, sizeof(Seed),fileLengths[i],tempFiles[i]);
        // Sort the table
        qsort(tempTable, fileLengths[i], sizeof(Seed), compareCALEntries);
        // Write it back into the file
        fseek(tempFiles[i] , 0 , SEEK_SET);
        clearerr(tempFiles[i]);
        fwrite(tempTable, sizeof(Seed),fileLengths[i],tempFiles[i]);
    }
    delete tempTable;
}

/**
* Combines all of the temporary files and puts them into the temp table.
* Also removes seeds that appear in the temporary CALtable 
* more than MAX_SAME_SEED times (defined in defines.h).
*
* @param tempFiles An array of the temp files
* @param fileLengths An array of the file lengths
* @param tempTable The table to combine all of the files into
* */
uint64_t combineAndRemoveDuplicates(FILE * tempFiles[], uint64_t fileLengths[], FILE* tempTable){
    Seed tempSeed;
    Seed *writeBuffer; // If on stack it segfaults for large MAX_SAME_SEEDs
    uint64_t numValid = 0;
    uint64_t curCount=0;
    uint64_t numRemoved=0;
    uint64_t sum = 0 ;

    writeBuffer = new Seed[MAX_SAME_SEED];
    fseek(tempTable, 0 , SEEK_SET);
    // Go through each file
    for(int i=0 ; i < (1<<LOG_NUM_TEMP_FILES); i++){
        fseek(tempFiles[i], 0 , SEEK_SET);
        fread(&writeBuffer[0], sizeof(Seed),1,tempFiles[i]);
        uint64_t currentHashVal = writeBuffer[0].getHashedValue();
        curCount = 1;
        // Go through all of the seeds in a file
        for(uint64_t currentSeed = 1; currentSeed < fileLengths[i]; currentSeed++){
            fread(&tempSeed, sizeof(Seed),1,tempFiles[i]);
            if(currentHashVal == tempSeed.getHashedValue()){
                // If the seed has the same hashed value, write it if 
                // it is under the max number of duplicates allowed
                if(curCount < MAX_SAME_SEED){
                    memcpy(&writeBuffer[curCount],&tempSeed, sizeof(Seed));
                }
                curCount++;
            }else{
                // The seed has a different hash value, so write it to the temp table
                if(curCount <= MAX_SAME_SEED){
                    fwrite(writeBuffer, sizeof(Seed),curCount,tempTable);
                    numValid +=curCount;
                }else{
                    numRemoved += curCount;
                }
                memcpy(&writeBuffer[0],&tempSeed, sizeof(Seed));
                currentHashVal = writeBuffer[0].getHashedValue();
                curCount = 1;
            }
        }
        if(curCount<=MAX_SAME_SEED){
            fwrite(writeBuffer, sizeof(Seed),curCount,tempTable);
            numValid+=curCount;
        }
    }
    printf("%lu Seeds Removed, ", numRemoved);
    printf("%lu Valid Seeds Remaining ", numValid);

    delete[] writeBuffer;
    return numValid;
}

FILE* CALTable::createTempCALFile(char * reference){
    FILE * tempTable = tmpfile();
    numCALs =0;
    uint64_t currentCAL = 0;
    uint64_t numValidBases =0;
    Seed* currentSeed = new Seed();  
    char currentBase = reference[currentCAL];
    uint64_t count = 0;
    FILE * tempFiles[1<<LOG_NUM_TEMP_FILES];
    uint64_t fileLengths[1<<LOG_NUM_TEMP_FILES];
    char fileNames[1<<LOG_NUM_TEMP_FILES][11];
    char strTemplate[11] = "fileXXXXXX";
    memset(fileLengths, 0, sizeof(uint64_t)*(1<<LOG_NUM_TEMP_FILES));
    for(int i =0 ; i < 1<<LOG_NUM_TEMP_FILES; i ++){
        strcpy(fileNames[i],strTemplate);
        mkstemp(fileNames[i]);
        printf("%s ",fileNames[i]);
        tempFiles[i]= fopen(fileNames[i],"wb+");
    }
    printf("\n");
    while(currentBase !=0){
        if(currentBase == 'n' || currentBase =='N'){
            numValidBases = 0;
            currentSeed->setValue(0LL);
        }else if (isBase(currentBase)){
            numValidBases++;
            currentSeed->addBase(currentBase);
        }
        if(numValidBases >= SEED_LENGTH){
            Seed temp;
            temp.setValue(currentSeed->getValue());
            temp.hashSeed();
            uint64_t fileIndex = temp.getHashedValue()
                >>(SEED_LENGTH*BITS_PER_BASE-LOG_NUM_TEMP_FILES);
            fileLengths[fileIndex]++;
            temp.setCAL(currentCAL-(SEED_LENGTH-1));
            fwrite(&temp, sizeof(Seed),1,tempFiles[fileIndex]);
            numCALs++;
            if(temp.getPalindrome()){
                fileLengths[fileIndex]++;
                temp.setRevComp(true);
                fwrite(&temp, sizeof(Seed),1,tempFiles[fileIndex]);
                numCALs++;
            }
        }
        currentCAL++;
        currentBase = reference[currentCAL];
    }
    printf("Sorting CALs...");fflush(stdout);    
    sortFiles(tempFiles, fileLengths);
    printf("Removing Duplicate CALs (%d)... ", MAX_SAME_SEED);fflush(stdout); 
    numCALs = combineAndRemoveDuplicates(tempFiles, fileLengths, tempTable);
    for(int i =0 ; i < 1<<LOG_NUM_TEMP_FILES; i ++){
        fclose(tempFiles[i]);
        remove(fileNames[i]);
    }
    return tempTable;
}

CALTable_t * CALTable::access(uint64_t index){
    return &table[index];
}

CALTable_t * CALTable::getRandomCAL(uint64_t *indexDest){
    uint64_t randomIndex = rand() % numCALs;
    *indexDest = randomIndex;
    //    printf("random index: %llu\n", randomIndex);
    return &table[randomIndex];
}

uint64_t CALTable::getNumCals(){
    return numCALs;
}

CALTable_t * CALTable::getCALTable(){
    return table;
}
