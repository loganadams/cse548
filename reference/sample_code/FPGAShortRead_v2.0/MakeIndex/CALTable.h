/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CALTABLE_H
#define CALTABLE_H
/**
 * @file CALTable.h
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * CALTable.h defines the CALTable class and it's associated methods and fields.
 *
 */
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "defines.h"
#include "Seed.h"
#include <string.h>
#define LOG_NUM_TEMP_FILES 5

/**
 * the tempCAL_t struct is used in the temporary CALTable, which is used to create
 * the final CALTable, and in the construction of the pointer table.
 *
 * To invalidate a tempCAL_t entry, the seed is deleted, and the value of seed is set
 * to NULL
 *
 * */
typedef struct tempCAL_t{
    uint64_t CAL;
    Seed * seed;
}tempCAL_t;


/**
 * The CALTable Class is used for looking up potential allignment locations in
 * the BFAST algorithm.
 *
 * 
 * */
class CALTable{
 private:
    /**
     * numCALs represents the number of valid CALs in the temp CALTable and
     * the length of the final CALTable*/
    uint64_t numCALs; 
    CALTable_t * table; /// A pointer to the CALTable array
    
 public:
    /**
     * The constructor for this CALTable object. It allocates no memory when
     * initialized.
     * */
    CALTable();
    /**
     * The destructor for this CALTable object. 
     * If a table has been created from a reference file, or read from an index
     * file during runtime, this method will delete/free the CALTable memory.
     * */
    ~CALTable();

    /**
     * read reads binary CALTable data from a previously opened FILE pointer
     * into this object's CALTable field. 
     *
     * @param source The FILE pointer to read binary CALTable data from
     * @param size The size of the CALTable in bytes. 
     *
     * */
    void read(FILE* source, uint64_t size);
    /**
     * write writes the current CALTable (in binary format) to a FILE pointer
     *
     * @param destination the FILE pointer to which the binary data should be written
     * */
    void write(FILE * destination);
    void writeToTextFile(const char * textFile);
    /**
    * createCALTable creates the final CALTable from a temporary CALTable.
    * This CALTable can be used in Corey's BFAST implementation.
    *
    * @param tempTable the temporary CALTable (also created by this object) that should be
    * used to create the final CALTable
    * */
    void createCALTable(FILE * tempTable);
    /**
    * compareCALEntries compares two tempCAL_t entries and returns 1 if
    * (the hashed seed value of a) > (the hashed seed value of b) and -1 if a < b.
    * If (the hashed seed value of a) = (the hashed seed value of b), then return 1 if
    * (the cal of a) > (the cal of b), 0 if a = b, or -1 if a < b.
    *
    * @param a a tempCAL_t that has been cast to a void *
    * @param b a tempCAL_t that has been cast to a void *
    * @returns an integer, representing which of a or b is greater, or if the two are equal
    * */
    int compareCALEntries(const void *a,const void *b);
    /**
     * createTempCALTable creates the temporary CALTable from a reference
     * (in ascii format, with N's). 
     *
     *
     * @param reference The ascii reference to create the temporary CAL table from
     *
     * @return a pointer to where the temporary table that was created from the reference
     *
    * */
    FILE* createTempCALFile(char * reference);

    /**
     * access takes an index and returns a pointer to the entry in the
     * final CALTable at that index
     *
     * The CALTable must be read (using read())or
     * constructed from a reference (using createCALTable()) before using this method
     * 
     * @param index the index in the cal table to return a pointer to
     * @return returns a pointer to the entry at the given index in the CALTable
     * */
    CALTable_t * access(uint64_t index);

    /**
     * getRandomCAL returns a pointer to a random cal table entry, and puts the random index
     * in a given integer destination
     *
     * @param indexDestination a pointer to destination for the random index that was generated
     * @return a pointer to a random CAL entry in the CAL table
     * */
    CALTable_t * getRandomCAL(uint64_t *indexDest);

    /**
     * getNumCals returns the number of CALTable entries in the final CALTable,
     * which is equal to the number of valid entries in temporary CALTable
     *
     * @return an unsigned 64-bit integer that represents the number of entries in the CALTable
     * */
    uint64_t getNumCals();

    /**
     * getCALTable returns a pointer to the final calTable
     *
     * @returns a pointer to the final CALTable associated with this object
     *
     * */
    CALTable_t * getCALTable();
};

#endif
