/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_256_H
#define CHR21_256_H

#define TYPE "CHR21_256_32BP"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 256
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/chr21_256_32bp.index"
#define DEFAULT_REFERENCE "../../data/chr21.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 64   // number of bits used for the start pointer
#define IDX_BITS       24   // number of bits of the seed used as address
#define OFFSET_BITS        32   // number of bits used for each offset
#define TAG_BITS           4    // number of bits of the seed used as tag
#define SEED_LENGTH   32        // number of bases used for the seed

#define MAX_SAME_SEED 100000000      // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
    uint64_t startPtr : START_POINTER_BITS;
    uint64_t offsets[1LL<<TAG_BITS];
} ptrTable_t;
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define OFFS_BITS (sizeof(uint64_t)*8)

#include <stdlib.h>
#include <stdio.h>
inline void checkOffset(uint64_t value, uint64_t offset_index){
  if(offset_index>=(1LL<<TAG_BITS)) {
    printf("Error: invalid pointer table index %lu\n", offset_index);
    exit(EXIT_FAILURE);
  }
  if(value > ((1LL<<OFFSET_BITS) -1)){
    printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
    exit(1);
  }
}

inline void setOffset(ptrTable_t *name, uint64_t index, uint64_t value){
  checkOffset(value, index);
  name->offsets[index] = value;
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]&=~((OFFSET_MASK)<<((index*OFFSET_BITS)%OFFS_BITS));
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]|=((value&OFFSET_MASK) <<((index*OFFSET_BITS)%OFFS_BITS));
}

inline uint64_t getOffset(ptrTable_t *name,uint64_t index){
  return name->offsets[index];
  //    return ((((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]>>((index*OFFSET_BITS)%OFFS_BITS))&OFFSET_MASK);    
}

inline uint64_t do_hash(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x349273038507561b, 0x4fd8aed1361a68ea, 0xf9d3b464fc1c03ea,
    0x28cf4fbd7d32d93d, 0xc7c567afc1881df9, 0x86a111d6a094c6dc, 0xf2d19550c01e3637,
    0xbc0a5e8675888b78, 0x6d719bb4731037ae, 0xe48a19e901b0d0e2, 0xe2b8c495fb1cde70,
    0xb6d36745ae001a08, 0x28580fabcc6626ae, 0xe52b1c661fbf1bc2, 0x9fbe8a9bb7d7e137
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x036923a16343f375, 0x40346de6945263a0, 0xf5750e68732e0686,
    0x42d2e281dce098dc, 0x116a769893f90b5d, 0xbc469251547a72f5, 0x89d8c6c82ddaf7b0,
    0x860091d144a85951, 0xcebfe0f46fe0be2e, 0x9e7ee8257ea3ca4b, 0x963857b5ceb66dda,
    0xce0651c7cbda94a8, 0x010875a799d553f9, 0x265ebdee8ab6c986, 0x6886b5626db895a6
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0x994f8ca2a0c1e59b, 0x62af86d5fb60d8ea, 0x76d40f83e0b4be72,
    0xbe9718bb85fc44a4, 0x2b3354774a1116de, 0xd2e559ff2beba87e, 0x3d31126fced82431,
    0x9a8cc75f4ae40cc0, 0x63386f5f2ec10c98, 0x23a44142bef9fdf7, 0x9ec44e3b77409b88,
    0xa827263c143e1aa5, 0x06d63b707acbdb67, 0xa09f28f4c574e879, 0xc738e50a0540ca21
  };
  static const uint64_t mask_table4[16] = {
    0x00000000, 0xad79c26455687ef7, 0x37c4d9a81b66e0f3, 0x292617c2545e8f0d,
    0x37305aa24d27c856, 0x89080114c7399aca, 0xb84963f01e73d501, 0x2ddfb8d776bc09e0,
    0xd5d498cd1668161e, 0x08fbff9523bb6892, 0x675ebadecf4a450b, 0x40e6d27c7f1fc727,
    0x68c3c532d451121f, 0xca14f4c661be02cb, 0xb1c2ddcdd15b05cc, 0x11b6939a16abd365
  };
  static const uint64_t mask_table5[16] = {
    0x00000000, 0x7fc86097da60fbaa, 0x1f14a4740747a8c2, 0xa0736c2842c4ba2b,
    0x78e2182a2d89e311, 0xf935bd1ccdb44f59, 0xd1517f0ee0915e22, 0x926ab30ce18b6be0,
    0x1da2cadd8ab0e191, 0xbd38cb71b7d2ef87, 0xa8c0b1f564c6d2ef, 0x203899cac13bb943,
    0xf37175433315ccce, 0x48be65fee7861980, 0x634dea038a530d4d, 0xfbb0404f65d3d00f
  };
  static const uint64_t mask_table6[16] = {
    0x00000000, 0xfee4e16ca21248b5, 0xeb35b1eab55b7114, 0xab01234335b15683,
    0x9ea5061e60b6d692, 0x423b80dcc0eedb91, 0xc079f48f9afc61e0, 0x8fd88c917351e447,
    0xdf762929dd59496d, 0x4d2c7521e81e6d8d, 0x8eb0480df651a5fd, 0xdd4d4906b7b4df58,
    0x561c1d87bc6e820e, 0x464dd4062d6b58cf, 0xed211eb73bfb0cf3, 0xf59fa81f5c8939c0
  };
  static const uint64_t mask_table7[16] = {
    0x00000000, 0xfaa3837a3c915c50, 0x62a3b81a6d7b3189, 0xa5fc1f04dd8fee4c,
    0x5ee697de08f01185, 0x7fad27167b9dd8a9, 0x4a8e8d22971b31f8, 0x0f07638457391e5e,
    0x86a339330cbd685f, 0xc143280c010b7c1b, 0xa4fe73dcf654d110, 0x683d7b7e690e21fe,
    0xfa20fc23b6466e10, 0xe516dac45eb6d72f, 0x9ceeddae5d74b157, 0x1e808cb14f3f65ea
  };
  static const uint64_t mask_table8[16] = {
    0x00000000, 0x75f57b87124ff296, 0x8736be8760bb86d2, 0x6df55380b4ffdf4d,
    0xf2bb6fd785a18b6a, 0x6782e6620cfa3bde, 0x01117b688b805a7b, 0x64fe023eaa2f720e,
    0x124a4efee98f8715, 0x825c1a2307dc7528, 0xed00bfe0ac8ef8a3, 0x16b9dfeca1da5f69,
    0x52aec1251752d0f3, 0x41e5fe74954b21ee, 0xea8aa57f39929672, 0xb868cc543e92b273
  };
  static const uint64_t mask_table9[16] = {
    0x00000000, 0x88d6c7b08307f3e6, 0x6194c21324efee20, 0x2fe1430e6cb0f2f0,
    0x5b85d2c84621985a, 0xe53809d3a15a5dea, 0x4e9c85a3e6633ba3, 0x18e3660239ababa5,
    0x5edd6a59990c7638, 0xec5179c5fe7805b1, 0x3e924f7969a9804f, 0xb986139af1a6f52e,
    0x03e90584f066d2ad, 0xb90146010001d054, 0xdb2dc7713209395e, 0x49c4a0357980de5a
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table6[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table7[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table8[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table9[ temp & 0xf ];
//  temp >>= 4; // NSM - I don't know why some seed lengths have this empty lookup
//  accum ^= (uint64_t) mask_table10[ temp & 0x0 ];


  accum <<= 36;
  return in ^ accum;

}

#endif
