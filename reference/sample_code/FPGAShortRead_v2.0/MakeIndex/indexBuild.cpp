/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file indexBuild.cpp
 * @author Dustin Richmond
 *
 * @version 1.0
 *
 * indexBuild.cpp contains the main method for the index building software.
 * Most of the parameters and defines for this file are located in defines.h
 * and its included header files.
 *
 * To see a helpful descriptions of the available command line arguments,
 * compile the program and type --help at execution time
 */

#include "indexBuild.h"

char * indexFilePath = (char*)DEFAULT_INDEX; /// String containing path to the indexFile that will be written
char * referenceFilePath = (char*)DEFAULT_REFERENCE; ///String containing the path to the reference file that will be used to build the index
bool write_text_file = false; /// Flag for writing the index file as a separate text file (for Java). Set by command line argsi
bool write_statistics = false;
int main(int argc, char* argv[]) {
    argp_parse (&argp, argc, argv, 0, 0, NULL);
    printf("Building index, please wait...\n");
    printf("Index type: %s\n", TYPE);
    printf("Reference file: %s\n", referenceFilePath);
    printf("Output file: %s\n", indexFilePath);

    // Initialization...
    Reference *reference = new Reference();
    CALTable * calTable = new CALTable();
    PointerTable * pointerTable = new PointerTable();

    // Read in the reference file from the file defined in referenceFilePath
    printf("Reading reference file... "); fflush(stdout);
    reference->readReference(referenceFilePath);
    printf("Done!\n");
    uint64_t numBases = reference->getNumBases();
    printf("Found %lu Bases in reference\n", numBases);
    //printf("*** getNumBases says %llu\n", reference->getNumBases());
    // After the reference is created, we can create the CAL table using
    // the character reference. First create the temporary CAL table, 
    printf("Creating temporary CAL table... "); fflush(stdout);
    FILE * tempTableFile = calTable->createTempCALFile(reference->getCharReference());
    printf("Done!\n");
    // Using the temporary CAL Table, create the final CAL Table, then the Pointer Table
    printf("Creating CALTable... ");fflush(stdout);
    calTable-> createCALTable(tempTableFile);    
    printf("Done!\n");
    printf("Creating pointer table... "); fflush(stdout);
    pointerTable->buildPointerTable(tempTableFile);
    printf("Done!\n");
    
    // Write the created tables to the file specified by indexFilePath
    if(!write_text_file) {
        printf("Writing index tables to file... ");fflush(stdout);
        FILE * outputFile = fopen(indexFilePath,"w");
        header_t header_out = {DRAM_BLOCK_BITS,
			   SEED_LENGTH,
			   IDX_BITS,
			   TAG_BITS,
			   OFFSET_BITS,
			   calTable->getNumCals()*sizeof(CALTable_t),
			   reference->getReferenceSize(),
			   1};
        fwrite (&header_out, sizeof(header_t),1, outputFile);
        pointerTable->write(outputFile);
        reference->write(outputFile);
        calTable->write(outputFile);
        fclose(outputFile);
        printf("Done!\n");
    } else { // write_text_file
      char* filePath = new char[32 + strlen(indexFilePath)];
      printf("Writing text files... ");fflush(stdout);
      printf("PointerTable ");fflush(stdout);
      strcpy(filePath, indexFilePath);
      strcat(filePath, "pointer_table.txt");
      pointerTable->writeToTextFile(filePath);
      //printf("Reference ");fflush(stdout);
      //strcpy(filePath, indexFilePath);
      //strcat(filePath, "Reference.txt");
      //reference->writeToTextFile(filePath);
      printf("CALTable ");fflush(stdout);
      strcpy(filePath, indexFilePath);
      strcat(filePath, "cal_table.txt");
      calTable->writeToTextFile(filePath);
      printf("Done!\n");
      delete filePath;
    }
    if(write_statistics){
      printf("Creating statistics files... ");fflush(stdout);
      pointerTable->printStatistics();
      printf("Done!\n");
    }   
    // Free open memory
    delete reference;
    delete calTable;
    delete pointerTable;
    fclose(tempTableFile);
}

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
    /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    switch (key)
        {
        case 's':
            printf("Writing index_statistics.csv");
            write_statistics = true;
            break;
        case 't':
            printf("Writing text index files for java\n");
            write_text_file = true;
            break;
        case 'i':
            indexFilePath = arg;
            break;
        case 'r':
            referenceFilePath = arg;
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= 4)
                /* Too many arguments. */
                argp_usage (state);
            break;
        case ARGP_KEY_END:
            if (state->arg_num < 0)
                /* Not enough arguments. */
                argp_usage (state);
            break;
        default:
            return ARGP_ERR_UNKNOWN;
        }
    return 0;
}

