/*
 * Aligner.cpp
 *
 *  Created on: Nov 5, 2011
 *      Author: cbolson1
 * Description: Aligns reads to the specified reference data
 * Copyright 2011 Pico Computing
 */

#include "Aligner.h"

// no objects to allocated
Aligner::Aligner() {
}

// no dynamic memory allocated
Aligner::~Aligner() {
}

// aligns the current read against the current reference
//  and sets the score and alignment
// also, sets the sourceCell for each cell of the matrix to enable
//  traceback
void Aligner::alignRead(){
    alignment = 0;
    score = C_GAP_OPEN * READ_LENGTH;

    // sourceCell explanation
    // 0 = diagonal
    // 1 = ref gap
    // 2 = read gap
    // D = destination cell
    //
    //   reference
    // r  --- ---
    // e | 0 | 1 |
    // a  --- ---
    // d | 2 | D |
    //    --- ---

    // initialize the first row and column of the matrices
    initMatrices();

    // recursive calculation of the matrices section
    int i, j;
    for(j=1;j<=REF_LENGTH;j++){
        for(i=1;i<=READ_LENGTH;i++){            
            // track the values for ref gaps
            if((refGap[i][j] = similarity[i-1][j] + C_GAP_OPEN) < refGap[i-1][j] + C_GAP_EXTEND){
                refGap[i][j] = refGap[i-1][j] + C_GAP_EXTEND;
            }
            // track the values for read gaps
            if ((readGap[i][j] = similarity[i][j-1] + C_GAP_OPEN) < readGap[i][j-1] + C_GAP_EXTEND){
                readGap[i][j] = readGap[i][j-1] + C_GAP_EXTEND;
            }

            // calculate the new value for the similarity matrix
            if(get_base(read,i-1) == get_base(reference,j-1)){
                similarity[i][j] = similarity[i-1][j-1] + C_MATCH;
            }else{
                similarity[i][j] = similarity[i-1][j-1] + C_MISMATCH;
            }

            // find the max of the refGap, readGap, and match scores
            sourceCell[i][j] = 0;
            if (refGap[i][j] > similarity[i][j]){
                similarity[i][j] = refGap[i][j];
                sourceCell[i][j] = 1;
            }
            if (readGap[i][j] > similarity[i][j]){
                similarity[i][j] = readGap[i][j];
                sourceCell[i][j] = 2;
            }
        } // end of a column
        
        // this looks for the highest score along the bottom row,
        //  should return the position of the last base in the read
        if (similarity[READ_LENGTH][j] > score){
            score = similarity[READ_LENGTH][j];
            alignment = j - 1;
        }
    } // end of all rows
}

// copies the read from the pointer to the current read
void Aligner::setRead(uint64_t* newRead){
    read = newRead;
}

// copies the reference from the pointer to the current reference
void Aligner::setReference(uint64_t * newRef){
    reference = newRef;
}

// returns a pointer to the current read
uint64_t * Aligner::getRead(){
    return read;
}

// returns a pointer to the current reference
uint64_t * Aligner::getReference(){
    return reference;
}

// returns the score of the previous alignment
int Aligner::getScore(){
    return score;
}

// returns the previous alignment
uint32_t Aligner::getAlignment(){
    return alignment;
}


// initializes the entire SW matrix
void Aligner::initMatrices(){

    // first column should be initialized to deter read gaps
    for(int i=1; i<=READ_LENGTH; i++){
        similarity[i][0] = i*C_GAP_EXTEND-1;
        refGap[i][0] = i*C_GAP_EXTEND-1;
        readGap[i][0] = i*C_GAP_EXTEND-1;
        sourceCell[i][0] = 1;
    }

    // first row should be gap open and extend for read gap
    // similarity and ref gap should be zero in first row
    for(int i=0; i<=REF_LENGTH; i++){
        readGap[0][i] = 0;
        similarity[0][i] = 0;
        refGap[0][i] = 0;
        sourceCell[0][i] = 0;
    }
}

// prints ones of the matrices to the screen
void Aligner::printMatrices(){

    // print the reference along the top
    printf("\tN\t\t");
    for(int i=0; i<REF_LENGTH; i++){
        printf("%c\t\t", baseToChar(get_base(reference,i)));
    }
    printf("\n");

    // print the array with read bases in the first column
    for(int i=0;i<=READ_LENGTH;i++){
        for(int k=0;k<3;k++){
            for(int j=0;j<=REF_LENGTH;j++){
                // read printed down along the side
                if(i==0 && j==0 && k==1){
                    printf("N\t");
                }else if(j==0 && k==1){
                    printf("%c\t", baseToChar(get_base(read,i-1)));
                }else{
                    printf(" \t");
                }

                // matrix data
                if(k==0){
                    printf("%i\t", similarity[i][j]);
                }else if(k==1){
                    printf("%i\t", refGap[i][j]);
                }else if(k==2){
                    printf("%i\t", readGap[i][j]);
                }
            }
            printf("\n");
        }
        printf("\n");
    }
}

// function to trace back along the current read and print out the actual alignment
void Aligner::traceBack(){
    // 0=don't print read,
    // 1=don't print ref,
    // 2=don't print both,
    // 3=ref gap(print read),
    // 4=read gap(print ref),
    // 5=print both
    int readIndex;
    int refIndex;

    char readForPrint[REF_LENGTH+READ_LENGTH+1] = {};
    char refForPrint[REF_LENGTH+READ_LENGTH+1] = {};

    // sourceCell explanation
    // 0 = diagonal
    // 1 = ref gap
    // 2 = read gap
    // D = destination cell
    //
    //   reference
    // r  --- ---
    // e | 0 | 1 |
    // a  --- ---
    // d | 2 | D |
    //    --- ---

    // trace all the way back through the matrix
    readIndex = READ_LENGTH-1;
    refIndex = REF_LENGTH-1;
  
    // set the last base of the reference and the read to '\0'
    readForPrint[REF_LENGTH+READ_LENGTH] = '\0';
    refForPrint[REF_LENGTH+READ_LENGTH] = '\0';

    for(int i=REF_LENGTH+READ_LENGTH-1; i>=0; i--){
        
        // after the alignment of the read
        if(refIndex > (int) alignment){
            readForPrint[i] = '.';
            refForPrint[i] = baseToChar(get_base(reference,readIndex));
            refIndex--;
        }
        // during the reads alignment
        else if(readIndex >= 0){

            // diagonal
            if(sourceCell[readIndex+1][refIndex+1] == 0){
                readForPrint[i] = baseToChar(get_base(reference,readIndex));
                refForPrint[i] = baseToChar(get_base(reference,readIndex));
                readIndex--;
                refIndex--;
            }
            // ref gap
            else if(sourceCell[readIndex+1][refIndex+1] == 1){
                refForPrint[i] = '-';
                readForPrint[i] = baseToChar(get_base(read, readIndex));
                readIndex--;
            }
            // read gap
            else{
                readForPrint[i] = '-';
                refForPrint[i] = baseToChar(get_base(reference,readIndex));
                refIndex--;

            }
        }
        // before the alignment of the read
        else {
            readForPrint[i] = '.';
            // if there are still reference bases to print
            if (refIndex >= 0){
                refForPrint[i] = baseToChar(get_base(reference,readIndex));
                refIndex--;
            }
            // if all read and reference bases have been printed
            else{
                refForPrint[i] = '.';
            }
        }
    }

    // print the read and reference
    int k;
    for(k=0; refForPrint[k]=='.'; k++);
    printf("Ref  = %s\n", &refForPrint[k]);
    printf("Read = %s\n", &readForPrint[k]);
    printf("Score= %i\n", score);

}

