/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <string.h>

#include "../SoftwareIndex/hashfunc.h"

using namespace std;

/*
 * Stream oriented C program for hashing the datafile
 *
 * Supports numbers up to 64 bits.
 *
 * The input file is multiple lines of: <value> <index> <bit>
 * The output file is the same lines, of: hash(<value>) <index> <bit>
 *   That is, the output is the same as the input, except the value field is hashed.
 */

int main(int argc, char *argv[])
{

  if (argc != 5) {
    cerr << "Incorrect number of arguments: " << argv[0] << " <seed_bits> <key_bits> <in_file> <out_file>\n";
    exit(EXIT_FAILURE);
  }

  // Eventually we can use the seed and key bits to pick the right hashing function.  For now,
  // make sure the arguments match the hash call below.
  if (strcmp(argv[1],"44") != 0 || strcmp(argv[2], "16")) {
    cerr << "Unsupported hashing function requested.\n"
	 << "Check seed_bits and key_bits arguments, or change hashing function\n";
    exit(EXIT_FAILURE);
  }

  const char *inFilename = argv[3];
  const char *outFilename = argv[4];

  ifstream inFile(inFilename);
  ofstream outFile(outFilename);

  uint64_t value, hvalue, index, bit, line, hhvalue;

  line = 1;
  while (inFile >> value) {
    if (!(inFile >> index) || !(inFile >> bit)) {
      cerr << "Malformed line " << line << "\n";
      return EXIT_FAILURE;
    }
    line++;
    hvalue = hashfunc_44_16(value);
    hhvalue = hashfunc_44_16(hvalue);
    if (hhvalue != value) {
      cerr << "Hash is supposed to be reversable, but isn't.\n"
	   << "value: " << " hash(value): " << hvalue << " hash(hash(value)): " << hhvalue << endl;
      exit(EXIT_FAILURE);
    }
    outFile << hvalue << " " << index << " " << bit << endl;
  }

  inFile.close();
  outFile.close();

  return EXIT_SUCCESS;
}
