/*
 * Aligner.h
 *
 *  Created on: Nov 5, 2011
 *      Author: cbolson1
 * Description:
 * Copyright 2011 Pico Computing
 */

#ifndef ALIGNER_H_
#define ALIGNER_H_



#define C_GAP_EXTEND -1
#define C_GAP_OPEN -2
#define C_MATCH 2
#define C_MISMATCH -2

#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include "defines.h"
#include "util.h"

// affine gap model SW-NW
// V=similarity, E=ref_gap, F=read_gap
typedef struct{
    int similarity;
    int read_gap;
    int ref_gap;
    int sourceCell; // 0=diag, 1=ref gap, 2=read gap
} affineGapCell;

class Aligner {
private:
    /*
    char* read [READ_LENGTH+1];
    char* reference [REF_LENGTH+1];
    */

    uint64_t* read;
    uint64_t* reference;

    int score;
    uint32_t alignment;
        
    //affineGapCell swArray[READ_LENGTH+1][REF_LENGTH+1];
    int similarity[READ_LENGTH+1][REF_LENGTH+1];
    int readGap[READ_LENGTH+1][REF_LENGTH+1];
    int refGap[READ_LENGTH+1][REF_LENGTH+1];
    int sourceCell[READ_LENGTH+1][REF_LENGTH+1];

    // methods
    void initMatrices();

public:
    Aligner();
    ~Aligner();

    // methods
    uint64_t * getRead();                    // returns a pointer to the read
    uint64_t * getReference();               // returns a pointer to the reference

    void setRead(uint64_t * newRead);        // copies 'newRead' into the read string
    void setReference(uint64_t * newRef);    // copies 'newRef' into the reference string

    void alignRead();                   // aligns the read against the reference
    int getScore();                     // returns the score of the alignment
    uint32_t getAlignment();            // returns the alignment location

    void traceBack();                   // prints out the trace back information (shows the alignment)
    void printMatrices();               // prints the SW matrices
};

#endif /* ALIGNER_H_ */
