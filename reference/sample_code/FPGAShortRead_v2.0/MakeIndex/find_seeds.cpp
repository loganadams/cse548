/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
C++ script for extracting (seed, CAL) pairs from a fasta file which contains
multiple sequences.  CALS are indexed as though sequences are concatenated, but
seeds are not extract across sequence boundaries.
The lexicographically smaller of the two seeds (normal and reverse complement) for each CAL is
chosen, or both if they have the same value.  These values are then hashed before being outputted.

example usage:
./find_seeds 22 sequences.fasta > seeds.txt
*/

#ifndef FIND_SEEDS
#define FIND_SEEDS

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <string>

#include "../SoftwareIndex/hashfunc.h"

using namespace std;

// STATIC METHODS
// Encodes bases in 2 bits
static int charToBase(char bs) {
	switch(bs) {
		case 'A':
			return 0;
		case 'C':
			return 1;
		case 'G':
			return 2;
		case 'T':
			return 3;
		case 'a':
			return 0;
		case 'c':
			return 1;
		case 'g':
			return 2;
		case 't':
			return 3;
		default:
			cerr << "Error with read in base, not a base" << endl;
			exit(1);
	}
	return -1;
}
// Returns the complement of the input base
static char getComp(char bs) {
	switch(bs) {
		case 'A':
			return 'T';
		case 'a':
			return 'T';
		case 'C':
			return 'G';
		case 'c':
			return 'G';
		case 'G':
			return 'C';
		case 'g':
			return 'C';
		case 'T':
			return 'A';
		case 't':
			return 'A';
		default:
			cerr << "Error with read in base, not a base" << endl;
			exit(1);
	}
	return ' ';
}

int main(int argc, const char* argv[]) {
  // Check for correct number of params
  if(argc != 4) {
    cerr << "Error: wrong number of arguments" << endl;
    cerr << "Usage: ./find_seeds <seed_length> <fasta file> <Hash?0/1> > seeds.txt" << endl;
    exit(1);
  }
  
  // INITIALIZE VARIABLES
  // Set seed length and seed mask to given values
  int SEED_LENGTH = atoi(argv[1]);	
  uint64_t one = 1;
  uint64_t SEED_MASK = (one << (2 * SEED_LENGTH)) - 1;
  // Deal with hashing
  int shouldHash = atoi(argv[3]);
  if (shouldHash != 0 && shouldHash != 1) {
    cerr << "Invalid hash value - must be 0(don't hash) or 1(do hash)\n";
    exit(EXIT_FAILURE);
  }
  if (shouldHash == 1 && SEED_LENGTH != 22) {
    cerr << "find_seeds only currently supports hashing length 22 seeds\n";
    exit(EXIT_FAILURE);
  }
  // Current position in the reference genome
  uint64_t base_num = 0;
  uint64_t start_base_num = 0;
  // Tracked seeds
  uint64_t seed = 0;
  uint64_t rev_comp_seed = 0;
  // These represent the current foward and reverse bases and should be 0-3
  int current_f_base = 0;
  uint64_t current_r_base = 0; // unsigned long long for bitwise operations that need at least 64 bits
  // Number of bases in the currently computed seeds
  int num_current_seed_bases = 0;
  
  // Open the sequence file
  ifstream indata;
  string line;
  indata.open(argv[2]);
  
  // Check if the file actually opened
  if(!indata) {
    cerr << "Error: file could not be opened" << endl;
    exit(1);
  }
  
  // Grab data until the end of the file
  getline(indata, line, '\n');
  while(!indata.eof()) {
    if(line[0] == '>') {
      // CLEAR ARRAY
      seed = 0;
      rev_comp_seed = 0;
      num_current_seed_bases = 0;
    } else {
      int string_len = line.length();
      int i;
      for(i = 0; i < string_len; i++) {
	// Grab the current base and add it if valid
	char base = line[i];
	if(base == 'n' || base == 'N') {
	  // CLEAR ARRAY
	  seed = 0;
	  rev_comp_seed = 0;
	  num_current_seed_bases = 0;
	} else {
	  // ADD BASE
	  // Track the base count
	  if(num_current_seed_bases < SEED_LENGTH) {
	    num_current_seed_bases++;
	  }
	  
	  // Store the most recent foward and rev_comp bases for scoring
	  current_f_base = charToBase(base);
	  current_r_base = charToBase(getComp(base));	
	}
	
	// UPDATE SEED
	// Update the foward and reverse complement seeds
	seed = ((seed << 2) & SEED_MASK) + current_f_base;
	rev_comp_seed = (rev_comp_seed >> 2) + (current_r_base << (2 * (SEED_LENGTH - 1)));
	
	// PRINT SEED
	// If seed is valid, print a line of output
	if(num_current_seed_bases == SEED_LENGTH) {
	  start_base_num = base_num - (SEED_LENGTH - 1);
	  // CHOOSE_SMALLER: Choose the lexicographically smaller of the two seeds, or both if they are the same
	  if(seed < rev_comp_seed || seed == rev_comp_seed) {
	    // Use the seed, not the rev_comp
	    if (!shouldHash) {
	      cout << seed << " " << start_base_num << " " << "0" << endl;
	    } else {
	      // HASH THE SEED
	      uint64_t hvalue = hashfunc_44_16(seed);
	      uint64_t hhvalue = hashfunc_44_16(hvalue);
	      if (hhvalue != seed) {
		cerr << "Hash is supposed to be reversable, but isn't.\n"
		     << "value: " << seed 
		     << " hash(value): " << hvalue 
		     << " hash(hash(value)): " << hhvalue << endl;
		exit(EXIT_FAILURE);
	      }
	      cout << hvalue << " " << start_base_num << " " << "0" << endl;
	    }
	  }
	  if (seed > rev_comp_seed || seed == rev_comp_seed) {
	    // Use the rev_comp, not the seed
	    if (!shouldHash) {
	      cout << rev_comp_seed << " " << start_base_num << " " << 1 << endl;
	    } else {
	      // HASH THE REVERSE COMPLEMENT SEED
	      uint64_t hvalue_r = hashfunc_44_16(rev_comp_seed);
	      uint64_t hhvalue_r = hashfunc_44_16(hvalue_r);
	      if (hhvalue_r != rev_comp_seed) {
		cerr << "Hash is supposed to be reversable, but isn't.\n"
		     << "value: " << rev_comp_seed << " hash(value): " << hvalue_r << " hash(hash(value)): " << hhvalue_r << endl;
		exit(EXIT_FAILURE);
	      }
	      cout << hvalue_r << " " << start_base_num << " " << 1 << endl;
	    }
	  }
	}
	
	// Update the base number
	base_num++;
      }
    }
    getline(indata, line, '\n');
  }
  return 0;
}

#endif // FIND_SEEDS
