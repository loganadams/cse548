/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_256_H
#define CHR21_256_H

#define TYPE "CHR21_256_29BP"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 256
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/chr21_256_32bp.index"
#define DEFAULT_REFERENCE "../../data/chr21.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 64   // number of bits used for the start pointer
#define IDX_BITS       24   // number of bits of the seed used as address
#define OFFSET_BITS        32   // number of bits used for each offset
#define TAG_BITS           4    // number of bits of the seed used as tag
#define SEED_LENGTH   29        // number of bases used for the seed

#define MAX_SAME_SEED 128      // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
    uint64_t startPtr : START_POINTER_BITS;
    uint64_t offsets[1LL<<TAG_BITS];
} ptrTable_t;
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define OFFS_BITS (sizeof(uint64_t)*8)

#include <stdlib.h>
#include <stdio.h>
inline void checkOffset(uint64_t value, uint64_t offset_index){
  if(offset_index>=(1LL<<TAG_BITS)) {
    printf("Error: invalid pointer table index %lu\n", offset_index);
    exit(EXIT_FAILURE);
  }
  if(value > ((1LL<<OFFSET_BITS) -1)){
    printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
    exit(1);
  }
}

inline void setOffset(ptrTable_t *name, uint64_t index, uint64_t value){
  checkOffset(value, index);
  name->offsets[index] = value;
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]&=~((OFFSET_MASK)<<((index*OFFSET_BITS)%OFFS_BITS));
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]|=((value&OFFSET_MASK) <<((index*OFFSET_BITS)%OFFS_BITS));
}

inline uint64_t getOffset(ptrTable_t *name,uint64_t index){
  return name->offsets[index];
  //    return ((((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]>>((index*OFFSET_BITS)%OFFS_BITS))&OFFSET_MASK);    
}

inline uint64_t do_hash(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x49273038507561b, 0x3d8aed1361a68ea, 0x1d3b464fc1c03ea,
0x0cf4fbd7d32d93d, 0x7c567afc1881df9, 0x6a111d6a094c6dc, 0x2d19550c01e3637,
0x00a5e8675888b78, 0x1719bb4731037ae, 0x48a19e901b0d0e2, 0x2b8c495fb1cde70,
0x6d36745ae001a08, 0x0580fabcc6626ae, 0x52b1c661fbf1bc2, 0x3be8a9bb7d7e137
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x36923a16343f375, 0x0346de6945263a0, 0x5750e68732e0686,
0x2d2e281dce098dc, 0x16a769893f90b5d, 0x0469251547a72f5, 0x1d8c6c82ddaf7b0,
0x60091d144a85951, 0x2bfe0f46fe0be2e, 0x27ee8257ea3ca4b, 0x63857b5ceb66dda,
0x20651c7cbda94a8, 0x10875a799d553f9, 0x65ebdee8ab6c986, 0x086b5626db895a6
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0x14f8ca2a0c1e59b, 0x2af86d5fb60d8ea, 0x6d40f83e0b4be72,
0x29718bb85fc44a4, 0x33354774a1116de, 0x2e559ff2beba87e, 0x131126fced82431,
0x28cc75f4ae40cc0, 0x3386f5f2ec10c98, 0x3a44142bef9fdf7, 0x2c44e3b77409b88,
0x027263c143e1aa5, 0x6d63b707acbdb67, 0x09f28f4c574e879, 0x738e50a0540ca21
  };
  static const uint64_t mask_table4[16] = {
    0x00000000, 0x179c26455687ef7, 0x7c4d9a81b66e0f3, 0x12617c2545e8f0d,
0x7305aa24d27c856, 0x1080114c7399aca, 0x04963f01e73d501, 0x1dfb8d776bc09e0,
0x5d498cd1668161e, 0x0fbff9523bb6892, 0x75ebadecf4a450b, 0x0e6d27c7f1fc727,
0x0c3c532d451121f, 0x214f4c661be02cb, 0x1c2ddcdd15b05cc, 0x1b6939a16abd365
  };
  static const uint64_t mask_table5[16] = {
    0x00000000, 0x3c86097da60fbaa, 0x314a4740747a8c2, 0x0736c2842c4ba2b,
0x0e2182a2d89e311, 0x135bd1ccdb44f59, 0x1517f0ee0915e22, 0x26ab30ce18b6be0,
0x1a2cadd8ab0e191, 0x138cb71b7d2ef87, 0x0c0b1f564c6d2ef, 0x03899cac13bb943,
0x37175433315ccce, 0x0be65fee7861980, 0x34dea038a530d4d, 0x3b0404f65d3d00f
  };
  static const uint64_t mask_table6[16] = {
    0x00000000, 0x2e4e16ca21248b5, 0x335b1eab55b7114, 0x301234335b15683,
0x2a5061e60b6d692, 0x23b80dcc0eedb91, 0x079f48f9afc61e0, 0x3d88c917351e447,
0x3762929dd59496d, 0x12c7521e81e6d8d, 0x2b0480df651a5fd, 0x14d4906b7b4df58,
0x61c1d87bc6e820e, 0x64dd4062d6b58cf, 0x1211eb73bfb0cf3, 0x59fa81f5c8939c0
  };
  static const uint64_t mask_table7[16] = {
    0x00000000, 0x2a3837a3c915c50, 0x2a3b81a6d7b3189, 0x5fc1f04dd8fee4c,
0x2e697de08f01185, 0x3ad27167b9dd8a9, 0x28e8d22971b31f8, 0x307638457391e5e,
0x6a339330cbd685f, 0x143280c010b7c1b, 0x4fe73dcf654d110, 0x03d7b7e690e21fe,
0x220fc23b6466e10, 0x516dac45eb6d72f, 0x0eeddae5d74b157, 0x2808cb14f3f65ea
  };
  static const uint64_t mask_table8[16] = {
    0x00000000, 0x5f57b87124ff296, 0x736be8760bb86d2, 0x1f55380b4ffdf4d,
0x2bb6fd785a18b6a, 0x782e6620cfa3bde, 0x1117b688b805a7b, 0x4fe023eaa2f720e,
0x24a4efee98f8715, 0x25c1a2307dc7528, 0x100bfe0ac8ef8a3, 0x6b9dfeca1da5f69,
0x2aec1251752d0f3, 0x1e5fe74954b21ee, 0x28aa57f39929672, 0x068cc543e92b273
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table6[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table7[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table8[ temp & 0x3 ];


  accum <<= 30;
  return in ^ accum;

}

#endif
