/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file Reference.h
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * Reference.cpp implements the methods defined in the Reference class and Reference.h
 *
 * This class holds binary and character representations of the 
 * reference used to create the index file when building the index file
 * and contains only the binary representation when aligning short reads.
 */

#include "Reference.h"

/**
 * Constructor for the Reference Class. Allocates nothing.
 * @returns A Reference Object
 */
Reference::Reference(){
    refSize = 0;
    numBases = 0;
}

/**
 * Deconstructor for the Reference Class. If the character reference or
 * binary reference has been allocated, then the data is freed.
 */
Reference::~Reference(){
    if(referenceArray != NULL){
    delete referenceArray;
    }

    if(charReferenceArray != NULL){
    delete charReferenceArray;
    }

}

/**
 * readReference reads the character reference from a reference file
 * (typically a .fasta file), removing all chromisome annotations, newlines,
 * and other extraneous characters. If this method is called, charReferenceArray
 * will no longer be NULL, and instead will point to an allocated character
 * array.
 *
 * @param referenceFileName The filename and path to the file that contains
 * the ascii reference string. (typically a .fasta file)
 *
 * @returns the number of bases read from the file (numBases) DEPRECATED - USE getNumBases()
 */
uint64_t Reference::readReference(char * referenceFileName){
    FILE * referenceFile = fopen(referenceFileName,"rb");
    rewind(referenceFile);
    fseek(referenceFile, 0L,SEEK_END);
    uint64_t numPossibleBases = ftell(referenceFile);
    rewind(referenceFile);
    charReferenceArray = new char[numPossibleBases+1]();
    fread(charReferenceArray, sizeof(char), numPossibleBases, referenceFile);
    //printf("\n*** numPossibleBases: %llu\n", numPossibleBases);
    numBases = 0;
    for(uint64_t currentIdx = 0 ; currentIdx < numPossibleBases; currentIdx++){
      if(charReferenceArray[currentIdx] == '>'){
        while(currentIdx < numPossibleBases && charReferenceArray[currentIdx] != '\n'){
	  currentIdx++;
        }
      } else if(isBase(charReferenceArray[currentIdx])) {
        charReferenceArray[numBases++]=charReferenceArray[currentIdx];
      }
    }
    
    memset(&charReferenceArray[numBases], 0, numPossibleBases-numBases);
    refSize = (numBases/4 + (DRAM_BLOCK_BITS/8 - 1)) & ~(DRAM_BLOCK_BITS/8 - 1);
    convertToBinary();
    fclose(referenceFile);
    return numBases;
}

/**
 * convertToBinary converts the ascii string in charReferenceArray to a binary
 * string, with each base compressed to a two-bit value.
 *
 * The array containing the binary data can be accesed with getBinaryReference()
 * and contains refSize bytes (accessed with getRefSize())
 */
void Reference::convertToBinary(){
    referenceArray = new uint64_t[refSize/8];
    charToBinary(charReferenceArray, referenceArray, numBases);
}

/**
 * write writes the binary reference to the given file pointer
 *
 * @param destination An open FILE pointer where the binary data can be written
 */
void Reference::write(FILE * destination){
    fwrite(referenceArray,
       sizeof(char), 
       refSize,
       destination);
}

void Reference::writeToTextFile(const char * textFile){
    FILE * refTextFile = fopen(textFile, "w");
    if(refTextFile == NULL){
        fprintf(stderr, "Could not open reference.txt file for output\n");
        exit(1);
    }
    // Write header information for reference text file
    fprintf(refTextFile, "%lu\n", numBases);
    int charCount = 0;
    for (uint64_t i=0; i<numBases; i++) {
      fprintf(refTextFile, "%c", charReferenceArray[i]);
      if ((i & (DRAM_BLOCK_BITS/BITS_PER_BASE-1)) == (DRAM_BLOCK_BITS/BITS_PER_BASE-1))
        fprintf(refTextFile, "\n");
    }
    // Fill out the last block with N's
    uint64_t rest = numBases & (DRAM_BLOCK_BITS/BITS_PER_BASE-1);
    rest = (DRAM_BLOCK_BITS/BITS_PER_BASE - rest) & (DRAM_BLOCK_BITS/BITS_PER_BASE-1);
    for (uint64_t i=0; i<rest; i++) {
      fprintf(refTextFile, "N");
    }
    if (rest > 0) fprintf(refTextFile, "\n");
    fclose(refTextFile);
}

/**
 * read reads the binary reference from an already open FILE pointer. It reads size
 * bytes from the file (this 
 *
 * @param indexFile An open FILE pointer that points to the indexFile, where the binary
 * data will be read from.
 *
 * @param size The expected size of the binary reference, in bytes. The value of size
 * is typically found in the header.
 * 
 */
void  Reference::read(FILE * source, uint64_t size){
    referenceArray = new uint64_t[size/sizeof(uint64_t)];
    int numBytesRead = fread(referenceArray,sizeof(char),size,source);
    if(numBytesRead != size){
    fprintf(stderr, "Error: Expected to read %lu bytes from reference file for reference, got %d\n",
        size,
        numBytesRead);
    exit(1);
    }
    refSize = size;
}

/**
 * getReferenceSize returns the current size of the binary reference (in bytes)
 *
 * The size of the reference can vary for the same reference across different arch's,
 * even if the same reference was used for both architectures
 * 
 * @returns the size of the binary reference (in bytes)
 */
uint64_t Reference::getReferenceSize(){
    return refSize;
}

/**
 * getNumBases returns the current size of the character reference. If this method is called
 * when the binary reference was read from a file, it will return 0.
 *
 * Unlike the reference size, the number of bases will not change across architectures
 * 
 * @returns a unsigned 64 bit integer equal to the number of bases in the reference
 */
uint64_t  Reference::getNumBases(){
    return numBases;
}

/**
 * getBinaryReference returns a pointer to the array containing the binary reference data
 *
 * The size of this array (in bytes) is given by getReferenceSize()
 * 
 * @returns pointer to the uisigned 64 bit integer array containing the binary reference
 * data
 */
uint64_t * Reference::getBinaryReference(){
    return referenceArray;
}
/**
 * getCharReference returns a pointer to the array containing the ascii reference data
 *
 * The length of this array is given by getNumBases(), and should be \0 terminated
 * 
 * @returns a pointer to the ascii string containing the reference
 * 
 * */

char * Reference::getCharReference(){
    return charReferenceArray;
}

/**
 * getBinary reads a numBases long subset of the binary reference data starting at start
 * and returns it in dest. If revComp is true, it returns the revComp of the binary string
 *
 * If this method is called while the index file is being created,
 * convertToBinary() should be called before this method.
 *
 * (start + numBases -1) should be less than the number of bases in the reference
 * 
 * @param dest where the method will store the binary data
 * @param start the start index in the reference
 * @param numBases The number of bases to read from the binary reference
 * @param revComp A boolean value. If revComp is true, then the binary string returned
 * will be the reverse complement of the binary string in the reference
 *
 * */
void Reference::getBinarySubstring(uint64_t * dest, uint64_t start, uint64_t numBases, bool revComp){
    uint64_t start_index = (start * BITS_PER_BASE) / (sizeof(referenceArray[0])*8);
    uint64_t end_index = ((start+numBases) * BITS_PER_BASE)/(sizeof(referenceArray[0])*8);
    uint64_t r_shift_amt = (start * BITS_PER_BASE) % (sizeof(referenceArray[0])*8);
    uint64_t l_shift_amt = (sizeof(referenceArray[0])*8)-r_shift_amt;
    uint64_t num_indexes = ((numBases* BITS_PER_BASE) + (sizeof(referenceArray[0])*8)-1)/ (sizeof(referenceArray[0])*8);
    for(int i = 0 ; i < num_indexes; i ++){
        dest[i] |= referenceArray[i+start_index] >> r_shift_amt;
        if( l_shift_amt != 64){ // eff this bug
            dest[i] |= referenceArray[i+start_index+1] << l_shift_amt;
        }
    }
    if(revComp){
        reverseCompBinaryString(dest, numBases);
    }
}


/**
 * 
 * getCharacterSubstring reads a numBases long subset of the binary reference data starting at start
 * and returns it in dest. If revComp is true, it returns the revComp of the binary string
 *
 * This method will read from the binary reference (for simplicity's sake) so
 * If this method is called while the index file is being created,
 * convertToBinary() should be called before this method.
 * 
 * (start + numBases -1) should be less than the number of bases in the reference
 * 
 * @param dest where the method will store the character data
 * @param start the start index in the reference
 * @param numBases The number of bases to read from the reference
 * @param revComp A boolean value. If revComp is true, then the binary string returned
 * will be the reverse complement of the binary string in the reference
 *
 */
void Reference::getCharacterSubstring(char * dest, uint64_t start, uint64_t numBases, bool revComp){
    uint64_t binaryData[(REF_LENGTH*BITS_PER_BASE)/(sizeof(uint64_t)*8)] = {0LL,0LL,0LL};
    getBinarySubstring(binaryData,start, numBases, 0);
    binaryToString(binaryData, numBases,dest);
    if(revComp){
    convert_to_revcomp(dest, numBases);
    }
}

