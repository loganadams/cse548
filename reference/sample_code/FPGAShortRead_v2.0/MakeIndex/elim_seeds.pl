
# check for correct number of parameters
if($#ARGV != 1){
    die "Required arguments:\n\t1) Maximum number of seeds before elimination from CAL table\n\t2) Name of file containing seeds and CALs (sorted by seed)\n";
}

my $maxSeeds = shift;
my @arry;
my $current_seed = -1; #not sure initial value even matters
my $numElimSeeds=0;

while(<>) {

    my @tokens = split ' ';
    my $this_seed = $tokens[0];
    
    if($this_seed == $current_seed) {
        #do nothing
    } else {

        if($#arry + 1 > $maxSeeds) {
            #do nothing
            $numElimSeeds++;
        } else {
            for(@arry) {
                print $_;
            }
        }

        @arry = ();
        $current_seed = $this_seed;
    }
    push @arry, $_;
}

# print the last seed here
if ($#arry < $maxSeeds){
    for(@arry){
        print $_;
    }
}else{
    $numElimSeeds++;
}

#print "Total number of eliminated seeds = $numElimSeeds\n";
