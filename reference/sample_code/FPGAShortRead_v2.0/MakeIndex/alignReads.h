/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef ALIGNREADS_H
#define ALIGNREADS_H
#include "defines.h"
#include "CALTable.h"
#include "PointerTable.h"
#include "Reference.h"
#include "util.h"
#include "argp.h"
#include "ShortRead.h"
#include "Aligner.h"
#include "Filter.h"

#define DEFAULT_READS "../../data/chr21.fastq"
#define DEFAULT_GOLD "gold.txt"

void validateTables(PointerTable * pointerTable, CALTable* calTable, Reference* reference, int validateNum);
void ValidateTables(PointerTable* pointerTable, CALTable* calTable, Reference* reference, int validateNum);

/* The options we understand. */
static struct argp_option options[] = {
    //    {"verbose",      's',      0, 0, "Produce more console output" },
    {"align",        'a',      0, 0, "Perform alignment on seeds" },
    {"print_cals",   'c',      0, 0, 
     "Print all of the CAL's associated with a seed, following the seed" },
    {"print_sw",     'p',      0, 0, "Print out all SmithWaterman results"},
    {"index_file",   'i', "FILE", 0,
     "Path to index file containing the pointer table, reference and cal table DEFAULT="DEFAULT_INDEX },
    {"reads_file",   'r', "FILE", 0,
     "Path to reads file, which contains a list of short reads DEFAULT="DEFAULT_READS },
    {"gold_file_type",    'g', "INTEGER", 0,
     "type of gold file to write - seeds(1), cals(2), SW(3), or all SW(4)" },
    {"gold_file",    'o', "FILE", 0,
     "name of file where golden values should be written, DEFAULT="DEFAULT_GOLD },
    {"validate",     'v', "INTEGER", 0, "Run validation test on the index tables" },
    { 0 }
};
/* Program documentation. */
static char doc[] ="Aligns a set of short reads to a reference file";
/* A description of the arguments we accept. */
static char args_doc[] = "-i,--index_file -r,--reads_file ";
static error_t parse_opt (int key, char *arg, struct argp_state *state);
static struct argp argp = { options, parse_opt, args_doc, doc };


#endif
