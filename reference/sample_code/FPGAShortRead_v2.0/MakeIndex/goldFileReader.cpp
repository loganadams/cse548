/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "goldFileReader.h"

char * fileName =(char *) "gold.txt";
FILE * goldFile;
int main(int argc, char ** argv){
    argp_parse (&argp, argc, argv, 0, 0, NULL);
    printf("Trying to open output file: %s\n", fileName);
    goldFile = fopen(fileName, "r");
    if (goldFile == NULL) {
        fprintf(stderr, "Error opening file %s\n", fileName);
        exit(1);
    }else{
        printf("gold file is open\n");
    }
    sw_gold_t currentScore = {0,0,0,0};
    while(!feof(goldFile)){
        fread(&currentScore, sizeof(sw_gold_t), 1,goldFile);
        if(!feof(goldFile)){
            printf("Read: %lu, Alignment: %lu, Score: %lu, revComp: %lu\n", 
                   currentScore.readId,
                   currentScore.alignment, 
                   currentScore.score,
                   currentScore.revComp);
        }
    }
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
     /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    switch (key)
    {
    case 'f':
        fileName = arg;
    case ARGP_KEY_ARG:
        if (state->arg_num >= 4)
        /* Too many arguments. */
        argp_usage (state);
        break;
    case ARGP_KEY_END:
        if (state->arg_num < 0)
        /* Not enough arguments. */
        argp_usage (state);
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}
