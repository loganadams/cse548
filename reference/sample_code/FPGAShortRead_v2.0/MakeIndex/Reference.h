/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef REFERENCE_H
#define REFERENCE_H
/**
 * @file Reference.h
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * Reference.h defines the Reference class.
 *
 * This class holds binary and character representations of the 
 * reference used to create the index file when building the index file
 * and contains only the binary representation when aligning short reads.
 * 
 */


#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "util.h"
#include "defines.h"

class Reference{
 private: 
    uint64_t *    referenceArray; /// Holds the binary reference (when created)
    uint64_t    refSize;        /// size of the reference (in bytes) 
                                /// Can vary for the same reference across different arch's
    // Private Fields below only used when building the index file
    uint64_t    numBases;    /// Number of Bases in the reference (Used for building the index, not reading)
    char *    charReferenceArray; /// Pointer to the string that contains the charReferenceArray

 public:
    /**
     * Constructor for the Reference Class. Allocates nothing.
     * @returns A Reference Object
     */
    Reference();
    /**
     * Deconstructor for the Reference Class. If the character reference or
     * binary reference has been allocated, then the data is freed.
     */
    ~Reference();

    // The next three methods focus on creating the index file
    /**
     * readReference reads the character reference from a reference file
     * (typically a .fasta file), removing all chromisome annotations, newlines,
     * and other extraneous characters. If this method is called, charReferenceArray
     * will no longer be NULL, and instead will point to an allocated character
     * array.
     *
     * @param referenceFileName The filename and path to the file that contains
     * the ascii reference string. (typically a .fasta file)
     *
     * @returns the number of bases read from the file (numBases) DEPRECATED - USE getNumBases()
     */
    uint64_t    readReference(char * referenceFileName);

    /**
     * convertToBinary converts the ascii string in charReferenceArray to a binary
     * string, with each base compressed to a two-bit value.
     *
     * The array containing the binary data can be accesed with getBinaryReference()
     * and contains refSize bytes (accessed with getRefSize())
     */
    void    convertToBinary();

    /**
     * write writes the binary reference to the given file pointer
     *
     * @param destination An open FILE pointer where the binary data can be written
     */
    void    write(FILE * destination);
    void writeToTextFile(const char * textFile);
    /**
     * read reads the binary reference from an already open FILE pointer. It reads size
     * bytes from the file (this 
     *
     * @param indexFile An open FILE pointer that points to the indexFile, where the binary
     * data will be read from.
     *
     * @param size The expected size of the binary reference, in bytes. The value of size
     * is typically found in the header.
     * 
     */
    void    read(FILE* indexFile, uint64_t size);

    /**
     * getReferenceSize returns the current size of the binary reference (in bytes)
     *
     * The size of the reference can vary for the same reference across different arch's,
     * even if the same reference was used for both architectures
     * 
     * @returns the size of the binary reference (in bytes)
     */
    uint64_t    getReferenceSize();

    /**
     * getNumBases returns the current size of the character reference. If this method is called
     * when the binary reference was read from a file, it will return 0.
     *
     * Unlike the reference size, the number of bases will not change across architectures
     * 
     * @returns a unsigned 64 bit integer equal to the number of bases in the reference
     */
    uint64_t    getNumBases();

    /**
     * getBinaryReference returns a pointer to the array containing the binary reference data
     *
     * The size of this array (in bytes) is given by getReferenceSize()
     * 
     * @returns pointer to the uisigned 64 bit integer array containing the binary reference
     * data
     */
    uint64_t *    getBinaryReference();

    /**
     * getCharReference returns a pointer to the array containing the ascii reference data
     *
     * The length of this array is given by getNumBases(), and should be \0 terminated
     * 
     * @returns a pointer to the ascii string containing the reference
     * 
     * */
    char *    getCharReference();

    /**
     * getBinary reads a numBases long subset of the binary reference data starting at start
     * and returns it in dest. If revComp is true, it returns the revComp of the binary string
     *
     * If this method is called while the index file is being created,
     * convertToBinary() should be called before this method.
     *
     * (start + numBases -1) should be less than the number of bases in the reference
     * 
     * @param dest where the method will store the binary data
     * @param start the start index in the reference
     * @param numBases The number of bases to read from the binary reference
     * @param revComp A boolean value. If revComp is true, then the binary string returned
     * will be the reverse complement of the binary string in the reference
     *
     * */
    void    getBinarySubstring(uint64_t * dest, uint64_t start, uint64_t numBases, bool revComp);

    /**
     * NOT YET IMPLEMENTED -- COMING SOON
     * 
     * getCharacterSubstring reads a numBases long subset of the binary reference data starting at start
     * and returns it in dest. If revComp is true, it returns the revComp of the binary string
     *
     * This method will read from the binary reference (for simplicity's sake) so
     * If this method is called while the index file is being created,
     * convertToBinary() should be called before this method.
     * 
     * (start + numBases -1) should be less than the number of bases in the reference
     * 
     * @param dest where the method will store the character data
     * @param start the start index in the reference
     * @param numBases The number of bases to read from the reference
     * @param revComp A boolean value. If revComp is true, then the binary string returned
     * will be the reverse complement of the binary string in the reference
     *
     */
    void    getCharacterSubstring(char * dest, uint64_t start, uint64_t numBases, bool revComp);

};

#endif
