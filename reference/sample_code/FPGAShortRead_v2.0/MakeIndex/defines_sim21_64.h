/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SIM21_64_H
#define SIM21_64_H

#define TYPE "SIM21_64"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 64
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/sim21.index"
#define DEFAULT_REFERENCE "../../data/sim21.fasta"
// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 32   // number of bits used for the start pointer
#define IDX_BITS       18   // number of bits of the seed used as address
#define OFFSET_BITS        8   // number of bits used for each offset
#define TAG_BITS           2    // number of bits of the seed used as tag
#define SEED_LENGTH 22 // number of bases used for the seed

#define MAX_SAME_SEED 8         // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
  uint64_t startPtr : START_POINTER_BITS;
  uint64_t offset0 : OFFSET_BITS;
  uint64_t offset1 : OFFSET_BITS;
  uint64_t offset2 : OFFSET_BITS;
  uint64_t offset3 : OFFSET_BITS;
} ptrTable_t;

#define setOffset(name, index, value)                    \
    switch (index) {                            \
    case (0) : (name)->offset0 = (value); break;            \
    case (1) : (name)->offset1 = (value); break;            \
    case (2) : (name)->offset2 = (value); break;            \
    case (3) : (name)->offset3 = (value); break;            \
    default : {fprintf(stderr, "setOffset index = %d\n", index); exit(1);} \
    }

#define getOffset(name, index)                    \
    (((index) == 0) ? (name)->offset0 :                \
     ((index) == 1) ? (name)->offset1 :                \
     ((index) == 2) ? (name)->offset2 :                \
     ((index) == 3) ? (name)->offset3 :                \
     (fprintf(stderr, "setOffset index = %d\n", index), 0))

inline uint64_t do_hash(uint64_t in) {

  uint64_t mask_table1[16] = {
    0x00000000, 0x8106fdfa51d, 0x1aae13b2251, 0x9ba8ee4874c,
    0xe5a1297656f, 0x64a7d48c072, 0xff0f3ac473e, 0x7e09c73e223,
    0x5c588eab03a, 0xdd5e7351527, 0x46f69d1926b, 0xc7f060e3776,
    0xb9f9a7dd555, 0x38ff5a27048, 0xa357b46f704, 0x22514995219
  };
  uint64_t mask_table2[16] = {
    0x00000000, 0xbd9344e5c46, 0xb2768f3c36, 0xb6b42c16070,
    0x7c6b77ceb8e, 0xc1f8332b7c8, 0x774c1f3d7b8, 0xcadf5bd8bfe,
    0xc63df9ced97, 0x7baebd2b1d1, 0xcd1a913d1a1, 0x7089d5d8de7,
    0xba568e00619, 0x7c5cae5a5f, 0xb171e6f3a2f, 0xce2a216669
  };
  uint64_t mask_table3[16] = {
    0x00000000, 0x821fe6b787b, 0x4796ed85559, 0xc5890b32d22,
    0xa56c319d0c0, 0x2773d72a8bb, 0xe2fadc18599, 0x60e53aafde2,
    0xd3dbdc4218d, 0x51c43af59f6, 0x944d31c74d4, 0x1652d770caf,
    0x76b7eddf14d, 0xf4a80b68936, 0x3121005a414, 0xb33ee6edc6f
  };
  uint64_t mask_table4[16] = {
    0x00000000, 0xdce185e95bf, 0x7515ff79239, 0xa9f47a90786,
    0x41e783c653, 0xd8fffdd53ec, 0x710b874546a, 0xadea02ac1d5,
    0xc6cb6f71fa2, 0x1a2aea98a1d, 0xb3de9008d9b, 0x6f3f15e1824,
    0xc2d5174d9f1, 0x1e3492a4c4e, 0xb7c0e834bc8, 0x6b216ddde77
  };
  uint64_t mask_table5[16] = {
    0x00000000, 0xbfe231f6359, 0x33059318e0c, 0x8ce7a2eed55,
    0x656cdd0758e, 0xda8eecf16d7, 0x56694e1fb82, 0xe98b7fe98db,
    0xc3aace3345b, 0x7c48ffc5702, 0xf0af5d2ba57, 0x4f4d6cdd90e,
    0xa6c613341d5, 0x192422c228c, 0x95c3802cfd9, 0x2a21b1dac80
  };
  uint64_t mask_table6[16] = {
    0x00000000, 0xbda80150045, 0x9c2d0a03901, 0x21850b53944,
    0xc083eb1a3bb, 0x7d2bea4a3fe, 0x5caee119aba, 0xe106e049aff,
    0x4ae9bdee7cf, 0xf741bcbe78a, 0xd6c4b7edece, 0x6b6cb6bde8b,
    0x8a6a56f4474, 0x37c257a4431, 0x16475cf7d75, 0xabef5da7d30
  };
  uint64_t mask_table7[4] = {
    0x00000000, 0xa452f215d51, 0x9400cca5e40, 0x30523eb0311
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table6[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table7[ temp & 0x3 ];


  accum <<= 26;
  return in ^ accum;

}



#endif
