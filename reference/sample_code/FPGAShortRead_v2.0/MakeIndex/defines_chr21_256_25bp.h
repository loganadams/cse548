/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_256_H
#define CHR21_256_H

#define TYPE "CHR21_256_25BP"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 256
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/chr21_256_25bp.index"
#define DEFAULT_REFERENCE "../../data/chr21.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 64   // number of bits used for the start pointer
#define IDX_BITS       24   // number of bits of the seed used as address
#define OFFSET_BITS        32   // number of bits used for each offset
#define TAG_BITS           4    // number of bits of the seed used as tag
#define SEED_LENGTH   25        // number of bases used for the seed

#define MAX_SAME_SEED 128      // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
    uint64_t startPtr : START_POINTER_BITS;
    uint64_t offsets[1LL<<TAG_BITS];
} ptrTable_t;
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define OFFS_BITS (sizeof(uint64_t)*8)

#include <stdlib.h>
#include <stdio.h>
inline void checkOffset(uint64_t value, uint64_t offset_index){
  if(offset_index>=(1LL<<TAG_BITS)) {
    printf("Error: invalid pointer table index %lu\n", offset_index);
    exit(EXIT_FAILURE);
  }
  if(value > ((1LL<<OFFSET_BITS) -1)){
    printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
    exit(1);
  }
}

inline void setOffset(ptrTable_t *name, uint64_t index, uint64_t value){
  checkOffset(value, index);
  name->offsets[index] = value;
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]&=~((OFFSET_MASK)<<((index*OFFSET_BITS)%OFFS_BITS));
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]|=((value&OFFSET_MASK) <<((index*OFFSET_BITS)%OFFS_BITS));
}

inline uint64_t getOffset(ptrTable_t *name,uint64_t index){
  return name->offsets[index];
  //    return ((((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]>>((index*OFFSET_BITS)%OFFS_BITS))&OFFSET_MASK);    
}

inline uint64_t do_hash(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x10281d3bc5889, 0x3fbd18f3d2aa4, 0x2f9505c81722d,
    0x22f5c2eb99e8f, 0x32dddfd05c606, 0x1d48da184b42b, 0xd60c7238eca2,
    0x312a1aa323794, 0x21020798e6f1d, 0xe970250f1d30, 0x1ebf1f6b345b9,
    0x13dfd848ba91b, 0x3f7c5737f192, 0x2c62c0bb683bf, 0x3c4add80adb36
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x20c904b038176, 0x54a99ff410b6, 0x25839d4f791c0,
    0x146c1a04237f, 0x218fc5107a209, 0x40c585f033c9, 0x24c55cef3b2bf,
    0x2340ec9671a5a, 0x389e82649b2c, 0x260a756930aec, 0x6c371d908b9a,
    0x22062d3633925, 0x2cf29860b853, 0x274cb4c972993, 0x785b0794a8e5
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0x1a2504b4f2045, 0xdfd2224b5af3, 0x17d8269047ab6,
    0x31a2fb28e5fa5, 0x2b87ff9c17fe0, 0x3c5fd90c50556, 0x267addb8a2513,
    0x37b4695e9313a, 0x2d916dea6117f, 0x3a494b7a26bc9, 0x206c4fced4b8c,
    0x616927676e9f, 0x1c3396c284eda, 0xbebb052c346c, 0x11ceb4e631429
  };
  static const uint64_t mask_table4[16] = {
    0x00000000, 0x32c477e214018, 0x29a1e09e97d99, 0x1b65977c83d81,
    0x2ef254352cf1, 0x302b52a146ce9, 0x2b4ec5ddc5168, 0x198ab23fd1170,
    0xf4de8aabc798, 0x3d899f48a8780, 0x26ec08342ba01, 0x14287fd63fa19,
    0xda2cde9eeb69, 0x3f66ba0bfab71, 0x24032d77796f0, 0x16c75a956d6e8
  };
  static const uint64_t mask_table5[16] = {
    0x00000000, 0xf07c4149224a, 0x2df6d87b3157c, 0x22f11c6fa3736,
    0x3e3151d8571f5, 0x313695ccc53bf, 0x13c789a366489, 0x1cc04db7f46c3,
    0x319bc17b46ade, 0x3e9c056fd4894, 0x1c6d190077fa2, 0x136add14e5de8,
    0xfaa90a311b2b, 0xad54b783961, 0x225c48d820e57, 0x2d5b8cccb2c1d
  };
  static const uint64_t mask_table6[4] = {
    0x00000000, 0x2d067962fc7f5, 0x1b691444f41e4, 0x366f6d2608611
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table6[ temp & 0x3 ];


  accum <<= 22;
  return in ^ accum;

}

#endif
