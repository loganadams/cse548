/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef INDEXBUILD_H
#define INDEXBUILD_H
/**
    * @file indexBuild.h
 * @author Dustin Richmond
 *
 * @version 1.0
 *
 * indexBuild.h is the header for indexBuild.cpp
 *
 * indexBuild.cpp contains the main method for the index building software.
 * Most of the parameters and defines for this file are located in defines.h
 * and its included header files.
 *
 * To see a helpful descriptions of the available command line arguments,
 * compile the program and type --help at execution time
 */
#include <argp.h>        // Argument parsing library
#include <stdio.h>
#include "defines.h"
#include "util.h"
#include "Reference.h"
#include "CALTable.h"
#include "PointerTable.h"

// --- All code below here is used for the argument parser --- 

/* parse_opt is a user-defined function that the parser calls. This function
 * sets flags and parses arguments from the command line. These arguments are
 * used to direct the output of the program.
 */
static error_t    parse_opt (int key, char *arg, struct argp_state *state);

/* options contains a list of command line arguments that the argument 
 * parser understands. Each line corresponds to a single parseable argument
 */
static struct argp_option options[] = {
    {"text_file",         't',      0, 0, "Create reference.txt, pointerTable.txt, and CALTable.txt" },
    {"statistics_file",         's',      0, 0, "Create ptrTableStats.csv and CALTableStates.csv which contain statistics relating to the number of entries in the pointer table and the size of the buckets in the cal table " },
    {"index_file",        'i', "FILE", 0, "Path to index file that will contain the pointer table, reference and cal table" },
    {"reference_file",    'r', "FILE", 0, "Path to the reference file. The reference file will be used to make the index table"},
    { 0 }};

// doc is a string that the argument parser prints out when --help is used

static char doc[]      = "Builds the index tables that are used for alignment";
// A shorter description of the arguments we accept.
static char args_doc[] = "-i,--index_file -r,--reference_file ";

// Another field required by the argp library. 
static struct argp    argp = { options, parse_opt, args_doc, doc };

#endif
