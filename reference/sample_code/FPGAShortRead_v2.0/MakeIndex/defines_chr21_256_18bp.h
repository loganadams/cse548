/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_256_H
#define CHR21_256_H

#define TYPE "CHR21_256_18BP"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 256
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/chr21_256_25bp.index"
#define DEFAULT_REFERENCE "../../data/chr21.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 64   // number of bits used for the start pointer
#define IDX_BITS       24   // number of bits of the seed used as address
#define OFFSET_BITS        32   // number of bits used for each offset
#define TAG_BITS           4    // number of bits of the seed used as tag
#define SEED_LENGTH   18        // number of bases used for the seed

#define MAX_SAME_SEED 100000000      // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
    uint64_t startPtr : START_POINTER_BITS;
    uint64_t offsets[1LL<<TAG_BITS];
} ptrTable_t;
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define OFFS_BITS (sizeof(uint64_t)*8)

#include <stdlib.h>
#include <stdio.h>
inline void checkOffset(uint64_t value, uint64_t offset_index){
  if(offset_index>=(1LL<<TAG_BITS)) {
    printf("Error: invalid pointer table index %lu\n", offset_index);
    exit(EXIT_FAILURE);
  }
  if(value > ((1LL<<OFFSET_BITS) -1)){
    printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
    exit(1);
  }
}

inline void setOffset(ptrTable_t *name, uint64_t index, uint64_t value){
  checkOffset(value, index);
  name->offsets[index] = value;
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]&=~((OFFSET_MASK)<<((index*OFFSET_BITS)%OFFS_BITS));
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]|=((value&OFFSET_MASK) <<((index*OFFSET_BITS)%OFFS_BITS));
}

inline uint64_t getOffset(ptrTable_t *name,uint64_t index){
  return name->offsets[index];
  //    return ((((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]>>((index*OFFSET_BITS)%OFFS_BITS))&OFFSET_MASK);    
}

inline uint64_t do_hash(uint64_t in) {
  static const uint64_t mask_table1[16] = {
    0x00000000, 0x49642248b, 0xe6a9bcb38, 0xafcd9efb3,
    0x71d15715d, 0x38b5755d6, 0x9778eba65, 0xde1cc9eee,
    0xbd3cb575, 0x42b7e91fe, 0xed7a77e4d, 0xa41e55ac6,
    0x7a029c428, 0x3366be0a3, 0x9cab20f10, 0xd5cf02b9b
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0x13446d1e3, 0xe38676941, 0xf0c21b8a2,
    0xa75edf67a, 0xb41ab2799, 0x44d8a9f3b, 0x579cc4ed8,
    0x89063d2d9, 0x9a425033a, 0x6a804bb98, 0x79c426a7b,
    0x2e58e24a3, 0x3d1c8f540, 0xcdde94de2, 0xde9af9c01
  };
  static const uint64_t mask_table3[16] = {
    0x00000000
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0x0 ];


  accum <<= 8;
  return in ^ accum;

}

#endif
