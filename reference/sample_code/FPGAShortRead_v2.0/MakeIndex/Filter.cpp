/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 * This class is a filter to reduce a set of CALs to a unique set.  One at a time CALs are passed
 * to this class, and they are written into a hash table along with the revComp bit and a version
 * number.  The hash table is cleared by incrementing the version number.  When addCAL is called,
 * the CAL is hashed, and the entry of the table associated with that hashed CAL is read.  If the
 * data contained in that entry matches the current data, the CAL is redundant and gets filtered.
 */

#include "Filter.h"

// this should allocate the memory space for the filter table, clear the filter table by writing
//  all version numbers to 0, then initializing the current version number to 1
Filter::Filter(){
    
    // allocate the table
    table = new filterTableEntry[FILTER_TABLE_ENTRIES]; 

    // set the table version numbers to 0
    for(uint32_t i=0; i<FILTER_TABLE_ENTRIES; i++){
        table[i].version = 0;
    }

    // initialize the current version number
    currentVersion = 0;
}

// free up the allocated table memory
Filter::~Filter(){
    delete [] table;
}

// clear the table for the next read by incrementing the version number
void Filter::nextRead(){
    currentVersion++;
}

// hash the input CAL, read the table data at that entry, return true if the version number, CAL,
//  or revComp fields don't match; always write the new data to the table entry
bool Filter::addCAL(uint32_t CAL, bool revComp){
    
    uint32_t targetEntryNum;
    filterTableEntry* currentEntryData;
    filterTableEntry newEntryData;
    bool CALnotFiltered;

    // hash the CAL and get the target entry
    targetEntryNum = hashCAL(CAL);
    currentEntryData = getEntry(targetEntryNum);

    // form a new entry 
    newEntryData.CAL = CAL;
    newEntryData.revComp = revComp;
    newEntryData.version = getCurrentVersion();

    // compare the new entry data to the existing entry data
    CALnotFiltered = compareEntries(currentEntryData, &newEntryData);

    // overwrite the current data in the table with the new data
    copyIntoTable(&newEntryData, targetEntryNum);

    return CALnotFiltered;
}

// this compares the contents of 2 filter table entries and returns false if their contents match
//  perfectly, returns true otherwise
bool Filter::compareEntries(filterTableEntry* entry1, filterTableEntry* entry2){
    
    if ((entry1->CAL == entry2->CAL) &&
        (entry1->version == entry2->version) &&
        (entry1->revComp == entry2->revComp)){
        return false;
    }else{
        return true;
    }
}

// returns the current version number
uint32_t Filter::getCurrentVersion(){
    return currentVersion;
}

// returns a pointer to the entry in the table specified by the index number
filterTableEntry* Filter::getEntry(uint32_t index){
    return &table[index];
}

// hashes the input CAL and returns an index into the filter table
uint32_t Filter::hashCAL(uint32_t CAL){
    //return (CAL/(DRAM_BUS_BITS/BITS_PER_BASE)) % FILTER_TABLE_ENTRIES;
    return (CAL/(DRAM_BLOCK_BITS/BITS_PER_BASE)) % FILTER_TABLE_ENTRIES;
}

// copies data from the newData entry into the table at the specified index
void Filter::copyIntoTable(filterTableEntry* newData, uint32_t tableIndex){
    
    // copy the new data explicitly    
    table[tableIndex].CAL = newData->CAL;
    table[tableIndex].version = newData->version;
    table[tableIndex].revComp = newData->revComp;
}
