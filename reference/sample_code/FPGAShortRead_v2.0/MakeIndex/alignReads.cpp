/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "alignReads.h"
header_t inputHeader;
char * indexFilePath =(char*)DEFAULT_INDEX;
char * readsFilePath =(char*)DEFAULT_READS;
char * goldFilePath = (char*)DEFAULT_GOLD;
char * alignmentsFilePath;
bool write_gold_seeds_file = false, 
    write_gold_cals_file = false,
    write_gold_sw_file = false,
    write_gold_all_sw_file = false,
    verbose = false,
    validate_tables = false,
    printSW = false,
    alignment = true,
    print_all_cals = false;

FILE * output_file = NULL;
int validateNum = 1000, goldFileType=0;

int main(int argc, char* argv[]) {
    argp_parse (&argp, argc, argv, 0, 0, NULL);
    Reference *reference = new Reference();
    CALTable * calTable = new CALTable();
    PointerTable * pointerTable = new PointerTable();
    Aligner * aligner = new Aligner();
    Filter * filter = new Filter();
    FILE* inputFile = fopen(indexFilePath, "rb");
    uint64_t readId = 0;
    printf("Reading index file: %s\n", indexFilePath);
    printf("size of sw_gold_t %lu\n", sizeof(sw_gold_t));
    if(write_gold_seeds_file || write_gold_cals_file || write_gold_sw_file || write_gold_all_sw_file)
    {
        printf("Trying to open output file: %s\n", goldFilePath);
        output_file = fopen(goldFilePath, "wb");
        if (output_file == NULL) {
            fprintf(stderr, "Error opening file %s\n", goldFilePath);
            exit(1);
        }else{
         printf("output file is open\n");
        }
    }

    fread(&inputHeader, sizeof(header_t), 1,inputFile);
    printf("Block Size: %d\n", inputHeader.block_bits);
    printf("Seed Length: %d\n", inputHeader.seed_length);
    printf("Address Bits: %d\n", inputHeader.index_bits);
    printf("Tag Bits: %d\n", inputHeader.tag_bits);
    printf("Offset Bits: %d\n", inputHeader.offset_bits);
    printf("Size of CALTable (bytes): %u\n", inputHeader.calTableLength);
    printf("Size of Reference (bytes): %u\n", inputHeader.ref_length);
    pointerTable->read(inputFile);
    reference->read(inputFile, inputHeader.ref_length);
    calTable->read(inputFile, inputHeader.calTableLength);
    if(validate_tables){
        validateTables(pointerTable, calTable, reference, validateNum);
    }else{
    FILE* shortReadsFP = fopen(readsFilePath, "rb");
    char * SRString = new char[READ_LENGTH +1]();
    char * SeedString = new char[SEED_LENGTH+1]();
    char * refString = new char[REF_LENGTH+1]();
    ShortRead * currentRead;
    CALmetaData_t currentMetadata;
    CALTable_t * currentEntry;
    uint64_t currentReadNum = 0;
    uint64_t binaryRef[8];
    int numWritten=0;
    while( !feof(shortReadsFP)){
        int bestScore = INT_MIN;
        int bestAlignment = 0;
        int bestRevComp =0;
        currentRead = new ShortRead(shortReadsFP);
        if(currentRead->isValid()){
            currentRead->toString(SRString);
            printf("Short Read %lu: %s\n",readId, SRString);
            Seed* currentSeed;
            bestScore = INT_MIN;
            while(currentSeed = currentRead->getNextSeed()){
                currentSeed->toString(SeedString);
                if(print_all_cals){
                    printf("Seed: %s\n", SeedString);
                }
                currentSeed->hashSeed();
                pointerTable->getCALMetadata(currentSeed->getIndex(), currentSeed->getTag(), &currentMetadata);
                // When writing to the gold file, the first 64 bit chunk of data
                // is the current seed value, followed by it's revcomp bit at bit -0
                // This is followed by all of the (CAL, revcomp) pairs associated with
                // with this seed in 64 bit accesses
                if(write_gold_seeds_file && output_file != NULL){
                    // commented out versoin is what appears on rapid website
                    //uint64_t current_seed_val = (currentSeed->getValue() & ((1LL<<44)-1)) | (((uint64_t)currentSeed->getRevComp() & 1) << 44);      
                    uint64_t current_seed_val = (currentSeed->getValue()<<1) | ((uint64_t)(currentSeed->getRevComp() & 1) );
                    printf("%lx\n", current_seed_val);
                    fwrite(&current_seed_val, sizeof(uint64_t), 1, output_file);
                }
                if (verbose) {
                  printLine(pointerTable->getPointerTable(), currentSeed->getIndex());
                }
                for(int i =currentMetadata.startIndex; i < currentMetadata.startIndex + currentMetadata.length; i++){
                    currentEntry = calTable->access(i);
                    if(currentEntry->key == currentSeed->getKey()){
                        if(print_all_cals){
                            printf("CAL: %u\n", currentEntry->cal);
                        }
                        int revCompCAL = currentEntry->revComp ^ currentSeed->getRevComp();
                        uint64_t CAL_bucket;
                        if(revCompCAL){
                            CAL_bucket = currentEntry->cal - ((READ_LENGTH-SEED_LENGTH)- (currentRead->getCurrentPosition()));
                        } else {
                            CAL_bucket = currentEntry->cal - (currentRead->getCurrentPosition());
                        }
                        CAL_bucket = (CAL_bucket - INDEL_OFFSET) & ~((REFERENCE_GRANULARITY/BITS_PER_BASE) - 1);
                        if(alignment){
                            if (filter->addCAL(CAL_bucket, revCompCAL)){
                                memset(binaryRef,0,sizeof(binaryRef));
                                printf("Seed: %lu %s", currentRead->getCurrentPosition()+1,SeedString);
                                printf(" CAL: %u(0x%x) Bucket: %lu(0x%lx)\n",
                                       currentEntry->cal, currentEntry->cal, CAL_bucket, CAL_bucket);
                                // get the reference bucket data
                                reference->getBinarySubstring(binaryRef, CAL_bucket, REF_LENGTH, revCompCAL);       
                                //reference->getCharacterSubstring(refString, CAL_bucket, REF_LENGTH,revCompCAL);
                                // set up the read for alignment
                                aligner->setRead(currentRead->getShortRead());
                                aligner->setReference(binaryRef);
                                // tell the aligner to go off and align the read
                                aligner->alignRead();
                                for(int i = 0 ; i < ((int)aligner->getAlignment()-READ_LENGTH+1); i++){
                                    printf(" "); // for aligning the reference and the short read in the terminal
                                }
                                binaryToString(binaryRef,REF_LENGTH,refString);
                                printf("%s\n",SRString);
                                printf("%s\n",refString);
                                int curAlignment;
                                int curScore = aligner->getScore();
                                if(revCompCAL)
                                {
                                    curAlignment = (REF_LENGTH-aligner->getAlignment())+CAL_bucket-1;
                                }
                                else
                                {  
                                    curAlignment = aligner->getAlignment()+CAL_bucket-READ_LENGTH+1;
                                }                                            
                                if(printSW)
                                {
                                    printf("SW- Read: %lu Alignment:%d(0x%x) revComp: %d Score %d \n", 
                                           currentReadNum, curAlignment, curAlignment, revCompCAL, curScore);
                                }
                                if(bestScore < curScore){
                                    bestScore = curScore;
                                    bestAlignment = curAlignment;
                                    bestRevComp = revCompCAL;
                                }
                                if(write_gold_cals_file && output_file != NULL)
                                {
                                    // write current cal to output file
                                    uint64_t writeCAL = 
                                        (readId & ((1<<30)-1)) | 
                                        ((CAL_bucket & ((1<<27)-1)) << 30) |
                                        (((uint64_t)revCompCAL & 1) << 57);
                                    fwrite(&writeCAL, sizeof(uint64_t), 1, output_file);
                                }
                                if(write_gold_all_sw_file && output_file != NULL)
                                {
                                    numWritten++;
                                    sw_gold_t writeSW  = {readId, curAlignment, curScore, revCompCAL};
                                    fwrite(&writeSW, sizeof(sw_gold_t), 1, output_file);
                                }
                            }

                        }

                    }

                }
            }
            delete currentSeed;

            if(write_gold_sw_file && output_file != NULL)
            {
                sw_gold_t writeSW  = {readId, bestAlignment, bestScore, bestRevComp};
                fwrite(&writeSW, sizeof(sw_gold_t), 1, output_file);
                numWritten++;
            }

            // if the score is better than the current score, update the score,
            // alignment, and revcomp info
            printf("Current Read: %lu Best Alignment:%u revComp: %d Score %d \n", currentReadNum,bestAlignment, bestRevComp, bestScore);
            printf("\n\n\n");
            currentReadNum++;
        }
        filter->nextRead();
        readId++;
        delete currentRead;
    }
    delete SRString;
    delete SeedString;
    delete refString;
    
    if(output_file != NULL)    
    {
        printf("closing output_file %d\n", numWritten);
        fclose(output_file);
    }
    
    }
}
/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
    int goldType;

    /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    switch (key)
    {
    case 'a':
        alignment = true;
        break;
    case 's':
        verbose = true;
        break;
    case 'i':
        indexFilePath = arg;
        printf("Now using index file : %s\n",arg);
        break;
    case 'r':
        readsFilePath = arg;
        printf("Now using reads file : %s\n",arg);
        break;
    case 'p':
        printSW = true;
        break;
    case 'g':
        goldType = atoi(arg);
        if(goldType == 1)
        {
            printf("Generating seeds gold file\n");
            write_gold_seeds_file = true;
        } 
        else if(goldType == 2)
        {
            printf("Generating cals gold file\n");
            write_gold_cals_file = true;
        }
        else if(goldType == 3)
        {
            printf("Generating SW gold file\n");
            write_gold_sw_file = true;
        }
        else if(goldType == 4)
        {
            printf("Generation every SW comparison gold file\n");
            write_gold_all_sw_file = true;
        }
        else
        {
            fprintf(stderr, "Invalid gold gen type: %d\n", goldType);
            exit(1);
        }
        break;
    case 'o':
        printf("It's gold! Now writing gold file : %s\n",arg);
        goldFilePath = arg;
        break;
    case 'c':
        print_all_cals = true;
        break;
    case ARGP_KEY_ARG:
        if (state->arg_num >= 4)
        /* Too many arguments. */
        argp_usage (state);
        break;
    case ARGP_KEY_END:
        if (state->arg_num < 0)
        /* Not enough arguments. */
        argp_usage (state);
        break;
    case 'v':
        validate_tables = true;
        validateNum = atoi(arg);
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

void validateTables(PointerTable * pointerTable, CALTable* calTable, Reference* reference, int validateNum){
    uint64_t calTableIndex;
    CALTable_t *randomEntry;
    uint64_t seedValue = 0;
    CALmetaData_t * randomMeta = new CALmetaData_t();
    uint64_t numCorrect =0 ;
    CALTable_t *currentEntry;
    for(int i=0 ; i < validateNum; i ++){
        Seed * randomSeed = new Seed();
        seedValue=0;
        randomEntry = calTable->getRandomCAL(&calTableIndex);
        reference->getBinarySubstring(&seedValue,randomEntry->cal, SEED_LENGTH,0);
        randomSeed->setValue(seedValue);
        randomSeed->hashSeed();
        uint64_t index = randomSeed->getIndex();
        uint64_t tag = randomSeed->getTag();
        uint64_t key = randomSeed->getKey();
        pointerTable->getCALMetadata(index, tag,randomMeta);
//        int i; 
        bool found = false;
        for(i = randomMeta->startIndex-1; i < randomMeta->startIndex + randomMeta->length; i++){
            currentEntry = calTable->access(i);
            if((currentEntry->key == key && currentEntry->revComp == randomSeed->getRevComp())){
                numCorrect++;
                found = true;
                break;
            }
            
        }
        if((i == randomMeta->startIndex + randomMeta->length) && !found){
            for(i = randomMeta->startIndex-1; i < randomMeta->startIndex + randomMeta->length; i++){
                calTable_t * current = calTable->access(i);
                printf("%u : ",i);
                printf("Entry - CAL: %d RevComp: %u Key: %u\n", current->cal, current->revComp, current->key);
            }
            printf("\n");
            printf("Error: random cal entry at index %lu was not found\n", calTableIndex);
            printf("Entry - CAL: %d RevComp: %u Key: %u\n", randomEntry->cal, randomEntry->revComp, randomEntry->key);

            printf("Seed from ref - Index: %lu Tag: %lu Key: %lu revComp: %d\n", index, tag, key, randomSeed->getRevComp());
            printf("Palindrome: %d\n", randomSeed->getRevComp());
        }
        delete randomSeed;
    }

    printf("Validation complete. Percent Correct %.2f\n", numCorrect*100.0/validateNum);
}
