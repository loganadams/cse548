/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_256_H
#define CHR21_256_H

#define TYPE "CHR21_256"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 256
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/human_genome__256.index"
#define DEFAULT_REFERENCE "$(SRDAT)/nickerson/human_genome.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 32   // number of bits used for the start pointer
#define IDX_BITS       24   // number of bits of the seed used as address
#define OFFSET_BITS        14   // number of bits used for each offset
#define TAG_BITS           4    // number of bits of the seed used as tag
#define SEED_LENGTH 22 // number of bases used for the seed

#define MAX_SAME_SEED 8         // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
    uint64_t startPtr : START_POINTER_BITS;
    uint32_t offsets[7];
} ptrTable_t;
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define OFFS_BITS (sizeof((name)->offsets[0])*8)

inline void setOffset(ptrTable_t *name, uint64_t index, uint64_t value){
    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]&=~((OFFSET_MASK)<<((index*OFFSET_BITS)%OFFS_BITS));
    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]|=((value&OFFSET_MASK) <<((index*OFFSET_BITS)%OFFS_BITS));
}

inline uint64_t getOffset(ptrTable_t *name,uint64_t index){
    return ((((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]>>((index*OFFSET_BITS)%OFFS_BITS))&OFFSET_MASK);    
}
inline uint64_t do_hash(uint64_t in) {

  static const uint32_t mask_table1[16] = {
    0x00000000, 0x05dcb2a3, 0x0f6e9761, 0x0ab225c2,
    0x0bfde90c, 0x0e215baf, 0x04937e6d, 0x014fccce,
    0x0687898b, 0x035b3b28, 0x09e91eea, 0x0c35ac49,
    0x0d7a6087, 0x08a6d224, 0x0214f7e6, 0x07c84545
  };
  static const uint32_t mask_table2[16] = {
    0x00000000, 0x09f35641, 0x0f057c30, 0x06f62a71,
    0x026141cc, 0x0b92178d, 0x0d643dfc, 0x04976bbd,
    0x09108d51, 0x00e3db10, 0x0615f161, 0x0fe6a720,
    0x0b71cc9d, 0x02829adc, 0x0474b0ad, 0x0d87e6ec
  };
  static const uint32_t mask_table3[16] = {
    0x00000000, 0x06b873ea, 0x0b483aec, 0x0df04906,
    0x0f4006b0, 0x09f8755a, 0x04083c5c, 0x02b04fb6,
    0x084ff610, 0x0ef785fa, 0x0307ccfc, 0x05bfbf16,
    0x070ff0a0, 0x01b7834a, 0x0c47ca4c, 0x0affb9a6
  };
  static const uint32_t mask_table4[16] = {
    0x00000000, 0x00578abc, 0x0bf29dcc, 0x0ba51770,
    0x05851539, 0x05d29f85, 0x0e7788f5, 0x0e200249,
    0x03482f1a, 0x031fa5a6, 0x08bab2d6, 0x08ed386a,
    0x06cd3a23, 0x069ab09f, 0x0d3fa7ef, 0x0d682d53
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];

  accum <<= 16;
  return in ^ accum;

}
/*
inline uint64_t do_hash(uint64_t in) {

  static const uint64_t mask_table1[16] = {
    0x00000000, 0x37a664abc85, 0x4416ffc0496, 0x73b09b6b813,
    0xed3823329f6, 0xda9e4799573, 0xa92edcf2d60, 0x9e88b8591e5,
    0x7d57b89a1e1, 0x4af1dc31d64, 0x3941475a577, 0xee723f19f2,
    0x906f9ba8817, 0xa7c9ff03492, 0xd4796468c81, 0xe3df00c3004
  };
  static const uint64_t mask_table2[16] = {
    0x00000000, 0xd9ee78402e6, 0xea1ffd22238, 0x33f185620de,
    0x940b3777a93, 0x4de54f37875, 0x7e14ca558ab, 0xa7fab215a4d,
    0x730fca95c36, 0xaae1b2d5ed0, 0x991037b7e0e, 0x40fe4ff7ce8,
    0xe704fde26a5, 0x3eea85a2443, 0xd1b00c049d, 0xd4f5788067b
  };
  static const uint64_t mask_table3[16] = {
    0x00000000, 0xd88e86a092f, 0x23335a9c329, 0xfbbddc3ca06,
    0x43550a7ca0, 0xdcbbd60758f, 0x27060a3bf89, 0xff888c9b6a6,
    0xc7f2dbd6532, 0x1f7c5d76c1d, 0xe4c1814a61b, 0x3c4f07eaf34,
    0xc3c78b71992, 0x1b490dd10bd, 0xe0f4d1edabb, 0x387a574d394
  };
  static const uint64_t mask_table4[16] = {
    0x00000000, 0xcbee6ecc9d5, 0x667917a5f01, 0xad9779696d4,
    0xbcd7140dcff, 0x77397ac152a, 0xdaae03a83fe, 0x11406d64a2b,
    0x5cbffe79bac, 0x975190b5279, 0x3ac6e9dc4ad, 0xf1288710d78,
    0xe068ea74753, 0x2b8684b8e86, 0x8611fdd1852, 0x4dff931d187
  };
  static const uint64_t mask_table5[4] = {
    0x00000000, 0xf781c21e7d2, 0x603a5ad3867, 0x97bb98cdfb5
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table2[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table3[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table4[ temp & 0xf ];
  temp >>= 4;
  accum ^= (uint64_t) mask_table5[ temp & 0x3 ];


  accum <<= 18;
  return in ^ accum;

}

*/
#endif
