/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef CHR21_256_H
#define CHR21_256_H

#define TYPE "CHR21_256_16BP"
// Define DRAM block size for platform
#define DRAM_BLOCK_BITS 256
#define INTS_PER_BLOCK (DRAM_BLOCK_BITS/(sizeof(int)*8))
#define BYTES_PER_BLOCK (DRAM_BLOCK_BITS/8)
#define DEFAULT_INDEX "../../data/chr21_256_16bp.index"
#define DEFAULT_REFERENCE "../../data/chr21.fasta"

// "Bucket" size of the reference table - should match DRAM block size
// This determines the granularity of the CALs
#define REFERENCE_GRANULARITY DRAM_BLOCK_BITS

// Pointer table definition
#define START_POINTER_BITS 64   // number of bits used for the start pointer
#define IDX_BITS       24   // number of bits of the seed used as address
#define OFFSET_BITS        32   // number of bits used for each offset
#define TAG_BITS           4    // number of bits of the seed used as tag
#define SEED_LENGTH   16        // number of bases used for the seed

#define MAX_SAME_SEED 1024      // Throw out seeds that appear more than this many times

// This has to be built-in as arrays of bit fields are not allowed
typedef struct ptrTable_t {
    uint64_t startPtr : START_POINTER_BITS;
    uint64_t offsets[1LL<<TAG_BITS];
} ptrTable_t;
#ifndef OFFSET_MASK
#define OFFSET_MASK ((1LL<<OFFSET_BITS) -1)
#endif
#define OFFS_BITS (sizeof(uint64_t)*8)

#include <stdlib.h>
#include <stdio.h>
inline void checkOffset(uint64_t value, uint64_t offset_index){
  if(offset_index>=(1LL<<TAG_BITS)) {
    printf("Error: invalid pointer table index %lu\n", offset_index);
    exit(EXIT_FAILURE);
  }
  if(value > ((1LL<<OFFSET_BITS) -1)){
    printf("Error: Offset value was too large for the number of bits allocated to the offset\n");
    exit(1);
  }
}

inline void setOffset(ptrTable_t *name, uint64_t index, uint64_t value){
  checkOffset(value, index);
  name->offsets[index] = value;
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]&=~((OFFSET_MASK)<<((index*OFFSET_BITS)%OFFS_BITS));
  //    ((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]|=((value&OFFSET_MASK) <<((index*OFFSET_BITS)%OFFS_BITS));
}

inline uint64_t getOffset(ptrTable_t *name,uint64_t index){
  return name->offsets[index];
  //    return ((((uint64_t*)&((name)->offsets[index*OFFSET_BITS/OFFS_BITS]))[0]>>((index*OFFSET_BITS)%OFFS_BITS))&OFFSET_MASK);    
}

inline uint64_t do_hash(uint64_t in) {
  static const uint32_t mask_table1[16] = {
    0x00000000, 0xfeda9bad, 0x8d6936d8, 0x73b3ad75,
    0xadcd6082, 0x5317fb2f, 0x20a4565a, 0xde7ecdf7,
    0xaadb2106, 0x5401baab, 0x27b217de, 0xd9688c73,
    0x07164184, 0xf9ccda29, 0x8a7f775c, 0x74a5ecf1
  };

  uint64_t temp, accum;

  temp = in;
  accum = 0;

  //table lookups
  accum ^= (uint64_t) mask_table1[ temp & 0xf ];

  accum <<= 4;
  return in ^ accum;
}

#endif
