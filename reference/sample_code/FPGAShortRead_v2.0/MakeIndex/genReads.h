/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef GENREADS_H
#define GENREADS_H

#include <stdint.h>
#include <stdio.h>
#include <time.h>
//#include <iostream>
#include <sstream>
#include <string>

#include "defines.h"
#include "PointerTable.h"
#include "Reference.h"
#include "util.h"
#include "argp.h"

#define DEFAULT_OUTPUT "reads.fastq"
#define DEFAULT_NUM_SR 10

const char * getRandomRead(Reference * reference, uint64_t size, bool revcomp, char * dest);
const char * getRandomRead(char * reference, uint64_t ref_size, char * dest);
int getCharType();

typedef struct{
    int normalLow;
    int normalHigh;
    int insertionLow;
    int insertionHigh;
    int deletionLow;
    int deletionHigh;
    int mutationLow;
    int mutationHigh;
} param_ranges;

enum readDirection{
    forward = 0,
    reverse = 1,
};

enum readType{
    normal = 0,
    insertion = 1,
    deletion = 2,
    mutation = 3,
};

char bases[4] = {'A', 'T', 'G', 'C'};

/* The options we understand. */
static struct argp_option options[] = {
    //    {"verbose",      's',      0, 0, "Produce more console output" },
    {"number",         'n', "INTEGER", 0, "number of short reads to produce DEFAULT=10"},
    {"revComp",        'r', 0, 0, "produce reverse compliment short reads"},
    //{"index_file",     'i', "FILE", 0,
    // "Path to index file containing the pointer table, reference and cal table DEFAULT="DEFAULT_INDEX },
    {"reference_file", 'f', "FILE", 0, "Path to .fasta reference file DEFAULT="DEFAULT_REFERENCE},
    {"output_file",    'o', "FILE", 0,
     "name of file where short reads should be written, DEFAULT="DEFAULT_OUTPUT },
    {"wild",           'w', 0, 0, "produced a file with insetions, deletions, and muatations"},
    {"params",         'p', "STRING", 0,
     "probability and length of insertions, deletions, and mutations: NORMAL_PROB, INSERTION_PROB, DELETION_PROB, MUTATION_PROB, INSERTION_LEN, DELETION_LEN, MUTATION_LEN"},
    {"verbose",        'v', 0, 0, "produce verbose output info"},
    { 0 }
};
/* Program documentation. */
static char doc[] ="Produces a file of short reads";
/* A description of the arguments we accept. */
static char args_doc[] = "-i,--index_file -o,--output_file ";
static error_t parse_opt (int key, char *arg, struct argp_state *state);
static struct argp argp = { options, parse_opt, args_doc, doc };


#endif
