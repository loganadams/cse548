/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SEED_H
#define SEED_H

#include <stdlib.h>
#include <stdint.h>
#include "defines.h"
#include "util.h"

class Seed {
 private:
    uint64_t seedValue;//:SEED_LENGTH* BITS_PER_BASE;
    uint64_t hashedSeedValue;//: SEED_LENGTH*BITS_PER_BASE;
    uint32_t cal;
    bool isPalindrome;
    bool isRevComp;
    bool isValid;
 public:
    Seed(uint64_t value);
    Seed();
    ~Seed();
    Seed(FILE* sourceFile);
    bool getValid();
    void writeToFile(FILE* destFile);
    void setValue(uint64_t value);
    void toString(char * dest);
    void hashSeed();
    uint64_t reverse();
    void addBase(char base);
    uint64_t getValue();
    uint64_t getHashedSeedValue();
    uint64_t getIndex();
    uint64_t getTag();
    uint64_t getKey();
    uint64_t getCAL();
    void setCAL(uint64_t value);
    bool getPalindrome();
    uint64_t getHashedValue();
    bool getRevComp();
    void setRevComp(bool);
};
    
#endif
