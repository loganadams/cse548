/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Seed.h"
Seed::Seed(){
    seedValue = 0LL;
    hashedSeedValue = 0LL;
    isPalindrome = false; 
    isRevComp = false;
    isValid = true;
}

Seed::Seed(uint64_t value){
    seedValue = value&SEED_MASK;
    hashedSeedValue = 0LL;
    isPalindrome = false;
    isRevComp = false;
    isValid = true;
}

#define MAX_BUF 256
Seed::Seed(FILE* sourceFile){
    Seed tempSeed;
    fread(&tempSeed,sizeof(Seed),1,sourceFile);
    if(feof(sourceFile)){
        isValid = false;
        printf("is not valid");
    }else{
        isValid = true;
    }
    isPalindrome = tempSeed.getPalindrome();
    isRevComp = tempSeed.getRevComp();//&SEED_MASK;
    seedValue = tempSeed.getValue()&SEED_MASK;
    hashedSeedValue = tempSeed.getHashedValue()&SEED_MASK;
}

bool Seed::getValid(){
    return isValid;
}

Seed::~Seed(){
    // No memory allocated, do nothing
}

void Seed::toString(char * dest){
    uint64_t temp = seedValue&SEED_MASK;
    binaryToString(&temp, SEED_LENGTH,dest);
}

void Seed::hashSeed(){
    hashedSeedValue = 0LL;
    isPalindrome = false;
    isRevComp = false;
    isValid = true;
    uint64_t revComp = reverse()&SEED_MASK;
    if (revComp == seedValue){
        isPalindrome = true;
    }else if(revComp < seedValue){
        seedValue = revComp;
        isRevComp = true;
    } 
    uint64_t tempValue = seedValue&SEED_MASK;
    hashedSeedValue = do_hash(tempValue)&SEED_MASK;
}

void Seed::writeToFile(FILE* destFile){
    fwrite(this, sizeof(Seed), 1, destFile);
}

uint64_t Seed::reverse(){
    uint64_t revComp = 0;
    for(int i  = 0 ; i < SEED_LENGTH; i ++){
        revComp =  ((revComp << BITS_PER_BASE) | ((seedValue >> (i * BITS_PER_BASE)) & BASE_MASK));
    }

    return (~revComp)&SEED_MASK;
}

void Seed::addBase(char base){
    isPalindrome = false;
    isRevComp = false;
    if(isBase(base)){
        if(base == 'n' || base == 'N'){
            fprintf(stderr, "Warning: adding base n to seed\n");
        }
    }else{
        fprintf(stderr, "Error: Unknown Base %c\n", base);
    }
    seedValue = ((seedValue << 2) & SEED_MASK) + charToBase(base);
    //seedValue = (((seedValue >> BITS_PER_BASE) | ((uint64_t)charToBase(base) << (BITS_PER_BASE *(SEED_LENGTH-1)))) & SEED_MASK);
}

void Seed::setRevComp(bool revComp){
    isRevComp = revComp;
}

uint64_t Seed::getValue(){
    return seedValue&SEED_MASK;
}


void Seed::setValue(uint64_t newValue){
    seedValue = newValue&SEED_MASK;
}

void Seed::setCAL(uint64_t newcal){
    cal = newcal;
}


uint64_t Seed::getCAL(){
    return cal;
}

uint64_t Seed::getHashedValue(){
    return hashedSeedValue;
}

// return the index bits of the specified seed
uint64_t Seed::getIndex(){
    return (hashedSeedValue >> (TAG_BITS+KEY_BITS)) & IDX_MASK;
}

// return the tag bits of the specified seed
uint64_t Seed::getTag(){
    return (hashedSeedValue >> KEY_BITS) & TAG_MASK;
}

// return the key bits of the specified seed
uint64_t Seed::getKey(){
    return hashedSeedValue & KEY_MASK;
}

// returns true if the seed is a palindrome
bool Seed::getPalindrome(){
    return isPalindrome;
}

// returns true if the seed is a palindrome
bool Seed::getRevComp(){
    return isRevComp;
}
