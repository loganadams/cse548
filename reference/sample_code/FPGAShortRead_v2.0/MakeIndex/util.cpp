/*
Copyright (c) 2016 Scott Hauck and the ACME Lab

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * @file util.cpp
 * @author Dustin Richmond
 *
 * @version 1.0
 * 
 * util.h/cpp defines a collection of utility functions that are used
 * for building and reading the reference.
 */
#include "util.h"
/**
 * set_base sets a single binary base in a binary reference string
 *
 * @param string the array of uint64_t's that represents the reference string
 * @param index the index of the base to set in the binary string
 * @param value the value that the index in the string will be set to
 * */
inline void set_base(uint64_t *string,uint64_t index , uint64_t value){
    uint64_t bitIndex = ((index*BITS_PER_BASE)%(8*sizeof(string[0])));
    uint64_t arrIndex = ((index*BITS_PER_BASE)/(8*sizeof(string[0])));
    string[arrIndex] = ((~(BASE_MASK << bitIndex)) & string[arrIndex]) | ((value&BASE_MASK) << bitIndex);
}

/**
 * get_base returns the value of a single base in a binary reference string
 *
 * @param string the array of uint64_t's that represents the reference string
 * @param index the index of the base to read in the binary string
 *
 */
inline uint64_t get_base(uint64_t *string, uint64_t index){
    uint64_t bitIndex = ((index*BITS_PER_BASE)%(8*sizeof(string[0])));
    uint64_t arrIndex = ((index*BITS_PER_BASE)/(8*sizeof(string[0])));
    return (string[arrIndex]>>bitIndex)&BASE_MASK;
}

/**
 * reverseBinaryString reverses and complements
 * a string of binary bases. 
 *
 * @param source A pointer to the binary string of bases
 * @param numBases the number of bases (length) of the binary string
 *
 * */
void reverseCompBinaryString(uint64_t * string, uint64_t numBases){
    uint64_t temp;
    for(uint64_t i =0 ; i <numBases/2;i++){
        temp = get_base(string,i);
        set_base(string, i,(~get_base(string, numBases-i-1)) & BASE_MASK);
        set_base(string, numBases-i-1,(~temp) & BASE_MASK);
    }
}

/**
 * charToBinary converts a character string of bases num_bases long
 * into its corresponding binary representation and puts 
 * the result in dest
 *
 * @param ascii_buffer A pointer to the character string of bases
 * @param dest A pointer to where the binary reference should be stored
 * @param num_bases The number of bases in the ascii buffer
 */
void charToBinary(char * ascii_buffer,uint64_t *dest, uint64_t num_bases){
  for(int i = 0; i < num_bases; i++){
    int arr_index = (BITS_PER_BASE*i)/(sizeof(dest[0])*8);
    int bit_index = (BITS_PER_BASE*i)%(sizeof(dest[0])*8);
    if(!isBase(ascii_buffer[i])){
      fprintf(stderr,"Found invalid base %c at index %d \n",
              ascii_buffer[i],i);
    }
    dest[arr_index] |= (((uint64_t)charToBase(ascii_buffer[i]))<< bit_index);
    char cur =  baseToChar((dest[arr_index] >> bit_index)& BASE_MASK);
  }
}

/**
 * convert_to_revcomp converts a character string of DNA bases (A,C,T,G) to it's 
 * reverse complement. The method executes in place.
 *
 * @param data A pointer to the character string of bases
 * @param length The length of the character string pointed to by data
 */
void convert_to_revcomp(char * data, uint64_t length){
    char temp = 0;
    for(int i =0 ; i < (length +1)/ 2; i ++){
        temp = data[i];
        data[i] = getComplement(data[length-i-1]);
        data[length-i-1]=getComplement(temp);
    }
}

/**
 * charToBase converts a single character base to it's binary representation
 * 
 * @param base The character base to be converted
 * @return the binary representation of a single base
 */
int charToBase(char base) {
  switch(base){
  case 'A':
  case 'a':
    return 0;
  case 'C':
  case 'c':
    return 1;
  break;
  case 'G':
  case 'g':
    return 2;
  break;
  case 'T':
  case 't':
    return 3;
  break;
  case 'N':
  case 'n':
    return 0;
  break;
  default:
    return 0;
    break;
  }
  return 0;
}

void getBinarySubstring(uint64_t* source, uint64_t * dest, uint64_t start, uint64_t numBases, bool revComp){
    uint64_t start_index = (start * BITS_PER_BASE) / (sizeof(source[0])*8);
    uint64_t end_index = ((start+numBases) * BITS_PER_BASE)/(sizeof(source[0])*8);
    uint64_t r_shift_amt = (start * BITS_PER_BASE) % (sizeof(source[0])*8);
    uint64_t l_shift_amt = (sizeof(source[0])*8)-r_shift_amt;
    uint64_t num_indexes = ((numBases* BITS_PER_BASE) + (sizeof(source[0])*8)-1)/ (sizeof(source[0])*8);
    for(int i = 0 ; i < num_indexes; i ++){
        dest[i] |= source[i+start_index] >> r_shift_amt;
        if( l_shift_amt != 64){ // eff this bug
            dest[i] |= source[i+start_index+1] << l_shift_amt;
        }
    }
    if(revComp){
        reverseCompBinaryString(dest, numBases);
    }
}

/**
 * binaryToString takes a string of bases in binary representation and converts the
 * string into a character string of bases located in dest
 * @param data A pointer to the binary string of bases
 * @param dest A pointer to where the character reference should be stored
 * @param length The number of bases in the binary buffer
 *
 */
void binaryToString(uint64_t * data,uint64_t length,char * dest){
    for(int i =0 ; i <  length; i ++){
        dest[i]=baseToChar(get_base(data, i));
    }
}


/**
 * baseToChar takes a single binary base and returns the character value. It performs
 * the pseudo-inverse (see warning) of charToBase
 *
 * Warning, A and N map to the same binary representation. All N's in binary will
 * be interpreted as A's
 *
 * @param base The binary representation of a single base
 * @return the character representation of the binary base that was provided
 */
char baseToChar(int base) {
  switch(base){
  case 0:
    return 'A';
    break;
  case 1:
    return 'C';
    break;
  case 2:
    return 'G';
    break;
  case 3:
    return 'T';
    break;
  default:
    return 'N';
    break;
  }
  return 'N';
}


/**
 * getComplement takes a single character base and returns its complement.
 * 
 * Note: to convert a single BINARY base, just use the ~ operator
 * 
 * @param base The character base to be complemented
 * @return base The complement of the base that was provided
 */
char getComplement(char base) {
  switch(base){
  case 'A':
  case 'a':
    return 'T';
  break;
  case 'C':
  case 'c':
    return 'G';
  break;
  case 'G':
  case 'g':
    return 'C';
  break;
  case 'T':
  case 't':
    return 'A';
  break;
  default:
    return 'N';
    break;
  }
  return 'N';
}

/**
 * isBase returns true if the base passed to the method is A, C, T, G, or N
 *
 * @param base the character base to be tested
 * @return an int/bool. 1/True if the character is a base, 0/False if not.
 */
int isBase(char base){
  switch(base){
  case 'A':
  case 'a':
  case 'C':
  case 'c':
  case 'G':
  case 'g':
  case 'T':
  case 't':
  case 'N':
  case 'n':
    return 1;
  break;
  default:
    return 0;
    break;
  }
}   

uint64_t countNumReads(FILE * shortReadFile){
    // buffer that we will read into from the file.
    char buffer[MAX_READ_LINE_LENGTH];
    uint64_t num_reads = 0;
    if(shortReadFile == NULL){
        return -1;
    }
    clearerr(shortReadFile);
    //seek to start of file
    int err = fseek(shortReadFile, 0, SEEK_SET);
    while(!feof(shortReadFile)){
        fgets(buffer, MAX_READ_LINE_LENGTH, shortReadFile);
        if(!feof(shortReadFile)){
            if(isBase(buffer[0])){
                num_reads++;
            }
        }
    }
    clearerr(shortReadFile);
    fseek(shortReadFile, 0, SEEK_SET);

    return num_reads;
}
